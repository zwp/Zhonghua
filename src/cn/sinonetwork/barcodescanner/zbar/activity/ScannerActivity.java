package cn.sinonetwork.barcodescanner.zbar.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import cn.sinonetwork.barcodescanner.core.CameraUtils;
import cn.sinonetwork.barcodescanner.zbar.Result;
import cn.sinonetwork.barcodescanner.zbar.ZBarScannerView;

import com.sinonetwork.zhonghua.R;

public class ScannerActivity extends Activity implements ZBarScannerView.ResultHandler, View.OnClickListener {
    private ZBarScannerView mScannerView;
    
	private boolean isFlashlightOpen;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
       // mScannerView = new ZBarScannerView(this);
        setContentView(R.layout.activity_capture);
        
        mScannerView = (ZBarScannerView)findViewById(R.id.viewfinder_view);
		findViewById(R.id.capture_flashlight).setOnClickListener(this);
		ImageView light=(ImageView) findViewById(R.id.capture_flashlight);
		if(CameraUtils.isFlashSupported(ScannerActivity.this)){
			light.setVisibility(View.VISIBLE);
		}else{
			light.setVisibility(View.GONE);
		}
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
       
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        
    }

    @Override
    public void handleResult(Result rawResult) {
        
       // mScannerView.stopCamera();
        
		Intent intent = new Intent();
		intent.putExtra("barcode", rawResult.getContents());
//		intent.setClassName(getApplicationContext(), className);
//		startActivity(intent);
		setResult(-1, intent); 
		Toast.makeText(this, "Contents = " + rawResult.getContents() +
                ", Format = " + rawResult.getBarcodeFormat().getName(), Toast.LENGTH_SHORT).show();
        
        
		this.finish();
      //  mScannerView.startCamera();
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		  case R.id.capture_flashlight:
			if (isFlashlightOpen) {
				 // 关闭闪光灯
				isFlashlightOpen = false;
				 mScannerView.setFlash(false);
			}
			else {
				 // 打开闪光灯
				isFlashlightOpen = true;
				mScannerView.setFlash(true);
			}
			break;
		default:
			break;
	}
		
	}
}
