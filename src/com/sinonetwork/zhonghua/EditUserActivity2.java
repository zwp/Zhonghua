package com.sinonetwork.zhonghua;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class EditUserActivity2 extends Activity{
	private ImageView back;
	private TextView title;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit2);
		initView();
	}
	private void initView() {
		// TODO Auto-generated method stub
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		title.setText("修改用户信息");
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditUserActivity2.this.finish();
			}
		});
	}
	public void yonghuming(View v){
		startActivity(new Intent(EditUserActivity2.this,EditUserName.class));
	}
	public void mima(View v){
		startActivity(new Intent(EditUserActivity2.this,EditUserPassword.class));
	}
	public void shoujihao(View v){
		startActivity(new Intent(EditUserActivity2.this,EditUserPhone.class));
	}
}
