package com.sinonetwork.zhonghua;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class MyDengLu extends LandBaseActivity {

	private ImageView denglu_back;

	@SuppressLint("JavascriptInterface")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_denglu);
		denglu_back = (ImageView) findViewById(R.id.denglu_back);
		// WebView
		WebView browser = (WebView) findViewById(R.id.Toweb_denglu);

		// 开启javascript设置
		browser.getSettings().setJavaScriptEnabled(true);
		browser.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);//不缓存
		// 把RIAExample的一个实例添加到js的全局对象window中
		// 这样就可以使用window.javatojs来调用它的方法
		browser.addJavascriptInterface(this, "webView");
		showLoadProgressBar();
		browser.loadUrl(URLAddress.SHOP_URL + "fert_bbc/weixin/login.htm");
		// 加载网页
		
		// 如果页面中链接，如果希望点击链接继续在当前browser中响应，
		// 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象
		browser.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				hideLoadProgressBar();
			}

			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
		});
		denglu_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

	}

	// 登录成功后js调用的方法如果target 大于等于API 17，则需要加上如下注解
	@JavascriptInterface
	public void Login(String data) throws Exception {

//		Log.v("zhong", "login调用成功--" + data);
		JSONObject obj = new JSONObject(data);

		// 保存用户信息
		ZHAccount account = LandInfoParser.SaveCurrentAccount(obj);
		ZhAccountPrefUtil.saveZHAccount(this, account);

		PrefUtil.savePref(this, ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, true);
		finish();

	}

	// 登录失败后js调用的方法
	@JavascriptInterface
	public void LoginFailed(String data) throws JSONException {

		Toast.makeText(MyDengLu.this, data, Toast.LENGTH_SHORT).show();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		WebView browser = (WebView) findViewById(R.id.Toweb_denglu);
		// Check if the key event was the Back button and if there's history
		if ((keyCode == KeyEvent.KEYCODE_BACK) && browser.canGoBack()) {
			browser.goBack();
			return true;
		}
		// return true;
		// If it wasn't the Back key or there's no web page history, bubble up
		// to the default
		// system behavior (probably exit the activity)
		return super.onKeyDown(keyCode, event);
	}

}
