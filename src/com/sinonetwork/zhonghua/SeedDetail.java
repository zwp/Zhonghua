package com.sinonetwork.zhonghua;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.model.getSeedInfoById;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class SeedDetail extends Activity {
	// getSeedInfoById
	// private ViewPager viewPager;
	// private List<ImageView> imageViews;
	// private PagerAdapter adapter;
	// private int imagesID[];
	private ViewPager viewPager; // android-support-v4中的滑动组件
	private List<ImageView> imageViews; // 滑动的图片集合
	// private String[] titles; // 图片标题
	private int[] imageResId; // 图片ID
	// private List<View> dots; // 图片标题正文的那些点
	private TextView tv_title;
	private int currentItem = 0; // 当前图片的索引号
	private ScheduledExecutorService scheduledExecutorService;

	// 数据信息
	private TextView seed_code_content; // 审定编号
	private TextView seed_name_content; // 品种名称
	private TextView seed_unit_content; // 选育单位
	private TextView seed_orign_content; // 品种来源

	private TextView charact_content; // 特征特性
	private TextView output_content; // 产量表现
	private TextView skill_content; // 栽培技术要点
	private ImageView detail_back;

	private String id;

	// private Handler handler = new Handler() {
	// public void handleMessage(android.os.Message msg) {
	// viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
	// };
	// };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seed_detail);
		//寻找控件
		viewPager = (ViewPager) findViewById(R.id.seed_viewpager);
		seed_code_content = (TextView) findViewById(R.id.seed_code_content);
		seed_name_content = (TextView) findViewById(R.id.seed_name_content);
		seed_unit_content = (TextView) findViewById(R.id.seed_unit_content);
		seed_orign_content = (TextView) findViewById(R.id.seed_orign_content);
		detail_back = (ImageView) findViewById(R.id.detail_back);

		charact_content = (TextView) findViewById(R.id.charact_content);
		output_content = (TextView) findViewById(R.id.output_content);
		skill_content = (TextView) findViewById(R.id.skill_content);
		imageViews = new ArrayList<ImageView>();
		// imagesID = new int[] { R.drawable.a, R.drawable.b, R.drawable.c,
		// R.drawable.d, R.drawable.e };
		// for (int i = 0; i < imageViews.size(); i++) {
		// ImageView imageView = new ImageView(this);
		// imageView.setImageResource(imagesID[i]);
		// imageView.setScaleType(ScaleType.CENTER_CROP);
		// imageViews.add(imageView);
		// }
		detail_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		// adapter = new PagerAdapter() {
		//
		// @Override
		// public boolean isViewFromObject(View arg0, Object arg1) {
		// // TODO Auto-generated method stub
		// return arg0 == arg1;
		// }
		//
		// @Override
		// public int getCount() {
		// // TODO Auto-generated method stub
		// return imageViews.size();
		// }
		//
		// @Override
		// public Object instantiateItem(View container, int position) {
		// // TODO Auto-generated method stub
		// ((ViewPager) container).addView(imageViews.get(position));
		// return container;
		// }
		//
		// @Override
		// public void destroyItem(View container, int position, Object object)
		// {
		// // TODO Auto-generated method stub
		// ((ViewPager) container).removeView((View) object);
		//
		// }
		// };
		// viewPager.setAdapter(adapter);

		// titles = new String[imageResId.length];
		// titles[0] = "农垦实现农业现代化";
		// titles[1] = "农垦实现农业现代化";
		// titles[2] = "农垦实现农业现代化";
		// titles[3] = "农垦实现农业现代化";
		// titles[4] = "农垦实现农业现代化";

//		imageViews = new ArrayList<ImageView>();

		// dots = new ArrayList<View>();
		// dots.add(findViewById(R.id.seed_dot0));
		// dots.add(findViewById(R.id.seed_dot1));
		// dots.add(findViewById(R.id.seed_dot2));
		// dots.add(findViewById(R.id.seed_dot3));
		// dots.add(findViewById(R.id.seed_dot4));

		// tv_title = (TextView) findViewById(R.id.seed_title);
		// tv_title.setText(titles[0]);//

		viewPager = (ViewPager) findViewById(R.id.seed_viewpager);

		// 设置一个监听器，当ViewPager中的页面改变时调用
		viewPager.setOnPageChangeListener(new MyPageChangeListener());
		Bundle bundle = getIntent().getExtras();
		id = bundle.getString("id");
		loadData("getSeedInfoById", id);
	}

	public void loadData(String method, String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		FastJsonRequest<getSeedInfoById> fastJson = new FastJsonRequest<getSeedInfoById>(
				Method.POST, URLAddress.SeedInfoByIdURL, getSeedInfoById.class,
				null, map, new Response.Listener<getSeedInfoById>() {
					@Override
					public void onResponse(getSeedInfoById weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(SeedDetail.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							seed_code_content.setText(weather.getResultdata()
									.getJudgeCode().toString()); // 审定编号
							seed_name_content.setText(weather.getResultdata()
									.getSeedName().toString()); // 品种名称
							seed_unit_content.setText(weather.getResultdata()
									.getBreedUnit().toString()); // 选育单位
							seed_orign_content.setText(weather.getResultdata()
									.getSeedSource().toString()); // 品种来源

							charact_content.setText(weather.getResultdata()
									.getSeedTrait().toString()); // 特征特性
							try {
								output_content.setText(URLDecoder.decode(weather.getResultdata()
										.getYieldShow().toString(),"utf-8"));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} // 产量表现
							skill_content.setText(weather.getResultdata()
									.getTechPoint().toString()); // 栽培技术要点

							String tupian = weather.getResultdata()
									.getSeedPicture().toString();
							Logger.e("图片++++++++++++++============"+tupian);
							if(tupian==null||"".equals(tupian)){
								Toast.makeText(SeedDetail.this, "没有图片！",
										Toast.LENGTH_SHORT).show();
							}
							String[] seedPictures = null;
							// Log.i("----", tupian);
							if (!tupian.equals("") && tupian != null
									&& !tupian.equals("null")) {
								if (tupian.contains(",")) {
									seedPictures = tupian.split("\\,");
								} else {
									seedPictures = new String[1];
									seedPictures[0] = tupian;
								}
								initSeedPicture(seedPictures);
							}

							// Log.i("----", seedPictures[0]);

						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(SeedDetail.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

	public void initSeedPicture(String[] seedPictures) {
		// imageResId = new int[] { R.drawable.a, R.drawable.b };
		imageResId = new int[seedPictures.length];
		for (int i = 0; i < seedPictures.length; i++) {
			imageResId[i] = imageResId[i];
		}
		// 初始化图片资源
		for (int i = 0; i < seedPictures.length; i++) {
			ImageView imageView = new ImageView(SeedDetail.this);
//			ImageLoader.getInstance().displayImage(
//					URLAddress.TPURLER + seedPictures[i], imageView);
			BitmapHelp.getBitmapUtils(this).display(imageView, URLAddress.TPURLER + seedPictures[i]);
			// imageView.setImageResource(imageResId[i]);
			imageView.setScaleType(ScaleType.FIT_XY);
			imageViews.add(imageView);
		}
		viewPager.setAdapter(new SeedAdapter());// 设置填充ViewPager页面的适配器
	}

	@Override
	public void onStart() {
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		// 当Activity显示出来后，每两秒钟切换一次图片显示
		// scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2,
		// TimeUnit.SECONDS);
		scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2,
				TimeUnit.SECONDS);
		super.onStart();
	}

	@Override
	public void onStop() {
		// 当Activity不可见的时候停止切换
		scheduledExecutorService.shutdown();
		super.onStop();
	}

	private class ScrollTask implements Runnable {

		public void run() {
			synchronized (viewPager) {
				System.out.println("currentItem: " + currentItem);
				currentItem = (currentItem + 1) % imageViews.size();
				// handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
			}
		}

	}

	private class MyPageChangeListener implements OnPageChangeListener {
		private int oldPosition = 0;

		/**
		 * This method will be invoked when a new page becomes selected.
		 * position: Position index of the new selected page.
		 */
		public void onPageSelected(int position) {
			currentItem = position;
			// tv_title.setText(titles[position]);
			// dots.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
			// dots.get(position).setBackgroundResource(R.drawable.dot_focused);
			oldPosition = position;
		}

		public void onPageScrollStateChanged(int arg0) {

		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}
	}

	/**
	 * 填充ViewPager页面的适配器
	 * 
	 * @author Administrator
	 * 
	 */
	private class SeedAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageResId.length;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imageViews.get(arg1));
			return imageViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}
}
