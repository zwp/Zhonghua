package com.sinonetwork.zhonghua;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.apache.http.Header;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

public class QixiangSolid extends Activity {
	private ImageView iv;
	private ImageView back;

	private int year;
	private int month;
	private int day;
	private int h;
	private int mi;

	private String date = "";
	private String time = "";

	private TextView tv_date;
	private TextView tv_time;
	private Button bt_search;
	private ProgressBar pb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qixiang_solid);

		iv = (ImageView) findViewById(R.id.qixiang_solid_iv);
		back = (ImageView) findViewById(R.id.back);
		tv_date = (TextView) findViewById(R.id.tv_date);
		tv_time = (TextView) findViewById(R.id.tv_time);
		bt_search = (Button) findViewById(R.id.bt_search);
		pb=(ProgressBar) findViewById(R.id.qixiang_solid_progressBar);
		initTime();

		setDefault();

		setListener();
	}

	private void setDefault() {
		tv_date.setText(date);
		tv_time.setText(time);
	}

	private void setListener() {
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

		tv_date.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new DatePickerDialog(QixiangSolid.this,
						AlertDialog.THEME_HOLO_LIGHT, new OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year,
									int monthOfYear, int dayOfMonth) {
								monthOfYear++;
								date = "" + year;
								if (monthOfYear < 10) {
									date += "0" + monthOfYear;
								} else {
									date += monthOfYear;
								}
								if (dayOfMonth < 10) {
									date += "0" + dayOfMonth;
								} else {
									date += dayOfMonth;
								}
								tv_date.setText(date);
							}
						}, year, month, day).show();
			}
		});

		tv_time.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new TimePickerDialog(QixiangSolid.this,
						AlertDialog.THEME_HOLO_LIGHT, new OnTimeSetListener() {

							@Override
							public void onTimeSet(TimePicker view,
									int hourOfDay, int minute) {
								time = "" + hourOfDay + minute;
								tv_time.setText(time);
							}
						}, h, mi, true).show();

			}
		});

		bt_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				pb.setVisibility(View.VISIBLE);
				iv.setVisibility(View.INVISIBLE);
				loadData();
			}
		});
	}

	private void initTime() {
		Calendar mycalendar = Calendar.getInstance(Locale.CHINA);
		year = mycalendar.get(Calendar.YEAR);
		month = mycalendar.get(Calendar.MONTH);
		day = mycalendar.get(Calendar.DAY_OF_MONTH);
		h = mycalendar.get(Calendar.HOUR_OF_DAY);
		mi = mycalendar.get(Calendar.MINUTE);

		date = new SimpleDateFormat("yyyyMMdd").format(mycalendar.getTime());
		time = new SimpleDateFormat("HH:mm").format(mycalendar.getTime());
	}

	private void loadData() {
		String url = " http://211.94.93.238/zhnyxxgc/httpservice.action?method=searchTAsmm&date=" + date + time;
//		Log.i("lk", "url：" + url);
//		Log.i("gxx", "土壤url：" + url);

		new AsyncHttpClient().get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers,
					byte[] response) {

				String x = new String(response);
				JsonTuRang shuju = JSON.parseObject(x, JsonTuRang.class);
//				Log.i("lk", "code：" + shuju.getResultcode());
				if (shuju.getResultdata() != null) {
					byte[] array = Base64.decode(shuju.getResultdata().getD2(), Base64.DEFAULT);
					Bitmap bm = BitmapFactory.decodeByteArray(array, 0, array.length);
					pb.setVisibility(View.INVISIBLE);
					iv.setVisibility(View.VISIBLE);
					
					iv.setImageBitmap(bm);
					
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] errorResponse, Throwable e) {
			}

			@Override
			public void onRetry(int retryNo) {
			}
			
		});
		
	}
	
}
