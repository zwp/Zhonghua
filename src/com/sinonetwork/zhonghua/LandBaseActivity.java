package com.sinonetwork.zhonghua;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.utils.HttpUtil;

public class LandBaseActivity extends Activity {

	public String TAG = "zhong";
	final Handler handler = new Handler();
	boolean success = true;
	private TextView topTitleTV;
	private View backIV;
	private ProgressBar loadProgressBar;
	
	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!checkNetWorkIsAvailable()) {
			showShortToast(R.string.can_not_connect_server);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!checkNetWorkIsAvailable()) {
			showShortToast(R.string.can_not_connect_server);
		}
		
	}
	/**
	 * 检测网络是否可用
	 * 
	 * @return
	 */
	protected boolean checkNetWorkIsAvailable() {
		return HttpUtil.isNetworkAvailable(this);
	}
	 /*
		 * 设置返回button
		 */
		public void setBackBtn() {
			backIV = (ImageView) findViewById(R.id.back);
			backIV.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					finish();
				}
			});
		}

		
		public void setTopTitleTV(String str) {
			topTitleTV = (TextView) findViewById(R.id.top_title_textview);
			topTitleTV.setText(str);
		}
		protected void showLoadProgressBar() {
			loadProgressBar = (ProgressBar) findViewById(R.id.load_progressBar);
			loadProgressBar.setVisibility(View.VISIBLE);
		}

		protected void hideLoadProgressBar() {
			loadProgressBar.setVisibility(View.GONE);
		}
		public void showShortToast(String str) {
			Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
		}

		public void showShortToast(int resId) {
			Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
		}

		public void showLongToast(String str) {
			Toast.makeText(this, str, Toast.LENGTH_LONG).show();
		}

		public void showLongToast(int resId) {
			Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
		}
}
