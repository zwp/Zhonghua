package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sinonetwork.zhonghua.adapter.ExpandAdapter;

public class HomeActivity extends Activity implements OnChildClickListener {

	private ExpandableListView mListView = null;
	private ExpandAdapter mAdapter = null;
	private List<List<Item>> mData = new ArrayList<List<Item>>();
	private RelativeLayout diagnosis;
	private int[] mGroupArrays = { R.array.one, R.array.two, R.array.three,
			R.array.four };

	private int[][] mImageIds = new int[][] {
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1,
					R.drawable.check1, R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1,
					R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1 } };
	private ImageView back;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initData();
		mListView = new ExpandableListView(this);
		// mListView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
		// LayoutParams.FILL_PARENT));
		setContentView(R.layout.home);
		mListView = (ExpandableListView) findViewById(R.id.expandlistview);
		back = (ImageView) findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		diagnosis = (RelativeLayout)findViewById(R.id.diagnosis);
		// mListView.setGroupIndicator(getResources().getDrawable(
		// R.drawable.expander_floder));
		mListView.setGroupIndicator(null);
		mAdapter = new ExpandAdapter(this, mData);
		mListView.setAdapter(mAdapter);
		mListView
				.setDescendantFocusability(ExpandableListView.FOCUS_AFTER_DESCENDANTS);
		mListView.setOnChildClickListener(this);
		diagnosis.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeActivity.this, Result.class);
				startActivity(intent);
			}
		});
	}

	/*
	 * ChildView 璁剧疆 甯冨眬寰堝彲鑳給nChildClick杩涗笉鏉ワ紝瑕佸湪 ChildView layout 閲屽姞涓�
	 * android:descendantFocusability="blocksDescendants",
	 * 杩樻湁isChildSelectable閲岃繑鍥瀟rue
	 */
	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// TODO Auto-generated method stub
		Item item = mAdapter.getChild(groupPosition, childPosition);
		// new AlertDialog.Builder(ts
		

		return true;
	}

	private void initData() {
		for (int i = 0; i < mGroupArrays.length; i++) {
			List<Item> list = new ArrayList<Item>();
			String[] childs = getStringArray(mGroupArrays[i]);
			// String[] details = getStringArray(mDetailIds[i]);
			for (int j = 0; j < childs.length; j++) {
				Item item = new Item(mImageIds[i][j], childs[j]);
				list.add(item);
			}
			mData.add(list);
		}
		// List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		// for (int i = 0; i < mGroupArrays.length; i++) {
		// Map<String, Object> map = new HashMap<String, Object>();
		// map.put("mGroupArrays", mGroupArrays[i]);
		// map.put("mImageIds", mImageIds[i]);
		// list.add(map);
		// }
	}

	private String[] getStringArray(int mGroupArrays2) {
		return getResources().getStringArray(mGroupArrays2);
	}

}