package com.sinonetwork.zhonghua;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.CommentReplyAdapter;
import com.sinonetwork.zhonghua.adapter.EventsImagesGridViewAdapter;
import com.sinonetwork.zhonghua.model.ClickUser;
import com.sinonetwork.zhonghua.model.Comment;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.view.CollapsibleTextView;
import com.sinonetwork.zhonghua.view.CustomListView;
import com.sinonetwork.zhonghua.view.NoScrollGridView;

public class MyQuestionDetail extends LandBaseActivity {
	private TextView title;
	private CollapsibleTextView content;
	private NoScrollGridView gvImages;
	private TextView time;
	private TextView favorNums;
	private TextView commentNum;
	private TextView favorName;
	private CustomListView commentListview;
	private LinearLayout zanLineLayout;
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_question_detail);
		setBackBtn();//返回时调用
		initview();
		if (getIntent().getBooleanExtra("flag", true)) {
			setTopTitleTV("我的回复");
			int questionId = getIntent().getIntExtra("QuestionId", 0);
			loadQuestion(questionId);
		} else {
			setTopTitleTV("我的问题");
			Question question = (Question) getIntent().getSerializableExtra(
					"question");
			if (question != null) {
				initQuestionData(question);
			}
		}
	}

	private void initview() {
		title = (TextView) findViewById(R.id.title);
		content = (CollapsibleTextView) findViewById(R.id.content);
		gvImages = (NoScrollGridView) findViewById(R.id.gv_images);
		time = (TextView) findViewById(R.id.time);
		favorNums = (TextView) findViewById(R.id.tv_favor_num);
		commentNum = (TextView) findViewById(R.id.tv_comments_num);
		favorName = (TextView) findViewById(R.id.tv_favour);
		zanLineLayout = (LinearLayout) findViewById(R.id.ll_zan);
		commentListview = (CustomListView) findViewById(R.id.comments_lv);
	}

	private void initQuestionData(Question question) {
		title.setText(question.getQuestionTitle());
		content.setDesc(question.getQuestionText(), BufferType.NORMAL);
		try {
			time.setText(df2.format(df.parse(question.getPublishTime())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> images = getAllVisibleImages(question);
		if (!question.getPictures().equals("null")) {
			gvImages.setVisibility(View.VISIBLE);
			EventsImagesGridViewAdapter mImagesAdapter = new EventsImagesGridViewAdapter(
					getApplicationContext(), images);
			gvImages.setAdapter(mImagesAdapter);
		} else {
			gvImages.setVisibility(View.GONE);
		}
		if (question.getClickNum() != 0) {
			zanLineLayout.setVisibility(View.VISIBLE);
			favorName.setVisibility(View.VISIBLE);
			favorNums.setText(question.getClickNum() + "");
			JSONArray array = JSONArray.parseArray(question.getClickNumUsers());
			List<ClickUser> list = JSONArray.parseArray(array.toJSONString(),
					ClickUser.class);
			HashSet<String> hashSet = new HashSet<String>();
			for (ClickUser user : list) {
				hashSet.add(user.getUserName());
			}
			StringBuilder sb = new StringBuilder();
			Iterator<String> iterator = hashSet.iterator();
			while (iterator.hasNext()) {
				sb.append(iterator.next() + ";");
			}
			favorName.setText(sb.toString());
		} else {
			favorName.setVisibility(View.GONE);
		}
		JSONArray jsonArray = JSONArray.parseArray(question.getReplies());
		List<Comment> comments = JSONArray.parseArray(jsonArray.toJSONString(),
				Comment.class);
		if (comments.size() != 0) {
			commentNum.setText(comments.size() + "");
			zanLineLayout.setVisibility(View.VISIBLE);
			commentListview.setVisibility(View.VISIBLE);
			CommentReplyAdapter adapter = new CommentReplyAdapter(comments,
					getApplicationContext());
			commentListview.setAdapter(adapter);
		}
	}

	private void loadQuestion(int questionId) {
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "detailQuestion");
		params.addBodyParameter("id", questionId + "");
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLYI, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// TODO Auto-generated method stub
						System.out.println("error");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						// TODO Auto-generated method stub
						JSONObject jsonObject = JSONObject
								.parseObject(responseInfo.result);
						System.out.println(jsonObject.get("resultcode"));
						Log.i("gxx", "Jsonobject解析成功的值："+jsonObject.get("resultcode"));
						JSONObject json = JSONObject.parseObject(jsonObject
								.toString());
						Log.i("gxx", "json:"+json);
//						//进行书否还有问题的判断
//						if (jsonObject.get("resultcode").equals("error")) {
//							Toast.makeText(MyQuestionDetail.this, "提问者已删除问题",Toast.LENGTH_LONG).show();
//							return;
//						}
						Question question = JSONObject.parseObject(json
								.getJSONObject("resultdata").toJSONString(),
								Question.class);
						initQuestionData(question);
					}
				});
	}

	private List<String> getAllVisibleImages(Question question) {
		List<String> list = new ArrayList<String>();
		String pictures = question.getPictures();
		if (pictures != null) {
			String images[] = pictures.split(",");
			for (int i = 0; i < images.length; i++) {
				if (!TextUtils.isEmpty(images[i])) {
					list.add(images[i]);
				}

			}
		}
		return list;
	}
}
