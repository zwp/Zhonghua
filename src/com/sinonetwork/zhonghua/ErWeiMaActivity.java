package com.sinonetwork.zhonghua;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class ErWeiMaActivity extends Activity implements View.OnClickListener {
	private static final int THUMB_SIZE = 150;
	private IWXAPI api;

	private ImageView back;
	private LinearLayout shareFriends;
	private LinearLayout shareFriendsCircle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_erweima_img);
		api = WXAPIFactory.createWXAPI(this, null);

		// 将该app注册到微信
		api.registerApp("wx3448ae4c520058e9");
		shareFriends = (LinearLayout) this.findViewById(R.id.share_friends);
		shareFriendsCircle = (LinearLayout) this.findViewById(R.id.share_friends_circle);
		shareFriends.setOnClickListener(this);
		shareFriendsCircle.setOnClickListener(this);
	}
		
	public void fanhui(View v) {
		// TODO Auto-generated method stub
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.share_friends:
			Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.nongyibao_erweima);
			WXImageObject imgObj = new WXImageObject(bmp);

			WXMediaMessage msg = new WXMediaMessage();
			msg.mediaObject = imgObj;

			Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
			bmp.recycle();
			msg.thumbData = bmpToByteArray(thumbBmp, true); // 设置缩略图
				
			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("img");
			req.message = msg;
			req.scene = SendMessageToWX.Req.WXSceneSession;
			api.sendReq(req);

			finish();
			
			break;
		case R.id.share_friends_circle:
			Bitmap bmp2 = BitmapFactory.decodeResource(getResources(), R.drawable.nongyibao_erweima);
			WXImageObject imgObj2 = new WXImageObject(bmp2);

			WXMediaMessage msg2 = new WXMediaMessage();
			msg2.mediaObject = imgObj2;
			
			Bitmap thumbBmp2 = Bitmap.createScaledBitmap(bmp2, THUMB_SIZE, THUMB_SIZE, true);
			bmp2.recycle();
			msg2.thumbData = bmpToByteArray(thumbBmp2, true); // 设置缩略图

			SendMessageToWX.Req req2 = new SendMessageToWX.Req();
			req2.transaction = buildTransaction("img");
			req2.message = msg2;
			req2.scene = SendMessageToWX.Req.WXSceneTimeline;
			api.sendReq(req2);

			break;

		default:
			break;
		}

	}

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}

	public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.PNG, 100, output);
		if (needRecycle) {
			bmp.recycle();
		}

		byte[] result = output.toByteArray();
		try {
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return result;

	}
}
