package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.QuestionListAdapter;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;

public class MyQuestion extends Activity {
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;
	private static final int NO_MORE_DARA = 12;
	private MyListView question_list;
	private QuestionListAdapter adapter;
	private ImageView question_back;
	private String userName = "";
	private ArrayList<ArrayList<HashMap<String, Object>>> mArrayList;
	private List<Question> questions = new ArrayList<Question>();
	private int currentPage = 1;
	private int totalPage;
	private Handler mHandler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case NO_MORE_DARA:
				question_list.misHaveNewDatas = false;
				question_list.onLoadMoreComplete(); // 加载更多完成s
				question_list.setEndFootGone();
				break;
			case LOAD_DATA_FINISH:
				if (adapter != null) {
					adapter.setBigEventAdapterData((ArrayList<Question>) msg.obj);
					adapter.notifyDataSetChanged();
				}
				question_list.misHaveNewDatas = false;
				question_list.onLoadMoreComplete(); // 加载更多完成
				break;
			case REFRESH_DATA_FINISH:
				System.out.println("reflash");
				question_list.setEndFootVisiable();
				if (adapter != null) {
					adapter = new QuestionListAdapter(MyQuestion.this, questions);
					adapter.setBigEventAdapterData(questions);
					question_list.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}
				question_list.onRefreshComplete(); // 下拉刷新完成
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_question);
		question_back = (ImageView) findViewById(R.id.question_back);
		question_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		if (PrefUtil.getBooleanPref(getApplicationContext(), ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			userName = PrefUtil.getStringPref(getApplicationContext(), "zhonghua_userName");
			init();
		} else {
			Toast.makeText(getApplicationContext(), "用户名为空，请先登录！", 2000).show();
		}
	}

	private void init() {
		question_list = (MyListView) findViewById(R.id.question_list);
		question_list.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				loadData(0);
			}
		});
		question_list.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				loadData(1);
			}
		});
		loadData(0);
		adapter = new QuestionListAdapter(getApplicationContext(), questions);
		question_list.setAdapter(adapter);
		question_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> listview, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MyQuestion.this, MyQuestionDetail.class);
				intent.putExtra("QuestionId", questions.get(position - 1).getId());
				Bundle bundle = new Bundle();
				bundle.putSerializable("question", questions.get(position - 1));
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
	}

	private void loadData(final int type) {
		new Thread() {
			public void run() {
				switch (type) {
				case 0:
					questions.clear();
					currentPage = 1;
					addCotent(1, 0, userName);
					break;
				case 1:
					currentPage++;
					if (currentPage <= totalPage) {
						addCotent(currentPage, 1, userName);
					} else {
						currentPage--;
						Message _Msg = mHandler.obtainMessage(NO_MORE_DARA);
						mHandler.sendMessage(_Msg);

					}
					break;
				}
			};
		}.start();
	}

	public void addCotent(int currentPage, final int type, String userName) {
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "searchQuestions");
		params.addBodyParameter("userName", userName);
		params.addBodyParameter("pageRows", "10");
		params.addBodyParameter("curPage", currentPage + "");
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER, params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				System.out.println("error");
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				// TODO Auto-generated method stub
				JSONObject jsonObject = JSONObject.parseObject(responseInfo.result);
				System.out.println(jsonObject.get("resultcode"));
				JSONObject json = JSONObject.parseObject(jsonObject.toString());
				totalPage = json.getIntValue("totalPages");
				List<Question> datas = JSONArray.parseArray(json.getJSONArray("resultdata").toJSONString(), Question.class);
				questions.addAll(datas);
				if (type == 0) { // 下拉刷新
					Message _Msg = mHandler.obtainMessage(REFRESH_DATA_FINISH, questions);
					mHandler.sendMessage(_Msg);
				} else if (type == 1) {
					Message _Msg = mHandler.obtainMessage(LOAD_DATA_FINISH, questions);
					mHandler.sendMessage(_Msg);
				}
			}
		});

	}

}
