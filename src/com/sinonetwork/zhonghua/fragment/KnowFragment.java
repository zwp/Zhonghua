package com.sinonetwork.zhonghua.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.MyDengLu;
import com.sinonetwork.zhonghua.PublishQuestion;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.ReplyActivity;
import com.sinonetwork.zhonghua.adapter.QuestionAdapter;
import com.sinonetwork.zhonghua.event.CommentEvent;
import com.sinonetwork.zhonghua.event.UpEvent;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;

import de.greenrobot.event.EventBus;

public class KnowFragment extends Fragment implements View.OnClickListener {
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;
	private static final int NO_MORE_DARA = 12;

	private TextView know_publish;
	private Button searchBtn;
	private EditText keywordET;
	private List<Question> questions = new ArrayList<Question>();
	private QuestionAdapter adapter;
	private LinearLayout write_box;
	private ImageView write_comments_calcel;
	private ImageView write_comments_ok;
	private EditText write_comments_edittext;
	private Question question;
	private MyListView listView;
	private int currentPage = 1;
	private int totalPage;
	private static final int DIALOG = 1;
	private ProgressDialog progressDialog;
	private RelativeLayout rel_first;
	private Handler mHandler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case REFRESH_DATA_FINISH:
				if (adapter != null) {
					adapter = new QuestionAdapter(questions,
							KnowFragment.this.getActivity());
					adapter.setBigEventAdapterData(questions);
					listView.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}
				listView.onRefreshComplete(); // 下拉刷新完成
				break;
			case LOAD_DATA_FINISH:
				if (adapter != null) {
					adapter.setBigEventAdapterData((ArrayList<Question>) msg.obj);
					adapter.notifyDataSetChanged();
				}
				listView.misHaveNewDatas = false;
				listView.onLoadMoreComplete(); // 加载更多完成
				break;
			case NO_MORE_DARA:
				listView.misHaveNewDatas = false;
				listView.onLoadMoreComplete(); // 加载更多完成
				break;
			default:
				break;
			}
		};
	};

	private int mPosition = 0;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		EventBus.getDefault().register(this);

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View knowLayout = inflater.inflate(R.layout.activity_know, container,
				false);
//		字体设置
		AssetManager mgr = getActivity().getAssets();// 得到AssetManager
		Typeface tf = Typeface.createFromAsset(mgr, "fonts/mini.ttf");// 根据路径得到Typeface
		// Typeface tf2=Typeface.createFromAsset(mgr,
		// "fonts/mini.ttf");//根据路径得到Typeface
		((TextView) knowLayout.findViewById(R.id.tv_introduction_find))
				.setTypeface(tf);// 设置字体
		((TextView) knowLayout.findViewById(R.id.tv_know_introduction))
				.setTypeface(tf);// 设置字体
		((TextView) knowLayout.findViewById(R.id.tv_know_introduction2))
				.setTypeface(tf);// 设置字体
//提示设置
		rel_first = (RelativeLayout) knowLayout.findViewById(R.id.rel_first);
		if (PrefUtil.getBooleanPref(this.getActivity(), "first_know", false)) {
			rel_first.setVisibility(View.GONE);
		}
		((Button) knowLayout.findViewById(R.id.btn_know))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						PrefUtil.savePref(KnowFragment.this.getActivity(),
								"first_know", true);
						rel_first.setVisibility(View.GONE);
					}
				});

		rel_first.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				PrefUtil.savePref(KnowFragment.this.getActivity(),
						"first_know", true);
				rel_first.setVisibility(View.GONE);
			}
		});
		//初始化控件，加设置侦听。
		keywordET = (EditText) knowLayout.findViewById(R.id.keywordET);
		searchBtn = (Button) knowLayout.findViewById(R.id.searchBtn);
		write_box = (LinearLayout) knowLayout.findViewById(R.id.write_box);
		write_comments_calcel = (ImageView) knowLayout
				.findViewById(R.id.write_comments_calcel);
		write_comments_ok = (ImageView) knowLayout
				.findViewById(R.id.write_comments_ok);
		write_comments_edittext = (EditText) knowLayout
				.findViewById(R.id.write_comments_edittext);
		write_comments_calcel.setOnClickListener(this);
		write_comments_ok.setOnClickListener(this);
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentPage = 1;
				questions.clear();
				showProgressDialog("正在加载请稍后。。。");
				addContent(currentPage, 0, keywordET.getText().toString());
			}
		});
		//对话框
		know_publish = (TextView) knowLayout.findViewById(R.id.know_publish);
		know_publish.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Dialog dialog = new AlertDialog.Builder(getActivity())
						.setItems(new String[] { "发布问题", "在线咨询" },
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										// TODO Auto-generated method stub
										switch (which) {
										case 0:
											if (PrefUtil
													.getBooleanPref(
															getActivity()
																	.getApplicationContext(),
															ZhAccountPrefUtil.IS_LOGINED_ACCOUNT,
															false)) {

												Intent intent = new Intent(
														getActivity(),
														PublishQuestion.class);
												startActivity(intent);
											} else {
												Intent intent = new Intent(
														getActivity(),
														MyDengLu.class);
												startActivity(intent);
											}
											break;
										case 1:
											if (isAPKInstalled()) {
												PackageManager pManager = getActivity()
														.getPackageManager();
												Intent intent = pManager
														.getLaunchIntentForPackage("com.v2");
												startActivity(intent);
											} else {
												Uri uri = Uri
														.parse("http://sxsp.agricom.com.cn/autodownload/Agricom.apk");
												Intent it = new Intent(
														Intent.ACTION_VIEW, uri);
												startActivity(it);
											}
											break;
										}
										dialog.dismiss();
									}
								}).show();
				// createDialog();
			}
		});
		listView = (MyListView) knowLayout.findViewById(R.id.lv);
		adapter = new QuestionAdapter(questions, this.getActivity());

		listView.setAdapter(adapter);
		listView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO 下拉刷新
				loadData(0);
			}
		});

		listView.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				// TODO 加载更多
				loadData(1);
			}
		});

		// 添加加载更多 footer view
		loadData(0);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(KnowFragment.this.getActivity(),
						ReplyActivity.class);
				Question question = questions.get(position - 1);
				String questionId = questions.get(position - 1).getId() + "";
				String userName = questions.get(position - 1).getUserName1()
						+ "";
				intent.putExtra("questionId", questionId);
				intent.putExtra("userName", userName);
				intent.putExtra("question", question);
				startActivity(intent);
			}
		});
		return knowLayout;
	}

	public void loadData(final int type) {
		new Thread() {
			@Override
			public void run() {
				switch (type) {
				case 0:
					questions.clear();
					currentPage = 1;
					addContent(currentPage, 0, keywordET.getText().toString());
					break;

				case 1:
					currentPage++;
					if (currentPage <= totalPage) {
						addContent(currentPage, 1, keywordET.getText()
								.toString());
					} else {
						currentPage--;
						Message _Msg = mHandler.obtainMessage(NO_MORE_DARA);
						mHandler.sendMessage(_Msg);

					}

					break;
				}

			}
		}.start();

	}

	public void addContent(int currentPage, final int type, String text) {
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "searchQuestions");
		params.addBodyParameter("pageRows", "10");
		params.addBodyParameter("curPage", currentPage + "");
		if (!text.equals("")) {
			params.addBodyParameter("questionTitle", text);
		}
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						closeProgressDialog();
						Toast.makeText(KnowFragment.this.getActivity(),
								"加载失败。。", 2000).show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> resultInfo) {
						System.out.println("数据："+resultInfo.result);
						closeProgressDialog();
						JSONObject jsonObject = JSONObject
								.parseObject(resultInfo.result);
						JSONObject json = JSONObject.parseObject(jsonObject
								.toString());
						totalPage = json.getIntValue("totalPages");
						List<Question> datas = JSONArray.parseArray(json
								.getJSONArray("resultdata").toJSONString(),
								Question.class);
						questions.addAll(datas);
						if (type == 0) { // 下拉刷新
							// Collections.reverse(mList); //逆序
							Message _Msg = mHandler.obtainMessage(
									REFRESH_DATA_FINISH, questions);
							mHandler.sendMessage(_Msg);
						} else if (type == 1) {
							Message _Msg = mHandler.obtainMessage(
									LOAD_DATA_FINISH, questions);
							mHandler.sendMessage(_Msg);
						}

						adapter.setBigEventAdapterData(questions);
						adapter.notifyDataSetChanged();
						// setData();
						// listView.setSelection(mPosition);
					}
				});

	}

	// public void setData() {
	// if (questions.size() != 0) {
	// adapter.notifyDataSetChanged();
	// }
	// }
	
	//
	//混淆时出错,并不知道onEvent干什么用,用来接受消息
	public void onEvent(CommentEvent event) {
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			write_box.setVisibility(View.VISIBLE);
			write_comments_edittext.requestFocus();
			question = event.question;
			mPosition = event.position;
		} else {
			Toast.makeText(KnowFragment.this.getActivity(), "您尚未登录！", 2000)
					.show();

		}
	}
	//混淆时出错,并不知道onEvent干什么用
	public void onEvent(UpEvent event) {
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			question = event.question;
			mPosition = event.position;

			RequestParams params = new RequestParams();
			params.addBodyParameter("method", "addQuestionClickNum");
			params.addBodyParameter("id", question.getId() + "");
			params.addBodyParameter("userName", PrefUtil.getStringPref(
					this.getActivity(), "zhonghua_userName"));

			HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
					params, new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {

						}

						@Override
						public void onSuccess(ResponseInfo<String> resultInfo) {
							JSONObject jsonObject = JSONObject
									.parseObject(resultInfo.result);
							if (jsonObject.getString("resultcode").equals("ok")) {
								Toast.makeText(KnowFragment.this.getActivity(),
										"点赞成功", 2000).show();
								updataView();
							} else {
								Toast.makeText(KnowFragment.this.getActivity(),
										jsonObject.getString("resultdesc"),
										2000).show();

							}

						}

					});
		} else {
			Toast.makeText(KnowFragment.this.getActivity(), "您尚未登录！", 2000)
					.show();

		}

	}

	/**
	 * 刷新视图
	 */
	public void updataView() {
		RequestParams params2 = new RequestParams();
		params2.addBodyParameter("method", "searchQuestions");
		params2.addBodyParameter("pageRows", "10");
		params2.addBodyParameter("curPage", getPostPage(mPosition) + "");

		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER, params2,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@Override
					public void onSuccess(ResponseInfo<String> resultInfo) {

						JSONObject jsonObject = JSONObject
								.parseObject(resultInfo.result);
						JSONObject json = JSONObject.parseObject(jsonObject
								.toString());
						List<Question> datas = JSONArray.parseArray(json
								.getJSONArray("resultdata").toJSONString(),
								Question.class);
						for (int i = 0; i < datas.size(); i++) {
							if (datas.get(i).getId() == question.getId()) {
								questions.remove(mPosition);
								questions.add(mPosition, datas.get(i));
							}

						}
						adapter = new QuestionAdapter(questions,
								KnowFragment.this.getActivity());
						adapter.setBigEventAdapterData(questions);
						listView.setAdapter(adapter);
						adapter.notifyDataSetChanged();
						listView.setSelection(mPosition + 1);

					}
				});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.write_comments_calcel:
			write_box.setVisibility(View.GONE);

			break;
		case R.id.write_comments_ok:
			if (write_comments_edittext.getText().toString().equals("")) {
				Toast.makeText(this.getActivity(), "请输入评论内容", 2000).show();
				return;
			}
			RequestParams params = new RequestParams();
			params.addBodyParameter("method", "addReply");
			params.addBodyParameter("replyText", write_comments_edittext
					.getText().toString());
			params.addBodyParameter("questionId", question.getId() + "");
			params.addBodyParameter("userName", PrefUtil.getStringPref(
					this.getActivity(), "zhonghua_userName"));

			HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
					params, new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {

						}

						@Override
						public void onSuccess(ResponseInfo<String> resultInfo) {
							JSONObject jsonObject = JSONObject
									.parseObject(resultInfo.result);
							if (jsonObject.getString("resultcode").equals("ok")) {
								Toast.makeText(KnowFragment.this.getActivity(),
										"评论成功", 2000).show();
								updataView();
							}

						}
					});
			write_comments_edittext.setText("");
			write_box.setVisibility(View.GONE);

			break;
		default:
			break;
		}
	}

	public int getPostPage(int position) {
		return (position / 10) + 1;
	}

	private boolean isAPKInstalled() {
		PackageInfo pInfo;
		try {
			pInfo = getActivity().getPackageManager().getPackageInfo("com.v2",
					0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			pInfo = null;
			e.printStackTrace();
		}
		if (pInfo == null) {
			return false;
		} else {
			return true;
		}

	}

	public void showProgressDialog(String message) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this.getActivity());
			progressDialog.setCancelable(false);
		}

		if (message == null) {
			progressDialog.setMessage("数据加载中...");
		} else {
			progressDialog.setMessage(message);
		}

		progressDialog.show();
	}

	public void closeProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	@SuppressLint("NewApi")
	private void createDialog() {
		// MyDialog dialog = new MyDialog(getActivity());
		// dialog.show();
		// Builder builder = new AlertDialog.Builder
		// (getActivity(), R.style.custom_dialog);
		// builder.setMessage("sdfsdf");
		// builder.show();
	}
}
