package com.sinonetwork.zhonghua.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.TestSoilInfoById;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class CetuSolidElementInfoFragment extends Fragment {

	 private String id;
	// private String ph;
	// private String aa;
	// private String om;
	// private String nh4n;
	// private String no3n;
	// private String phosphor;
	// private String kalium;
	// private String calcium;
	// private String magnesium;
	// private String sulfur;
	// private String ferrum;
	// private String copper;
	// private String manganese;
	// private String zinc;
	// private String boron;

	private TextView phzhi;
	private TextView aa;
	private TextView om;
	private TextView antaidan;
	private TextView xiaotaidan;
	private TextView lin;
	private TextView jia;
	private TextView gai;
	private TextView mei;
	private TextView liu;
	private TextView tie;
	private TextView tong;
	private TextView meng;
	private TextView xin;
	private TextView peng;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.cetu_solid_element_info, null);
		phzhi = (TextView) view.findViewById(R.id.phzhi);
		aa = (TextView) view.findViewById(R.id.aa);
		om = (TextView) view.findViewById(R.id.om);
		antaidan = (TextView) view.findViewById(R.id.antaidan);
		xiaotaidan = (TextView) view.findViewById(R.id.xiaotaidan);
		lin = (TextView) view.findViewById(R.id.lin);
		jia = (TextView) view.findViewById(R.id.jia);
		gai = (TextView) view.findViewById(R.id.gai);
		mei = (TextView) view.findViewById(R.id.mei);
		liu = (TextView) view.findViewById(R.id.liu);
		tie = (TextView) view.findViewById(R.id.tie);
		tong = (TextView) view.findViewById(R.id.tong);
		meng = (TextView) view.findViewById(R.id.meng);
		xin = (TextView) view.findViewById(R.id.xin);
		peng = (TextView) view.findViewById(R.id.peng);
//		Bundle bundle = getActivity().getIntent().getExtras();
//		id = bundle.getString("id");
		SharedPreferences sharedPreferences= getActivity().getSharedPreferences("henduoshuju", getActivity().MODE_PRIVATE); 
		id =sharedPreferences.getString("yyid", "获取id失败"); 
		loadData("getTestSoilInfoById", id);

		return view;
	}

	public void loadData(String method,String yyid) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id",yyid);
		FastJsonRequest<TestSoilInfoById> fastJson = new FastJsonRequest<TestSoilInfoById>(
				Method.POST, URLAddress.queryMyOrderTestSoilURL, TestSoilInfoById.class,
				null, map, new Response.Listener<TestSoilInfoById >() {
					@Override
					public void onResponse(TestSoilInfoById weather) {
						// 赋值
//						orderCode = weather.getResultdata().getOrderCode().toString();
//						Toast.makeText(CetuDetail.this, "orderCode--------"+String.valueOf(orderCode), Toast.LENGTH_SHORT).show();
						
//						ph = weather.getResultdata().getOrderCode().toString();
//						aa = weather.getResultdata().getOrderCode().toString();
//						om = weather.getResultdata().getOrderCode().toString();
//						nh4n = weather.getResultdata().getOrderCode().toString();
//						no3n = weather.getResultdata().getOrderCode().toString();
//						phosphor = weather.getResultdata().getOrderCode().toString();
//						kalium = weather.getResultdata().getOrderCode().toString();
//						calcium = weather.getResultdata().getOrderCode().toString();
//						magnesium = weather.getResultdata().getOrderCode().toString();
//						sulfur = weather.getResultdata().getOrderCode().toString();
//						ferrum = weather.getResultdata().getOrderCode().toString();
//						copper = weather.getResultdata().getOrderCode().toString();
//						manganese = weather.getResultdata().getOrderCode().toString();
//						zinc = weather.getResultdata().getOrderCode().toString();
//						boron = weather.getResultdata().getOrderCode().toString();
						
						//ph值，AA，OM，铵态氮，硝态氮，磷，钾，钙，镁，硫，铁，铜，锰，锌，硼，
						if(weather.getResultdata()==null){
							Toast.makeText(getActivity(), "没有数据", Toast.LENGTH_SHORT).show();
						}else{
							phzhi.setText(weather.getResultdata().getPh().toString());
							aa.setText(weather.getResultdata().getAa().toString());
							om.setText(weather.getResultdata().getOm().toString());
							antaidan.setText(weather.getResultdata().getNh4n().toString());
							xiaotaidan.setText(weather.getResultdata().getNo3n().toString());
							lin.setText(weather.getResultdata().getPhosphor().toString());
							jia.setText(weather.getResultdata().getKalium().toString());
							gai.setText(weather.getResultdata().getCalcium().toString());
							mei.setText(weather.getResultdata().getMagnesium().toString());
							liu.setText(weather.getResultdata().getSulfur().toString());
							tie.setText(weather.getResultdata().getFerrum().toString());
							tong.setText(weather.getResultdata().getCopper().toString());
							meng.setText(weather.getResultdata().getManganese().toString());
							xin.setText(weather.getResultdata().getZinc().toString());
							peng.setText(weather.getResultdata().getBoron().toString());
						}
						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
//						Toast.makeText(getActivity(), "服务器异常！",
//								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}
}
