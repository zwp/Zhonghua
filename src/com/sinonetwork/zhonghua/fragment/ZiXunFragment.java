package com.sinonetwork.zhonghua.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.MyDengLu;
import com.sinonetwork.zhonghua.NewsActivity;
import com.sinonetwork.zhonghua.Price_information;
import com.sinonetwork.zhonghua.Qixiang;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.SeasonManager;
import com.sinonetwork.zhonghua.event.GetUserNameEvent;
import com.sinonetwork.zhonghua.model.AdvertiseImgInfo;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.view.CollapsibleTextView;

import de.greenrobot.event.EventBus;

public class ZiXunFragment extends Fragment {
	private ImageView imgxinwen, imgqixiang, imghq;
	private Bundle bundle;
	private Intent intent;
	private TextView zixun_bt1;

	private ViewPager viewPager; // android-support-v4中的滑动组件
	private List<ImageView> imageViews; // 滑动的图片集合
	private String[] titles; // 图片标题
	private int[] imageResId; // 图片ID
	private List<View> dots; // 图片标题正文的那些点
	private TextView tv_title;
	private int currentItem = 0; // 当前图片的索引号
	private ScheduledExecutorService scheduledExecutorService;

	private TextView zixun_txt1;
	private List<AdvertiseImgInfo> advImgList = new ArrayList<AdvertiseImgInfo>();
	private int cropId = -1;

	LayoutInflater inflater;
	private RelativeLayout LoginLayout;
	private RelativeLayout UserLayout;
	private TextView zixunuser_name;
	private TextView zixunuser_yumi;
	private TextView zixunuser_yue;
	private TextView zixunuser_hk;
	private TextView zixunuser_dz;
	private TextView zixunuser_wendu;
	private TextView zixunuser_fl;
	private TextView zixunuser_wr;
	private TextView zixunuser_jg;
	private TextView zixunuser_jiage;
	private CollapsibleTextView zixunuser_zy1;
	private TextView zixunchakan;
	private LinearLayout zixun_bottomrl;
	private RelativeLayout rel_first;
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
		};
	};

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View zixunLayout = inflater.inflate(R.layout.activity_zixun, null);
		loadAdvertiseImages();// 加载广告条
		setFont(zixunLayout);// 提示的字体设置。
		rel_first = (RelativeLayout) zixunLayout.findViewById(R.id.rel_first);
		if (PrefUtil.getBooleanPref(this.getActivity(), "first_zixun", false)) {
			rel_first.setVisibility(View.GONE);
		}
		// 安装后第一次进入，显示提示界面
		((Button) zixunLayout.findViewById(R.id.btn_know))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						PrefUtil.savePref(ZiXunFragment.this.getActivity(),
								"first_zixun", true);
						rel_first.setVisibility(View.GONE);
					}
				});
		rel_first.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				PrefUtil.savePref(ZiXunFragment.this.getActivity(),
						"first_zixun", true);
				rel_first.setVisibility(View.GONE);
			}
		});
		initView(zixunLayout);//初始化控件
		//登录信息模块信息的展示
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			ZHAccount currentAccount = ZhAccountPrefUtil
					.getZHAccount(getActivity().getApplicationContext());
			LoginLayout.setVisibility(View.GONE);
			UserLayout.setVisibility(View.VISIBLE);
			zixun_bt1.setVisibility(View.GONE);
			String userName = currentAccount.getUserName();
			zixunuser_name.setText(userName);
		} else {
			LoginLayout.setVisibility(View.VISIBLE);
			UserLayout.setVisibility(View.GONE);
			zixun_bt1.setVisibility(View.VISIBLE);
		}
		//登录按钮的点击事件
		zixun_bt1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MyDengLu.class);
				startActivity(intent);
			}
		});

		zixunchakan.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ZHAccount currentAccount = ZhAccountPrefUtil
						.getZHAccount(getActivity().getApplicationContext());
				Intent intent = new Intent(ZiXunFragment.this.getActivity(),
						SeasonManager.class);
				Bundle bundle = new Bundle();
				bundle.putString("id", currentAccount.getCare() + "");
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		imageResId = new int[] { R.drawable.a, R.drawable.b, R.drawable.c,
				R.drawable.d, R.drawable.e };
		titles = new String[imageResId.length];
		titles[0] = "农垦实现农业现代化";
		titles[1] = "农垦实现农业现代化";
		titles[2] = "农垦实现农业现代化";
		titles[3] = "农垦实现农业现代化";
		titles[4] = "农垦实现农业现代化";

		imageViews = new ArrayList<ImageView>();

		// 初始化图片资源
		for (int i = 0; i < imageResId.length; i++) {
			ImageView imageView = new ImageView(getActivity());
			imageView.setImageResource(imageResId[i]);
			imageView.setScaleType(ScaleType.CENTER_CROP);
			imageViews.add(imageView);
		}

		dots = new ArrayList<View>();
		dots.add(zixunLayout.findViewById(R.id.zixun_dot0));
		dots.add(zixunLayout.findViewById(R.id.zixun_dot1));
		dots.add(zixunLayout.findViewById(R.id.zixun_dot2));
		dots.add(zixunLayout.findViewById(R.id.zixun_dot3));
		dots.add(zixunLayout.findViewById(R.id.zixun_dot4));

		tv_title = (TextView) zixunLayout.findViewById(R.id.zixun_title);
		tv_title.setText(titles[0]);//

		viewPager = (ViewPager) zixunLayout.findViewById(R.id.zixun_viewpager);
		viewPager.setAdapter(new ZiXunAdapter());// 设置填充ViewPager页面的适配器
		// 设置一个监听器，当ViewPager中的页面改变时调用
		viewPager.setOnPageChangeListener(new MyPageChangeListener());
		imgxinwen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent = new Intent(getActivity(), NewsActivity.class);
				startActivity(intent);
			}
		});
		imghq.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent = new Intent(getActivity(), Price_information.class);
				startActivity(intent);
			}
		});

		imgqixiang.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				intent = new Intent(getActivity(), Qixiang.class);
				startActivity(intent);
			}
		});

		loadData();

		return zixunLayout;

	}
	//初始化控件
	private void initView(View zixunLayout) {
		imgxinwen = (ImageView) zixunLayout
				.findViewById(R.id.zixun_nyzx_icon01);
		imghq = (ImageView) zixunLayout.findViewById(R.id.zixun_nyzx_icon02);
		imgqixiang = (ImageView) zixunLayout
				.findViewById(R.id.zixun_nyzx_icon03);
		zixun_txt1 = (TextView) zixunLayout.findViewById(R.id.zixun_txt1);
		zixunuser_name = (TextView) zixunLayout
				.findViewById(R.id.zixunuser_name);
		zixun_bt1 = (TextView) zixunLayout.findViewById(R.id.zixun_bt1);
		LoginLayout = (RelativeLayout) zixunLayout
				.findViewById(R.id.zixun_centerrl);
		UserLayout = (RelativeLayout) zixunLayout
				.findViewById(R.id.zixunuser_centerrl);
		zixunuser_yumi = (TextView) zixunLayout
				.findViewById(R.id.zixunuser_yumi);
		zixunuser_yue = (TextView) zixunLayout.findViewById(R.id.zixunuser_yue);
		zixunuser_dz = (TextView) zixunLayout.findViewById(R.id.zixunuser_dz);
		zixunuser_hk = (TextView) zixunLayout.findViewById(R.id.zixunuser_hk);
		zixunuser_wendu = (TextView) zixunLayout
				.findViewById(R.id.zixunuser_wendu);
		zixunuser_fl = (TextView) zixunLayout.findViewById(R.id.zixunuser_fl);
		zixunuser_wr = (TextView) zixunLayout.findViewById(R.id.zixunuser_wr);
		zixunuser_jg = (TextView) zixunLayout.findViewById(R.id.zixunuser_jg);
		zixunuser_jiage = (TextView) zixunLayout
				.findViewById(R.id.zixunuser_jiage);
		zixunuser_zy1 = (CollapsibleTextView) zixunLayout
				.findViewById(R.id.zixunuser_zy1);
		zixunchakan = (TextView) zixunLayout.findViewById(R.id.zixunchakan);
		zixun_bottomrl = (LinearLayout) zixunLayout
				.findViewById(R.id.zixun_bottomrl);
		
	}

	// 设置字体
	private void setFont(View zixunLayout) {
		AssetManager mgr = getActivity().getAssets();// 得到AssetManager
		Typeface tf = Typeface.createFromAsset(mgr, "fonts/mini.ttf");// 根据路径得到Typeface
		// Typeface tf2=Typeface.createFromAsset(mgr,
		// "fonts/mini.ttf");//根据路径得到Typeface
		((TextView) zixunLayout.findViewById(R.id.tv_introduction_register))
				.setTypeface(tf);// 设置字体
	}

	/**
	 * 登录成功，返回首页的数据
	 */
	public void loadData() {
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {// 登录成功
			ZHAccount currentAccount = ZhAccountPrefUtil
					.getZHAccount(getActivity().getApplicationContext());
			GetUserNameEvent event = new GetUserNameEvent();
			event.account = currentAccount;
			//发送消息
			EventBus.getDefault().post(event);
			LoginLayout.setVisibility(View.GONE);
			UserLayout.setVisibility(View.VISIBLE);// 登录成功后显示的界面
			zixun_bt1.setVisibility(View.GONE);
			String userName = currentAccount.getUserName();// 用户名
			//显示用户名
			zixunuser_name.setText(userName);
			//显示地区
			if (currentAccount.getParent_areaName().equals(
					currentAccount.getAreaName())) {

				zixunuser_hk.setText(currentAccount.getParent_areaName());
			} else {

				zixunuser_hk.setText(currentAccount.getParent_areaName()
						+ currentAccount.getAreaName());

			}
			//网络Post请求所需要的参数
			RequestParams params = new RequestParams();
			params.addBodyParameter("method", "searchHomeMessage");
			if (currentAccount.getCare() == null
					|| "".equals(currentAccount.getCare())) {
			} else {
				params.addBodyParameter("cropId", currentAccount.getCare());
				System.out.println("cropId:"+currentAccount.getCare());
			}
			if (currentAccount.getParent_areaName() == null
					|| "".equals(currentAccount.getParent_areaName())) {
			} else {
				params.addBodyParameter("provinceName",
						currentAccount.getParent_areaName());
				System.out.println("provinceName"+currentAccount.getParent_areaName());
			}
			if (currentAccount.getAreaName() == null
					|| "".equals(currentAccount.getAreaName())) {
			} else {
				params.addBodyParameter("cityName",
						currentAccount.getAreaName());
				System.out.println("cityName"+currentAccount.getAreaName());
			}
			// 使用的是第三方xUtils请求数据
			HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
					params, new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {

						}

						@Override
						public void onSuccess(ResponseInfo<String> resultInfo) {
							System.out.println("访问网络登录后返回的数据："
									+ resultInfo.result);
							//设置月份
							zixunuser_yue.setText((Calendar.getInstance().get(
									Calendar.MONTH) + 1)
									+ "月");
							//设置关注作物
							JSONObject json = JSONObject
									.parseObject(resultInfo.result);
							// Log.v("zhong","json--"+json);
							if (json.getString("resultcode").equals("error")) {

							} else {
								//解析json对象
								JSONObject crop = json.getJSONObject("crop");
								JSONObject weather = json
										.getJSONObject("weather ");
								JSONObject quotation = json
										.getJSONObject("quotation");
								if (json.toString().indexOf("farmingGuide") > -1) {
									JSONArray farmGuide = json
											.getJSONArray("farmingGuide");
									ArrayList<String> guideList = new ArrayList<String>();

									if (farmGuide.size() > 0) {
										JSONObject jObject = JSONObject
												.parseObject(farmGuide.get(0)
														.toString());
										zixunuser_zy1.setDesc(
												"1."
														+ jObject
																.getString("context"),
												BufferType.NORMAL);
									}
								} else {
									zixun_bottomrl.setTop(60);
									if (crop != null) {
										if (!crop.getString("cropDescription")
												.equals("")) {
											zixunuser_zy1.setDesc(
													crop.getString("cropDescription"),
													BufferType.NORMAL);
										} else {
											zixun_bottomrl.setTop(60);
										}
									}
								}
								//获取作物相关的信息，并保存
								if (crop == null) {
									PrefUtil.savePref(getActivity(),
											"zhonghua_crop", "");// 把作物保存到本地
								} else {
									zixunuser_yumi.setText(crop
											.getString("cropName"));
									zixunuser_jg.setText(crop
											.getString("cropName") + "市场价格：");
									PrefUtil.savePref(getActivity(),
											"zhonghua_crop",
											crop.getString("cropName"));// 把作物保存到本地
								}

								if (weather == null) {
								} else {
									zixunuser_dz.setText(weather
											.getString("districtcn") + ": ");
									zixunuser_wendu.setText(weather
											.getString("temperature") + "度  ");
									zixunuser_fl.setText("风力："
											+ weather.getString("windPower")
											+ "级  ");
									zixunuser_wr.setText("污染指数:"
											+ weather.getString("humidity"));
								}
								if (quotation == null) {
								} else {
									// zixunuser_hk.setText(quotation.getString("d1"));

									zixunuser_jiage.setText(quotation
											.getString("d4") + "元/公斤");
								}
							}

						}
					});
		}
	}

	@Override
	public void onStart() {
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		// 当Activity显示出来后，每两秒钟切换一次图片显示
		// scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2,
		// TimeUnit.SECONDS);
		scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 5,
				TimeUnit.SECONDS);

		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			ZHAccount currentAccount = ZhAccountPrefUtil
					.getZHAccount(getActivity().getApplicationContext());
			LoginLayout.setVisibility(View.GONE);
			UserLayout.setVisibility(View.VISIBLE);
			zixun_bt1.setVisibility(View.GONE);
			String userName = currentAccount.getUserName();
			zixunuser_name.setText(userName);
			// loadData();
		} else {
			LoginLayout.setVisibility(View.VISIBLE);
			UserLayout.setVisibility(View.GONE);
			zixun_bt1.setVisibility(View.VISIBLE);
		}

		super.onStart();
	}

	@Override
	public void onStop() {
		// 当Activity不可见的时候停止切换
		scheduledExecutorService.shutdown();
		super.onStop();
	}

	/**
	 * 网络获取广告的图片，并替换掉默认的图片
	 */
	public void loadAdvertiseImages() {
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.send(HttpMethod.GET, URLAddress.URLER
				+ "?method=searchAdvertisements",
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@SuppressLint("ShowToast")
					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JSONObject jsonObject = JSONObject
								.parseObject(responseInfo.result);
						JSONObject json = JSONObject.parseObject(jsonObject
								.toString());
						List<AdvertiseImgInfo> advImgList = JSONArray
								.parseArray(json.getJSONArray("resultdata")
										.toJSONString(), AdvertiseImgInfo.class);
						titles = new String[advImgList.size()];
						imageViews = new ArrayList<ImageView>();
						// 初始化图片资源
						for (int i = 0; i < advImgList.size(); i++) {
							ImageView imageView = new ImageView(getActivity());
							String imageUrl = advImgList.get(i).getPictures();
							if (!"".equals(imageUrl) && null != imageUrl) {
								new BitmapUtils(getActivity()).display(
										imageView, URLAddress.TPURLER
												+ imageUrl);
							}
							titles[i] = advImgList.get(i).getTitle();
							imageView.setScaleType(ScaleType.CENTER_CROP);
							imageViews.add(imageView);
						}

					}
				});
	}

	private class ScrollTask implements Runnable {

		public void run() {
			synchronized (viewPager) {
				currentItem = (currentItem + 1) % imageViews.size();
				handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
			}
		}

	}

	private class MyPageChangeListener implements OnPageChangeListener {
		private int oldPosition = 0;

		/**
		 * This method will be invoked when a new page becomes selected.
		 * position: Position index of the new selected page.
		 */
		public void onPageSelected(int position) {
			currentItem = position;
			tv_title.setText(titles[position]);
			dots.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
			dots.get(position).setBackgroundResource(R.drawable.dot_focused);
			oldPosition = position;
		}

		public void onPageScrollStateChanged(int arg0) {

		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}
	}

	/**
	 * 填充ViewPager页面的适配器
	 * 
	 * @author Administrator
	 * 
	 */
	private class ZiXunAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageResId.length;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imageViews.get(arg1));
			return imageViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	// 设置用户信息模块
	public void setHomeView() {
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {// 判断用户是否登陆过
			ZHAccount currentAccount = ZhAccountPrefUtil
					.getZHAccount(getActivity().getApplicationContext());// 将信息保存到实体bean中
			LoginLayout.setVisibility(View.GONE);// 登录按钮
			UserLayout.setVisibility(View.VISIBLE);// 用户登陆后信息展示的布局
			zixun_bt1.setVisibility(View.GONE);// 用户登录之前信息展示的布局
			String userName = currentAccount.getUserName();
			zixunuser_name.setText(userName);
			loadData();
		} else {// 未登陆过
			LoginLayout.setVisibility(View.VISIBLE);
			UserLayout.setVisibility(View.GONE);
			zixun_bt1.setVisibility(View.VISIBLE);
		}
	}
}
