package com.sinonetwork.zhonghua.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.BasicMapActivity;
import com.sinonetwork.zhonghua.Comprehensive_list;
import com.sinonetwork.zhonghua.CropDoctorActivity;
import com.sinonetwork.zhonghua.Expariment;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.Seed_Selection;
import com.sinonetwork.zhonghua.VideoListActivity;
import com.sinonetwork.zhonghua.model.AdvertiseImgInfo;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class SolutionFragment extends Fragment {
	private RelativeLayout solutionrl1, solutionrl2, solutionrl3, solutionrl4, solutionrl5, solutionrl6, solutionrl7;
	private Intent intent;

	private ViewPager viewPager; // android-support-v4中的滑动组件
	private List<ImageView> imageViews; // 滑动的图片集合
	private String[] titles; // 图片标题
	private int[] imageResId; // 图片ID
	private List<View> dots; // 图片标题正文的那些点
	private TextView tv_title;
	private int currentItem = 0; // 当前图片的索引号
	private ScheduledExecutorService scheduledExecutorService;
	private List<AdvertiseImgInfo> advImgList = new ArrayList<AdvertiseImgInfo>();

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
		};
	};

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View solutionLayout = inflater.inflate(R.layout.activity_solution, null);
		loadAdvertiseImages();
		solutionrl1 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl1);
		solutionrl2 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl2);
		solutionrl3 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl3);
		solutionrl4 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl4);
		solutionrl5 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl5);
		solutionrl6 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl6);
		solutionrl7 = (RelativeLayout) solutionLayout.findViewById(R.id.solutionrl7);

		solutionrl1.setOnClickListener(new SoilListener());
		solutionrl2.setOnClickListener(new SoilListener());
		solutionrl3.setOnClickListener(new SoilListener());
		solutionrl4.setOnClickListener(new SoilListener());
		solutionrl5.setOnClickListener(new SoilListener());
		solutionrl6.setOnClickListener(new SoilListener());
		solutionrl7.setOnClickListener(new SoilListener());

		imageResId = new int[] { R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.e };
		titles = new String[imageResId.length];
		titles[0] = "农垦实现农业现代化";
		titles[1] = "农垦实现农业现代化";
		titles[2] = "农垦实现农业现代化";
		titles[3] = "农垦实现农业现代化";
		titles[4] = "农垦实现农业现代化";

		imageViews = new ArrayList<ImageView>();

		// 初始化图片资源
		for (int i = 0; i < imageResId.length; i++) {
			ImageView imageView = new ImageView(getActivity());
			imageView.setImageResource(imageResId[i]);
			imageView.setScaleType(ScaleType.CENTER_CROP);
			imageViews.add(imageView);
		}

		dots = new ArrayList<View>();
		dots.add(solutionLayout.findViewById(R.id.solution_dot0));
		dots.add(solutionLayout.findViewById(R.id.solution_dot1));
		dots.add(solutionLayout.findViewById(R.id.solution_dot2));
		dots.add(solutionLayout.findViewById(R.id.solution_dot3));
		dots.add(solutionLayout.findViewById(R.id.solution_dot4));

		tv_title = (TextView) solutionLayout.findViewById(R.id.solution_titletop);
		tv_title.setText(titles[0]);//

		viewPager = (ViewPager) solutionLayout.findViewById(R.id.solution_viewpager);
		viewPager.setAdapter(new MyAdapter());// 设置填充ViewPager页面的适配器
		// 设置一个监听器，当ViewPager中的页面改变时调用
		viewPager.setOnPageChangeListener(new MyPageChangeListener());

		return solutionLayout;
	}

	@Override
	public void onStart() {
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		// 当Activity显示出来后，每两秒钟切换一次图片显示
		scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 5, TimeUnit.SECONDS);
		super.onStart();
	}

	@Override
	public void onStop() {
		// 当Activity不可见的时候停止切换
		scheduledExecutorService.shutdown();
		super.onStop();
	}

	/**
	 * 网络获取广告的图片，并替换掉默认的图片
	 */
	public void loadAdvertiseImages() {
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.send(HttpMethod.GET, URLAddress.URLER + "?method=searchAdvertisements", new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {

			}

			@SuppressLint("ShowToast")
			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				JSONObject jsonObject = JSONObject.parseObject(responseInfo.result);
				JSONArray resultdata = jsonObject.getJSONArray("resultdata");
				for (int i = 0; i < resultdata.size(); i++) {
					JSONObject jObj = resultdata.getJSONObject(i);
					AdvertiseImgInfo advImgInfo = new AdvertiseImgInfo();
					advImgInfo.setUrl(jObj.getString("pictures"));
					advImgInfo.setTitle(jObj.getString("title"));
					advImgList.add(advImgInfo);

				}
				titles = new String[advImgList.size()];
				imageViews = new ArrayList<ImageView>();
				// 初始化图片资源
				for (int i = 0; i < advImgList.size(); i++) {
					ImageView imageView = new ImageView(getActivity());

					new BitmapUtils(getActivity()).display(imageView, URLAddress.TPURLER + advImgList.get(i).getUrl());
					titles[i] = advImgList.get(i).getTitle();
					imageView.setScaleType(ScaleType.CENTER_CROP);
					imageViews.add(imageView);
				}

			}
		});
	}

	private class ScrollTask implements Runnable {

		public void run() {
			synchronized (viewPager) {
				System.out.println("currentItem: " + currentItem);
				currentItem = (currentItem + 1) % imageViews.size();
				handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
			}
		}

	}

	private class MyPageChangeListener implements OnPageChangeListener {
		private int oldPosition = 0;

		/**
		 * This method will be invoked when a new page becomes selected.
		 * position: Position index of the new selected page.
		 */
		public void onPageSelected(int position) {
			currentItem = position;
			tv_title.setText(titles[position]);
			dots.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
			dots.get(position).setBackgroundResource(R.drawable.dot_focused);
			oldPosition = position;
		}

		public void onPageScrollStateChanged(int arg0) {

		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}
	}

	/**
	 * 填充ViewPager页面的适配器
	 * 
	 * @author Administrator
	 * 
	 */
	private class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageResId.length;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imageViews.get(arg1));
			return imageViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	public class SoilListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.solutionrl1:
				intent = new Intent(getActivity(), BasicMapActivity.class);
				startActivity(intent);
				break;

			case R.id.solutionrl2:
				intent = new Intent(getActivity(), Seed_Selection.class);
				startActivity(intent);
				break;

			case R.id.solutionrl3:
				// intent = new Intent(getActivity(), Plant_Nutrition.class);
				// startActivity(intent);
				Toast.makeText(getActivity(), "敬请期待", Toast.LENGTH_SHORT).show();
				break;
			case R.id.solutionrl4:
				// intent = new Intent(getActivity(), HomeActivity.class);
				intent = new Intent(getActivity(), CropDoctorActivity.class); // 作物医生
				// intent = new Intent(getActivity(),
				// IphoneExpandableListActivity.class);
				startActivity(intent);
				// Toast.makeText(getActivity(), "第四个", 0).show();
				break;
			// case R.id.solutionrl5:
			// intent = new Intent(getActivity(), Synthesize.class);
			// startActivity(intent);
			// break;
			case R.id.solutionrl5:
				// intent = new Intent(getActivity(), SeasonManager.class);
				// intent = new Intent(getActivity(), ShiLing.class);
				intent = new Intent(getActivity(), Comprehensive_list.class);
				startActivity(intent);
				break;
			case R.id.solutionrl6:
				intent = new Intent(getActivity(), Expariment.class); // 跳转试验示范
				startActivity(intent);
				break;
			case R.id.solutionrl7:

				intent = new Intent(getActivity(), VideoListActivity.class);
				startActivity(intent);
				break;

			default:
				break;
			}

		}
	}
}
