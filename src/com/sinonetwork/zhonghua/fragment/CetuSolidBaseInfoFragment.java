package com.sinonetwork.zhonghua.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.TestSoilInfoById;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class CetuSolidBaseInfoFragment extends Fragment {

	private String id;
	// private String agrotype;
	// private String character;
	// private String irrCondition;
	// private String sampleDepth;
	// private String cropId1;
	// private String cropId2;
	// private String targetYield;
	// private String yieldLevel;
	// private String keyWord;

	private TextView turangleixing;
	private TextView zhidi;
	private TextView guangaitiaojian;
	private TextView quyangshendu;
	private TextView dangjizuowu;
	private TextView qianchazuowu;
	private TextView mubiaochanliang;
	private TextView chanliangshuiping;
	private TextView huafeiguanjianzi;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.cetu_solid_base_info, null);
		turangleixing = (TextView) view.findViewById(R.id.turangleixing);
		zhidi = (TextView) view.findViewById(R.id.zhidi);
		guangaitiaojian = (TextView) view.findViewById(R.id.guangaitiaojian);
		quyangshendu = (TextView) view.findViewById(R.id.quyangshendu);
		dangjizuowu = (TextView) view.findViewById(R.id.dangjizuowu);
		qianchazuowu = (TextView) view.findViewById(R.id.qianchazuowu);
		mubiaochanliang = (TextView) view.findViewById(R.id.mubiaochanliang);
		chanliangshuiping = (TextView) view
				.findViewById(R.id.chanliangshuiping);
		huafeiguanjianzi = (TextView) view.findViewById(R.id.huafeiguanjianzi);
		// Bundle bundle = getActivity().getIntent().getExtras();
		// id = bundle.getString("id");
		SharedPreferences sharedPreferences = getActivity()
				.getSharedPreferences("henduoshuju", getActivity().MODE_PRIVATE);
		id = sharedPreferences.getString("yyid", "获取id失败");
		loadData("getTestSoilInfoById", id);

		return view;
	}

	public void loadData(String method, String yyid) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", yyid);
		Logger.e("方法名====="+method+" ， yyid====="+yyid);
		FastJsonRequest<TestSoilInfoById> fastJson = new FastJsonRequest<TestSoilInfoById>(
				Method.POST, URLAddress.queryMyOrderTestSoilURL,
				TestSoilInfoById.class, null, map,
				new Response.Listener<TestSoilInfoById>() {
					@Override
					public void onResponse(TestSoilInfoById weather) {
						// 赋值
						// orderCode =
						// weather.getResultdata().getOrderCode().toString();
						// Toast.makeText(CetuDetail.this,
						// "orderCode--------"+String.valueOf(orderCode),
						// Toast.LENGTH_SHORT).show();

						// sampleDepth =
						// weather.getResultdata().getOrderCode().toString();
						// agrotype =
						// weather.getResultdata().getOrderCode().toString();
						// character =
						// weather.getResultdata().getOrderCode().toString();
						// cropId1 =
						// weather.getResultdata().getOrderCode().toString();
						// targetYield =
						// weather.getResultdata().getOrderCode().toString();
						// irrCondition =
						// weather.getResultdata().getOrderCode().toString();
						// cropId2 =
						// weather.getResultdata().getOrderCode().toString();
						// yieldLevel =
						// weather.getResultdata().getOrderCode().toString();
						// keyWord =
						// weather.getResultdata().getOrderCode().toString();
						// 土壤类型，质地，灌溉条件，取样深度，当季作物，前茬作物，目标产量，产量水平，化肥关键字
						if (weather.getResultdata() == null) {
							Toast.makeText(getActivity(), "没有信息",
									Toast.LENGTH_SHORT).show();
						} else {
							turangleixing.setText(weather.getResultdata()
									.getAgrotype().toString());
							zhidi.setText(weather.getResultdata()
									.getCharacter().toString());
							guangaitiaojian.setText(weather.getResultdata()
									.getIrrCondition().toString());
							quyangshendu.setText(weather.getResultdata()
									.getSampleDepth().toString());
							dangjizuowu.setText(weather.getResultdata()
									.getCropId1().toString());
							qianchazuowu.setText(weather.getResultdata()
									.getCropId2().toString());
							mubiaochanliang.setText(weather.getResultdata()
									.getTargetYield().toString());
							chanliangshuiping.setText(weather.getResultdata()
									.getYieldLevel().toString());
							huafeiguanjianzi.setText(weather.getResultdata()
									.getKeyWord().toString());
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
//						Toast.makeText(getActivity(), "服务器异常！",
//								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}
}
