package com.sinonetwork.zhonghua.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Contacts;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.AbortNongyibaoActivity;
import com.sinonetwork.zhonghua.DaJia;
import com.sinonetwork.zhonghua.EditUserActivity2;
import com.sinonetwork.zhonghua.ErWeiMaActivity;
import com.sinonetwork.zhonghua.MessageActivity;
import com.sinonetwork.zhonghua.MyAttentionCrop;
import com.sinonetwork.zhonghua.MyDengLu;
import com.sinonetwork.zhonghua.MyPlantCrops;
import com.sinonetwork.zhonghua.MyQuestion;
import com.sinonetwork.zhonghua.MyReply;
import com.sinonetwork.zhonghua.MySolid;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.UpdateVersionActivity;
import com.sinonetwork.zhonghua.WebViewActivity;
import com.sinonetwork.zhonghua.event.GetUserNameEvent;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.zbar.lib.CaptureActivity;

import de.greenrobot.event.EventBus;

public class MyFragment extends Fragment {
	//查找各个条目的图标，也就是说各个条目的图标是固定的。
	private int[] resId = new int[] { R.drawable.wd_icon00,
			R.drawable.wd_icon01, R.drawable.wd_icon02, R.drawable.wd_icon03,
			R.drawable.wd_icon05, R.drawable.wd_icon07, R.drawable.wd_icon08,
			R.drawable.wd_icon04ma, R.drawable.zhihuidou, R.drawable.call_tel,
			R.drawable.message, R.drawable.user_edit, R.drawable.about,
			R.drawable.update };
	//登录过图标的描述
	private String[] name;
	//产品列表
	private GridView gridView;

	private Button logoutButton;//标题栏中退出按钮。
	private TextView my_txt1;//个人信息的描述文字

//	private TextView my_diqu;
	
	final Handler handler = new Handler();
	boolean success = true;

	private WebView logoutWebView;

	private SimpleAdapter simpleAdapter;//适配器
	private RelativeLayout rel_first;//提示信息，第一次启动界面时，出现，关键在于存储在什么文件中，重新安装后，置为空。
	private View myLayout;
	private String[] unName;//未登录过得图标描述信息

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		EventBus.getDefault().register(this);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		myLayout = inflater
				.inflate(R.layout.activity_my, container, false);
		//提示信息中字体的设置
		setFont(myLayout);//设置字体
		//提示信息，关键在于存储在什么地方，存储在本地
		rel_first = (RelativeLayout) myLayout.findViewById(R.id.rel_first);
		setNotice(rel_first);//设置提示信息
		logoutWebView = (WebView) myLayout.findViewById(R.id.logoutWebView);//登录
		gridView = (GridView) myLayout.findViewById(R.id.my_gv);//列表展示
		my_txt1 = (TextView) myLayout.findViewById(R.id.my_txt1);//个人信息
		// 登出
		logoutButton = (Button) myLayout.findViewById(R.id.logout_button);
		logoutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Logout();//退出登录

			}
		});
		setUserName();//显示数据
		//条目点击事件
		gridView.setOnItemClickListener(new MyOnItemClickListener());
		return myLayout;
	}
	//待返回结果的Activity，明天需要理解的重点
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			String result = data.getStringExtra("result");
			if (PrefUtil.getBooleanPref(getActivity(),
					ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
				ZHAccount account = ZhAccountPrefUtil
						.getZHAccount(getActivity());
				result = URLAddress.SHOP_URL
						+ "fert_bbc/weixin_login.htm?username="
						+ account.getUserName() + "&password="
						+ account.getPassword() + "&sequence="
						+ account.getSequence() + "&mark=qrcode&url=" + result;

				 
			} else {
				result = URLAddress.SHOP_URL + "fert_bbc" + result;
			}
			Intent intent = new Intent(getActivity(), WebViewActivity.class);
			intent.putExtra("url", result);
			startActivity(intent);
		}
	}

	// 退出登录,加载html。WebView与Html的交互
	protected void Logout() {
		String url = URLAddress.SHOP_URL
				+ "fert_bbc/iskyshop_logout.htm?iskyshop_view_type=weixin";//登录的网址
		logoutWebView.getSettings().setJavaScriptEnabled(true);
		logoutWebView.loadUrl(url);
		logoutWebView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				PrefUtil.savePref(getActivity(),
						ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false);//设置为未登录过

				ZhAccountPrefUtil.cleanZHAccount(getActivity());//清除账号信息
				logoutButton.setVisibility(View.GONE);
				setUserName();
			}

			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
		});

	}

	public void onEvent(GetUserNameEvent event) {
		setUserName();//显示数据

	}
	/**
	 * 根据是否登录两种情况做分类显示数据
	 */
	public void setUserName() {
		//登录成功的情况
		if (PrefUtil.getBooleanPref(getActivity().getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {//登录
			logoutButton.setVisibility(View.VISIBLE);
			ZHAccount currentAccount = ZhAccountPrefUtil
					.getZHAccount(getActivity().getApplicationContext());
			String crop = PrefUtil
					.getStringPref(getActivity(), "zhonghua_crop");
			//获取当前用户的用户名
			my_txt1.setText("你好" + currentAccount.getUserName() + "欢迎来到中化农易宝");
//			if (currentAccount.getParent_areaName().equals(
//					currentAccount.getAreaName())) {
//
//				// my_diqu.setText(currentAccount.getParent_areaName());
//			} else {
//
//				// my_diqu.setText(currentAccount.getParent_areaName() +
//				// currentAccount.getAreaName());
//
//			}
//			// my_zhonglei.setText(crop);
			if(name==null){
			name = new String[] {
					"扫描二维码",
					"我的问题",
					"我的回复",
					"打假",
					"我的测土",
					"种植作物",
					"关注作物",
					"分享二维码",
					"我的智慧豆："
							+ ZhAccountPrefUtil.getZHAccount(getActivity())//获取智慧豆的个数的方法
									.getUser_integ() + "个", "拨打电话", "发送短信",
					"个人信息修改", "关于农易宝", "检查更新" };}
			ArrayList<HashMap<String, Object>> item = new ArrayList<HashMap<String, Object>>();
			//以键值对的形式存储数据
			for (int i = 0; i < resId.length; i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("itemImage", resId[i]);
				map.put("itemName", name[i]);
				item.add(map);
			}

			simpleAdapter = new SimpleAdapter(getActivity(), item,
					R.layout.activity_my_item, new String[] { "itemImage",
							"itemName" }, new int[] { R.id.my_item_img1,
							R.id.my_item_txt1 });
			gridView.setAdapter(simpleAdapter);

		} else {//没有登录的情况
			logoutButton.setVisibility(View.GONE);
			my_txt1.setText("你好欢迎来到中化农易宝");
			if(unName==null){
			unName = new String[] { "扫描二维码", "我的问题", "我的回复", "打假", "我的测土",
					"种植作物", "关注作物", "分享二维码", "我的智慧豆", "拨打电话", "发送短信", "个人信息修改",
					"关于农易宝", "检查更新" };}
			//以键值对的形式存储数据
			ArrayList<HashMap<String, Object>> item = new ArrayList<HashMap<String, Object>>();
			for (int i = 0; i < resId.length; i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("itemImage", resId[i]);
				map.put("itemName", unName[i]);
				item.add(map);
			}

			simpleAdapter = new SimpleAdapter(getActivity(), item,
					R.layout.activity_my_item, new String[] { "itemImage",
							"itemName" }, new int[] { R.id.my_item_img1,
							R.id.my_item_txt1 });
			gridView.setAdapter(simpleAdapter);

		}
	}
	/**
	 * 设置字体
	 */
	private void setFont(View myLayout) {
		AssetManager mgr = getActivity().getAssets();// 得到AssetManager
		Typeface tf = Typeface.createFromAsset(mgr, "fonts/mini.ttf");// 根据路径得到Typeface
		((TextView) myLayout.findViewById(R.id.tv_1)).setTypeface(tf);// 设置字体
		((TextView) myLayout.findViewById(R.id.tv_2)).setTypeface(tf);// 设置字体
		((TextView) myLayout.findViewById(R.id.tv_3)).setTypeface(tf);// 设置字体
	}
	/**
	 * 设置提示信息
	 */
	private void setNotice(final RelativeLayout rel_first) {
		if (PrefUtil.getBooleanPref(this.getActivity(), "first_my", false)) {
			rel_first.setVisibility(View.GONE);
		}
		((Button) myLayout.findViewById(R.id.btn_know))//提示信息中的按钮
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						PrefUtil.savePref(MyFragment.this.getActivity(),
								"first_my", true);
						rel_first.setVisibility(View.GONE);
					}
				});
		//点击任意地方，提示信息都消失
		rel_first.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PrefUtil.savePref(MyFragment.this.getActivity(),
						"first_my", true);
				rel_first.setVisibility(View.GONE);				
			}
		});
	}
	//条目点击事件
	class MyOnItemClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			if (PrefUtil.getBooleanPref(getActivity()
					.getApplicationContext(),
					ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
				if (position == 0) {
					Intent intent = new Intent(getActivity(),
							CaptureActivity.class);
					startActivityForResult(intent, 0);
				} else if (position == 1) {
					Intent intent = new Intent(getActivity(),
							MyQuestion.class);
					startActivity(intent);
				} else if (position == 2) {
					Intent intent = new Intent(getActivity(), MyReply.class);
					startActivity(intent);
				} else if (position == 3) {
					Intent intent = new Intent(getActivity(), DaJia.class);
					startActivity(intent);
				} else if (position == 4) {
					Intent intent = new Intent(getActivity(), MySolid.class);
					startActivity(intent);
				} else if (position == 5) {
					Intent intent = new Intent(getActivity(),
							MyPlantCrops.class);
					startActivity(intent);
				} else if (position == 6) {
					Intent intent = new Intent(getActivity(),
							MyAttentionCrop.class);
					startActivity(intent);
				} else if (position == 7) {
					Intent intent = new Intent(getActivity(),
							ErWeiMaActivity.class);
					startActivity(intent);
				} else if (position == 11) {// 11----8
					Intent intent = new Intent(getActivity(),
							EditUserActivity2.class);
					startActivity(intent);
				} else if (position == 12) {// 12----9
					Intent intent = new Intent(getActivity(),
							AbortNongyibaoActivity.class);
					startActivity(intent);
				} else if (position == 13) {// 13----10
					Intent intent = new Intent(getActivity(),
							UpdateVersionActivity.class);
					startActivity(intent);
				} else if (position == 9) {// 9-----12
					Intent intent = new Intent();
					intent.setClassName("com.android.contacts",
							"com.android.contacts.activities.PeopleActivity");
					startActivity(intent);
				} else if (position == 10) {// 10----13
					Intent intent = new Intent(getActivity(),
							MessageActivity.class);
					startActivity(intent);
				}
			} else {
			
			 
				if (position == 0) {
					Intent intent = new Intent(getActivity(),
							CaptureActivity.class);
					startActivityForResult(intent, 0);
					// Log.i("gxx", ""+intent.getExtras());
				} else if (position == 3) {
					Intent intent = new Intent(getActivity(), DaJia.class);
					startActivity(intent);
				} else if (position == 7) {
					Intent intent = new Intent(getActivity(),
							ErWeiMaActivity.class);
					startActivity(intent);
				} else if (position == 12) {// 12----9
					Intent intent = new Intent(getActivity(),
							AbortNongyibaoActivity.class);
					startActivity(intent);
				} else if (position == 13) {// 13----10
					Intent intent = new Intent(getActivity(),
							UpdateVersionActivity.class);
					startActivity(intent);
				} else if (position == 9) {// 9----12
					Intent intent = new Intent();
					intent.setClassName("com.android.contacts",
							"com.android.contacts.activities.PeopleActivity");
					startActivity(intent);
				} else if (position == 10) {// 10---13
					Intent intent = new Intent(getActivity(),
							MessageActivity.class);
					startActivity(intent);
				} else {
					Intent intent = new Intent(getActivity(),
							MyDengLu.class);
					startActivity(intent);
				}
			}
			
		}
		
	}
	
}
