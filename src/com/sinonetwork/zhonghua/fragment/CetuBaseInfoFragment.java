package com.sinonetwork.zhonghua.fragment;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.TestSoilInfoById;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class CetuBaseInfoFragment extends Fragment {

	private String id;
//	private String sendPerson;
//	private String orderCode;
//	private String labNo;
//	private String samplesNo;
//	private String samplingDateStr;
//	private String detFinishDateStr;

	private TextView songyangren;
	private TextView quyangshijian;
	private TextView wanchengshijian;
	private TextView huodanhao;
	private TextView shiyanshibianhao;
	private TextView yangpinbianhao;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.cetu_base_info, null);
		songyangren = (TextView) view.findViewById(R.id.songyangren);
		quyangshijian = (TextView) view.findViewById(R.id.quyangshijian);
		wanchengshijian = (TextView) view.findViewById(R.id.wanchengshijian);
		huodanhao = (TextView) view.findViewById(R.id.huodanhao);
		shiyanshibianhao = (TextView) view.findViewById(R.id.shiyanshibianhao);
		yangpinbianhao = (TextView) view.findViewById(R.id.yangpinbianhao);

//		Bundle bundle = getActivity().getIntent().getExtras();
//		id = bundle.getString("id");
		
		SharedPreferences sharedPreferences= getActivity().getSharedPreferences("henduoshuju", getActivity().MODE_PRIVATE); 
		id =sharedPreferences.getString("yyid", "获取id失败"); 

//		Toast.makeText(getActivity(), "他的id是===" + id, Toast.LENGTH_SHORT)
//				.show();
		loadData("getTestSoilInfoById", id);

//		Toast.makeText(getActivity(),"id======sss"+ id, Toast.LENGTH_SHORT).show();
		return view;
	}

	public void loadData(String method, String yyid) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", yyid);
		FastJsonRequest<TestSoilInfoById> fastJson = new FastJsonRequest<TestSoilInfoById>(
				Method.POST, URLAddress.queryMyOrderTestSoilURL,
				TestSoilInfoById.class, null, map,
				new Response.Listener<TestSoilInfoById>() {
					@Override
					public void onResponse(TestSoilInfoById weather) {
						// 赋值
						// orderCode =
						// weather.getResultdata().getOrderCode().toString();
						// Toast.makeText(CetuDetail.this,
						// "orderCode--------"+String.valueOf(orderCode),
						// Toast.LENGTH_SHORT).show();
						if(weather.getResultdata()==null){
							Toast.makeText(getActivity(), "没有信息", Toast.LENGTH_SHORT).show();
						}else{
							songyangren.setText(weather.getResultdata().getSendPerson().toString());
							quyangshijian.setText(weather.getResultdata().getSamplingDateStr().toString());
							wanchengshijian.setText(weather.getResultdata().getDetFinishDateStr().toString());
							huodanhao.setText(weather.getResultdata().getOrderCode().toString());
							shiyanshibianhao.setText(weather.getResultdata().getLabNo().toString());
							yangpinbianhao.setText(weather.getResultdata().getSamplesNo().toString());

					}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "专家正在努力帮您测土，请耐心等待",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}
}
