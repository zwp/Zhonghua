package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.Seed_SelectionDeAdapter;
import com.sinonetwork.zhonghua.model.SeedInfoList;
import com.sinonetwork.zhonghua.model.SeedInfoList.SeedInfoLists;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;

public class Seed_SelectionDe extends Activity {
	private MyListView listView;
	private Seed_SelectionDeAdapter adapter;
	private ImageView back;
	private List<SeedInfoLists> data = new ArrayList<SeedInfoLists>();
	private String id;
	private EditText chaxun_zhongzi;
	private ImageView seedde_seach;
	
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;
	private static final int NO_MORE_DARA = 12;
	private int currentPage = 1;
	private int totalPage=10;

	// 刷新加载功能
			private Handler mHandler = new Handler() {

				public void handleMessage(android.os.Message msg) {
					switch (msg.what) {
					case REFRESH_DATA_FINISH:
						if (adapter != null) {
							// adapter = new QuestionAdapter(questions,
							// KnowFragment.this.getActivity());
							// adapter.setBigEventAdapterData(questions);
							// listView.setAdapter(adapter);
							adapter = new Seed_SelectionDeAdapter(
									Seed_SelectionDe.this, data);
							adapter.setData(data);
							listView.setAdapter(adapter);
							adapter.notifyDataSetChanged();
						}
						listView.onRefreshComplete(); // 下拉刷新完成
						break;
					case LOAD_DATA_FINISH:
						if (adapter != null) {
							adapter.setData((ArrayList<SeedInfoLists>) msg.obj);
							adapter.notifyDataSetChanged();
						}
						listView.misHaveNewDatas = false;
						listView.onLoadMoreComplete(); // 加载更多完成
						break;
					case NO_MORE_DARA:
						listView.misHaveNewDatas = false;
						listView.onLoadMoreComplete(); // 加载更多完成
						break;
					default:
						break;
					}
				};
			};
			
			// 刷新加载功能
			public void loadData(final int type) {
				new Thread() {
					@Override
					public void run() {
						switch (type) {
						case 0:
							data.clear();
							currentPage = 1;
							loadData("getSeedInfoListById", id,chaxun_zhongzi.getText().toString(),String.valueOf(totalPage),String.valueOf(currentPage),0);
							break;

						case 1:
							currentPage++;
							if (currentPage <= totalPage) {
								loadData("getSeedInfoListById", id,chaxun_zhongzi.getText().toString(),String.valueOf(totalPage),String.valueOf(currentPage),1);
							} else {
								currentPage--;
									Message _Msg = mHandler.obtainMessage(NO_MORE_DARA);
									mHandler.sendMessage(_Msg);
								
							}

							break;
						}

					}
				}.start();

			}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.seed_selectionde);
		//默认情况下软件盘不自动弹出
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		listView =  (MyListView) findViewById(R.id.seedde_lv);
		back = (ImageView) findViewById(R.id.back);
		chaxun_zhongzi = (EditText) findViewById(R.id.chaxun_zhongzi);
		seedde_seach = (ImageView) findViewById(R.id.seedde_seach);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		Bundle bundle = getIntent().getExtras();
		id = bundle.getString("id");
//		Logger.e("啊啊啊啊啊啊啊啊啊啊===="+id);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// for (int i = 0; i < title.length; i++) {
				// if (position == i) {
				// Intent intent = new
				// Intent(Seed_SelectionDe.this,SeedDetail.class);
				// startActivity(intent);
				// }
				// }
				Intent intent = new Intent(Seed_SelectionDe.this,
						SeedDetail.class);
				Bundle bundle = new Bundle();
				bundle.putString("id", data.get(position-1).getId().toString());
				intent.putExtras(bundle);
				startActivity(intent);

			}
		});
		seedde_seach.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				data = new ArrayList<SeedInfoList.SeedInfoLists>();
				loadData("getSeedInfoListById", id,chaxun_zhongzi.getText().toString(),String.valueOf(totalPage),String.valueOf(currentPage),0);
				adapter = new Seed_SelectionDeAdapter(Seed_SelectionDe.this, data);
				// Logger.e("data" + data.size() + adapter.getCount());
				adapter.setData(data);
				listView.setAdapter(adapter);
			}
		});
		loadData("getSeedInfoListById", id,chaxun_zhongzi.getText().toString(),String.valueOf(totalPage),String.valueOf(currentPage),0);
		adapter = new Seed_SelectionDeAdapter(Seed_SelectionDe.this, data);
		// Logger.e("data" + data.size() + adapter.getCount());
		adapter.setData(data);
		listView.setAdapter(adapter);
		
		// 刷新加载功能
//		listView.setOnRefreshListener(new OnRefreshListener() {
//
//					@Override
//					public void onRefresh() {
//						// TODO 下拉刷新
//						loadData(0);
//					}
//				});

				// 刷新加载功能
		listView.setOnLoadListener(new OnLoadMoreListener() {

					@Override
					public void onLoadMore() {
						// TODO 加载更多
						loadData(1);
					}
				});
				// 添加加载更多 footer view
//				loadData(0);
	}

	public void loadData(String method, String id,String seedName ,String pageRows,String curPage, final int type) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		map.put("seedName", seedName);
		map.put("pageRows", pageRows);
		map.put("curPage", curPage);
//		Logger.e("id==="+id+"，seedName==="+s);
		FastJsonRequest<SeedInfoList> fastJson = new FastJsonRequest<SeedInfoList>(
				Method.POST, URLAddress.URLER, SeedInfoList.class,
				null, map, new Response.Listener<SeedInfoList>() {
					@Override
					public void onResponse(SeedInfoList weather) {
						currentPage = Integer.valueOf(weather.getCurPage());
						// 赋值
						
						if (weather.getResultdata()==null||"".equals(weather)) {
							Toast.makeText(Seed_SelectionDe.this, "没有数据！",
									Toast.LENGTH_SHORT).show();
						}else{
							data.addAll(weather.getResultdata());
						}
						// 刷新加载功能
						if (type == 0) { // 下拉刷新
							// Collections.reverse(mList); //逆序
							Message _Msg = mHandler.obtainMessage(
									REFRESH_DATA_FINISH, data);
							mHandler.sendMessage(_Msg);
						} else if (type == 1) {
							Message _Msg = mHandler.obtainMessage(
									LOAD_DATA_FINISH, data);
							mHandler.sendMessage(_Msg);
						}

						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(Seed_SelectionDe.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}
}
