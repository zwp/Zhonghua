package com.sinonetwork.zhonghua.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;

public class PapulumAdapter extends BaseAdapter {
	private Context context;
	private List<Map<String, Object>> list;
	private LayoutInflater inflater;
	
	public PapulumAdapter(Context context,List<Map<String, Object>> list) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.list = list;
		inflater = inflater.from(context);
	}
	class ListItem{
		public TextView papulum_title;
		public TextView papulum_content;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = new ListItem();
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.papulum_list, null);
			listItem.papulum_title = (TextView)convertView.findViewById(R.id.papulum_title);
			listItem.papulum_content = (TextView)convertView.findViewById(R.id.papulum_content);
			convertView.setTag(listItem);
		}else {
			listItem = (ListItem) convertView.getTag();
		}
		listItem.papulum_title.setText(String.valueOf(list.get(position).get("title")));
		listItem.papulum_content.setText(String.valueOf(list.get(position).get("content")));
		return convertView;
	}

}
