package com.sinonetwork.zhonghua.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapCache;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.event.CommentEvent;
import com.sinonetwork.zhonghua.event.UpEvent;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.ListSortUtil;
import com.sinonetwork.zhonghua.utils.TimeUtils;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.view.CollapsibleTextView;
import de.greenrobot.event.EventBus;
/**
 * 知道界面的数据展示
 * @author enway
 *
 */
@SuppressLint({ "SimpleDateFormat", "InflateParams" })
public class QuestionAdapter extends BaseAdapter {
	HashMap<Integer, View> lmap = new HashMap<Integer, View>();
	private PopupWindow popupWindow;
	private Context context;
	ViewHolder vh;
	private List<Question> questions;
	private LayoutInflater inflater;
	BitmapCache cache;
	private BitmapUtils bitmapUtils;
	private BitmapDisplayConfig bitmapDisplayConfig;

	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");
	private CommentAdapter adapter;

	// private float leftX;
	// private int popWidth;

	public QuestionAdapter(List<Question> list, Context context) {
		this.context = context;
		this.questions = sortByHappenedTime(questions);
		inflater = LayoutInflater.from(context);
		// initPopWindow();
		//加载图片的初始化工作，此处应改为ImageLoader
		bitmapUtils = new BitmapUtils(context);
		bitmapDisplayConfig = new BitmapDisplayConfig();
		bitmapDisplayConfig.setLoadingDrawable(context.getResources()
				.getDrawable(R.drawable.empty_photo));
		bitmapDisplayConfig.setLoadFailedDrawable(context.getResources()
				.getDrawable(R.drawable.empty_photo));
	}

	/**
	 * 格式化时间轴日期格式，如“2015年6月”
	 * 
	 * @param date
	 * @return
	 */
	private String formatDate(String date) {
		SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd",
				Locale.CHINA);
		String formatDate = null;
		try {
			formatDate = mDateFormat.format(mDateFormat.parse(date));
			String year = formatDate.substring(0, 4);
			String month = formatDate.substring(5, 7);
			return year + "年" + month + "月";
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 按照大事记发生时间排序，最新的大事记排在最上面
	 * 
	 * @param list
	 *            需要排序的List
	 * @return 按时间排列的List
	 * 
	 */
	private List<Question> sortByHappenedTime(List<Question> list) {
		return ListSortUtil.getInstance().sortByDefault(list);
	}

	/**
	 * set the BigEventAdapter data
	 * 
	 * @param list
	 */
	public void setBigEventAdapterData(List<Question> list) {
		questions = sortByHappenedTime(list);
	}

	/**
	 * 每个ITEM中more按钮对应的点击动作
	 * */

	class popUpClick implements View.OnClickListener {
		Question question;
		int position;

		public popUpClick(Question question, int position) {
			this.question = question;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.ll_up:
				UpEvent upEvent = new UpEvent();
				upEvent.question = question;
				upEvent.position = position;
				EventBus.getDefault().post(upEvent);
				popupWindow.dismiss();
				break;
			case R.id.ll_comment:
				CommentEvent event = new CommentEvent();
				event.question = question;
				event.position = position;
				EventBus.getDefault().post(event);
				popupWindow.dismiss();

				break;

			default:
				break;

			}

		}
	}

	/**
	 * 
	 * @param bigEvent
	 * @return
	 */
	private List<String> getAllVisibleImages(Question question) {
		List<String> list = new ArrayList<String>();
		String pictures = question.getPictures();
		if (pictures != null) {
			String images[] = pictures.split(",");
			for (int i = 0; i < images.length; i++) {
				if (!TextUtils.isEmpty(images[i])) {
					list.add(images[i]);
				}

			}
		}
		return list;
	}

	@Override
	public int getCount() {
		return questions != null ? questions.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return questions.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	//初始化条目
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (lmap.get(position) == null) {
			vh = new ViewHolder();
			convertView = inflater.inflate(R.layout.know_list_item, null);
			// ViewUtils.inject(convertView);
			vh.user_image = (ImageView) convertView
					.findViewById(R.id.user_image);
			vh.title = (TextView) convertView.findViewById(R.id.title);
			vh.content = (CollapsibleTextView) convertView
					.findViewById(R.id.content);
			// vh.gv_images = (NoScrollGridView) convertView
			// .findViewById(R.id.gv_images);
			vh.time = (TextView) convertView.findViewById(R.id.time);
			vh.userName = (TextView) convertView.findViewById(R.id.user_name);
			vh.tv_favour_nums = (TextView) convertView
					.findViewById(R.id.tv_favor_num);
			vh.tv_comments_nums = (TextView) convertView
					.findViewById(R.id.tv_comments_num);
			vh.ll_zan = (LinearLayout) convertView.findViewById(R.id.ll_zan);
			vh.tv_favour = (TextView) convertView.findViewById(R.id.tv_favour);
			vh.comments_lv = (com.sinonetwork.zhonghua.view.CustomListView) convertView
					.findViewById(R.id.comments_lv);
			vh.picture_nums = (TextView) convertView
					.findViewById(R.id.picture_nums);
			lmap.put(position, convertView);
			convertView.setTag(vh);
			final Question question = questions.get(position);
			// BitmapHelp.getBitmapUtils(context).display(vh.user_image,
			// Interfaces.DOMAIN_FILE + question.get(), bitmapDisplayConfig);
			vh.title.setText(question.getQuestionTitle());
			vh.userName.setText(question.getUserName1());
			vh.content.setDesc(question.getQuestionText(), BufferType.NORMAL);
			// 加载图片
			final List<String> images = getAllVisibleImages(question);
			if (!images.get(0).equals("null")) {
				vh.title.setPadding(0, 0, BitmapHelp.dip2px(context, 90), 0);
				vh.content.setPadding(0, 0, BitmapHelp.dip2px(context, 90), 0);
				vh.picture_nums.setText(images.size() + "张");
			}
			if (!question.getPictures().equals("null")) {// 否则显示全部，最多5张
				BitmapHelp.getBitmapUtils(context).display(vh.user_image,
						URLAddress.TPURL + images.get(0), bitmapDisplayConfig);
				// vh.gv_images.setVisibility(View.VISIBLE);
				// EventsImagesGridViewAdapter mImagesAdapter = new
				// EventsImagesGridViewAdapter(
				// context, images);
				// vh.gv_images.setOnItemClickListener(new OnItemClickListener()
				// {
				//
				// @Override
				// public void onItemClick(AdapterView<?> parent, View view,
				// int position, long id) {
				// Intent intent = new Intent(context,
				// DetialPhotoActivity.class);
				// intent.putExtra("image", images.get(position));
				// context.startActivity(intent);
				// }
				// });
				// vh.gv_images.setAdapter(mImagesAdapter);
			} else {
				// vh.gv_images.setVisibility(View.GONE);
			}
			// vh.iv_comment.setOnClickListener(new popAction(question,
			// position));
			vh.tv_comments_nums.setText(question.getTotalReply() + "回复");

			vh.time.setText(TimeUtils.getDateTime(question.getPublishTime()));

			//JSONArray jsonArray = JSONArray.parseArray(question.getReplies());
		} else {
			convertView = lmap.get(position);

		}
		return convertView;
	}

	private static class ViewHolder {
		ImageView user_image;
		TextView title;
		CollapsibleTextView content;
		// NoScrollGridView gv_images;
		TextView time;
		TextView tv_favour_nums;
		TextView tv_comments_nums;
		LinearLayout ll_zan;
		TextView tv_favour;
		TextView userName;
		com.sinonetwork.zhonghua.view.CustomListView comments_lv;
		TextView picture_nums;

	}

	/**
	 * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
	 */
	public int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

}
