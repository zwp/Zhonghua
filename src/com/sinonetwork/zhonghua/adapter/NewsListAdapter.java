package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.News;

//新闻列表的数据展示
public class NewsListAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<News> list;

	public NewsListAdapter(ArrayList<News> list, Context context) {
		super();
		this.list = list;
		mInflater = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public News getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// 显示数据
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_news, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.title);// 标题
			holder.docContent = (TextView) convertView
					.findViewById(R.id.docContent);// 描述

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		News c = list.get(position);
		holder.title.setText(c.getTitle());
		holder.docContent.setText(Html.fromHtml(c.getDocContent()));
		return convertView;
	}

	class ViewHolder {
		TextView title;
		TextView docContent;

	}
}
