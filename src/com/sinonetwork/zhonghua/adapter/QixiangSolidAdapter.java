package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.TAsmm.TAsmms;

public class QixiangSolidAdapter extends BaseAdapter{
      private Context context;
      private LayoutInflater layoutInflater;
      private List<TAsmms> data;
	    
      public final class ListItem{
    	  private TextView title;
    	  private TextView neirong;
    	  private TextView HS12;
    	  private TextView HS14;
      }
      
      public QixiangSolidAdapter(Context context, List<TAsmms> data) {
  		layoutInflater = LayoutInflater.from(context);
  		this.context = context;
  		this.data = data;
  	}
      
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}
	public void setData(List<TAsmms> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.qixiang_solid_item, null);
			listItem.title = (TextView)convertView.findViewById(R.id.qixiang_solid_item_title);
			listItem.neirong = (TextView)convertView.findViewById(R.id.qixiang_solid_item_neirong);
			listItem.HS12 = (TextView)convertView.findViewById(R.id.qixiang_solid_item_HS12);
			listItem.HS14 = (TextView)convertView.findViewById(R.id.qixiang_solid_item_HS14);
			convertView.setTag(listItem);
		}else{
			listItem = (ListItem)convertView.getTag();
		}
		
		listItem.title.setText("地区:"+data.get(position).getNamecn().toString());
		listItem.neirong.setText("观测时间:"+data.get(position).getSeeYear().toString());
		listItem.HS14.setText("10-20cm土壤相对湿度:"+String.valueOf(data.get(position).getHS45())+"%");
		listItem.HS12.setText("40-50cm土壤相对湿度:"+String.valueOf(data.get(position).getHS12())+"%");
		
		return convertView;
	}

}
