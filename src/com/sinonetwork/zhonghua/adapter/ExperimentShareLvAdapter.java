package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.ExperimentShareModel.ExperimentShareResultdata;
import com.sinonetwork.zhonghua.model.ExperimentShareModel.ExperimentShareResultdata.ExperimentShareFiles;

public class ExperimentShareLvAdapter extends BaseAdapter {
	private Context context;
	private List<ExperimentShareResultdata> esultdataList;

	public ExperimentShareLvAdapter(Context context,
			List<ExperimentShareResultdata> esultdataList) {
		super();
		this.context = context;
		this.esultdataList = esultdataList;
	}

	public void setLvData(List<ExperimentShareResultdata> esultdataList) {
		this.esultdataList = esultdataList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return esultdataList.size() == 0 ? 0 : esultdataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return esultdataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(context).inflate(
				R.layout.experimentshare_lv_item, null);
		TextView experimentshare_lv_filepath = (TextView) convertView
				.findViewById(R.id.experimentshare_lv_filepath);
		TextView experimentshare_lv_uptime = (TextView) convertView
				.findViewById(R.id.experimentshare_lv_uptime);
		for (int i = 0; i < esultdataList.size(); i++) {
			ExperimentShareFiles shareFiles = esultdataList.get(position)
					.getFiles().get(0);
			experimentshare_lv_filepath.setText(shareFiles.getRname());
			experimentshare_lv_uptime.setText(shareFiles.getUptime());
		}

		return convertView;
	}
}
