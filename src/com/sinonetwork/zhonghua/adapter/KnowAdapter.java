package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.view.CollapsibleTextView;
import com.sinonetwork.zhonghua.view.NoScrollGridView;

public class KnowAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater mInflater = null;
	private List<Question> list = null;

	public KnowAdapter(Context context, List<Question> list) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		return null;
	}
	
	class ViewHoder{
		ImageView ivUser;
		CollapsibleTextView content;
		NoScrollGridView gridView;
		ImageView ivPop;
		TextView tvTime;
		TextView tvFavorNum;
		TextView tvCommentsNum;
		LinearLayout ll_zan;
		
		
	}

}
