package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Warning;
/**
 * 灾害预警
 * @author enway
 *
 */
public class QixiangAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<Warning> data;

	public final class ListItem {
		private TextView title;
		private TextView neirong;
	}

	public QixiangAdapter(Context context, ArrayList<Warning> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// 最多显示5条数据
		if (null == data) {
			return 0;
		} else {
			if (data.size() > 5) {
				return 5;
			} else {
				return data.size();
			}
		}

	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	//显示条目
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.qixiang_item, null);
			listItem.title = (TextView) convertView
					.findViewById(R.id.qixiang_item_title);
			listItem.neirong = (TextView) convertView
					.findViewById(R.id.qixiang_item_neirong);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		listItem.title.setText((String) data.get(position).getTitle());
		String str = (String) data.get(position).getDescription();
		str = str.replace("&nbsp;", "");
		listItem.neirong.setText(str);
		return convertView;
	}

}
