package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Doc;

public class DocListAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<Doc> list;
	private Context context;
	static ViewHolder holder;

	public DocListAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public void setData(ArrayList<Doc> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Doc getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_doc, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Doc c = list.get(position);
		holder.nameTV.setText(c.getRname());

		
		return convertView;
	}

	
	static class ViewHolder {
		TextView nameTV;
		ImageView arrowDownIV;
		

	}
}
