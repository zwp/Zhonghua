package com.sinonetwork.zhonghua.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Comment;

public class CommentReplyAdapter extends BaseAdapter {

	private List<Comment> list;
	private LayoutInflater inflater;
	private Context context;
	private SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public CommentReplyAdapter(List<Comment> list, Context context) {
		this.list = list;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {

		return list.size();
	}

	@Override
	public Object getItem(int position) {

		return list.get(position);
	}

	@Override
	public long getItemId(int position) {

		return Long.parseLong(list.get(position).getId());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;
		if (convertView == null) {
			vh = new ViewHolder();
			convertView = inflater.inflate(R.layout.comment_reply_item, null);
			vh.sendcommentuser = (TextView) convertView.findViewById(R.id.sendcommentuser);
			vh.commentContent = (TextView) convertView.findViewById(R.id.comment_content);
			vh.replyTime = (TextView) convertView.findViewById(R.id.replyTime);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		Comment comment = list.get(position);
		vh.sendcommentuser.setText(comment.getUserName());
		vh.commentContent.setText(comment.getReplyText());
		try {
			vh.replyTime.setText(df2.format(df.parse(comment.getReplyTime())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return convertView;
	}

	class ViewHolder {
		TextView sendcommentuser;
		TextView commentContent;
		TextView replyTime;

	}

}
