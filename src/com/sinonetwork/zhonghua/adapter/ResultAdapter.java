package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.serarchCropProtection.serarchCropProtections;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class ResultAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<serarchCropProtections> data;
	private String[] tupian;

	public final class ListItem {
		private ImageView img;
		private ImageView img2;
		private TextView title;
		private TextView neirong;//描述信息
	}

	public ResultAdapter(Context context, List<serarchCropProtections> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	public void setData(List<serarchCropProtections> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.result_item, null);
			listItem.title = (TextView) convertView
					.findViewById(R.id.result_item_title);
			listItem.img = (ImageView) convertView
					.findViewById(R.id.result_ite_img1);
			listItem.img2 = (ImageView) convertView
					.findViewById(R.id.result_item_img2);
			listItem.neirong = (TextView) convertView
					.findViewById(R.id.result_item_content);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}
		
		String tplujing = data.get(position).getPictures().toString();
		tupian = tplujing.split(",");

		// String url = tupian[0].toString();
		// Bitmap bitmap = getHttpBitmap(url);
		// Logger.e("路径====="+bitmap);
		// //显示
		// listItem.img.setImageBitmap(bitmap);
		if (tupian[0].toString() == "" || tupian[0] == null
				|| data.get(position).getPictures() == null) {
			listItem.img.setBackgroundResource(R.drawable.c);
		} else {
			disPlay(URLAddress.TPURL + tupian[0].toString(), listItem.img, null);
		}
		
		listItem.title.setText((String) data.get(position).getCpName().toString());
		
			serarchCropProtections s = 	data.get(position);
			listItem.neirong.setText(s.getTrait().toString());//修改内容
			System.out.println(s.getTrait());
		return convertView;
	}

	public void disPlay(String uri, ImageView imageView,
			DisplayImageOptions options) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		String uri2 = (String) imageView.getTag();
		if (uri2 == null || !uri.equals(uri2)) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
			if (options != null) {
				imageLoader.displayImage(uri, imageView, options);
			} else {
				imageLoader.displayImage(uri, imageView);
			}
			imageView.setTag(uri);
		}
	}

}
