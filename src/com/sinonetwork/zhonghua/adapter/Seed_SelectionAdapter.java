package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
 
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.CropInfoList.CropInfoLists;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class Seed_SelectionAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<CropInfoLists> data;
	private String[] tupian;
	 
	public final class ListItem {
		private ImageView img;//作物图片
		private TextView title;//作物名称
	}

	public Seed_SelectionAdapter(Context context, List<CropInfoLists> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}
	//设置数据
	public void setData(List<CropInfoLists> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.seed_selection_item, null);
			listItem.title = (TextView) convertView.findViewById(R.id.seed_item_title);
			listItem.img = (ImageView) convertView.findViewById(R.id.seed_ite_img1);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		String tplujing = data.get(position).getCropPicture().toString();
		tupian = tplujing.split(",");

		if (tupian[0].toString() == "" || tupian[0] == null || data.get(position).getCropPicture() == null) {
			listItem.img.setBackgroundResource(R.drawable.nongyibaoc);
			 
		} else {
			// disPlay(URLAddress.TPURLER + tupian[0].toString(), listItem.img,
			// null);
			BitmapHelp.getBitmapUtils(context).display(listItem.img, URLAddress.TPURLER + tupian[0].toString());
		}
		listItem.title.setText((String) data.get(position).getCropName().toString());
		// listItem.img.setBackgroundResource((Integer)
		// data.get(position).get("img"));
		// listItem.img2.setBackgroundResource((Integer)
		// data.get(position).get("img2"));
		// listItem.neirong.setText((String)data.get(position).getCropDescription().toString());
		return convertView;
	}
	//使用ImageLoader显示图片
	public void disPlay(String uri, ImageView imageView, DisplayImageOptions options) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		String uri2 = (String) imageView.getTag();
		if (uri2 == null || !uri.equals(uri2)) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
			if (options != null) {
				imageLoader.displayImage(uri, imageView, options);
			} else {
				imageLoader.displayImage(uri, imageView);
			}
			imageView.setTag(uri);
		}
	}

}
