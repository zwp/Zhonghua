package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.searchAgricultureNews.searchAgricultureNewss;

public class NewsAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<searchAgricultureNewss> data;
	private String biaoti;

	public final class ListItem {
		// private ImageView img;
		private TextView title;
		private TextView time;
	}

	public NewsAdapter(Context context, List<searchAgricultureNewss> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	public void setData(List<searchAgricultureNewss> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.activity_news_item,
					null);
			listItem.title = (TextView) convertView
					.findViewById(R.id.news_item_title);
			// listItem.img = (ImageView) convertView
			// .findViewById(R.id.news_ite_img1);
			listItem.time = (TextView) convertView
					.findViewById(R.id.news_item_neirong);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}
		String hehe = data.get(position).getTitle().toString();
		
		if(hehe.length()>5){
			 biaoti= hehe.substring(0,5);
		}

		listItem.title.setText(biaoti);
		listItem.time.setText(data.get(position).getTime().toString());
		// listItem.img.setBackgroundResource((Integer) data.get(position).get(
		// "img"));
		// listItem.neirong.setText((String) data.get(position).get("neirong"));
		return convertView;
	}

}
