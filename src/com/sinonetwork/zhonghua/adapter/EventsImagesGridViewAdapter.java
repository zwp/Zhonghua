package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.utils.ImageLoader;
import com.sinonetwork.zhonghua.utils.URLAddress;

/**
 * 
 * @author lxq
 */
public class EventsImagesGridViewAdapter extends BaseAdapter {
	private final String TAG = "EventsImagesGridViewAdapter";
	Context context;
	LayoutInflater inflater;
	List<String> imagesResource;
	ImageLoader imageLoader;
	ViewHolder mHolder;
	private BitmapUtils bitmapUtils;
	private BitmapDisplayConfig bitmapDisplayConfig;

	public EventsImagesGridViewAdapter(Context context, List<String> imagesResource) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.imagesResource = imagesResource;
		imageLoader = ImageLoader.getInstance();
		bitmapUtils = new BitmapUtils(context);
		bitmapDisplayConfig = new BitmapDisplayConfig();
		bitmapDisplayConfig.setLoadingDrawable(context.getResources().getDrawable(R.drawable.empty_photo));
		bitmapDisplayConfig.setLoadFailedDrawable(context.getResources().getDrawable(R.drawable.empty_photo));
	}

	@Override
	public int getCount() {
		return imagesResource == null ? 0 : imagesResource.size();
	}

	@Override
	public Object getItem(int position) {
		return imagesResource.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	//数据展示
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.know_lv_item_gv, null);
			mHolder = new ViewHolder();
			mHolder.iv_image = (ImageView) convertView.findViewById(R.id.iv_image_item);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		bitmapUtils.display(mHolder.iv_image, URLAddress.TPURL + imagesResource.get(position), bitmapDisplayConfig);

		return convertView;
	}

	private static class ViewHolder {
		ImageView iv_image;
	}

}