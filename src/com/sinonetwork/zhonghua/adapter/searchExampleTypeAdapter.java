package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.searchExampleType.searchExampleTypes;

public class searchExampleTypeAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater fil;
	private List<searchExampleTypes> data;
	
	public searchExampleTypeAdapter(Context context, List<searchExampleTypes> data) {
		fil = LayoutInflater.from(context);
		this.context = context;
		this.data = data;
	}

	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		return data.get(arg0);
	}

	public void setData(List<searchExampleTypes> data) {
		this.data = data;
		notifyDataSetChanged();
	}
	
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View v, ViewGroup arg2) {
		ViewHolder holder;
		if(v==null){
			v = fil.inflate(R.layout.experiment_child_item, null);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.experiment_title);
			v.setTag(holder);
			
		}else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.name.setText(data.get(arg0).getName().toString());
		
		return v;
	}
	
	private class ViewHolder{
		private TextView name;
	}

}
