package com.sinonetwork.zhonghua.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;

public class PublishQuestionListAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> data;

	public final class ListItem {
		private ImageView publish_listitem_img;
		private TextView publish_listitem_text;
	}

	public PublishQuestionListAdapter(Context context,
			List<Map<String, Object>> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.publish_listitem,
					null);
			listItem.publish_listitem_text = (TextView) convertView
					.findViewById(R.id.publish_listitem_text);
			listItem.publish_listitem_img = (ImageView) convertView
					.findViewById(R.id.publish_listitem_img);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		listItem.publish_listitem_text.setText(String.valueOf(data.get(position).get("listPics")));
		listItem.publish_listitem_img.setBackgroundResource((Integer) data.get(position).get("listText"));
		return convertView;
	}

}
