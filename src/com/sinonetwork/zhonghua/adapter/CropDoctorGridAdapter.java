package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.SubCategorys;

public class CropDoctorGridAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<SubCategorys> list;

	static ViewHolder holder;

	public CropDoctorGridAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setData(ArrayList<SubCategorys> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		if(list == null){
			return 0;
		}else{
			
			return list.size();
		}
	}

	@Override
	public SubCategorys getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_crop_grid, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		SubCategorys item = list.get(position);

		holder.nameTV.setText(item.getCropName());

		return convertView;
	}

	static class ViewHolder {
		TextView nameTV;
	}
}
