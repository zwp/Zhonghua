package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.JiWenDataModel;
import com.sinonetwork.zhonghua.QixiangQuery;
import com.sinonetwork.zhonghua.R;

public class QixiangQueryAdapter extends BaseAdapter{
	   
      private QixiangQuery act;
      private List<JiWenDataModel> datas;
      
	public QixiangQueryAdapter(QixiangQuery act, List<JiWenDataModel> datas) {
		super();
	
		this.act = act;
		this.datas = datas;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datas.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return datas.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View v;
		ViewHolder vh;
		if (convertView == null) {
			v=act.getLayoutInflater().inflate(R.layout.qixiang_query_item, null);
			vh=new ViewHolder();
			vh.tv1=(TextView) v.findViewById(R.id.qixiang_query_item_title);
			vh.tv2=(TextView) v.findViewById(R.id.qixiang_query_item_neirong);
			v.setTag(vh);
		}else{
			v=convertView;
			vh=(ViewHolder) v.getTag();
		}
		vh.tv1.setText(datas.get(position).getD1());
		vh.tv2.setText(datas.get(position).getD2());
		return v;
	}
	static class ViewHolder{
		TextView tv1;
		TextView tv2;
	}
	public void Delete(int position){
		datas.remove(position);
	}

}
