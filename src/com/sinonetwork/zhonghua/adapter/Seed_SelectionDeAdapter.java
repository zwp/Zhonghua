package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.SeedInfoList.SeedInfoLists;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class Seed_SelectionDeAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<SeedInfoLists> data;
	private String[] tupian;

	public final class ListItem {
		private ImageView img;
		private TextView title;
		// private TextView neirong;
	}

	public Seed_SelectionDeAdapter(Context context, List<SeedInfoLists> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void setData(List<SeedInfoLists> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = new ListItem();
		if (convertView == null) {
			
			convertView = layoutInflater.inflate(R.layout.seed_selectionde_item, null);
			listItem.title = (TextView) convertView.findViewById(R.id.seedde_item_title);
			listItem.img = (ImageView) convertView.findViewById(R.id.seedde_ite_img1);
			// listItem.neirong =
			// (TextView)convertView.findViewById(R.id.seedde_item_neirong);
			convertView.setTag(listItem);
		}
		else {
			listItem = (ListItem) convertView.getTag();
		}

		String tplujing = data.get(position).getSeedPicture().toString();
		tupian = tplujing.split(",");
		if (tupian[0].toString() == "" || tupian[0] == null || data.get(position).getSeedPicture() == null) {
			listItem.img.setBackgroundResource(R.drawable.c);
		} else {
			// Toast.makeText(context,
			// String.valueOf(URLAddress.TPURLER+tupian[0].toString()),
			// Toast.LENGTH_SHORT).show();
			Logger.e(URLAddress.TPURLER + tupian[0].toString());
			// disPlay(URLAddress.TPURLER+tupian[0].toString(), listItem.img,
			// null);
			BitmapHelp.getBitmapUtils(context).display(listItem.img, URLAddress.TPURLER + tupian[0].toString());
		}
		if(data==null||"".equals(data)){
			Toast.makeText(context, "data=空", 0).show();
		}else{
		listItem.title.setText(data.get(position).getSeedName().toString());
		}
		// listItem.img.setBackgroundResource((Integer)
		// data.get(position).get("img"));
		// listItem.neirong.setText(data.get(position).getYieldShow().toString());
		return convertView;
	}

	public void disPlay(String uri, ImageView imageView, DisplayImageOptions options) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		String uri2 = (String) imageView.getTag();
		if (uri2 == null || !uri.equals(uri2)) {
			imageLoader.init(ImageLoaderConfiguration.createDefault(context));
			if (options != null) {
				imageLoader.displayImage(uri, imageView, options);
			} else {
				imageLoader.displayImage(uri, imageView);
			}
			imageView.setTag(uri);
		}
	}

}
