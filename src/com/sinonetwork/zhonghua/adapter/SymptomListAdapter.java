package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.SubSymptom;
import com.sinonetwork.zhonghua.model.Symptom;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class SymptomListAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<Symptom> list;
	private Context context;
	static ViewHolder holder;
	private int type = 1;

	public SymptomListAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public void setData(ArrayList<Symptom> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Symptom getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_symptom, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			holder.arrowDownIV = (ImageView) convertView
					.findViewById(R.id.arrow_down_iv);
			holder.arrowDown = (RelativeLayout) convertView
					.findViewById(R.id.arrow_down);

			holder.listLV = (ListView) convertView.findViewById(R.id.list);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Symptom c = list.get(position);
		holder.nameTV.setText(c.getSymptomsTypeName());
		
		
		
		
		final SubSymptomListAdapter subAdapter = new SubSymptomListAdapter(
				context);

		final ArrayList<SubSymptom> subList = c.getSubSymptomList();
		subAdapter.setData(subList);
		holder.listLV.setAdapter(subAdapter);
		holder.listLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int postion, long arg3) {
				int id = subList.get(postion).getId();
//				Toast.makeText(context, "" + id, Toast.LENGTH_SHORT).show();

			}
		});

		String str = PrefUtil.getStringPref(context, "symptomType");
		String newStr = c.getId() + ",";
		//默认symptomType为空，列表为展开状态，点击收起，加id到symptomType，判断没有id，即为展开
		ArrayList<SubSymptom> newList = new ArrayList<SubSymptom>();
		
		if (str.indexOf(newStr) == -1) {
			subAdapter.setData(subList);

			holder.arrowDownIV.setImageResource(R.drawable.arrow_down);
		} else {
			subAdapter.setData(newList);
			holder.arrowDownIV.setImageResource(R.drawable.arrow_right);
			
		}
		holder.arrowDown.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View iv) {
			

				String str = PrefUtil.getStringPref(context, "symptomType");
				String newStr = c.getId() + ",";
				if (str.indexOf(newStr) == -1) {
					str += "" + c.getId() + ",";
					PrefUtil.savePref(context, "symptomType", str);

				} else {
					str = str.replace(newStr, "");
					PrefUtil.savePref(context, "symptomType", str);
				}


				changed();
			}
		});

		return convertView;
	}

	protected void changed() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();

	}

	static class ViewHolder {
		TextView nameTV;
		ImageView arrowDownIV;
		RelativeLayout arrowDown;
		ListView listLV;

	}
}
