package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList.farmingGuidInfoLists;

public class Comprehensive_listAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater layoutInflater;
	private List<farmingGuidInfoLists> data;

	public final class ListItem {

		private TextView title1;
		private TextView title3;
	}

	public Comprehensive_listAdapter(Context context,
			List<farmingGuidInfoLists> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	public void setData(List<farmingGuidInfoLists> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(
					R.layout.comprehensive_list_itme, null);
			listItem.title1 = (TextView) convertView.findViewById(R.id.title1);
			listItem.title3 = (TextView) convertView.findViewById(R.id.title3);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}
		listItem.title1.setText((String) data.get(position).getArea()
				.toString());
		listItem.title3.setText((String) data.get(position).getName()
				.toString());
		// listItem.title3.setText((String)data.get(position).getTargetYield().toString());
		// listItem.title4.setText((String)data.get(position).getYieldComponent().toString());
		return convertView;
	}

}
