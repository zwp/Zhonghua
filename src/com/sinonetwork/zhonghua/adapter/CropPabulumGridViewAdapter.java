package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.R;

public class CropPabulumGridViewAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, Object>> arrayList2;

	public CropPabulumGridViewAdapter(Context context,
			ArrayList<HashMap<String, Object>> arrayList2) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.arrayList2 = arrayList2;
	}

	class ViewHolder {
		ImageView crop_pabulum_item_gridview_item;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList2.size();
	}

	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrayList2.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.crop_pabulum_list_gridview_item, null, false);
			holder = new ViewHolder();
			holder.crop_pabulum_item_gridview_item = (ImageView) convertView
					.findViewById(R.id.crop_pabulum_item_gridview_item);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		HashMap<String, Object> hashMap = arrayList2.get(position);
		holder.crop_pabulum_item_gridview_item.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(context, "��" + (position + 1) + "��",
						Toast.LENGTH_SHORT).show();
			}
		});

		return convertView;
	}

}
