package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList.farmingGuidInfoLists;

public class ShiLingAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	private List<farmingGuidInfoLists> data;

	public ShiLingAdapter(Context context, List<farmingGuidInfoLists> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = LayoutInflater.from(context);
	}

	class ViewHolder {
		public TextView shiling_item_name;
		public TextView shiling_item_content;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.shiling_item, null);
			holder.shiling_item_content = (TextView) convertView
					.findViewById(R.id.shiling_item_content);
			holder.shiling_item_name = (TextView) convertView
					.findViewById(R.id.shiling_item_name);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		return convertView;
	}

}
