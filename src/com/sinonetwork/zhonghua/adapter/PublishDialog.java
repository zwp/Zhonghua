package com.sinonetwork.zhonghua.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.sinonetwork.zhonghua.R;

public class PublishDialog extends Dialog {
	private Context context;
	private View customView;
	private Button publish_dialog_img1;
	private Button publish_dialog_img2;
	LeaveMyDialogListener listener;

	public interface LeaveMyDialogListener {
		public void onClick(View view);
	}

	public PublishDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		LayoutInflater inflater = LayoutInflater.from(context);
		customView = inflater.inflate(R.layout.publish_dialog, null);
	}

	public PublishDialog(Context context, int theme) {
		super(context, theme);
		this.context = context;

	}

	public PublishDialog(Context context, int theme,
			LeaveMyDialogListener listener) {
		super(context, theme);
		this.listener = listener;
		LayoutInflater inflater = LayoutInflater.from(context);
		customView = inflater.inflate(R.layout.publish_dialog, null);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(customView);
		publish_dialog_img1 = (Button) findViewById(R.id.publish_dialog_img1);
		publish_dialog_img2 = (Button) findViewById(R.id.publish_dialog_img2);
		publish_dialog_img1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listener.onClick(v);
			}
		});

	}

	@Override
	public View findViewById(int id) {
		// TODO Auto-generated method stub
		return super.findViewById(id);
	}

	public View getCustomView() {
		return customView;
	}

}
