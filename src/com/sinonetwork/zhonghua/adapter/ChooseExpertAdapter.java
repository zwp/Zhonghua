package com.sinonetwork.zhonghua.adapter;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Expert;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class ChooseExpertAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<Expert> data;
	private String[] beans;
	// 用于记录每个RadioButton的状态，并保证只可选一个
	public HashMap<String, Boolean> states = new HashMap<String, Boolean>();

	public final class ListItem {
		private ImageView choosePic;
		private TextView chooseName;
		RadioButton ischoose;
		// private RelativeLayout choose_expert_rl;
		// private ImageView isChoose;
	}

	public ChooseExpertAdapter(Context context, List<Expert> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Expert getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(
					R.layout.choose_expert_listdetail, null);
			listItem.chooseName = (TextView) convertView
					.findViewById(R.id.expert_name);
			listItem.choosePic = (ImageView) convertView
					.findViewById(R.id.expert_pic);
			// listItem.choose_expert_rl =
			// (RelativeLayout)convertView.findViewById(R.id.choose_expert_rl);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		listItem.chooseName.setText(data.get(position).getRealName());
		BitmapHelp.getBitmapUtils(context).display(listItem.choosePic,
				URLAddress.TPURL + data.get(position).getPictures());
		// listItem.choosePic.setBackgroundResource((Integer) data.get(position)
		// .get("choosePic"));

		final RadioButton radio = (RadioButton) convertView
				.findViewById(R.id.ischoose);
		listItem.ischoose = radio;
		listItem.ischoose.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				// 重置，确保最多只有一项被选中
				for (String key : states.keySet()) {
					states.put(key, false);

				}
				states.put(String.valueOf(position), radio.isChecked());
				ChooseExpertAdapter.this.notifyDataSetChanged();
			}
		});

		boolean res = false;
		if (states.get(String.valueOf(position)) == null
				|| states.get(String.valueOf(position)) == false) {
			res = false;
			states.put(String.valueOf(position), false);
		} else
			res = true;

		listItem.ischoose.setChecked(res);

		return convertView;
	}

}
