package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.Item;
import com.sinonetwork.zhonghua.R;

public class CropExpandAdapter extends BaseExpandableListAdapter {

	private Context context;
	private LayoutInflater mInflater = null;
	private String[] mGroupStrings = null;
	private List<List<Item>> list = null;

	public CropExpandAdapter(Context context, List<List<Item>> list) {
		this.context = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mGroupStrings = context.getResources().getStringArray(R.array.groups);
		this.list = list;
	}

	class Listitem {
		public TextView zhengzhuang;
		public ImageView state;
	}

	public void setData(List<List<Item>> list) {
		this.list = list;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition).size();
	}

	@Override
	public List<Item> getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition);
	}

	@Override
	public Item getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return list.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.crop_group_item_layout, null);
		}
		GroupViewHolder holder = new GroupViewHolder();
		holder.mGroupName = (TextView) convertView
				.findViewById(R.id.crop_group_name);
		holder.group_jiantou = (ImageView) convertView
				.findViewById(R.id.crop_group_jiantou);
//		holder.mGroupCount = (TextView) convertView
//				.findViewById(R.id.group_count);
		// ImageView group_jiantou = (ImageView) convertView
		// .findViewById(R.id.group_jiantou);
		holder.mGroupName.setText(mGroupStrings[groupPosition]);

		// holder.mGroupCount.setText("[" + list.get(groupPosition).size() +
		// "]");
		if (isExpanded) {
			holder.group_jiantou.setImageResource(R.drawable.arrow_down);
		} else {
			holder.group_jiantou.setImageResource(R.drawable.arrow_right);
		}
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.crop_child_item_layout, null);
		}
		ChildViewHolder holder = new ChildViewHolder();
		holder.mIcon = (ImageView) convertView.findViewById(R.id.crop_state);
		holder.mIcon.setBackgroundResource(getChild(groupPosition,
				childPosition).getImageId());
		holder.mChildName = (TextView) convertView
				.findViewById(R.id.crop_zhengzhuang);
		holder.mChildName.setText(getChild(groupPosition, childPosition)
				.getName());
		// holder.mDetail = (TextView)
		// convertView.findViewById(R.id.item_detail);
		// holder.mDetail.setText(getChild(groupPosition, childPosition)
		// .getDetail());

		// Listitem listitem = null;
		// if (convertView == null) {
		// convertView = mInflater.inflate(R.layout.child_item_layout, null);
		// listitem.zhengzhuang =
		// (TextView)convertView.findViewById(R.id.zhengzhuang);
		// listitem.state = (ImageView)convertView.findViewById(R.id.state);
		// convertView.setTag(listitem);
		// }
		// else {
		// listitem = (Listitem)convertView.getTag();
		// }
		// listitem.zhengzhuang.setText(getChild(groupPosition, childPosition)
		// .getName());
		// listitem.state.setBackgroundResource(getChild(groupPosition,
		// childPosition).getImageId());
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		/* 很重要：实现ChildView点击事件，必须返回true */
		return true;
	}

	private class GroupViewHolder {
		TextView mGroupName;
//		TextView mGroupCount;
		ImageView group_jiantou;
	}

	private class ChildViewHolder {
		ImageView mIcon;
		TextView mChildName;
	}

}
