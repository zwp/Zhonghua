package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import com.sinonetwork.zhonghua.JsonTuRangx;
import com.sinonetwork.zhonghua.QixiangSolid;
import com.sinonetwork.zhonghua.R;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TuRangAdapter extends BaseAdapter {
	private QixiangSolid act;
	private List<JsonTuRangx> datas;

	public TuRangAdapter(QixiangSolid act, List<JsonTuRangx> datas) {
		super();

		this.act = act;
		this.datas = datas;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datas.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return datas.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View v;
		ViewHolder vh;
		if (arg1 == null) {
			v = act.getLayoutInflater().inflate(R.layout.qixiang_solid_item,
					null);
			vh = new ViewHolder();
			vh.tv1 = (TextView) v.findViewById(R.id.qixiang_solid_item_neirong);
			vh.tv2 = (TextView) v.findViewById(R.id.qixiang_solid_item_HS14);
			v.setTag(vh);
		} else {
			v = arg1;
			vh = (ViewHolder) v.getTag();
		}
		vh.tv1.setText(datas.get(arg0).getD1());
		vh.tv2.setText(datas.get(arg0).getD2());
		return v;
	}

	static class ViewHolder {
		TextView tv1;
		TextView tv2;
	}
}
