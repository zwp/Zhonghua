package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Warning;

public class QixiangWarmAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<Warning> data;

	public final class ListItem {
		private TextView title;
		private TextView neirong;
	}

	public QixiangWarmAdapter(Context context, ArrayList<Warning> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		if (null == data) {
			return 0;
		} else {
			return data.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.qixiang_warm_item,
					null);
			listItem.title = (TextView) convertView
					.findViewById(R.id.qixiang_warm_item_title);
			listItem.neirong = (TextView) convertView
					.findViewById(R.id.qixiang_warm_item_neirong);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		listItem.title.setText((String) data.get(position).getTitle());
		String str = (String) data.get(position).getDescription();
		str = str.replace("&nbsp;", "");
		listItem.neirong.setText(str);
		return convertView;
	}

}
