package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.searchExamplei.searchExampleis;
//套餐试验示范的详情
public class searchExamplesAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater fil;
	private List<searchExampleis> data;
	
	public searchExamplesAdapter(Context context, List<searchExampleis> data) {
		fil = LayoutInflater.from(context);
		this.context = context;
		this.data = data;
	}

	
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int arg0) {
		return data.get(arg0);
	}

	public void setData(List<searchExampleis> data) {
		this.data = data;
		notifyDataSetChanged();
	}
	
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	//显示数据
	@Override
	public View getView(int arg0, View v, ViewGroup arg2) {
		ViewHolder holder;
		if(v==null){
			v = fil.inflate(R.layout.searchexampletypei_item, null);
			holder = new ViewHolder();
			holder.ProductComboInfo = (TextView) v.findViewById(R.id.searchexampletypei_title);
			v.setTag(holder);
			
		}else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.ProductComboInfo.setText(data.get(arg0).getPrincipal().toString());
		
		return v;
	}
	
	private class ViewHolder{
		private TextView ProductComboInfo;
	}

}
