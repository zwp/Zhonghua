package com.sinonetwork.zhonghua.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.queryMyOrderTestSoil.queryMyOrderTestSoils;

public class MySolidAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<queryMyOrderTestSoils> data;
	
	
	


	public final class ListItem {
		private TextView my_solid_item_time;
		private TextView result_item_state;
	}

	public MySolidAdapter(Context context, List<queryMyOrderTestSoils> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void setData(List<queryMyOrderTestSoils> data) {
		this.data = data;
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.my_solid_item, null);
			listItem.my_solid_item_time = (TextView) convertView
					.findViewById(R.id.my_solid_item_time);
			listItem.result_item_state = (TextView) convertView
					.findViewById(R.id.result_item_state);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}
		listItem.my_solid_item_time.setText(data.get(position)
				.getOrderTimeStr().toString());
		if ("0".equals(data.get(position).getStatus().toString())) {
			listItem.result_item_state.setText("处理中");
		} else if ("1".equals(data.get(position).getStatus().toString())) {
			listItem.result_item_state.setText("已完成");
		} else if ("2".equals(data.get(position).getStatus().toString())) {
			listItem.result_item_state.setText("已关闭");
		}
		

		return convertView;
	}

}
