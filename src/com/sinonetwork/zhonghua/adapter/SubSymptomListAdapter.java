package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.SubSymptom;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class SubSymptomListAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<SubSymptom> list;
	private Context context;
	static ViewHolder holder;
	private int type = 1;

	public SubSymptomListAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public void setData(ArrayList<SubSymptom> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public SubSymptom getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_sub_symptom, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			holder.arrowDownIV = (ImageView) convertView
					.findViewById(R.id.arrow_down_iv);
			holder.arrowDown = (RelativeLayout) convertView
					.findViewById(R.id.arrow_down);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final SubSymptom c = list.get(position);
		holder.nameTV.setText(c.getSymptomsName());
		String str = PrefUtil.getStringPref(context, "subSymptom");
		String newStr = c.getId() + ",";
		if (str.indexOf(newStr) == -1) {
			holder.arrowDownIV.setImageResource(R.drawable.check2);
		} else {
			holder.arrowDownIV.setImageResource(R.drawable.check1);
		}

		holder.arrowDown.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View iv) {
				String str = PrefUtil.getStringPref(context, "subSymptom");
				String newStr = c.getId() + ",";
				if (str.indexOf(newStr) == -1) {
					str += "" + c.getId() + ",";
					PrefUtil.savePref(context, "subSymptom", str);

				} else {
					str = str.replace(newStr, "");
					PrefUtil.savePref(context, "subSymptom", str);
				}


				changed();

			}
		});

		return convertView;
	}

	protected void changed() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();

	}

	static class ViewHolder {
		TextView nameTV;
		ImageView arrowDownIV;
		RelativeLayout arrowDown;

	}

}
