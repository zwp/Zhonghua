package com.sinonetwork.zhonghua.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;

public class CetuSolidBaseInfoAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> data;

	public final class ListItem {
		private ImageView choosePic;
		private TextView chooseName;
		private ImageView isChoose;
	}

	public CetuSolidBaseInfoAdapter(Context context, List<Map<String, Object>> data) {
		layoutInflater = LayoutInflater.from(context);
		this.context = context;
		this.data = data;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(
					R.layout.choose_expert_listdetail, null);
			listItem.chooseName = (TextView) convertView
					.findViewById(R.id.expert_name);
			listItem.choosePic = (ImageView) convertView
					.findViewById(R.id.expert_pic);
			convertView.setTag(listItem);
		} else {
			listItem = (ListItem) convertView.getTag();
		}

		listItem.chooseName.setText((String) data.get(position).get("chooseName"));
		listItem.choosePic.setBackgroundResource((Integer) data.get(position)
				.get("choosePic"));
		return convertView;
	}

}
