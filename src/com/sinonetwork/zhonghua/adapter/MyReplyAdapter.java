package com.sinonetwork.zhonghua.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.lidroid.xutils.bitmap.core.BitmapCache;
import com.sinonetwork.zhonghua.DetialPhotoActivity;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.model.Reply;
import com.sinonetwork.zhonghua.utils.SortReplylistUtil;
import com.sinonetwork.zhonghua.view.CollapsibleTextView;
import com.sinonetwork.zhonghua.view.NoScrollGridView;

public class MyReplyAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater inflater;
	@SuppressLint("UseSparseArrays")
	HashMap<Integer, View> lmap = new HashMap<Integer, View>();
	BitmapCache cache;
	private List<Reply> replies;
	private BitmapUtils bitmapUtils;
	private SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");
	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private BitmapDisplayConfig bitmapDisplayConfig;

	public MyReplyAdapter(Context context, List<Reply> list) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.replies = sortByHappenedTime(replies);
		inflater = LayoutInflater.from(context);
		bitmapUtils = new BitmapUtils(context);
		bitmapDisplayConfig = new BitmapDisplayConfig();
		bitmapDisplayConfig.setLoadingDrawable(context.getResources()
				.getDrawable(R.drawable.empty_photo));
		bitmapDisplayConfig.setLoadFailedDrawable(context.getResources()
				.getDrawable(R.drawable.empty_photo));
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return replies != null ? replies.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return replies.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Reply reply = replies.get(position);
		ViewHolder holder = null;
		if (lmap.get(position) == null  ) {
			
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.my_reply_item, null);
			holder.question_item_headpic = (ImageView) convertView
					.findViewById(R.id.question_item_headpic);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.content = (CollapsibleTextView) convertView
					.findViewById(R.id.content);
			holder.gv_images = (NoScrollGridView) convertView
					.findViewById(R.id.gv_images);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.replyContent = (CollapsibleTextView) convertView
					.findViewById(R.id.replyTxt);
			lmap.put(position, convertView);
			convertView.setTag(holder);
			

			holder.replyContent.setDesc(reply.getReplyText() + "",
					BufferType.NORMAL);
			
			if (!reply.getQuestions().toString().equals("[null]")) {
				if (null != reply.getQuestions()) {
					
					holder.title.setText(reply.getQuestions().get(0)
							.getQuestionText());
					
						holder.content.setDesc(reply.getQuestions().get(0)
								.getQuestionText(), BufferType.NORMAL);
					
					Log.i("gxx",
							"adapter中的问题内容显示："
									+ reply.getQuestions().get(0)
											.getQuestionText()
									+ BufferType.NORMAL);

					// 加载图片
					final List<String> images = getAllVisibleImages(reply
							.getQuestions().get(0));
					if (!reply.getQuestions().get(0).getPictures()
							.equals("null")) {
						holder.gv_images.setVisibility(View.VISIBLE);
						EventsImagesGridViewAdapter mImagesAdapter = new EventsImagesGridViewAdapter(
								context, images);
						holder.gv_images
								.setOnItemClickListener(new OnItemClickListener() {

									@Override
									public void onItemClick(
											AdapterView<?> arg0, View arg1,
											int arg2, long arg3) {
										// TODO Auto-generated method stub
										Intent intent = new Intent(context,
												DetialPhotoActivity.class);
										intent.putExtra("image",
												images.get(arg2));
										context.startActivity(intent);
									}
								});
						holder.gv_images.setAdapter(mImagesAdapter);
					} else {
						holder.gv_images.setVisibility(View.GONE);
					}
					try {
						holder.time.setText(df2.format(df.parse(reply
								.getQuestions().get(0).getPublishTime())));
						Log.i("gxx",
								"adapter中显示的time："
										+ df2.format(df.parse(reply
												.getQuestions().get(0)
												.getPublishTime())));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		} else {
			convertView = lmap.get(position);
			Log.i("gxx", "getQuestions为空的时候："+lmap.get(position));
			Log.i("gxx", "getQuestions为空的时候："+position);
		}
		return convertView;
	}

	class ViewHolder {
		private ImageView question_item_headpic;
		// private ImageView question_item_img1;
		// private ImageView question_item_img2;
		// private ImageView question_item_img3;
		private TextView title;
		private CollapsibleTextView content;
		private NoScrollGridView gv_images;
		private TextView time;
		private CollapsibleTextView replyContent;
	}

	/**
	 * 按照大事记发生时间排序，最新的大事记排在最上面
	 * 
	 * @param list
	 *            需要排序的List
	 * @return 按时间排列的List
	 * 
	 */
	private List<Reply> sortByHappenedTime(List<Reply> list) {
		return SortReplylistUtil.getInstance().sortByDefault(list);
	}

	/**
	 * set the BigEventAdapter data
	 * 
	 * @param list
	 */
	public void setBigEventAdapterData(List<Reply> list) {
		replies = sortByHappenedTime(list);
	}

	/**
	 * 
	 * @param bigEvent
	 * @return
	 */
	private List<String> getAllVisibleImages(Question question) {
		List<String> list = new ArrayList<String>();
		String pictures = question.getPictures();
		if (pictures != null) {
			String images[] = pictures.split(",");
			for (int i = 0; i < images.length; i++) {
				if (!TextUtils.isEmpty(images[i])) {
					list.add(images[i]);
				}

			}
		}
		return list;
	}

}
