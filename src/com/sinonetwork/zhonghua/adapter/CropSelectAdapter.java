package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;
import java.util.HashSet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.Categorys;
import com.sinonetwork.zhonghua.model.SubCategorys;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class CropSelectAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<Categorys> list;
	private Context context;
	static ViewHolder holder;
	boolean flag = false;

	private ArrayList<SubCategorys> selectList;
	private int type;
	private static HashSet<Integer> Idset = new HashSet<Integer>();

	public CropSelectAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public void setData(ArrayList<Categorys> list) {
		this.list = list;
	}

	public void setCurrentPlant(int type) {
		this.type = type;

	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Categorys getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_crop_doctor, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			holder.arrowDownIV = (ImageView) convertView
					.findViewById(R.id.arrow_down_iv);
			holder.arrowDown = (RelativeLayout) convertView
					.findViewById(R.id.arrow_down);

			holder.subGridView = (GridView) convertView
					.findViewById(R.id.sub_gv);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Categorys c = list.get(position);
		holder.nameTV.setText(c.getCropName());

		holder.arrowDownIV.setImageResource(R.drawable.arrow_down);
		final PlantGridAdapter subAdapter = new PlantGridAdapter(
				context);

		final ArrayList<SubCategorys> subList = c.getSubCategorysList();
		subAdapter.setData(subList);
		holder.subGridView.setAdapter(subAdapter);

		String str = PrefUtil.getStringPref(context, "cropDoctor");
		String newStr = c.getId() + ",";
		// 默认cropDoctor为空，列表为展开状态，点击收起，加id到cropDoctor，判断没有id，即为展开
		ArrayList<SubCategorys> newList = new ArrayList<SubCategorys>();

		if (str.indexOf(newStr) == -1) {
			subAdapter.setData(subList);

			holder.arrowDownIV.setImageResource(R.drawable.arrow_down);
		} else {
			subAdapter.setData(newList);
			holder.arrowDownIV.setImageResource(R.drawable.arrow_right);

		}

		holder.subGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int postion, long arg3) {
				int id = subList.get(postion).getId();
				String str = PrefUtil.getStringPref(context, "cropSelectPlant");
				String newStr = id + ",";
				//0为种植作物可多选，1为关注作物单选
				if(type == 0){
					if (str.indexOf(newStr) == -1) {
						
						str += ""+ newStr;
						PrefUtil.savePref(context, "cropSelectPlant", str);

					} else {
						str = str.replace(newStr, "");
						PrefUtil.savePref(context, "cropSelectPlant", str);
					}
				}else{
					PrefUtil.savePref(context, "cropSelectPlant", newStr);
				}
				
				changed();
//				HashSet<Integer> set = getSelectCrops();
//				if (set != null && set.contains(id)) {
//					view.setBackgroundColor(Color.WHITE);
//					Idset.remove(id);
//				} else {
//					view.setBackgroundColor(Color.GRAY);
//					Idset.add(id);
//				}
			}
		});

		holder.arrowDown.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View iv) {

				String str = PrefUtil.getStringPref(context, "cropDoctor");
				String newStr = c.getId() + ",";
				if (str.indexOf(newStr) == -1) {
					str += "" + c.getId() + ",";
					PrefUtil.savePref(context, "cropDoctor", str);

				} else {
					str = str.replace(newStr, "");
					PrefUtil.savePref(context, "cropDoctor", str);
				}

				changed();

			}
		});

		return convertView;
	}

	protected void changed() {
		// TODO Auto-generated method stub
		notifyDataSetChanged();

	}

	static class ViewHolder {
		TextView nameTV;
		ImageView arrowDownIV;
		RelativeLayout arrowDown;
		GridView subGridView;

	}

	public static HashSet<Integer> getSelectCrops() {
		return Idset;
	}

	public static void cleanSeectCrops() {
		Idset.clear();
	}

}
