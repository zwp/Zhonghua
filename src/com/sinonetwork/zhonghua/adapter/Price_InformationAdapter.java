package com.sinonetwork.zhonghua.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
/**
 * 价格行情的数据显示的适配器， 本次迭代需要修改的地方，
 * @author enway
 *
 */
public class Price_InformationAdapter extends BaseAdapter{
      private Context context;
      private LayoutInflater layoutInflater;
      private List<Map<String, Object>> data;
	    
      public final class ListItem{
    	  private TextView name;
    	  private TextView jiage;
    	  private TextView shichang;
    	  private TextView shijian;
      }
      
      public Price_InformationAdapter(Context context, List<Map<String, Object>> data) {
  		layoutInflater = LayoutInflater.from(context);
  		this.context = context;
  		this.data = data;

  	}
      
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListItem listItem = null;
		if (convertView == null) {
			listItem = new ListItem();
			convertView = layoutInflater.inflate(R.layout.price_infor_item, null);
			listItem.name = (TextView)convertView.findViewById(R.id.price_item_txt1);
			listItem.jiage = (TextView)convertView.findViewById(R.id.price_item_txt2);
			listItem.shichang = (TextView)convertView.findViewById(R.id.price_item_txt3);
			listItem.shijian = (TextView)convertView.findViewById(R.id.price_item_txt4);
			convertView.setTag(listItem);
		}else{
			listItem = (ListItem)convertView.getTag();
		}
		
		listItem.name.setText((String)data.get(position).get("name"));
		listItem.jiage.setText((String)data.get(position).get("jiage"));
		listItem.shichang.setText((String)data.get(position).get("shichang"));
		listItem.shijian.setText((String)data.get(position).get("shijian"));
		return convertView;
	}

}
