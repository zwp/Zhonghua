package com.sinonetwork.zhonghua.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.model.SubCategorys;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class PlantGridAdapter extends BaseAdapter {

	final LayoutInflater mInflater;
	private ArrayList<SubCategorys> list;
	private Context context;

	static ViewHolder holder;

	public PlantGridAdapter(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	public void setData(ArrayList<SubCategorys> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		if(list == null){
			return 0;
		}else{
			
			return list.size();
		}
	}

	@Override
	public SubCategorys getItem(int index) {
		return list.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (null == list)
			return null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.item_crop_grid, null);
			holder = new ViewHolder();
			holder.nameTV = (TextView) convertView.findViewById(R.id.name);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		SubCategorys item = list.get(position);

		holder.nameTV.setText(item.getCropName());
		String str = PrefUtil.getStringPref(context, "cropSelectPlant");
		String newStr = item.getId() + ",";
		if (str.indexOf(newStr) == -1) {
			holder.nameTV.setBackgroundColor(Color.WHITE);
			
		} else {
			holder.nameTV.setBackgroundColor(Color.GRAY);
		}

		return convertView;
	}

	static class ViewHolder {
		TextView nameTV;
	}
}
