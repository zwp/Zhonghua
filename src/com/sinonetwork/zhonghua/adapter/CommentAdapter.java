package com.sinonetwork.zhonghua.adapter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.R;
import com.sinonetwork.zhonghua.ReplyActivity;
import com.sinonetwork.zhonghua.event.CommentDingEvent;
import com.sinonetwork.zhonghua.event.CommentDownEvent;
import com.sinonetwork.zhonghua.event.GetUserNameEvent;
import com.sinonetwork.zhonghua.model.Comment;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.TimeUtils;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

import de.greenrobot.event.EventBus;

public class CommentAdapter extends BaseAdapter {

	private List<Comment> list;
	private LayoutInflater inflater;
	private Context context;

	public CommentAdapter(List<Comment> list, Context context) {
		this.list = list;
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {

		return list.size();
	}

	@Override
	public Object getItem(int position) {

		return list.get(position);
	}

	@Override
	public long getItemId(int position) {

		return Long.parseLong(list.get(position).getId());
	}
//显示数据
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh;
		if (convertView == null) {
			vh = new ViewHolder();
			convertView = inflater.inflate(R.layout.comment_item, null);
			vh.sendcommentuser = (TextView) convertView
					.findViewById(R.id.sendcommentuser);
			vh.commentContent = (TextView) convertView
					.findViewById(R.id.comment_content);
			vh.tvDing = (TextView) convertView.findViewById(R.id.tv_ding);
			vh.tvDown = (TextView) convertView.findViewById(R.id.tv_down);
			vh.ivDing = (LinearLayout) convertView.findViewById(R.id.iv_ding);
			vh.ivDown = (LinearLayout) convertView.findViewById(R.id.iv_down);
			vh.tvDate = (TextView) convertView.findViewById(R.id.iv_date);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		final Comment comment = list.get(position);
		vh.sendcommentuser.setText(comment.getUserName());
		vh.commentContent.setText(comment.getReplyText());
		vh.tvDate.setText(TimeUtils.getDateTime(comment.getReplyTime()));
		if (comment.getReplyAgree() != null) {
			vh.tvDing.setText(comment.getReplyAgree());
		}
		if (comment.getReplyDisAgree() != null) {
			vh.tvDown.setText(comment.getReplyDisAgree());
		}
		vh.ivDing.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (PrefUtil.getBooleanPref(context,
						ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
					String userName = PrefUtil.getStringPref(context,
							"zhonghua_userName");
					HttpHelp.getInstance().send(
							HttpMethod.GET,
							URLAddress.addReplyClickNum("addReplyClickNum",
									comment.getId(), userName),
							new RequestCallBack<String>() {

								@Override
								public void onFailure(HttpException arg0,
										String arg1) {

								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {
									JSONObject jsonObject = JSONObject
											.parseObject(arg0.result);
									Toast.makeText(context,
											jsonObject.getString("resultdesc"),
											2000).show();
									if (jsonObject.getString("resultcode")
											.equals("ok")) {
										CommentDingEvent event = new CommentDingEvent();
										event.replyId = comment.getId();
										EventBus.getDefault().post(event);

										HttpHelp.getInstance().send(
												HttpMethod.GET,
												URLAddress.getUserIntegral(
														comment.getUserName(),
														"1", "add"),
												new RequestCallBack<String>() {

													@Override
													public void onFailure(
															HttpException arg0,
															String arg1) {

													}

													@Override
													public void onSuccess(
															ResponseInfo<String> arg0) {
													}
												});

									}
								}
							});
				} else {
					Toast.makeText(context, "请您先登录", 2000).show();
				}
			}
		});

		vh.ivDown.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (PrefUtil.getBooleanPref(context,
						ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
					String userName = PrefUtil.getStringPref(context,
							"zhonghua_userName");
					HttpHelp.getInstance().send(
							HttpMethod.GET,
							URLAddress.addReplyDisClickNum(
									"addReplyDisClickNum", comment.getId(),
									userName), new RequestCallBack<String>() {

								@Override
								public void onFailure(HttpException arg0,
										String arg1) {

								}

								@Override
								public void onSuccess(ResponseInfo<String> arg0) {
									JSONObject jsonObject = JSONObject
											.parseObject(arg0.result);
									Toast.makeText(context,
											jsonObject.getString("resultdesc"),
											2000).show();
									if (jsonObject.getString("resultcode")
											.equals("ok")) {
										CommentDownEvent event = new CommentDownEvent();
										event.replyId = comment.getId();
										EventBus.getDefault().post(event);
									}
								}
							});
				} else {
					Toast.makeText(context, "请您先登录", 2000).show();
				}
			}
		});
		return convertView;
	}

	class ViewHolder {
		TextView sendcommentuser;
		TextView commentContent;
		TextView tvDing;
		TextView tvDown;
		TextView tvDate;
		LinearLayout ivDing;
		LinearLayout ivDown;
	}

}
