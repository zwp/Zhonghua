package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.os.Bundle;
import android.widget.ListView;

import com.sinonetwork.zhonghua.adapter.CropDoctorListAdapter;
import com.sinonetwork.zhonghua.model.Categorys;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class CropDoctorActivity extends LandBaseActivity {
	protected ArrayList<Categorys> list;
	private CropDoctorListAdapter mAdapter;
	private ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_crop_doctor);
		setBackBtn();
		setTopTitleTV("作物医生");
		PrefUtil.savePref(this, "cropDoctor", "");//只在每次进入时清空本地字符串，从上个页面返回不请空
		listView = (ListView) findViewById(R.id.list);
		loadData();  
	}

	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					list = LandInfoParser.getCropDoctorList();

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void setView() {
		
		mAdapter = new CropDoctorListAdapter(this);
		mAdapter.setData(list);
		listView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}
}
