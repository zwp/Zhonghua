package com.sinonetwork.zhonghua;

public class Item {
	private boolean ischoose;
	public boolean isIschoose() {
		return ischoose;
	}

	public void setIschoose(boolean ischoose) {
		this.ischoose = ischoose;
	}


	private int resId;
	private String name;

	public Item(int resId, String name) {
		this.resId = resId;
		this.name = name;
	}

	public void setImageId(int resId) {
		this.resId = resId;
	}

	public int getImageId() {
		return resId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}


	public String toString() {
		return "Item[" + resId + ", " + name + "]";
	}

}
