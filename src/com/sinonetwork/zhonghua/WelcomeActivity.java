package com.sinonetwork.zhonghua;

import java.util.Timer;
import java.util.TimerTask;

import com.sinonetwork.zhonghua.utils.PrefUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class WelcomeActivity extends Activity {

	private Timer timer = new Timer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		timer.schedule(new TimerTask() {

			@Override
			public void run() {

				if (PrefUtil.getBooleanPref(WelcomeActivity.this, "first",
						false)) {
					startActivity(new Intent(WelcomeActivity.this,
							MainActivity.class));
					WelcomeActivity.this.finish();
				} else {
					startActivity(new Intent(WelcomeActivity.this,
							MainActivity.class));
					WelcomeActivity.this.finish();
					PrefUtil.savePref(WelcomeActivity.this, "first", true);
				}

			}
		}, 2000);

	}

}
