package com.sinonetwork.zhonghua;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.NewsListAdapter;
import com.sinonetwork.zhonghua.model.News;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;
/**
 * 农业新闻，添加了UTF-8的编码方式
 * @author enway
 *
 */
public class NewsActivity extends LandBaseActivity {
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;

	private MyListView listView;
	private ImageView back;
	private NewsListAdapter mAdapter;
	protected ArrayList<News> list = new ArrayList<News>();
	private int pageNow = 1;//初始化第几页
	private Handler mHandler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case REFRESH_DATA_FINISH:
				if (mAdapter != null) {
					// adapter = new AddressAdapter(AddressActivity.this, _List,
					// R.layout.address_item);
					mAdapter.notifyDataSetChanged();
				}
				listView.onRefreshComplete(); // 下拉刷新完成
				break;
			case LOAD_DATA_FINISH:
				if (mAdapter != null) {
					// adapter.getmDatas().addAll((ArrayList<Good>) msg.obj);
					mAdapter.notifyDataSetChanged();
				}
				listView.misHaveNewDatas = false;
				listView.onLoadMoreComplete(); // 加载更多完成
				break;
			default:
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		listView = (MyListView) findViewById(R.id.news_lv);
//		返回键
		back = (ImageView) findViewById(R.id.back);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
//		显示数据
		mAdapter = new NewsListAdapter(list, NewsActivity.this);
		listView.setAdapter(mAdapter);
//		下拉刷新
		listView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				list.clear();

				pageNow = 1;
				loadData(0);
			}
		});
//上拉加载
		listView.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				pageNow++;
				loadData(1);
			}
		});
//		条目点击时间，arg2条目点击的位置，从1开始
		listView.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Log.e("农业新闻arg2:", arg2+"");
				Intent intent = new Intent(NewsActivity.this, NewsDetailActivity.class);
				intent.putExtra("ID", list.get(arg2-1).getId());
				startActivity(intent);
			}
		});

		loadData(0);

	}
//加载数据
	public void loadData(final int type) {
		// showProgressDialog("正在加载数据请稍后...");
		new Thread() {
			@Override
			public void run() {
				// _List = null;
				switch (type) {
				case 0:
					addContent(pageNow);//基本第几页的数据
					break;
				}

				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				if (type == 0) { // 下拉刷新
					Message _Msg = mHandler.obtainMessage(REFRESH_DATA_FINISH);
					mHandler.sendMessage(_Msg);
				} else if (type == 1) {
					addContent(pageNow);
				}
			}
		}.start();
	}
	//网络请求数据并加载数据
	public void addContent(final int page) {
		//分页记载农业新闻的数据
		HttpHelp.getInstance().send(HttpMethod.GET, URLAddress.URLYI + "?method=searchAgricultureNews&curPage=" + page, new RequestCallBack<String>() {
			
			@Override
			public void onFailure(HttpException arg0, String arg1) {

			}

			@Override
			public void onSuccess(ResponseInfo<String> resultInfo) {
				//hideLoadProgressBar();
				Log.i("gxx", "url:"+URLAddress.URLYI + "?method=searchAgricultureNews&curPage=" + page);
				//解析数据
				com.alibaba.fastjson.JSONObject jsonObj = com.alibaba.fastjson.JSONObject.parseObject(resultInfo.result);
				ArrayList<News> data = new ArrayList<News>();
				if (jsonObj.getString("resultcode").equals("ok")) {

					JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
					int size = jsonArray.size();
					//将网络中的数据
					for (int i = 0; i < size; i++) {
						JSONObject o = jsonArray.getJSONObject(i);
						News d = new News();
						d.setId(o.getString("id"));
						//添加编码方式
						try {
							d.setTitle(URLDecoder.decode(o.getString("title"), "UTF-8"));
							d.setTime(o.getString("time"));
							d.setDocContent(URLDecoder.decode(o.getString("docContent"), "UTF-8"));
							d.setInfoTypeId(o.getString("infoTypeId"));
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}

						data.add(d);
					}

					if (data.size() > 0) {
						list.addAll(data);
					}
					Message _Msg = mHandler.obtainMessage(LOAD_DATA_FINISH);//加载更多
					mHandler.sendMessage(_Msg);
				}

			}
		});
	}
}
