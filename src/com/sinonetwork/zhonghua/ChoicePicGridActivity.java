package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.adapter.ChoiceLocationImageGridAdapter;
import com.sinonetwork.zhonghua.adapter.ChoiceLocationImageGridAdapter.TextCallback;
import com.sinonetwork.zhonghua.model.ImageItem;
import com.sinonetwork.zhonghua.utils.AlbumHelper;
import com.sinonetwork.zhonghua.utils.Bimp;

public class ChoicePicGridActivity extends FragmentActivity {
	public static final String EXTRA_IMAGE_LIST = "imagelist";

	// ArrayList<Entity> dataList;
	List<ImageItem> dataList;
	GridView mGridView;
	ChoiceLocationImageGridAdapter mAdapter;
	AlbumHelper helper;
	Button btn_certain;

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				Toast.makeText(ChoicePicGridActivity.this, "最多选择6张图片", Toast.LENGTH_LONG).show();
				break;

			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_picture_choice_picitem);
		helper = AlbumHelper.getHelper();
		helper.init(getApplicationContext());

		dataList = (List<ImageItem>) getIntent().getSerializableExtra(EXTRA_IMAGE_LIST);
		initView();

	}

	private void initView() {
		((TextView) findViewById(R.id.top_title_textview)).setText("选择图片");
		((ImageView) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ChoicePicGridActivity.this.finish();
			}
		});
		mGridView = (GridView) findViewById(R.id.gv_picitem);
		mAdapter = new ChoiceLocationImageGridAdapter(ChoicePicGridActivity.this, dataList, mHandler);
		mGridView.setAdapter(mAdapter);
		mAdapter.setTextCallback(new TextCallback() {
			public void onListen(int count) {
				btn_certain.setText("确定" + "(" + count + ")");
			}
		});

		mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				mAdapter.notifyDataSetChanged();
			}

		});

		btn_certain = (Button) findViewById(R.id.btn_certain);
		btn_certain.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				ArrayList<String> list = new ArrayList<String>();
				Collection<String> c = mAdapter.map.values();
				Iterator<String> it = c.iterator();
				for (; it.hasNext();) {
					list.add(it.next());
				}
				for (int i = 0; i < list.size(); i++) {
					if (Bimp.drr.size() < 6) {
						Bimp.drr.add(list.get(i));
					}
				}

				// Intent myIntent = new Intent(ChoicePicGridActivity.this,
				// PublishFruitActivity.class);
				// startActivity(myIntent);
				finish();
			}

		});

	}

}
