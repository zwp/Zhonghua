package com.sinonetwork.zhonghua;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.model.farmingGuidInfoById;
import com.sinonetwork.zhonghua.model.farmingGuidInfoByIdB;
import com.sinonetwork.zhonghua.model.farmingGuidInfoByIdB.farmingGuidInfoByIdBs;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class SeasonManager extends Activity implements OnClickListener {
	private TextView crop_name, diqu, mubiaochanliang, chanlianggoucheng,
			variety, seed_type, explain, seed_content1, seed_content2,
			seed_content3, seed_content4, seed_qujian, shengyu_content,
			jieduan_content, bingchong_content, zaihai_content;
	private ImageView season_img1, season_img2, season_img3, season_img4, back;
	private RelativeLayout season_rl1, season_rl2, season_rl3, season_rl4;
	private LinearLayout season_ll1, season_ll2, season_ll3, season_ll4;
	private boolean ischoose = true;

	private String id;
	private String hehe;
	private List<farmingGuidInfoByIdBs> datas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.season_manager);
		//寻找控件，此处应该加一个条目和缩放功能
		crop_name = (TextView) findViewById(R.id.crop_name);
		diqu = (TextView) findViewById(R.id.diqu);
		mubiaochanliang = (TextView) findViewById(R.id.mubiaochanliang);
		chanlianggoucheng = (TextView) findViewById(R.id.chanlianggoucheng);
		// seed_qujian = (TextView) findViewById(R.id.seed_qujian);
		variety = (TextView) findViewById(R.id.variety);
		seed_type = (TextView) findViewById(R.id.seed_type);
		explain = (TextView) findViewById(R.id.explain);
		// seed_content1 = (TextView) findViewById(R.id.seed_content1);
		// seed_content2 = (TextView) findViewById(R.id.seed_content2);
		// seed_content3 = (TextView) findViewById(R.id.seed_content3);
		// seed_content4 = (TextView) findViewById(R.id.seed_content4);
		shengyu_content = (TextView) findViewById(R.id.shengyu_content);
		jieduan_content = (TextView) findViewById(R.id.jieduan_content);
		bingchong_content = (TextView) findViewById(R.id.bingchong_content);
		zaihai_content = (TextView) findViewById(R.id.zaihai_content);
		season_img1 = (ImageView) findViewById(R.id.season_img1);
		back = (ImageView) findViewById(R.id.back);
		season_img2 = (ImageView) findViewById(R.id.season_img2);
		season_img3 = (ImageView) findViewById(R.id.season_img3);
		season_img4 = (ImageView) findViewById(R.id.season_img4);
		season_rl1 = (RelativeLayout) findViewById(R.id.season_rl1);
		season_rl2 = (RelativeLayout) findViewById(R.id.season_rl2);
		season_rl3 = (RelativeLayout) findViewById(R.id.season_rl3);
		season_rl4 = (RelativeLayout) findViewById(R.id.season_rl4);
		season_ll1 = (LinearLayout) findViewById(R.id.season_ll1);
		season_ll2 = (LinearLayout) findViewById(R.id.season_ll2);
		season_ll3 = (LinearLayout) findViewById(R.id.season_ll3);
		season_ll4 = (LinearLayout) findViewById(R.id.season_ll4);
		Bundle bundle = getIntent().getExtras();
		id = bundle.getString("id");

		// Log.v("zhong","id--"+id);
		season_rl1.setOnClickListener(this);
		season_rl2.setOnClickListener(this);
		season_rl3.setOnClickListener(this);
		season_rl4.setOnClickListener(this);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		loadData("getfarmingGuidInfoById", id);

		// seed_content1.setOnClickListener(listener);
		// seed_content2.setOnClickListener(listener);
		// seed_content3.setOnClickListener(listener);
		// seed_content4.setOnClickListener(listener);

	}

	// private OnClickListener listener = new OnClickListener() {
	//
	// @Override
	// public void onClick(View v) {
	// // TODO Auto-generated method stub
	// switch (v.getId()) {
	// case R.id.seed_content1:
	// loadData2("getfarmingGuidInfoById",id,"1");
	// break;
	// case R.id.seed_content2:
	// loadData2("getfarmingGuidInfoById",id,"2");
	// break;
	// case R.id.seed_content3:
	// loadData2("getfarmingGuidInfoById",id,"3");
	// break;
	// case R.id.seed_content4:
	// loadData2("getfarmingGuidInfoById",id,"4");
	// break;
	//
	// default:
	// break;
	// }
	// }
	// };

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.season_rl1:
			if (ischoose) {
				season_ll1.setVisibility(View.VISIBLE);
				season_img1.setImageResource(R.drawable.arrow_down);
				ischoose = false;
				loadData2("getfarmingGuidInfoById", id, "1");
				// Toast.makeText(SeasonManager.this, "数据的id是=====" + id,
				// Toast.LENGTH_SHORT).show();
			} else {
				season_ll1.setVisibility(View.GONE);
				season_img1.setImageResource(R.drawable.arrow_right);
				ischoose = true;
			}

			break;
		case R.id.season_rl2:
			if (ischoose) {
				season_ll2.setVisibility(View.VISIBLE);
				season_img2.setImageResource(R.drawable.arrow_down);
				ischoose = false;
				loadData2("getfarmingGuidInfoById", id, "2");
				// Toast.makeText(SeasonManager.this, "数据的id是=====" + id,
				// Toast.LENGTH_SHORT).show();
			} else {
				season_ll2.setVisibility(View.GONE);
				season_img2.setImageResource(R.drawable.arrow_right);
				ischoose = true;
			}

			break;
		case R.id.season_rl3:
			if (ischoose) {
				season_ll3.setVisibility(View.VISIBLE);
				season_img3.setImageResource(R.drawable.arrow_down);
				ischoose = false;
				loadData2("getfarmingGuidInfoById", id, "3");
				// Toast.makeText(SeasonManager.this, "数据的id是=====" + id,
				// Toast.LENGTH_SHORT).show();
			} else {
				season_ll3.setVisibility(View.GONE);
				season_img3.setImageResource(R.drawable.arrow_right);
				ischoose = true;

			}

			break;
		case R.id.season_rl4:
			if (ischoose) {
				season_ll4.setVisibility(View.VISIBLE);
				season_img4.setImageResource(R.drawable.arrow_down);
				ischoose = false;
				loadData2("getfarmingGuidInfoById", id, "4");
				// Toast.makeText(SeasonManager.this, "数据的id是=====" + id,
				// Toast.LENGTH_SHORT).show();
			} else {
				season_ll4.setVisibility(View.GONE);
				season_img4.setImageResource(R.drawable.arrow_right);
				ischoose = true;
			}

			break;

		default:
			break;
		}
	}

	public void loadData(String method, String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		FastJsonRequest<farmingGuidInfoById> fastJson = new FastJsonRequest<farmingGuidInfoById>(
				Method.POST, URLAddress.farmingGuidInfoByIdURL,
				farmingGuidInfoById.class, null, map,
				new Response.Listener<farmingGuidInfoById>() {
					@Override
					public void onResponse(farmingGuidInfoById weather) {
						// Logger.e("数据============"+weather.);
						// 赋值
						// data.addAll(weather.getResultdata());
						// if(data.size()==0||data==null){
						// Toast.makeText(SeasonManager.this, "没有数据",
						// Toast.LENGTH_SHORT).show();
						// }
						// adapter.notifyDataSetChanged();

						// private TextView crop_name, diqu, mubiaochanliang,
						// chanlianggoucheng,
						// seed_qujian, variety, seed_type, explain,
						// seed_content;
						if (weather.getResultdata() == null) {
							Toast.makeText(SeasonManager.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							crop_name.setText(weather.getResultdata().getName()
									.toString());
							diqu.setText(weather.getResultdata().getArea()
									.toString());
							mubiaochanliang.setText(weather.getResultdata()
									.getTargetYield().toString());
							chanlianggoucheng.setText(weather.getResultdata()
									.getYieldComponent().toString());
							variety.setText(weather.getResultdata().getBreed()
									.toString());
							explain.setText(weather.getResultdata()
									.getExplain().toString());
							seed_type.setText(weather.getResultdata()
									.getSeedTypes().toString());
						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(SeasonManager.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

	public void loadData2(String method, String id, String type) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		map.put("type", type);
		hehe = type;
		datas = new ArrayList<farmingGuidInfoByIdBs>();
		FastJsonRequest<farmingGuidInfoByIdB> fastJson = new FastJsonRequest<farmingGuidInfoByIdB>(
				Method.POST, URLAddress.farmingGuidInfoByIdBURL,
				farmingGuidInfoByIdB.class, null, map,
				new Response.Listener<farmingGuidInfoByIdB>() {
					@Override
					public void onResponse(farmingGuidInfoByIdB weather) {
						// Logger.e("数据============"+weather.);
						// 赋值
						// data.addAll(weather.getResultdata());
						// if(data.size()==0||data==null){
						// Toast.makeText(SeasonManager.this, "没有数据",
						// Toast.LENGTH_SHORT).show();
						// }
						// adapter.notifyDataSetChanged();

						// private TextView crop_name, diqu, mubiaochanliang,
						// chanlianggoucheng,
						// seed_qujian, variety, seed_type, explain,
						// seed_content;
						if (weather.getResultdata() == null) {
							Toast.makeText(SeasonManager.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							// Toast.makeText(SeasonManager.this, "有数据",
							// Toast.LENGTH_SHORT).show();
							datas.addAll(weather.getResultdata());
							StringBuffer shuju = new StringBuffer();
							for (int i = 0; i < datas.size(); i++) {
								shuju.append(datas.get(i).getContext()
										.toString()
										+ ",");
								// Logger.e("getContext是======"
								// + datas.get(i).getContext().toString()
								// + "走的第=" + i + "次");
							}

							String neirong = shuju.toString();
							Logger.e("数据时==========" + neirong.toString());
							if ("1".equals(hehe)) {
								shengyu_content.setText(neirong.toString());

							} else if ("2".equals(hehe)) {
								jieduan_content.setText(neirong.toString());
							} else if ("3".equals(hehe)) {
								bingchong_content.setText(neirong.toString());
							} else if ("4".equals(hehe)) {
								zaihai_content.setText(neirong.toString());
							}
						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(SeasonManager.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

}
