package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.ResultAdapter;
import com.sinonetwork.zhonghua.model.serarchCropProtection;
import com.sinonetwork.zhonghua.model.serarchCropProtection.serarchCropProtections;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class Result extends Activity {
	private ListView listView;
	private ResultAdapter adapter;
	private ImageView back;
	private List<serarchCropProtections> data;
	
	private String pageRows="10";		//每页条数（默认10条）
	private String curPage="1";		//当前页（默认第一页）
	private String bhoryy="0";		//标识字段，见上面注<1>
	private String cropId;		//作物id，单选
	private String symptomsIds;		//症状id，多选，逗号隔开
	
	private TextView result_choose_crop_name;		//作物名称
	private TextView result_choose_symptom_name;		//选择的症状
	
	private String bingzhengId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result);
		listView = (ListView) findViewById(R.id.result_lv);
		back = (ImageView) findViewById(R.id.back);
		result_choose_crop_name = (TextView) findViewById(R.id.result_choose_crop_name);
		result_choose_symptom_name = (TextView) findViewById(R.id.result_choose_symptom_name);
		
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		cropId = bundle.getString("id");
		symptomsIds = bundle.getString("symptomsIds");		
		result_choose_symptom_name.setText(bundle.getString("symptomsNames"));
		result_choose_crop_name.setText(bundle.getString("name"));
		



		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		listView.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
//				for (int i = 0; i < title.length; i++) {
//					if (position == i) {
//						// Intent intent = new Intent(Result.this,
//						// Seed_SelectionDe.class);
//						// startActivity(intent);
//					}
//				}
				
				

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				// for (int i = 0; i < title.length; i++) {
				// if (position == i) {
				// // Intent intent = new Intent(Result.this,
				// // Seed_SelectionDe.class);
				// // startActivity(intent);
				// }
				// }

//				for (int j = 0; j < title.length; j++) {
//					if (position == j) {
						Intent intent = new Intent(Result.this,
								PabulumDetail.class);
						Bundle bundle = new Bundle();
						bundle.putString("id", data.get(position).getId().toString());
						intent.putExtras(bundle);
						startActivity(intent);
//					}
//
//				}
			}
		});
		loadData("serarchCropProtection", pageRows, curPage, bhoryy, cropId, symptomsIds);
		Logger.e("serarchCropProtection，"+"pageRows====="+pageRows+"，curPage====="+curPage+"，bhoryy====="+bhoryy+"，cropId====="+cropId+"，symptomsIds====="+symptomsIds);
		adapter = new ResultAdapter(Result.this, data);
		adapter.setData(data);
		listView.setAdapter(adapter);
	}
	
	public void loadData(String method,String pageRows,String curPage,String bhoryy,String cropId,String symptomsIds) {
//	Toast.makeText(Result.this, "来咯来咯", Toast.LENGTH_SHORT).show();
	Map<String, String> map = new HashMap<String, String>();
	map.put("method", method);
	map.put("pageRows", pageRows);
	map.put("curPage", curPage);
	map.put("bhoryy", bhoryy);
	map.put("cropId", cropId);
	map.put("symptomsIds", symptomsIds);
	data = new ArrayList<serarchCropProtections>();
	FastJsonRequest<serarchCropProtection> fastJson = new FastJsonRequest<serarchCropProtection>(
			Method.POST, URLAddress.serarchCropProtectionURL, serarchCropProtection.class,
			null, map, new Response.Listener<serarchCropProtection>() {
				@Override
				public void onResponse(serarchCropProtection weather) {
					// 赋值
					if(weather.getResultdata()==null){
						Toast.makeText(Result.this, "数据为空"+weather.getResultdata().get(0).getCpName(),
								Toast.LENGTH_SHORT).show();
					}else{
//						Toast.makeText(Result.this, "有数据"+weather.getResultdata().get(0).getCpName(),
//								Toast.LENGTH_SHORT).show();
					data.addAll(weather.getResultdata());
					adapter.notifyDataSetChanged();
					}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError arg0) {
					// TODO Auto-generated method stub
					Logger.e("No3");
					Logger.e("VolleyError===" + arg0.getMessage());
					Toast.makeText(Result.this, "服务器异常！",
							Toast.LENGTH_SHORT).show();
				}
			});
	Logger.e("No4"+fastJson);
	RequestManager.addRequest(fastJson, this);
}
	
	
	
}
