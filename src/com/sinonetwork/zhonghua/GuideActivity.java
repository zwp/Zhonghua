package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class GuideActivity extends Activity {

	private ViewPager vp;
	private ImageView iv_1;
	private ImageView iv_2;
	private ImageView iv_3;
	private ArrayList<View> viewpages;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_guide);

		vp = (ViewPager) this.findViewById(R.id.vp);
		iv_1 = (ImageView) this.findViewById(R.id.iv_1);
		iv_2 = (ImageView) this.findViewById(R.id.iv_2);
		iv_3 = (ImageView) this.findViewById(R.id.iv_3);
		vp.setPageMargin(2);
		initData();

		vp.setAdapter(new MyAdapter());

		vp.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				iv_1.setImageResource(R.drawable.page_indicator_unfocused);
				iv_2.setImageResource(R.drawable.page_indicator_unfocused);
				iv_3.setImageResource(R.drawable.page_indicator_unfocused);
				if (position == 0) {
					iv_1.setImageResource(R.drawable.page_indicator_focused);
				} else if (position == 1) {
					iv_2.setImageResource(R.drawable.page_indicator_focused);
				} else if (position == 2) {
					iv_3.setImageResource(R.drawable.page_indicator_focused);
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});

	}

	private void initData() {
		LayoutInflater inflater = LayoutInflater.from(this);
		View view = inflater.inflate(R.layout.guide_item_01, null);
		viewpages = new ArrayList<View>();
		viewpages.add(view);
		view = inflater.inflate(R.layout.guide_item_02, null);
		viewpages.add(view);
		view  = inflater.inflate(R.layout.guide_item_03, null);
		viewpages.add(view);

		Button start = (Button) view.findViewById(R.id.kaishishiyong);
		start.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(GuideActivity.this, MainActivity.class));
				GuideActivity.this.finish();
			}
		});

	}

	class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {

			return viewpages.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {

			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {

			container.removeView(viewpages.get(position));
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {

			container.addView(viewpages.get(position));
			return viewpages.get(position);
		}

	}

}
