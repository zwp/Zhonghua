package com.sinonetwork.zhonghua;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class MessageActivity extends Activity implements OnClickListener {

	private TextView topTitle;
	private ImageView back;
	private LinearLayout choose_people_linearlayout;
	private TextView tv_people;
	private EditText message_content;
	private Button btn_send;
	private Button btn_clear;
	private final int PICK_CODE = 100010;
	private String username;
	private String usernumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		topTitle = (TextView) this.findViewById(R.id.top_title_textview);
		back = (ImageView) this.findViewById(R.id.back);
		choose_people_linearlayout = (LinearLayout) this
				.findViewById(R.id.choose_people_linearlayout);
		tv_people = (TextView) this.findViewById(R.id.tv_people);
		message_content = (EditText) this.findViewById(R.id.message_content);
		btn_send = (Button) this.findViewById(R.id.send);
		btn_clear = (Button) this.findViewById(R.id.clear);
		topTitle.setText("发送短信");
		back.setOnClickListener(this);
		choose_people_linearlayout.setOnClickListener(this);
		btn_send.setOnClickListener(this);
		btn_clear.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back:
			this.finish();
			break;
		case R.id.choose_people_linearlayout:

			Intent intent = new Intent(Intent.ACTION_PICK,
					ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_CODE);
			break;
		case R.id.send:
			if (usernumber == null) {
				Toast.makeText(MessageActivity.this, "请选择联系人！", 2000).show();
				return;
			}
			if (message_content.getText().toString().equals("")) {
				Toast.makeText(MessageActivity.this, "短信内容不能为空！", 2000).show();
				return;
			}
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

			HttpHelp.getInstance().send(
					HttpMethod.GET,
					URLAddress.getMessageToOthers(usernumber, message_content
							.getText().toString()),
					new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {
						}

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							Toast.makeText(MessageActivity.this, "发送成功", 2000)
									.show();
							message_content.setText("");
						}
					});
			break;
		case R.id.clear:
			new AlertDialog.Builder(MessageActivity.this)
					.setTitle("确定清空？")
					.setIcon(android.R.drawable.ic_dialog_info)
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									message_content.setText("");
									dialog.dismiss();
								}
							})
					.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).show();
			break;
		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == PICK_CODE) {
				ContentResolver reContentResolverol = getContentResolver();
				Uri contactData = data.getData();
				@SuppressWarnings("deprecation")
				Cursor cursor = managedQuery(contactData, null, null, null,
						null);
				cursor.moveToFirst();
				username = cursor
						.getString(cursor
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				String contactId = cursor.getString(cursor
						.getColumnIndex(ContactsContract.Contacts._ID));
				Cursor phone = reContentResolverol.query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = " + contactId, null, null);
				while (phone.moveToNext()) {
					usernumber = phone
							.getString(phone
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				}
				tv_people.setText("选择联系人：" + username + "(" + usernumber + ")");

			}
		}

	}

}
