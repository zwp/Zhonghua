package com.sinonetwork.zhonghua.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class MyImageView extends ImageView {

	private Paint paint;
	private int width;
	private int height;

	public MyImageView(Context context) {
		super(context);
	}

	public MyImageView(Context context, AttributeSet attr) {
		super(context, attr);
		paint = new Paint();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		width = widthMeasureSpec;
		height = heightMeasureSpec;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// canvas.drawText("wocao", x, y, paint);
	}
}
