package com.sinonetwork.zhonghua;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.R.string;
import com.sinonetwork.zhonghua.adapter.QixiangAdapter;
import com.sinonetwork.zhonghua.model.TAwss;
import com.sinonetwork.zhonghua.model.Warning;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.HttpUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class Qixiang extends LandBaseActivity implements AMapLocationListener,
		Runnable {
	private string[] listt;
	private ListView listView;
	private QixiangAdapter adapter;
	private AMapLocation aMapLocation;// 用于判断定位超时
	private int index;
	private double getLat;
	private double getLng;
	private ImageView back;
	private ImageView qixiang_warm;
	private ImageView qixiang_solid;
	private ImageView qixiang_query;

	private TextView qixiang_temp_content; // 温度
	private TextView qixiang_humidity_content; // 湿度
	private TextView qixiang_precipitation_content; // 降水量
	private TextView qixiang_wushuangqi_content; // 无霜期
	private TextView qixiang_sunlight_content; // 日照时长
	private TextView qixiang_summarize1; // 风向
	private TextView qixiang_summarize2; // 风力
	private TextView qixiang_time; // 发布日期
	private TextView qixiang_address; // 地区
	private TextView qixiang_temperature; // 摄氏度
	private LocationManagerProxy aMapLocManager = null;

	private double latitude = 0.0;
	private double longitude = 0.0;

	private LocationManager locationManager;
	protected ArrayList<Warning> list;
	protected JSONObject jsonObj;
	protected JSONObject jsonObjAwss;

	private String cityCode = "";
	private String cityma;
	private String cityquyu = "";

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qixiang);
		initView();
		index = 0;
		init();
		// 灾害预警
		qixiang_warm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != list) {
					Intent intent = new Intent(Qixiang.this, QixiangWarm.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("data", list);
					intent.putExtras(bundle);
					startActivity(intent);
				}

			}
		});
		// 土壤墒情
		qixiang_solid.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Qixiang.this, QixiangSolid.class);
				intent.putExtra("data", cityma);
				startActivity(intent);
			}
		});
		// 积温查询
		qixiang_query.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Qixiang.this, QixiangQuery.class);
				intent.putExtra("data", cityma);
				startActivity(intent);
			}
		});
		//返回键
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

	}
	//初始化控件
	private void initView() {
		listView = (ListView) findViewById(R.id.qixiang_lv);
		back = (ImageView) findViewById(R.id.back);
		qixiang_warm = (ImageView) findViewById(R.id.qixiang_warm);
		qixiang_solid = (ImageView) findViewById(R.id.qixiang_solid);
		qixiang_query = (ImageView) findViewById(R.id.qixiang_query);
		qixiang_temp_content = (TextView) findViewById(R.id.qixiang_temp_content);
		qixiang_humidity_content = (TextView) findViewById(R.id.qixiang_humidity_content);
		qixiang_precipitation_content = (TextView) findViewById(R.id.qixiang_precipitation_content);
		qixiang_wushuangqi_content = (TextView) findViewById(R.id.qixiang_wushuangqi_content);
		qixiang_sunlight_content = (TextView) findViewById(R.id.qixiang_sunlight_content);
		qixiang_summarize1 = (TextView) findViewById(R.id.qixiang_summarize1);
		qixiang_summarize2 = (TextView) findViewById(R.id.qixiang_summarize2);
		qixiang_time = (TextView) findViewById(R.id.qixiang_time);
		qixiang_address = (TextView) findViewById(R.id.qixiang_address);
		qixiang_temperature = (TextView) findViewById(R.id.qixiang_temperature);
		
	}

	@Override
	protected void onStart() {
//		init();
		super.onStart();
	}

	// 初始化定位
	private void init() {
		aMapLocManager = LocationManagerProxy.getInstance(this);
		/*
		 * mAMapLocManager.setGpsEnable(false);//
		 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
		 * API定位采用GPS和网络混合定位方式
		 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
		 */
		aMapLocManager.requestLocationData(LocationProviderProxy.AMapNetwork,
				2000, 10, this);
		aMapLocManager.setGpsEnable(false);//纯网络定位
		handler.postDelayed(this, 12000);// 设置超过12秒还没有定位到就停止定位
	}

	/**
	 * 混合定位回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation location) {
		if (location != null) {
			// mListener.onLocationChanged(location);// 显示系统小蓝点
			this.aMapLocation = location;// 判断超时机制
			getLat = location.getLatitude();
			getLng = location.getLongitude();

			String desc = "";
			Bundle locBundle = location.getExtras();
			if (locBundle != null) {
				cityCode = locBundle.getString("citycode");
				desc = locBundle.getString("desc");
				cityquyu = location.getDistrict();// 定位到区域 套转到积温查询界面时
			}
			String str = ("定位成功:(" + getLat + "," + getLng + ")"
					+ "\n精    度    :" + location.getAccuracy() + "米"
					+ "\n定位方式:" + location.getProvider() + "\n定位时间:"
					+ "\n城市编码:" + cityCode + "\n位置描述:" + desc + "\n省:"
					+ location.getProvince() + "\n市:" + location.getCity()
					+ "\n区(县):" + location.getDistrict() + "\n区域编码:" + location
					.getAdCode());

			 Log.i("gxx", str);
			initDingWei();
			// 初次定位成功去接口请求数据
			if (index == 0) {
				index++;
				try {
					loadWarningData(location.getProvince(), location.getCity(),
							location.getDistrict());
					loadCityData(location.getProvince(), location.getCity(),
							location.getDistrict());
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 获取气象数据
	 * 
	 * @param method
	 *            方法名
	 * @param latitude
	 *            纬度
	 * @param longitude
	 *            //经度
	 */
	public void RiZhaoShiChang(String method, String latitude, String longitude) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("latitude", latitude);
		map.put("longitude", longitude);
		//使用Volley框架获取信息
		FastJsonRequest<TAwss> fastJson = new FastJsonRequest<TAwss>(
				Method.POST, URLAddress.TAwssURL, TAwss.class, null, map,
				new Response.Listener<TAwss>() {
					@Override
					public void onResponse(TAwss weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(Qixiang.this, "没有日照数据",
									Toast.LENGTH_SHORT).show();
						} else {
							// qixiang_wushuangqi_content.setText(weather.getResultdata());
							// //无霜期
							qixiang_sunlight_content.setText(weather
									.getResultdata().getSunShine().toString()
									+ "小时"); // 日照时长
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						Toast.makeText(Qixiang.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}

	/**
	 * 获取灾害预警列表
	 */
	private void loadWarningData(final String provinceName,
			final String cityName, final String countyName) {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					list = LandInfoParser.getWarningList(provinceName,
							cityName, countyName);

				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}
	/**
	 * 显示灾害预警列表的信息
	 */
	private void setView() {

		if (null != list) {
			if (list.size() > 0) {
				adapter = new QixiangAdapter(Qixiang.this, list);
				listView.setAdapter(adapter);
				listView.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						Intent intent = new Intent(Qixiang.this,
								WarningDetailActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("data", list.get(arg2));
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
			} else {
				showShortToast("当前地区没有灾害预警信息");
			}

		}

	}

	
	private void initDingWei() {
		String shuju = getJson("siteInfo.json");// 把文件中的json信息读取为json数据
		String a = new String(shuju);
		StringBuffer sb = new StringBuffer();
		sb.append(cityquyu);
		sb.delete(sb.length() - 1, sb.length());
		String diqu2 = sb.toString();

		// 使用自己的类来对数据进行解析
		JiWenCityCode code = JSON.parseObject(a, JiWenCityCode.class);
		for (int i = 0; i < code.getCity().size(); i++) {
			if (diqu2.equals(code.getCity().get(i).getC())) {
				cityma = code.getCity().get(i).getA();
				// Log.i("gxx", "城市的编码需要传递的："+cityma);
			}
		}
	}

	private String getJson(String fileName) {

		StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					getAssets().open(fileName)));
			String line;
			while ((line = bf.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	private void loadCityData(String province, String city, String district)
			throws JSONException {
		String siteInfo = getFromAssets("siteInfo.json");

		// Log.v(TAG, "province--" + province);
		// Log.v(TAG, "city--" + city);
		// Log.v(TAG, "district--" + district);
		// district="昌平";
		JSONObject obj = new JSONObject(siteInfo);
		JSONArray cityArray = obj.getJSONArray("city");

		int size = cityArray.length();
		String code = "";
		for (int i = 0; i < size; i++) {
			JSONObject o = cityArray.getJSONObject(i);
			if (province.indexOf(o.getString("g")) > -1) {

				if (city.indexOf(o.getString("e")) > -1) {

					if (district.indexOf(o.getString("c")) > -1) {
						code = o.getString("a");
					} else {
					}

				}

			}

		}
		if (code.length() == 0) {
			for (int i = 0; i < size; i++) {
				JSONObject o = cityArray.getJSONObject(i);
				if (province.indexOf(o.getString("g")) > -1) {

					if (city.indexOf(o.getString("e")) > -1) {

						if (city.indexOf(o.getString("c")) > -1) {
							code = o.getString("a");
						}

					}

				}

			}

		}
		final String cityCode = code;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		final String dateStr = formatter.format(curDate);
		Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time Zone资料。
		t.setToNow(); // 取得系统时间。
		int year = t.year;
		int month = t.month + 1;
		int date = t.monthDay;
		int hour = t.hour; // 0-23
		int minute = t.minute;//0-60
		int second = t.second;
		//时间的处理
		if(minute>=0 && minute<10){
			 qixiang_time.setText("" + year + "年" + month + "月" + date + "日"
						+ "    " + hour + ":" +"0"+ minute);
		}else{
		   qixiang_time.setText("" + year + "年" + month + "月" + date + "日"
				+ "    " + hour + ":" + minute);}
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					String url = "http://product.xn121.com/api/?areaid="
							+ cityCode
							+ "&type=observe&date="
							+ dateStr
							+ "&appid=ade570&key=41a6e865052ade5702d2e773f2164a5f";
					String urlAwss = "http://product.xn121.com/api/?areaid="
							+ cityCode
							+ "&type=awss&date="
							+ dateStr
							+ "&appid=ade570&key=41a6e865052ade5702d2e773f2164a5f";
//					Log.i("gxx", "url--" + url);
//					Log.i("gxx", "url2--" + url2);
//					Log.i("gxx", "urlAwss--" + urlAwss);
//					Log.i("gxx", "urlAwss2--" + urlAwss2);
//					Log.i("gxx", "datastr:"+dateStr);
//					Log.i("gxx", "codecity:"+cityCode);
					jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);
					jsonObjAwss = HttpUtil.getJSONObjFromUrlByGet(urlAwss,
							false);
//					Log.i("gxx", "jsonObj:"+jsonObj);
//					Log.i("gxx", "jsonObjAwss:"+jsonObjAwss);
					// Log.v("zhong", "jsonObjAwss.getString--" +
					// jsonObjAwss.toString());	
					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							try {
								setCityView();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();

	}

	private void setCityView() throws JSONException {
		// Log.i("gxx", "setcityview方法");
		if (null != jsonObj) {
			if (jsonObj.getJSONArray("datas").length() > 0) {
				JSONObject obj = (JSONObject) jsonObj.getJSONArray("datas")
						.get(0);
				qixiang_precipitation_content.setText(obj.getString("d17")
						+ "mm"); // 降水量
				qixiang_address.setText(obj.getString("d5")); // 地址
				// Log.i("gxx", "set的获取的地址：" + obj.getString("d5"));
				qixiang_temp_content.setText(obj.getString("d13") + "℃"); // 温度
//				Log.i("gxx", "温度：" + obj.getString("d13") + "℃");
//				Log.i("gxx", "shuju:" + obj);
				qixiang_temperature.setText(obj.getString("d13") + "℃"); // 温度
				qixiang_humidity_content.setText(obj.getString("d16") + "%"); // 湿度
//				Log.i("gxx", "湿度："+obj.getString("d16") + "%");
				qixiang_summarize2.setText("风力" + obj.getString("d15") + "级"); // 风
			}

		}
		if (null != jsonObjAwss) {

			// Log.v("zhong", "jsonObjAwss.getString--" +
			// jsonObjAwss.toString());
			if (jsonObjAwss.getJSONArray("datas").length() > 0) {
				JSONObject o = (JSONObject) jsonObjAwss.getJSONArray("datas")
						.get(0);
				// if(o.getString("d18").equals("0")){
				// qixiang_sunlight_content.setText( "0小时"); // 时长
				// }else{
				Double time = Double.valueOf(o.getString("d18")) * 0.1;
				DecimalFormat df = new DecimalFormat("######0.00");

				String t = df.format(time);
				qixiang_sunlight_content.setText("" + t + "小时"); // 时长
				// }

			}
		}

	}

	@Override
	public void run() {
		if (aMapLocation == null) {

			stopLocation();// 销毁掉定位12秒内还没有定位成功，停止定位
		}
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}

	public String getFromAssets(String fileName) {
		String result = "";
		try {
			InputStream in = getResources().getAssets().open(fileName);
			// 获取文件的字节数
			int lenght = in.available();
			// 创建byte数组
			byte[] buffer = new byte[lenght];
			// 将文件中的数据读到byte数组中
			in.read(buffer);
			result = EncodingUtils.getString(buffer, "UTF-8");// 你的文件的编码
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		stopLocation();// 停止定位
	}

	/**
	 * 销毁定位
	 */
	private void stopLocation() {
		if (aMapLocManager != null) {
			aMapLocManager.removeUpdates(this);
			aMapLocManager.destroy();
		}
		aMapLocManager = null;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

}
