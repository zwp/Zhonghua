package com.sinonetwork.zhonghua;

import java.util.List;

public class JsonTuRang {

	private String resultcode;

	private String resultdesc;

	private String curPage;

	private String totalPages;

	private JsonTuRangx resultdata;

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getResultdesc() {
		return resultdesc;
	}

	public void setResultdesc(String resultdesc) {
		this.resultdesc = resultdesc;
	}

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public JsonTuRangx getResultdata() {
		return resultdata;
	}

	public void setResultdata(JsonTuRangx resultdata) {
		this.resultdata = resultdata;
	}

	@Override
	public String toString() {
		return "resultcode=" + resultcode + ", resultdesc=" + resultdesc + ", curPage=" + curPage + ", totalPages="
				+ totalPages + ", resultdata=" + resultdata;
	}

}
