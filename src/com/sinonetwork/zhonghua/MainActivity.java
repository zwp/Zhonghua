package com.sinonetwork.zhonghua;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.fragment.KnowFragment;
import com.sinonetwork.zhonghua.fragment.MyFragment;
import com.sinonetwork.zhonghua.fragment.SolutionFragment;
import com.sinonetwork.zhonghua.fragment.ZiXunFragment;
import com.sinonetwork.zhonghua.utils.AndroidUtils;
import com.sinonetwork.zhonghua.utils.Commons;
import com.sinonetwork.zhonghua.utils.HttpUtil;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class MainActivity extends FragmentActivity implements OnClickListener {
	private ZiXunFragment ziXunFragment;
	private ZiXunUser ziXunUser;
	private KnowFragment knowFragment;
	private SolutionFragment solutionFragment;
	private ShopFragment shopFragment;
	private MyFragment myFragment;
	private View zixunlayout;
	private View knowlayout;
	private View solutionlayout;
	private View shoplayout;
	private View mylayout;
	private ImageView zixun;
	private ImageView know; 
	private ImageView solution;
	private ImageView shop;
	private ImageView my;
	private FragmentManager fragmentManager;
	private long exitTime;
	protected JSONObject versionObj;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Commons.DEVICEWIDTH = AndroidUtils.getScreenSize(this)[0];

		PrefUtil.savePref(this, "fromShop", "main");
		fragmentManager = getSupportFragmentManager();
		// 初始化布局元素
		initViews();

		// 第一次启动时选中第0个tab
		// setTabSelection(0);
		update();//检测版本更新
	}

	/**
	 * 在这里获取到每个需要用到的控件的实例，并给它们设置好必要的点击事件。
	 */
	private void initViews() {
//		寻找底部按钮控件
		zixunlayout = findViewById(R.id.zixun_layout);
		knowlayout = findViewById(R.id.know_layout);
		solutionlayout = findViewById(R.id.solution_layout);
		shoplayout = findViewById(R.id.shop_layout);
		mylayout = findViewById(R.id.my_layout);
		zixun = (ImageView) findViewById(R.id.zixun_image);
		know = (ImageView) findViewById(R.id.know_image);
		solution = (ImageView) findViewById(R.id.solution_image);
		shop = (ImageView) findViewById(R.id.shop_image);
		my = (ImageView) findViewById(R.id.my_image);
		zixunlayout.setOnClickListener(this);
		knowlayout.setOnClickListener(this);
		solutionlayout.setOnClickListener(this);
		shoplayout.setOnClickListener(this);
		mylayout.setOnClickListener(this);

		ziXunFragment = new ZiXunFragment();
		knowFragment = new KnowFragment();
		solutionFragment = new SolutionFragment();
		shopFragment = new ShopFragment();
		myFragment = new MyFragment();
		//将Fragment作为对象，以键值对的形式存储。
		Commons.mCache.put("ziXunFragment", ziXunFragment);
		Commons.mCache.put("knowFragment", knowFragment);
		Commons.mCache.put("solutionFragment", solutionFragment);
		Commons.mCache.put("shopFragment", shopFragment);
		Commons.mCache.put("myFragment", myFragment);
		//使用setArgument方法实现Activity和Fragment之间的数据交换。
		FragmentTransaction tx = fragmentManager.beginTransaction();
		tx.add(R.id.content, Commons.mCache.get("ziXunFragment"));
		tx.add(R.id.content, Commons.mCache.get("knowFragment"));
		tx.add(R.id.content, Commons.mCache.get("solutionFragment"));
		tx.add(R.id.content, Commons.mCache.get("shopFragment"));
		tx.add(R.id.content, Commons.mCache.get("myFragment"));
		//此处属于冗余代码，本次迭代时处理
		tx.show(Commons.mCache.get("ziXunFragment"));
		tx.hide(Commons.mCache.get("solutionFragment"));
		tx.hide(Commons.mCache.get("shopFragment"));
		tx.hide(Commons.mCache.get("myFragment"));
		tx.hide(Commons.mCache.get("knowFragment"));
		tx.commit();
	}
	
	//处理点击事件
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.zixun_layout:
			// 当点击了农业咨询tab时，选中第1个tab
			setTabSelection2(0);
			break;
		case R.id.know_layout:

			setTabSelection2(1);
			break;
		case R.id.solution_layout:

			setTabSelection2(2);
			break;
		case R.id.shop_layout:

			setTabSelection2(3);
			break;
		case R.id.my_layout:

			setTabSelection2(4);
			break;
		default:
			break;
		}
	}
	//当点击底部按钮时，显示的内容改变。
	private void setTabSelection2(int index) {
		clearSelection();//清除以前改变得。
		FragmentTransaction tx = fragmentManager.beginTransaction();//冗余代码，本次迭代需要修改的地方。
		switch (index) {
		case 0:
			//此处可用状态选择器来处理，属于冗余代码
			zixun.setImageResource(R.drawable.foot_menu_on01);
			tx.show(Commons.mCache.get("ziXunFragment"));
			tx.hide(Commons.mCache.get("solutionFragment"));
			tx.hide(Commons.mCache.get("shopFragment"));
			tx.hide(Commons.mCache.get("myFragment"));
			tx.hide(Commons.mCache.get("knowFragment"));
			((ZiXunFragment) Commons.mCache.get("ziXunFragment")).setHomeView();

			break;
		case 1:
			know.setImageResource(R.drawable.foot_menu_on02);
			tx.show(Commons.mCache.get("knowFragment"));
			tx.hide(Commons.mCache.get("solutionFragment"));
			tx.hide(Commons.mCache.get("shopFragment"));
			tx.hide(Commons.mCache.get("myFragment"));
			tx.hide(Commons.mCache.get("ziXunFragment"));
			break;

		case 2:
			solution.setImageResource(R.drawable.foot_menu_on03);
			tx.show(Commons.mCache.get("solutionFragment"));
			tx.hide(Commons.mCache.get("knowFragment"));
			tx.hide(Commons.mCache.get("shopFragment"));
			tx.hide(Commons.mCache.get("myFragment"));
			tx.hide(Commons.mCache.get("ziXunFragment"));
			break;
		case 3:
			shop.setImageResource(R.drawable.foot_menu_on05);

			Intent intent = new Intent(MainActivity.this, ShopActivity.class);
			startActivity(intent);
			break;
		case 4:
			my.setImageResource(R.drawable.foot_menu_on06);

			tx.show(Commons.mCache.get("myFragment"));
			tx.hide(Commons.mCache.get("knowFragment"));
			tx.hide(Commons.mCache.get("shopFragment"));
			tx.hide(Commons.mCache.get("solutionFragment"));
			tx.hide(Commons.mCache.get("ziXunFragment"));

			break;

		default:
			break;
		}
		tx.commit();

	}

	/**
	 * 根据传入的index参数来设置选中的tab页。
	 * 
	 * @param index
	 *            每个tab页对应的下标。0表示消息，1表示联系人，2表示动态，3表示设置。
	 */
	private void setTabSelection(int index) {
		// 每次选中之前先清楚掉上次的选中状态 ״̬
		clearSelection();
		// 开启一个Fragment事务
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		// 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
		hideFragments(transaction);
		switch (index) {
		case 0:
			// 当点击了农业咨询tab时，改变控件的图片和文字颜色
			zixun.setImageResource(R.drawable.foot_menu_on01);

			// if (ziXunFragment == null) {

			ziXunFragment = new ZiXunFragment();//属于冗余代码，本次迭代应处理。
			transaction.replace(R.id.content, (Fragment) ziXunFragment);
			// } else {
			// transaction.show(ziXunFragment);
			// }
			break;
		// case 0:
		// // 当点击了农业咨询tab时，改变控件的图片和文字颜色
		// zixun.setImageResource(R.drawable.foot_menu_on01);
		//
		// if (ziXunUser == null) {
		//
		// ziXunUser = new ZiXunUser();
		// transaction.replace(R.id.content, (Fragment) ziXunUser);
		// } else {
		// transaction.show(ziXunUser);
		// }
		// break;
		case 1:

			know.setImageResource(R.drawable.foot_menu_on02);
			// if (knowFragment == null) {

			knowFragment = new KnowFragment();
			transaction.replace(R.id.content, knowFragment);
			// } else {
			// transaction.show(knowFragment);
			// }
			break;
		case 2:

			solution.setImageResource(R.drawable.foot_menu_on03);
			// if (solutionFragment == null) {
			solutionFragment = new SolutionFragment();
			transaction.replace(R.id.content, solutionFragment);
			// } else {
			// transaction.show(solutionFragment);
			// }
			break;
		case 3:
		default:

			shop.setImageResource(R.drawable.foot_menu_on05);

			// if (shopFragment == null) {
			// 如果shopFragment为空，则创建一个并添加到界面上
			// shopFragment = new ShopFragment();
			// transaction.add(R.id.content, shopFragment);

			Intent intent = new Intent(MainActivity.this, ShopActivity.class);
			startActivity(intent);

			// } else {
			// 如果shopFragment不为空，则直接将它显示出来
			// transaction.show(shopFragment);

			// }
			break;
		case 4:

			my.setImageResource(R.drawable.foot_menu_on06);
			// if (myFragment == null) {
			myFragment = new MyFragment();
			transaction.replace(R.id.content, myFragment);
			// } else {
			// transaction.show(myFragment);
			// }
			break;
		}

		transaction.commit();
	}

	/**
	 * 清除掉所有的选中状态。
	 */
	private void clearSelection() {
		zixun.setImageResource(R.drawable.foot_menu01);
		know.setImageResource(R.drawable.foot_menu02);
		solution.setImageResource(R.drawable.foot_menu03);
		shop.setImageResource(R.drawable.foot_menu05);
		my.setImageResource(R.drawable.foot_menu06);

	}

	/**
	 * 将所有的Fragment都置为隐藏状态。
	 * 
	 * @param transaction
	 *            用于对Fragment执行操作的事务
	 */
	private void hideFragments(FragmentTransaction transaction) {
		if (ziXunFragment != null) {
			transaction.hide(ziXunFragment);
		}
		if (knowFragment != null) {
			transaction.hide(knowFragment);
		}
		if (solutionFragment != null) {
			transaction.hide(solutionFragment);
		}
		if (shopFragment != null) {
			transaction.hide(shopFragment);
		}
		if (myFragment != null) {
			transaction.hide(myFragment);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		((ZiXunFragment) Commons.mCache.get("ziXunFragment")).setHomeView();//登录模块
		((MyFragment) Commons.mCache.get("myFragment")).setUserName();
		if (PrefUtil.getStringPref(this, "fromShop").equals("shop")) {
			setTabSelection2(0);
			PrefUtil.savePref(this, "fromShop", "main");
		}
	}
	//检测当前有没有新版本
		private void update() {

			new Thread(new Runnable() {

				boolean success = true;
				final Handler handler = new Handler();

				@Override
				public void run() {

					try {
						JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(URLAddress.URLYI+"?method=getAllVersion", false);


						if (jsonObj.getString("resultcode").equals("ok")) {
							versionObj = jsonObj.getJSONObject("resultdata");
							success = true;
						}else{
							
							success = false;
						}
					} catch (Exception e) {
						success = false;
						e.printStackTrace();
					}

					handler.post(new Runnable() {

						@Override
						public void run() {
							if (success) {
								int v = 0;
								try {
									v = Integer.valueOf(versionObj.getString("versionNum"));
								} catch (NumberFormatException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								int currentV=getVersionCode(MainActivity.this);
								if(v>currentV){
									
									try {
										newVersionAlert(versionObj.getString("versionDescription"));
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							} else {

							}
						}
					});
				}
			}).start();
		}
			/**
		 * 获取版本code
		 * 
		 * @param context
		 * @return
		 */
		public static int getVersionCode(Context context) {
			try {
				return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return 0;
			}
		}
		/**
		 * 新版本提示框
		 */
		private void newVersionAlert(String summary) {
			final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).create();
			dialog.setCancelable(false);
			View v = View.inflate(MainActivity.this, R.layout.update_layout, null);
			TextView tv_summary = (TextView) v.findViewById(R.id.tv_summary);
			tv_summary.setText(summary);
			Button btn_cancle = (Button) v.findViewById(R.id.btn_cancle);
			Button btn_update = (Button) v.findViewById(R.id.btn_update);
			btn_cancle.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			btn_update.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://211.94.93.238/zhnyxxgc/download.html"));
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			});
			dialog.setView(v, 0, 0, 0, 0);
			dialog.show();
		}
		
	//点击返回键两次时退出（指定的时间内）
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {

			getExit();

			return true;
		} else
			return super.onKeyDown(keyCode, event);
	}

	private void getExit() {
		if ((System.currentTimeMillis() - exitTime) > 1500) {
			Toast.makeText(getApplicationContext(), "再按一次退出应用", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);
		}
	}
}
