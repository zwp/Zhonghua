package com.sinonetwork.zhonghua;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.model.Expert;
import com.sinonetwork.zhonghua.utils.Bimp;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.Commons;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.view.NoScrollGridView;

public class PublishQuestion extends Activity implements OnClickListener {
	// private GridView publish_pic;
	private ImageView iv;
	private ListView listView;
	private SimpleAdapter adapter;
	// private PublishQuestionListAdapter listAdapter;
	private List<Map<String, Object>> list;
	private List<Map<String, Object>> list1;
	private ImageView back;
	private RelativeLayout choose_expert_img;
	private StringBuilder imgs = new StringBuilder();
	private Expert expert;
	private PopupWindow popUpWindow;
	private ImageView ivPublish;
	private EditText tvTitle;
	private EditText tvContent;
	private TextView choosed_expert_name;
	private ImageView choosed_expert_img;
	private ProgressDialog progressDialog;
	private MyAdapter mAdapter;
	private NoScrollGridView mGridView;
	private List<String> drr = new ArrayList<String>();
	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				mAdapter.notifyDataSetChanged();
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.know_publish);
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		list = new ArrayList<Map<String, Object>>();
		tvTitle = (EditText) findViewById(R.id.publish_title);
		tvContent = (EditText) findViewById(R.id.publish_content);
		back = (ImageView) findViewById(R.id.back);
		ivPublish = (ImageView) findViewById(R.id.publish_photo);
		choosed_expert_name = (TextView) findViewById(R.id.choosed_expert_name);
		choosed_expert_img = (ImageView) findViewById(R.id.choosed_expert_img);
		mGridView = (NoScrollGridView) findViewById(R.id.publish_pic);
		mAdapter = new MyAdapter();
		mAdapter.update();
		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
				if (position == Bimp.bmp.size()) {
					// 调起相机
					showTakePhotoPopUpWindow(mGridView);
				}
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bimp.bmp.clear();
				Bimp.drr.clear();
				Bimp.max = 0;
				finish();
			}
		});

		choose_expert_img = (RelativeLayout) findViewById(R.id.choose_expert_img);
		choose_expert_img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PublishQuestion.this, ChooseExpert.class);
				startActivityForResult(intent, Commons.CHOOSE_EXPERT);
			}
		});
		ivPublish.setOnClickListener(this);

		// EventBus.getDefault().register(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mAdapter.update();
	}

	public void showPopupWindow(View view, View windowTokenView, boolean showAsDropDown) {
		if (popUpWindow == null) {
			popUpWindow = new PopupWindow();
			// 设置SelectPicPopupWindow的View
			popUpWindow.setContentView(view);
			// 设置SelectPicPopupWindow弹出窗体的宽
			if (showAsDropDown) {
				popUpWindow.setWidth(LayoutParams.WRAP_CONTENT);
			} else {
				popUpWindow.setWidth(LayoutParams.MATCH_PARENT);
			}
			// 设置SelectPicPopupWindow弹出窗体的高
			popUpWindow.setHeight(LayoutParams.WRAP_CONTENT);
			// 设置SelectPicPopupWindow弹出窗体可点击
			popUpWindow.setFocusable(true);
			if (!showAsDropDown) {
				// 设置SelectPicPopupWindow弹出窗体动画效果
				popUpWindow.setAnimationStyle(R.style.PopWindowAnim);
			}
			// 实例化一个ColorDrawable颜色为半透明
			ColorDrawable dw = new ColorDrawable(0xb0000000);
			// 设置SelectPicPopupWindow弹出窗体的背景
			popUpWindow.setBackgroundDrawable(dw);
		} else {
			popUpWindow.setContentView(view);
		}

		if (showAsDropDown) {
			popUpWindow.showAsDropDown(windowTokenView, 20, -120);
		} else {
			popUpWindow.showAtLocation(windowTokenView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
		}
	}

	public void closePopupWindow() {
		if (popUpWindow != null && popUpWindow.isShowing()) {
			popUpWindow.dismiss();
		}
	}

	/**
	 * view初始化
	 */

	public void showTakePhotoPopUpWindow(View windowTokenView) {
		View view = LayoutInflater.from(this).inflate(R.layout.select_photo_menu, null);
		showPopupWindow(view, windowTokenView, false);

		TextView takePhoto = (TextView) view.findViewById(R.id.take_photo);
		TextView selectFromAlbum = (TextView) view.findViewById(R.id.select_from_album);
		TextView cancel = (TextView) view.findViewById(R.id.cancel_photo);

		takePhoto.setOnClickListener(takePhotoButtonOnClickListener);
		selectFromAlbum.setOnClickListener(takePhotoButtonOnClickListener);
		cancel.setOnClickListener(takePhotoButtonOnClickListener);
	}

	private OnClickListener takePhotoButtonOnClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.take_photo:
				String tempName = System.currentTimeMillis() + ".png";
				File file = getPickPhotoTmpFile(tempName);
				Commons.phoneName = file.getAbsolutePath();
				openCamera(file);

				closePopupWindow();
				break;
			case R.id.select_from_album:
				Intent intent = new Intent(PublishQuestion.this, ChoicePicActivity.class);
				startActivity(intent);
				closePopupWindow();
				break;
			case R.id.cancel_photo:
				closePopupWindow();
				break;
			}
		}
	};

	public File getPickPhotoTmpFile(String tmpFileName) {

		File root = this.getExternalFilesDir(null);

		if (root != null) {
			String absPath = root.getAbsolutePath();
			File tmpFile = new File(absPath, tmpFileName);
			return tmpFile;
		}
		return null;
	}

	/**
	 * 打开相机
	 * 
	 * @param file
	 *            目标保存文件
	 */
	public void openCamera(File file) {
		if (file == null) {
			return;
		}

		try {
			Uri uri = Uri.fromFile(file);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
			startActivityForResult(intent, Commons.REQUEST_CODE_CAMERA);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == Commons.CHOOSE_EXPERT && resultCode == RESULT_OK) {
			expert = (Expert) data.getSerializableExtra("expert");
			if (expert != null) {
				choosed_expert_name.setText(expert.getRealName());
				BitmapHelp.getBitmapUtils(this).display(choosed_expert_img, URLAddress.TPURL + expert.getPictures());
			}
		} else if (requestCode == Commons.REQUEST_CODE_CAMERA) {
			if (Bimp.drr.size() < 6 && resultCode == -1) {
				Bimp.drr.add(Commons.phoneName);
				mAdapter.notifyDataSetChanged();
			}
		}

	}

	// public void onEvent(TakePhotoOutputEvent event) {
	// if (event == null) {
	// return;
	// }
	//
	// if ((event.requestCode == Commons.REQUEST_CODE_ALBUM || event.requestCode
	// == Commons.REQUEST_CODE_CAMERA) && event.resultCode ==
	// Activity.RESULT_OK) { // 相册
	// File file = new File(event.filePath);
	// File newFile = null;
	// try {
	// newFile =
	// BitmapHelp.saveBitmapFile(BitmapHelp.revitionImageSize(file.getAbsolutePath()),
	// Environment.getExternalStorageDirectory().getPath() + "/zhonghua/" +
	// System.currentTimeMillis() + ".jpg");
	// } catch (IOException e) {
	//
	// e.printStackTrace();
	// }
	//
	// if (iv == iv1) {
	// mDatas.put("iv1", newFile);
	// } else if (iv == iv2) {
	// mDatas.put("iv2", newFile);
	// } else if (iv == iv3) {
	// mDatas.put("iv3", newFile);
	// } else if (iv == iv4) {
	// mDatas.put("iv4", newFile);
	// } else if (iv == iv5) {
	// mDatas.put("iv5", newFile);
	// } else if (iv == iv6) {
	// mDatas.put("iv6", newFile);
	// }
	// BitmapHelp.getBitmapUtils(this).display(iv, newFile.getAbsolutePath());
	// // iv.setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
	//
	// }
	// }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.publish_photo:
			if (tvTitle.getText().toString().equals("")) {
				Toast.makeText(this, "请填写问题标题", 2000).show();
				return;
			}
			if (tvContent.getText().toString().equals("")) {
				Toast.makeText(this, "请填写问题内容", 2000).show();
				return;
			}

			if (drr.size() != 0) {
				Iterator<String> iterators = drr.iterator();
				while (iterators.hasNext()) {
					final String key = iterators.next();
					RequestParams params = new RequestParams();
					params.addBodyParameter("method", "upimg");
					File file = new File(key);
					params.addBodyParameter("upload", file);
					uploadImg(params);

				}
			} else {
				if (progressDialog == null) {
					showProgressDialog("问题发布中...");
				}
				AddQuestion();
			}
			break;
		default:
			break;
		}

	}

	public void uploadImg(RequestParams params) {
		if (progressDialog == null) {
			showProgressDialog("问题发布中...");
		}

		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.TPURLER + "upimg.action", params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				closeProgressDialog();
				Toast.makeText(PublishQuestion.this, "发布问题失败", 2000).show();
			}

			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				JSONObject jsonObject = JSONObject.parseObject(responseInfo.result);
				if (jsonObject.getString("resultcode").equals("ok")) {
					imgs.append(jsonObject.getString("resultdata")).append(",");
					int length = imgs.toString().split(",").length;
					if (length == (Bimp.bmp.size())) {
						AddQuestion();
					}
				}
			}
		});

	}

	public void AddQuestion() {

		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "addQuestion");
		params.addBodyParameter("questionText", tvContent.getText().toString());
		params.addBodyParameter("questionTitle", tvTitle.getText().toString());

		params.addBodyParameter("userName", PrefUtil.getStringPref(this, "zhonghua_userName"));

		if (expert != null) {
			params.addBodyParameter("expertName", expert.getUserName());
		} else {
			params.addBodyParameter("expertName", "");
		}

		params.addBodyParameter("region", "4521984");

		if (imgs.length() != 0) {
			String picutres = imgs.toString().substring(0, imgs.length() - 1);
			params.addBodyParameter("pictures", picutres);
		}
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER, params, new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				closeProgressDialog();
				Toast.makeText(PublishQuestion.this, "发布问题失败", 2000).show();

			}

			@SuppressLint("ShowToast")
			@Override
			public void onSuccess(ResponseInfo<String> responseInfo) {
				closeProgressDialog();
				JSONObject jsonObject = JSONObject.parseObject(responseInfo.result);
				if (jsonObject.getString("resultcode").equals("ok")) {
					imgs.setLength(0);
					Toast.makeText(PublishQuestion.this, "问题发布成功", 2000).show();
					Bimp.bmp.clear();
					Bimp.drr.clear();
					Bimp.max = 0;
					PublishQuestion.this.finish();
				}
			}
		});
	}

	public void showProgressDialog(String message) {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this);
			progressDialog.setCancelable(false);
		}

		if (message == null) {
			progressDialog.setMessage("数据加载中...");
		} else {
			progressDialog.setMessage(message);
		}

		progressDialog.show();
	}

	public void closeProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public class MyAdapter extends BaseAdapter {
		LayoutInflater inflater = LayoutInflater.from(PublishQuestion.this);
		private int selectedPosition = -1;// 选中的位置
		private boolean shape;

		public boolean isShape() {
			return shape;
		}

		public void setShape(boolean shape) {
			this.shape = shape;
		}

		public void update() {
			loading();
		}

		@Override
		public int getCount() {
			return (Bimp.bmp.size() + 1);
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		public void setSelectedPosition(int position) {
			selectedPosition = position;
		}

		public int getSelectedPosition() {
			return selectedPosition;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {

				convertView = inflater.inflate(R.layout.pictures_publish_photoitem, parent, false);
				holder = new ViewHolder();
				holder.image = (ImageView) convertView.findViewById(R.id.iv_photo);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			if (position == Bimp.bmp.size()) {
				holder.image.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.phone_icon));
				if (position == 6) {
					holder.image.setVisibility(View.GONE);
				}
			} else {
				holder.image.setImageBitmap(Bimp.bmp.get(position));
			}

			return convertView;
		}

		public class ViewHolder {
			public ImageView image;
		}

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					mAdapter.notifyDataSetChanged();
					break;
				}
				super.handleMessage(msg);
			}
		};

		public void loading() {
			new Thread(new Runnable() {
				public void run() {
					while (true) {
						if (Bimp.max == Bimp.drr.size()) {
							Message message = new Message();
							message.what = 1;
							handler.sendMessage(message);
							break;
						} else {
							try {
								String path = Bimp.drr.get(Bimp.max);
								System.out.println(path);
								Bitmap bm = Bimp.revitionImageSize(path);
								bm = Bimp.rotateBitmap(Bimp.readPicDegree(path), bm);
								Bimp.bmp.add(bm);
								String newStr = path.substring(path.lastIndexOf("/"));
								drr.add(Environment.getExternalStorageDirectory().getPath() + "/Zhonghua/images/" + newStr);
								Bimp.saveBitmap(bm, "" + newStr);
								Bimp.max += 1;
								Message message = new Message();
								message.what = 1;
								handler.sendMessage(message);
							} catch (IOException e) {

								e.printStackTrace();
							}
						}
					}
				}
			}).start();
		}
	}

}
