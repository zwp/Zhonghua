package com.sinonetwork.zhonghua;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.fragment.CetuBaseInfoFragment;
import com.sinonetwork.zhonghua.fragment.CetuSolidBaseInfoFragment;
import com.sinonetwork.zhonghua.fragment.CetuSolidElementInfoFragment;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class CetuDetail extends FragmentActivity implements OnClickListener {
	private Button base_info;
	private Button solid_info;
	private Button solid_base_info;
	private CetuBaseInfoFragment cetuBaseInfoFragment;
	private CetuSolidBaseInfoFragment cetuSolidBaseInfoFragment;
	private CetuSolidElementInfoFragment cetuSolidElementInfoFragment;
	FragmentManager fragmentManager;
	Fragment cetu_fragment_info;

	private String yyid; // 预约信息id
	private String yyorderTimeStr; // 时间
	private String yyregionId; // 地区id
	private String yyregionName; // 地区名

	private TextView cetu_yuyueshijian_time;
	private TextView cetu_diqu_time;

	private Bundle bundle;
	private ImageView back;

	private Button[] btns;

	// TestSoilInfoById //实体类
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cetu_detail);
		base_info = (Button) findViewById(R.id.base_info);
		solid_info = (Button) findViewById(R.id.solid_info);
		solid_base_info = (Button) findViewById(R.id.solid_base_info);
		cetu_yuyueshijian_time = (TextView) findViewById(R.id.cetu_yuyueshijian_time);
		cetu_diqu_time = (TextView) findViewById(R.id.cetu_diqu_time);
		back = (ImageView) findViewById(R.id.back);

		btns = new Button[] { base_info, solid_info, solid_base_info };
		btns[0].setBackgroundColor(Color.parseColor("#007fc6"));
		btns[0].setTextColor(Color.parseColor("#ffffff"));
		initViews();

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		fragmentManager = getSupportFragmentManager();

		// 第一次启动时选中第0个tab
		setTabSelection(0);

		// 获取上个页面的数据
		Bundle bundle = getIntent().getExtras();
		yyid = bundle.getString("yyid");
		yyorderTimeStr = bundle.getString("yyorderTimeStr");
		yyregionId = bundle.getString("yyregionId");
		yyregionName = bundle.getString("yyregionName");
		cetu_yuyueshijian_time.setText(yyorderTimeStr);
		cetu_diqu_time.setText(yyregionName);

		SharedPreferences preferences = getSharedPreferences("henduoshuju", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("yyid", yyid);
		Logger.e("选种传过来的yyid====" + yyid);
		editor.commit();

		// loadData("getTestSoilInfoById","58") ;

	}

	private void initViews() {
		base_info.setOnClickListener(this);
		solid_info.setOnClickListener(this);
		solid_base_info.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.base_info:
			// Intent intent = new Intent()
			setTabSelection(0);

			break;
		case R.id.solid_info:
			// Intent intent = new Intent()
			setTabSelection(2);
			break;
		case R.id.solid_base_info:
			// Intent intent = new Intent()
			setTabSelection(1);
			break;

		default:
			break;
		}
		setBtns(v.getId());
	}

	private void setTabSelection(int index) {
		// 每次选中之前先清楚掉上次的选中状态 ״̬
		// clearSelection();
		// 开启一个Fragment事务
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		// 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
		hideFragments(transaction);
		switch (index) {
		case 0:
			// 当点击了农业咨询tab时，改变控件的图片和文字颜色
			// base_info.setImageResource(R.drawable.foot_menu_on01);

			if (cetuBaseInfoFragment == null) {

				cetuBaseInfoFragment = new CetuBaseInfoFragment();
				bundle = new Bundle();
				bundle.putString("id", yyid);
				cetuBaseInfoFragment.setArguments(bundle);
				transaction.replace(R.id.cetu_fragment_info, (Fragment) cetuBaseInfoFragment);
			} else {
				transaction.show(cetuBaseInfoFragment);
			}
			break;
		case 1:

			// solid_base_info.setImageResource(R.drawable.foot_menu_on02);
			if (cetuSolidBaseInfoFragment == null) {

				cetuSolidBaseInfoFragment = new CetuSolidBaseInfoFragment();
				bundle = new Bundle();
				bundle.putString("id", yyid);
				cetuSolidBaseInfoFragment.setArguments(bundle);
				transaction.add(R.id.cetu_fragment_info, cetuSolidBaseInfoFragment);
			} else {
				transaction.show(cetuSolidBaseInfoFragment);
			}
			break;
		case 2:

			// solid_info.setImageResource(R.drawable.foot_menu_on03);
			if (cetuSolidElementInfoFragment == null) {
				cetuSolidElementInfoFragment = new CetuSolidElementInfoFragment();
				bundle = new Bundle();
				bundle.putString("id", yyid);
				cetuSolidElementInfoFragment.setArguments(bundle);
				transaction.add(R.id.cetu_fragment_info, cetuSolidElementInfoFragment);
			} else {
				transaction.show(cetuSolidElementInfoFragment);
			}
			break;
		}

		transaction.commit();
	}

	// 初始化三个按钮点击后的颜色
	public void setBtns(int cilckBtnId) {
		for (int i = 0; i < btns.length; i++) {
			if (btns[i].getId() == cilckBtnId) {
				btns[i].setBackgroundColor(Color.parseColor("#007fc6"));
				btns[i].setTextColor(Color.parseColor("#ffffff"));
			} else {
				btns[i].setBackgroundColor(Color.WHITE);
				btns[i].setTextColor(Color.parseColor("#000000"));
			}
		}
	}

	private void hideFragments(FragmentTransaction transaction) {
		if (cetuBaseInfoFragment != null) {
			transaction.hide(cetuBaseInfoFragment);
		}
		if (cetuSolidBaseInfoFragment != null) {
			transaction.hide(cetuSolidBaseInfoFragment);
		}
		if (cetuSolidElementInfoFragment != null) {
			transaction.hide(cetuSolidElementInfoFragment);
		}
	}

}
