package com.sinonetwork.zhonghua;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EditUserActivity extends FragmentActivity {
	private ImageView back;
	private TextView title;
	private EditText ed_username;
	private EditText ed_password;
	private EditText ed_new_password;
	private EditText ed_phone;
	private Button btn_confirm;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_edit);
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		ed_username = (EditText) this.findViewById(R.id.ed_username);
		ed_password = (EditText) this.findViewById(R.id.ed_password);
		ed_new_password = (EditText) this.findViewById(R.id.ed_new_password);
		ed_phone = (EditText) this.findViewById(R.id.ed_phone);
		btn_confirm = (Button) this.findViewById(R.id.btn_confirm);
		title.setText("修改用户信息");
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditUserActivity.this.finish();
			}
		});
		btn_confirm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (ed_username.getText().toString().equals("")) {
					Toast.makeText(EditUserActivity.this, "新用户名不能为空", 2000).show();
					return;
				} else if (ed_password.getText().toString().equals("")) {
					Toast.makeText(EditUserActivity.this, "旧密码不能为空", 2000).show();
					return;
				} else if (ed_new_password.getText().toString().equals("")) {
					Toast.makeText(EditUserActivity.this, "新密码不能为空", 2000).show();
					return;
				} else if (ed_phone.getText().toString().equals("")) {
					Toast.makeText(EditUserActivity.this, "新电话号码不能为空", 2000).show();
					return;
				}

				HttpHelp.getInstance().send(HttpMethod.GET, URLAddress.changeInfo(ed_username.getText().toString(), ed_password.getText().toString(), ed_new_password.getText().toString(), ed_phone.getText().toString()), new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JSONObject jsonObject = JSONObject.parseObject(responseInfo.result);
						Toast.makeText(EditUserActivity.this, jsonObject.getString("resultData"), 2000).show();
					}
				});

			}
		});
	}

}
