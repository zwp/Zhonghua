package com.sinonetwork.zhonghua;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.ExperimentShareLvAdapter;
import com.sinonetwork.zhonghua.model.ExperimentShareModel;
import com.sinonetwork.zhonghua.model.ExperimentShareModel.ExperimentShareResultdata;
import com.sinonetwork.zhonghua.model.ExperimentShareModel.ExperimentShareResultdata.ExperimentShareFiles;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class ExperimentShareActivity extends Activity implements
		OnClickListener {
	private ImageView experimentshare_back;
	private ListView experimentshare_lv;
	private ExperimentShareLvAdapter adapterLv;

	private List<ExperimentShareResultdata> esultdataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.experimentshare);

		initView();
		setMonitor();
		esultdataList = new ArrayList<ExperimentShareResultdata>();
		adapterLv = new ExperimentShareLvAdapter(getApplicationContext(),
				esultdataList);
		experimentshare_lv.setAdapter(adapterLv);
		initData();
	}

	public void initView() {
		experimentshare_back = (ImageView) findViewById(R.id.experimentshare_back);
		experimentshare_lv = (ListView) findViewById(R.id.experimentshare_lv);

	}

	public void setMonitor() {
		experimentshare_back.setOnClickListener(this);
		experimentshare_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				ExperimentShareFiles files = esultdataList.get(position)
						.getFiles().get(0);
				String filepath = files.getFilepath();
				String uptime = files.getUptime();
				// if (position == 0) {
				// Intent intent = new Intent(getApplicationContext(),
				// ExperimentSharefileActivity.class);
				// intent.putExtra("file",
				// "http://123.127.160.90:8080/zhnyxxgc/attachment/"
				// + uptime + "/" + filepath);
				// startActivity(intent);
				// } else {
				String urlString = "http://123.127.160.90:8080/zhnyxxgc/attachment/"
						+ uptime + "/" + filepath;
				uploadFile(urlString);
				// File file = new File(urlString);
				// String urlString = "http://123.127.160.90:8080/zhnyxxgc";
				// uploadFile(urlString, uptime, filepath);
				// Intent intent = new Intent();
				// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				// // 设置intent的Action属性
				// intent.setAction(Intent.ACTION_VIEW);
				// // 获取文件file的MIME类型
				// String type = getMIMEType(file);
				// // 设置intent的data和Type属性。
				// intent.setDataAndType(Uri.fromFile(file), type);
				// // 跳转
				// startActivity(intent);
				// }

			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.experimentshare_back:
			Intent intent = new Intent(getApplicationContext(),
					Expariment.class);
			startActivity(intent);

			break;

		default:
			break;
		}
	}

	public void initData() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", "searchExamples");
		map.put("exampleType", "3");
		FastJsonRequest<ExperimentShareModel> fastJsonRequest = new FastJsonRequest<ExperimentShareModel>(
				Method.POST, URLAddress.AreaUrl, ExperimentShareModel.class,
				null, map, new Response.Listener<ExperimentShareModel>() {
					@Override
					public void onResponse(ExperimentShareModel response) {
						// TODO Auto-generated method stub
						esultdataList = response.getResultdata();
						adapterLv.setLvData(esultdataList);
						adapterLv.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub

					}
				});
		RequestManager.addRequest(fastJsonRequest, this);
	}

	public void uploadFile(final String urlString) {

		new Thread() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				try {
					URL url = new URL(urlString);
					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setRequestMethod("POST");
					connection.setDoInput(true);
					connection.setDoOutput(true);
					connection.setUseCaches(false);
					connection.setConnectTimeout(5000);
					connection.setReadTimeout(5000);
					// 实现连接
					connection.connect();
					System.out.println("connection.getResponseCode()="
							+ connection.getResponseCode());
					if (connection.getResponseCode() == 200) {
						InputStream is = connection.getInputStream();
						// 以下为下载操作
						byte[] arr = new byte[512];
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						BufferedOutputStream bos = new BufferedOutputStream(
								baos);
						int n = is.read(arr);
						while (n > 0) {
							bos.write(arr);
							n = is.read(arr);
						}
						bos.close();
						String path = Environment.getExternalStorageDirectory()
								+ "/download/";
						String[] name = urlString.split("/");
						path = path + name[name.length - 1];
						System.out.println("name=" + name);
						System.out.println("path=" + path);
						File file = new File(path);
						FileOutputStream fos = new FileOutputStream(file);
						fos.write(baos.toByteArray());
						fos.close();
						// 关闭网络连接
						connection.disconnect();
						System.out.println("下载完成");
						if (file.exists()) {
							System.out.println("打开");

							// Intent intent = new Intent();
							// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							// // 设置intent的Action属性
							// intent.setAction(Intent.ACTION_VIEW);
							// 获取文件file的MIME类型
							String type = getMIMEType(file);
							// 设置intent的data和Type属性。
							// intent.setDataAndType(Uri.fromFile(file), type);
							Uri path1 = Uri.fromFile(file);
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setDataAndType(path1, type);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							try {
								// 跳转
								startActivity(intent);
							} catch (ActivityNotFoundException e) {
								System.out.println("打开失败");
							}
						}
					}
				} catch (IOException e) {
					// TODO: handle exception
					System.out.println(e.getMessage() + "");
				}

			}
		}.start();

	}

	/**
	 * 根据文件后缀名获得对应的MIME类型。
	 * 
	 * @param file
	 */
	private String getMIMEType(File file) {

		String type = "*/*";
		String fName = file.getName();
		// 获取后缀名前的分隔符"."在fName中的位置。
		int dotIndex = fName.lastIndexOf(".");
		if (dotIndex < 0) {
			return type;
		}
		/* 获取文件的后缀名 */
		String end = fName.substring(dotIndex, fName.length()).toLowerCase();
		if (end == "")
			return type;
		// 在MIME和文件类型的匹配表中找到对应的MIME类型。
		for (int i = 0; i < MIME_MapTable.length; i++) { //
			if (end.equals(MIME_MapTable[i][0]))
				type = MIME_MapTable[i][1];
		}
		return type;
	}

	private final String[][] MIME_MapTable = {
			// {后缀名，MIME类型}
			{ ".3gp", "video/3gpp" },
			{ ".apk", "application/vnd.android.package-archive" },
			{ ".asf", "video/x-ms-asf" },
			{ ".avi", "video/x-msvideo" },
			{ ".bin", "application/octet-stream" },
			{ ".bmp", "image/bmp" },
			{ ".c", "text/plain" },
			{ ".class", "application/octet-stream" },
			{ ".conf", "text/plain" },
			{ ".cpp", "text/plain" },
			{ ".doc", "application/msword" },
			{ ".docx",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
			{ ".xls", "application/vnd.ms-excel" },
			{ ".xlsx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
			{ ".exe", "application/octet-stream" },
			{ ".gif", "image/gif" },
			{ ".gtar", "application/x-gtar" },
			{ ".gz", "application/x-gzip" },
			{ ".h", "text/plain" },
			{ ".htm", "text/html" },
			{ ".html", "text/html" },
			{ ".jar", "application/java-archive" },
			{ ".java", "text/plain" },
			{ ".jpeg", "image/jpeg" },
			{ ".jpg", "image/jpeg" },
			{ ".js", "application/x-javascript" },
			{ ".log", "text/plain" },
			{ ".m3u", "audio/x-mpegurl" },
			{ ".m4a", "audio/mp4a-latm" },
			{ ".m4b", "audio/mp4a-latm" },
			{ ".m4p", "audio/mp4a-latm" },
			{ ".m4u", "video/vnd.mpegurl" },
			{ ".m4v", "video/x-m4v" },
			{ ".mov", "video/quicktime" },
			{ ".mp2", "audio/x-mpeg" },
			{ ".mp3", "audio/x-mpeg" },
			{ ".mp4", "video/mp4" },
			{ ".mpc", "application/vnd.mpohun.certificate" },
			{ ".mpe", "video/mpeg" },
			{ ".mpeg", "video/mpeg" },
			{ ".mpg", "video/mpeg" },
			{ ".mpg4", "video/mp4" },
			{ ".mpga", "audio/mpeg" },
			{ ".msg", "application/vnd.ms-outlook" },
			{ ".ogg", "audio/ogg" },
			{ ".pdf", "application/pdf" },
			{ ".png", "image/png" },
			{ ".pps", "application/vnd.ms-powerpoint" },
			{ ".ppt", "application/vnd.ms-powerpoint" },
			{ ".pptx",
					"application/vnd.openxmlformats-officedocument.presentationml.presentation" },
			{ ".prop", "text/plain" }, { ".rc", "text/plain" },
			{ ".rmvb", "audio/x-pn-realaudio" }, { ".rtf", "application/rtf" },
			{ ".sh", "text/plain" }, { ".tar", "application/x-tar" },
			{ ".tgz", "application/x-compressed" }, { ".txt", "text/plain" },
			{ ".wav", "audio/x-wav" }, { ".wma", "audio/x-ms-wma" },
			{ ".wmv", "audio/x-ms-wmv" },
			{ ".wps", "application/vnd.ms-works" }, { ".xml", "text/plain" },
			{ ".z", "application/x-compress" },
			{ ".zip", "application/x-zip-compressed" }, { "", "*/*" } };

}
