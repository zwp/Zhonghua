package com.sinonetwork.zhonghua.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
//WebView和Js之间的数据交换。
public class AndroidForJs {

	private Context mContext;
	  private String cmdCode, resultKey;
	  private long visitTime;

	public AndroidForJs(Context context) {
	this.mContext = context;
	}

	// 以json实现webview与js之间的数据交互
	public String jsontohtml(String abc) {
	JSONObject map;
	JSONArray array = new JSONArray();
	try {
	map = new JSONObject();
	map.put("name", abc);
	map.put("age", 25);
	map.put("address", abc);
	array.put(map);

	map = new JSONObject();
	map.put("name", "jacky");
	map.put("age", 22);
	map.put("address", "中国北京");
	array.put(map);

	map = new JSONObject();
	map.put("name", "vans");
	map.put("age", 26);
	map.put("address", "中国深圳");
	map.put("phone", "13888888888");
	array.put(map);
	} catch (JSONException e) {
	e.printStackTrace();
	}
	return array.toString();
	}
}
