package com.sinonetwork.zhonghua.parser;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.sinonetwork.zhonghua.model.Categorys;
import com.sinonetwork.zhonghua.model.Doc;
import com.sinonetwork.zhonghua.model.LandInfo;
import com.sinonetwork.zhonghua.model.News;
import com.sinonetwork.zhonghua.model.SubCategorys;
import com.sinonetwork.zhonghua.model.SubSymptom;
import com.sinonetwork.zhonghua.model.Symptom;
import com.sinonetwork.zhonghua.model.Warning;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class LandInfoParser {

	/**
	 * 根据经纬度请求测土配肥信息列表
	 * 
	 * @param longitude
	 * @param latitude
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<LandInfo> getList(Double longitude, Double latitude)
			throws Exception {

		String url = URLAddress.URLYI
				+ "?method=getTestSoilInfoListByCoordinates&longitude="
				+ longitude + "&latitude=" + latitude;
		//获取网络数据
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);

		ArrayList<LandInfo> list = new ArrayList<LandInfo>();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				LandInfo d = new LandInfo();
				d.setId(o.getInt("id"));
				d.setLongitude(o.getString("longitude"));
				d.setLatitude(o.getString("latitude"));
				list.add(d);
			}
		}
		return list;
	}

	/**
	 * 根据id请求测土配肥详情
	 * 
	 * @param id
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public static LandInfo getLandInfoDetail(String id, int type)
			throws Exception {
		String url;
		if (type == 0) {

			url = URLAddress.URLYI + "?method=getTestSoilInfoByTestSoilId&id="
					+ id;
		} else {
			url = URLAddress.URLYI + "?method=getTestSoilInfoById&id=" + id;

		}
		// Log.v("zhong", "url--" + url);
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);

		JSONObject obj = jsonObj.getJSONObject("resultdata");

		LandInfo detail = new LandInfo();
		if (jsonObj.getString("resultcode").equals("ok")) {
			detail.setId(obj.getInt("id"));
			detail.setLongitude(obj.getString("longitude"));
			detail.setLatitude(obj.getString("latitude"));
			detail.setOrderCode(obj.getString("orderCode"));
			detail.setCompany(obj.getString("company"));
			detail.setSendPerson(obj.getString("sendPerson"));
			detail.setLabNo(obj.getString("labNo"));
			detail.setSamplesNo(obj.getString("samplesNo"));
			detail.setSampleDepth(obj.getString("sampleDepth"));
			detail.setRegionId(obj.getString("regionId"));
			detail.setArea(obj.getString("area"));
			detail.setAgrotype(obj.getString("agrotype"));
			detail.setCharacter(obj.getString("character"));
			detail.setCropId1(obj.getString("cropId1"));
			detail.setTargetYield(obj.getString("targetYield"));
			detail.setIrrCondition(obj.getString("irrCondition"));
			detail.setCropId2(obj.getString("cropId2"));
			detail.setYieldLevel(obj.getString("yieldLevel"));
			detail.setKeyWord(obj.getString("keyWord"));
			detail.setPh(obj.getString("ph"));
			detail.setAa(obj.getString("aa"));
			detail.setOm(obj.getString("om"));
			detail.setNh4n(obj.getString("nh4n"));
			detail.setNo3n(obj.getString("no3n"));
			detail.setPhosphor(obj.getString("phosphor"));
			detail.setKalium(obj.getString("kalium"));
			detail.setCalcium(obj.getString("calcium"));
			detail.setMagnesium(obj.getString("magnesium"));
			detail.setSulfur(obj.getString("sulfur"));
			detail.setFerrum(obj.getString("ferrum"));
			detail.setCopper(obj.getString("copper"));
			detail.setManganese(obj.getString("manganese"));
			detail.setZinc(obj.getString("zinc"));
			detail.setBoron(obj.getString("boron"));
			detail.setUniqueId(obj.getString("uniqueId"));
			detail.setProvince(obj.getString("province"));
			detail.setCity(obj.getString("city"));
			detail.setVillage(obj.getString("village"));
			detail.setBurg(obj.getString("burg"));
			detail.setDetFinishDateStr(obj.getString("detFinishDateStr"));
			detail.setSamplingDateStr(obj.getString("samplingDateStr"));
		}

		return detail;
	}

	public static ArrayList<Categorys> getCropDoctorList() throws Exception {

		String url = URLAddress.URLYI + "?method=searchAllCrops1";

		// Log.v("zhong", "url--" + url);
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);

		ArrayList<Categorys> list = new ArrayList<Categorys>();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				Categorys d = new Categorys();
				d.setId(o.getInt("id"));
				d.setCropName(o.getString("cropName"));

				ArrayList<SubCategorys> subList = new ArrayList<SubCategorys>();
				JSONArray subArray = o.getJSONArray("childern");
				for (int j = 0; j < subArray.length(); j++) {
					JSONObject sub = subArray.getJSONObject(j);
					SubCategorys s = new SubCategorys();
					s.setId(sub.getInt("id"));
					s.setCropName(sub.getString("cropName"));
					subList.add(s);
				}
				d.setSubCategorysList(subList);
				list.add(d);

			}
		}
		return list;
	}

	/**
	 * 获取作物症状
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Symptom> getSymptomList(String id) throws Exception {

		String url = URLAddress.URLYI
				+ "?method=searchAllSymptoms&bhoryy=1&cropId=" + id;

		Log.v("zhong", "url--" + url);
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);

		ArrayList<Symptom> list = new ArrayList<Symptom>();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				Symptom d = new Symptom();
				d.setId(o.getInt("id"));
				d.setSymptomsTypeName(o.getString("symptomsTypeName"));

				ArrayList<SubSymptom> subList = new ArrayList<SubSymptom>();
				JSONArray subArray = o.getJSONArray("symptoms");
				for (int j = 0; j < subArray.length(); j++) {
					JSONObject sub = subArray.getJSONObject(j);
					SubSymptom s = new SubSymptom();
					s.setId(sub.getInt("id"));
					s.setSymptomsName(sub.getString("symptomsName"));
					s.setRemark(URLDecoder.decode(sub.getString("remark"),
							"UTF-8"));
					subList.add(s);
				}
				d.setSubSymptomList(subList);
				list.add(d);

			}
		}
		return list;
	}

	/**
	 * 获取灾害预警列表
	 * 
	 * @param countyName
	 * @param cityName
	 * @param provinceName
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Warning> getWarningList(String provinceName,
			String cityName, String countyName) throws Exception {

		// String url = URLAddress.URLYI
		// + "?method=searchWeatherWarning&provinceName=" + provinceName
		// + "&cityName=" + cityName + "&countyName=" + countyName;
		String url = URLAddress.URLYI;

		Log.v("zhong", "url--" + url);
		Log.i("gxx", "url--" + url);
		// JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("method", "searchWeatherWarning"));
		params.add(new BasicNameValuePair("provinceName", provinceName));
		params.add(new BasicNameValuePair("cityName", cityName));
		params.add(new BasicNameValuePair("countyName", countyName));
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByPost(url, params,
				false);

		ArrayList<Warning> list = new ArrayList<Warning>();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				Warning d = new Warning();
				d.setId(o.getString("id"));
				d.setTitle(o.getString("title"));
				d.setDescription(URLDecoder.decode(o.getString("description")));
				d.setSendTime(o.getString("sendTime"));
				d.setArea(o.getString("area"));

				list.add(d);
			}
		}
		return list;
	}

	/**
	 * 获取新闻详情
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public static News getNewsDetail(String id) throws Exception {

		String url = URLAddress.URLYI + "?method=detailAgricultureNew&id=" + id;
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);// 获取并解析Json数据，Get方式
		News detail = new News();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONObject o = jsonObj.getJSONObject("resultdata");

			detail.setId(o.getString("id"));
			detail.setTitle(URLDecoder.decode(o.getString("title"), "UTF-8"));// 编码方式的设置
			detail.setTime(o.getString("time"));

			detail.setDocContent(URLDecoder.decode(o.getString("docContent"),
					"UTF-8"));
			detail.setInfoTypeId(o.getString("infoTypeId"));

		}
		return detail;
	}

	/**
	 * 获取新闻列表
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<News> getNewsList(int page) throws Exception {

		String url = URLAddress.URLYI
				+ "?method=searchAgricultureNews&curPage=" + page;

		// Log.v("zhong", "url--" + url);
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);

		ArrayList<News> list = new ArrayList<News>();
		if (jsonObj.getString("resultcode").equals("ok")) {

			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				News d = new News();
				d.setId(o.getString("id"));
				d.setTitle(URLDecoder.decode(o.getString("title"), "UTF-8"));
				d.setTime(o.getString("time"));
				d.setDocContent(URLDecoder.decode(o.getString("docContent"),
						"UTF-8"));
				d.setInfoTypeId(o.getString("infoTypeId"));

				list.add(d);

			}
		}
		return list;
	}

	/**
	 * 得到用户信息
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	public static ZHAccount SaveCurrentAccount(JSONObject obj) throws Exception {

		ZHAccount account = new ZHAccount();
		account.setCare(obj.getString("care"));
		account.setCul_area(obj.getString("cul_area"));
		account.setUserName(obj.getString("userName"));
		account.setAreaName(obj.getString("areaName"));
		account.setAreaId(obj.getInt("areaId"));
		account.setPlant(obj.getString("plant"));
		account.setPassword(obj.getString("password"));
		account.setParent_areaName(obj.getString("parent_areaName"));
		account.setParent_areaId(obj.getInt("parent_areaId"));
		account.setTelephone(obj.getString("telephone"));
		account.setSequence(obj.getString("sequence"));
		account.setUser_integ(obj.getInt("user_integ"));
		return account;
	}

	/**
	 * 获取试验示范中的优秀试验报告分享数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<Doc> getDocList() throws Exception {

		String url = URLAddress.URLYI + "?method=searchExamples&exampleType=3";
		JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(url, false);// 自定义获取网路数据
		ArrayList<Doc> list = new ArrayList<Doc>();
		if (jsonObj.getString("resultcode").equals("ok")) {
			JSONArray jsonArray = jsonObj.getJSONArray("resultdata");
			int size = jsonArray.length();
			for (int i = 0; i < size; i++) {
				JSONObject o = jsonArray.getJSONObject(i);
				JSONArray filesArray = o.getJSONArray("files");
				int s = filesArray.length();
				for (int j = 0; j < s; j++) {
					Doc d = new Doc();
					JSONObject f = filesArray.getJSONObject(j);
					d.setUptime(f.getString("uptime"));
					d.setRname(f.getString("rname"));
					d.setContextType(f.getString("contextType"));
					d.setFilepath(f.getString("filepath"));
					list.add(d);
				}
			}
		}
		return list;
	}
}
