package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.ChooseExpertAdapter;
import com.sinonetwork.zhonghua.model.Expert;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class ChooseExpert extends Activity {
	private List<Expert> mDatas = new ArrayList<Expert>();
	List<Expert> list;
	// private ImageView isChoose;
	private ListView choose_expert_listitem;
	private ChooseExpertAdapter adapter;
	ImageView back;
	boolean ischange = true;
	private ImageView know_publish_sure;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_expert);
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		// isChoose = (ImageView) findViewById(R.id.ischoose);
		choose_expert_listitem = (ListView) findViewById(R.id.choose_expert_listitem);
		back = (ImageView) findViewById(R.id.back);
		know_publish_sure = (ImageView) this
				.findViewById(R.id.know_publish_sure);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		loadData();
		know_publish_sure.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String strPosition = "";
				HashMap<String, Boolean> states = adapter.states;
				Set<String> sets = states.keySet();
				for (String string : sets) {
					if (states.get(string)) {
						strPosition = string;
					}
				}
				if (strPosition.equals("")) {
					Toast.makeText(ChooseExpert.this, "请选择专家!", 2000).show();
					return;
				}
				Expert expert = adapter.getItem(Integer.valueOf(strPosition));
				Intent it = new Intent();
				it.putExtra("expert", expert);
				setResult(Activity.RESULT_OK, it);
				finish();

			}
		});
	}

	/**
	 * 读取专家数据
	 */
	public void loadData() {
		HttpHelp.getInstance().send(HttpMethod.GET,
				URLAddress.URLER + "?method=searchExperts",
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(ResponseInfo<String> resultInfo) {
						JSONObject jsonObject = JSONObject
								.parseObject(resultInfo.result);
						JSONArray array = JSONArray.parseArray(jsonObject
								.getString("resultdata"));

						mDatas = JSONArray.parseArray(array.toJSONString(),
								Expert.class);

						list = new ArrayList<Expert>();
						for (int i = 0; i < mDatas.size(); i++) {
							list.add(mDatas.get(i));
						}

						choose_expert_listitem
								.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
						adapter = new ChooseExpertAdapter(ChooseExpert.this,
								list);
						choose_expert_listitem.setAdapter(adapter);
						choose_expert_listitem
								.setOnItemClickListener(new OnItemClickListener() {

									@Override
									public void onItemClick(
											AdapterView<?> parent, View view,
											int position, long id) {

									}
								});

					}
				});

	}
}
