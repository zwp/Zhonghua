package com.sinonetwork.zhonghua;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.model.LandInfo;
import com.sinonetwork.zhonghua.parser.LandInfoParser;

@SuppressLint("ResourceAsColor") public class LandInfoDetailActivity extends LandBaseActivity {
	private String id;
	public static final String ID = "ID";
	private LandInfo detail;
	
	private TextView areaTV;
	private TextView leftTV;
	private TextView midTV;
	private TextView rightTV;
	private TextView firstTV;
	private TextView secondTV;
	private TextView thirdTV;
	private TextView fourthTV;
	private TextView fifthTV;
	private TextView sixTV;
	private TextView seventhTV;
	private TextView eighthTV;
	private TextView ninthTV;
	private TextView seventhLineTV;
	private TextView eighthLineTV;
	private TextView ninthLineTV;
	private LinearLayout thirdLayout;
	private LinearLayout firstAndSecondLayout;
	private TextView antaidanTV;
	private TextView xiaotaidanTV;
	private TextView phzhi;
	private TextView aa;
	private TextView peng;
	private TextView xin;
	private TextView meng;
	private TextView tong;
	private TextView tie;
	private TextView liu;
	private TextView mei;
	private TextView gai;
	private TextView jia;
	private TextView lin;
	private TextView om;
	private int type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_land_info_detail);
		setBackBtn();
		setTopTitleTV("测土详细");
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		id = bundle.getString("ID");
		type = bundle.getInt("type");//0从测土地图，1从我的测土
		loadData();
	}

	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					detail = LandInfoParser.getLandInfoDetail(id,type);

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void setView() {

		firstAndSecondLayout = (LinearLayout)findViewById(R.id.firstandsecondLayout);
		thirdLayout = (LinearLayout)findViewById(R.id.thirdLayout);
		areaTV = (TextView)findViewById(R.id.areatv);
		leftTV = (TextView)findViewById(R.id.lefttv);
		midTV = (TextView)findViewById(R.id.midtv);
		rightTV = (TextView)findViewById(R.id.righttv);
		firstTV = (TextView)findViewById(R.id.firsttv);
		secondTV = (TextView)findViewById(R.id.secondtv);
		thirdTV = (TextView)findViewById(R.id.thirdtv);
		fourthTV = (TextView)findViewById(R.id.fourthtv);
		fifthTV = (TextView)findViewById(R.id.fifthtv);
		sixTV = (TextView)findViewById(R.id.sixthtv);
		seventhTV = (TextView)findViewById(R.id.seventhtv);
		eighthTV = (TextView)findViewById(R.id.eighthtv);
		ninthTV = (TextView)findViewById(R.id.ninthtv);
		seventhLineTV = (TextView)findViewById(R.id.seventhlinetv);
		eighthLineTV = (TextView)findViewById(R.id.eighthlinetv);
		ninthLineTV = (TextView)findViewById(R.id.ninthlinetv);
		antaidanTV = (TextView)findViewById(R.id.antaidan);
		xiaotaidanTV = (TextView)findViewById(R.id.xiaotaidan);
		phzhi = (TextView)findViewById(R.id.phzhi);
		aa = (TextView) findViewById(R.id.aa);
		om = (TextView) findViewById(R.id.om);
		lin = (TextView) findViewById(R.id.lin);
		jia = (TextView) findViewById(R.id.jia);
		gai = (TextView) findViewById(R.id.gai);
		mei = (TextView) findViewById(R.id.mei);
		liu = (TextView) findViewById(R.id.liu);
		tie = (TextView) findViewById(R.id.tie);
		tong = (TextView) findViewById(R.id.tong);
		meng = (TextView) findViewById(R.id.meng);
		xin = (TextView) findViewById(R.id.xin);
		peng = (TextView) findViewById(R.id.peng);
		setLandContentView(0);
		
		
		areaTV.setText("地区："+detail.getProvince()+detail.getCity()+detail.getVillage()+detail.getBurg());
		leftTV.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				setLandContentView(0);
				
			}
		});
		midTV.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				setLandContentView(1);
				
			}
		});
		rightTV.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				setLandContentView(2);
				
			}
		});
	}

	private void setLandContentView(int i) {
		
		if(i == 0){
			leftTV.setTextColor(this.getResources().getColor(R.color.white));
			leftTV.setBackgroundResource(R.color.color_title);
			midTV.setTextColor(this.getResources().getColor(R.color.black));
			midTV.setBackgroundResource(R.color.white);
			rightTV.setTextColor(this.getResources().getColor(R.color.black));
			rightTV.setBackgroundResource(R.color.white);
			seventhTV.setVisibility(View.GONE);
			eighthTV.setVisibility(View.GONE);
			ninthTV.setVisibility(View.GONE);
			seventhLineTV.setVisibility(View.GONE);
			eighthLineTV.setVisibility(View.GONE);
			ninthLineTV.setVisibility(View.GONE);
			firstAndSecondLayout.setVisibility(View.VISIBLE);
			thirdLayout.setVisibility(View.GONE);
			
			
			firstTV.setText("送样人："+detail.getSendPerson());
			secondTV.setText("取样时间："+detail.getSamplingDateStr());
			thirdTV.setText("检测完成时间："+detail.getDetFinishDateStr());
			fourthTV.setText("货单号："+detail.getOrderCode());
			fifthTV.setText("实验室编号："+detail.getLabNo());
			sixTV.setText("样品编号："+detail.getSamplesNo());
			
		}else if(i == 1){
			leftTV.setTextColor(this.getResources().getColor(R.color.black));
			leftTV.setBackgroundResource(R.color.white);
			midTV.setTextColor(this.getResources().getColor(R.color.white));
			midTV.setBackgroundResource(R.color.color_title);
			rightTV.setTextColor(this.getResources().getColor(R.color.black));
			rightTV.setBackgroundResource(R.color.white);
			seventhTV.setVisibility(View.VISIBLE);
			eighthTV.setVisibility(View.VISIBLE);
			ninthTV.setVisibility(View.VISIBLE);
			seventhLineTV.setVisibility(View.VISIBLE);
			eighthLineTV.setVisibility(View.VISIBLE);
			ninthLineTV.setVisibility(View.VISIBLE);
			firstAndSecondLayout.setVisibility(View.VISIBLE);
			thirdLayout.setVisibility(View.GONE);
			
			
			firstTV.setText("土壤类型："+detail.getAgrotype());
			secondTV.setText("质地："+detail.getCharacter());
			thirdTV.setText("灌溉条件："+detail.getIrrCondition());
			fourthTV.setText("取样深度（cm）："+detail.getSampleDepth());
			fifthTV.setText("当季作物："+detail.getCropId1());
			sixTV.setText("前茬作物："+detail.getCropId2());
			seventhTV.setText("目标产量（公斤/亩）："+detail.getTargetYield());
			eighthTV.setText("产量水平（公斤/亩）："+detail.getYieldLevel());
			ninthTV.setText("化肥关键字："+ deleteNullStr(detail.getKeyWord().toString()));
		}else if(i == 2){
			leftTV.setTextColor(this.getResources().getColor(R.color.black));
			leftTV.setBackgroundResource(R.color.white);
			midTV.setTextColor(this.getResources().getColor(R.color.black));
			midTV.setBackgroundResource(R.color.white);
			rightTV.setTextColor(this.getResources().getColor(R.color.white));
			rightTV.setBackgroundResource(R.color.color_title);

			firstAndSecondLayout.setVisibility(View.GONE);
			thirdLayout.setVisibility(View.VISIBLE);
			antaidanTV.setText(detail.getNh4n());
			xiaotaidanTV.setText(detail.getNo3n());
			phzhi.setText(detail.getPh().toString());
			aa.setText(detail.getAa().toString());
			om.setText(detail.getOm().toString());
			lin.setText(detail.getPhosphor().toString());
			jia.setText(detail.getKalium().toString());
			gai.setText(detail.getCalcium().toString());
			mei.setText(detail.getMagnesium().toString());
			liu.setText(detail.getSulfur().toString());
			tie.setText(detail.getFerrum().toString());
			tong.setText(detail.getCopper().toString());
			meng.setText(detail.getManganese().toString());
			xin.setText(detail.getZinc().toString());
			peng.setText(detail.getBoron().toString());
			 

			
			
		}
	}

	private String deleteNullStr(String str) {
		
		
		if(str.equals("null")){
			return "";
		}
		return str;
	}
	
	
}
