package com.sinonetwork.zhonghua;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;

import com.sinonetwork.zhonghua.model.News;

public class VideoListActivity extends LandBaseActivity implements OnClickListener{

	private String id;
	private WebView webView;
	protected News detail;
	public static final String ID = "ID";
	public static final String NAME = "NAME";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_list);
		setBackBtn();
		setTopTitleTV("种植视频(建议WiFi环境下观看)");
		findViewById(R.id.video1).setOnClickListener(this);  
        findViewById(R.id.video2).setOnClickListener(this);  
        findViewById(R.id.video3).setOnClickListener(this);  
        findViewById(R.id.video4).setOnClickListener(this);  
        findViewById(R.id.video5).setOnClickListener(this);  
        findViewById(R.id.video6).setOnClickListener(this);  
		
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		Intent intent = new Intent(VideoListActivity.this, VideoActivity.class);
		
		 switch(v.getId()){  
	        case R.id.video1:  
	        {
	        	intent.putExtra("URL", "cetu_xiaomai");
	        }
	            break;  
	        case R.id.video2:  
	        {
	        	intent.putExtra("URL", "yumigwrhyf");
	        }
	            break;  
	        case R.id.video3: 
	        {
	        	intent.putExtra("URL", "turangys_bamai");
	        }
	            break;  
	        case R.id.video4:  
	        {
	        	intent.putExtra("URL", "cetu_yumi");
	        }
	            break;  
	        case R.id.video5: 
	        {
	        	intent.putExtra("URL", "guoshubfjs");
	        }
	            break;  
	        case R.id.video6:  
	        {
	        	intent.putExtra("URL", "dapengxhskxsf");
	        }
	            break;  
	       
	        }  
		 startActivity(intent);
	}
	
}