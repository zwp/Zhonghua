package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.CropSelectAdapter;
import com.sinonetwork.zhonghua.model.Categorys;
import com.sinonetwork.zhonghua.model.SubCategorys;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class MyPlantCrops extends LandBaseActivity {

	protected ArrayList<Categorys> croplist;
	private CropSelectAdapter mAdapter;
	private ListView listView;
	private Button submitBtn;
	private ZHAccount account;
	private TextView selectCropTxt;
    public String ChangeUserInfoURL = URLAddress.SHOP_URL+"fert_bbc/changePlant.htm?";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plant);
		setBackBtn();
		setTopTitleTV("种植作物");
		listView = (ListView) findViewById(R.id.croplist);

		account = ZhAccountPrefUtil.getZHAccount(this);
		if(account.getPlant().length()>0){
			if(account.getPlant().substring(account.getPlant().length() - 1,account.getPlant().length()).equals(",")){
				
				PrefUtil.savePref(MyPlantCrops.this, "cropSelectPlant", account.getPlant());
			}else{
				PrefUtil.savePref(MyPlantCrops.this, "cropSelectPlant", account.getPlant()+",");
				
			}
		}else{
			PrefUtil.savePref(MyPlantCrops.this, "cropSelectPlant", account.getPlant());
			
		}
		
		selectCropTxt = (TextView) findViewById(R.id.selected_crop);
		submitBtn = (Button) findViewById(R.id.submit);
		submitBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String str = PrefUtil.getStringPref(MyPlantCrops.this, "cropSelectPlant");
				if(str.length()>0){
					account.setPlant(str);
					submit(account);
				}else{
					Toast.makeText(getApplicationContext(), "没有选择数据，请选择",
							Toast.LENGTH_SHORT).show();
				}

			}
		});
		loadData();
	}

	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					croplist = LandInfoParser.getCropDoctorList();

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void submit(ZHAccount account) {
		showLoadProgressBar();

		RequestParams params = new RequestParams();
		params.addBodyParameter("userName", account.getUserName());
		params.addBodyParameter("cul_area", account.getCul_area());
		params.addBodyParameter("care", account.getCare());
		params.addBodyParameter("plant", account.getPlant());
		HttpUtils httpUtils = new HttpUtils();

		httpUtils.send(HttpMethod.POST, ChangeUserInfoURL, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// TODO Auto-generated method stub
						System.out.println("error");
						hideLoadProgressBar();
						Toast.makeText(getApplicationContext(), "提交失败",
								Toast.LENGTH_LONG).show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						hideLoadProgressBar();
						JSONObject returnData = JSONObject
								.parseObject(responseInfo.result);
						if ("OK".equals(returnData.getString("resultCode"))) {
							String plant = PrefUtil.getStringPref(MyPlantCrops.this, "cropSelectPlant");//本地保存作物种植id
							PrefUtil.savePref(MyPlantCrops.this,
									"zhonghua_plant", plant);
							showSelectCrop(croplist, plant);
							Toast.makeText(getApplicationContext(), "提交成功",
									Toast.LENGTH_LONG).show();
						}else{
							
							Toast.makeText(getApplicationContext(), returnData.getString("resultData"),
									Toast.LENGTH_LONG).show();
						}
					}
				});
	}

	private void setView() {
		System.out.println("account" + account.getPlant() + "account");
		if (account.getPlant() != null && account.getPlant() != "") {
			showSelectCrop(croplist, account.getPlant());
		}
		mAdapter = new CropSelectAdapter(this);
		mAdapter.setData(croplist);
		mAdapter.setCurrentPlant(0);// 0为种植作物，1为关注作物
		listView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}

	private void showSelectCrop(ArrayList<Categorys> croplist, String ids) {
		String id[] = ids.split(",");
		String nameStr = "";
		for (int k = 0; k < id.length; k++) {
			for (int i = 0; i < croplist.size(); i++) {
				ArrayList<SubCategorys> subCategorys = croplist.get(i)
						.getSubCategorysList();
				for (int j = 0; j < subCategorys.size(); j++) {
					if (Integer.parseInt(id[k]) == subCategorys.get(j).getId()) {
						nameStr += subCategorys.get(j).getCropName() + ",";
						break;
					}
				}
			}
		}

		if(nameStr.length()>0){
			selectCropTxt.setText(nameStr.substring(0, nameStr.length() - 1)
					.replace(",", " "));
		}
		

	}
}
