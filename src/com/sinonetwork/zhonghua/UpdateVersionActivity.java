package com.sinonetwork.zhonghua;

import org.json.JSONException;
import org.json.JSONObject;

import com.sinonetwork.zhonghua.utils.HttpUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 检查更新
 * 
 * @author Enway
 * 
 */
public class UpdateVersionActivity extends LandBaseActivity {
	TextView version, new_version, update_state, publish_date;
	JSONObject versionObj;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_abort_update_layout);
		setBackBtn();
		setTopTitleTV("检查更新");
		initView();
		update();
	}

	private void initView() {
		version = (TextView) findViewById(R.id.reuse_textView1);
		new_version = (TextView) findViewById(R.id.reuse_textView2);
		update_state = (TextView) findViewById(R.id.reuse_textView3);
		publish_date = (TextView) findViewById(R.id.reuse_textView4);
		version.setVisibility(View.VISIBLE);
		version.setText("当前版本：V "
				+ Double.parseDouble(getVersionCode(UpdateVersionActivity.this)
						+ ""));
	}

	// 检测当前有没有新版本
	private void update() {

		new Thread(new Runnable() {

			boolean success = true;
			final Handler handler = new Handler();

			@Override
			public void run() {

				try {
					JSONObject jsonObj = HttpUtil.getJSONObjFromUrlByGet(
							URLAddress.URLYI + "?method=getAllVersion", false);
					System.out.println(jsonObj);
					if (jsonObj.getString("resultcode").equals("ok")) {
						versionObj = jsonObj.getJSONObject("resultdata");
						success = true;
					} else {

						success = false;
					}
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {
							int v = 0;
							new_version.setVisibility(View.VISIBLE);
							update_state.setVisibility(View.VISIBLE);
							publish_date.setVisibility(View.VISIBLE);

							try {
								new_version.setText("最新版本：V "
										+ Double.parseDouble(versionObj
												.get("versionNum") + ""));
								publish_date.setText("发布日期："
										+ versionObj.getString("publishTime"));
								v = Integer.valueOf(versionObj
										.getString("versionNum"));
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							int currentV = getVersionCode(UpdateVersionActivity.this);
							if (v > currentV) {
								update_state.setText("更新状态：可以更新");
								try {
									newVersionAlert(versionObj
											.getString("versionDescription"));
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							} else {
								update_state.setText("更新状态：当前版本为最新版本");
							}
						} else {
							Toast.makeText(UpdateVersionActivity.this,
									"请求网络失败", 0).show();
						}
					}
				});
			}
		}).start();
	}

	/**
	 * 获取版本code
	 * 
	 * @param context
	 * @return
	 */
	public static int getVersionCode(Context context) {
		try {
			return context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 新版本提示框
	 */
	private void newVersionAlert(String summary) {
		final AlertDialog dialog = new AlertDialog.Builder(
				UpdateVersionActivity.this).create();
		dialog.setCancelable(false);
		View v = View.inflate(UpdateVersionActivity.this,
				R.layout.update_layout, null);
		TextView tv_summary = (TextView) v.findViewById(R.id.tv_summary);
		tv_summary.setText(summary);
		Button btn_cancle = (Button) v.findViewById(R.id.btn_cancle);
		Button btn_update = (Button) v.findViewById(R.id.btn_update);
		btn_cancle.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btn_update.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://211.94.93.238/zhnyxxgc/download.html"));
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		dialog.setView(v, 0, 0, 0, 0);
		dialog.show();
	}

}
