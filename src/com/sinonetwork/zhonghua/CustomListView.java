package com.sinonetwork.zhonghua;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class CustomListView extends ListView {

	public CustomListView(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	public CustomListView(Context context) {
		super(context);
	}

	public CustomListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	//���Զ���ؼ�ֻ����д��ListView��onMeasure������ʹ�䲻����ֹ�������ScrollViewǶ��GridViewҲ��ͬ��ĵ��?����׸���� 
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expand = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);

		super.onMeasure(widthMeasureSpec, expand);
	}

}
