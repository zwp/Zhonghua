package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.AMap.CancelableCallback;
import com.amap.api.maps2d.AMap.OnMarkerClickListener;
import com.amap.api.maps2d.CameraUpdate;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.sinonetwork.zhonghua.model.LandInfo;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class BasicMapActivity extends LandBaseActivity implements
		OnMarkerClickListener, AMapLocationListener, Runnable, LocationSource {
	private LocationManagerProxy aMapLocManager = null;
	private AMapLocation aMapLocation;// 用于判断定位超时
	private MapView mapView;
	private AMap aMap;
	protected ArrayList<LandInfo> list;

	private double geoLat;
	private double geoLng;
	private int index;
	private boolean frist = true;

	private Button rightBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.basicmap_activity);
		setBackBtn();
		setRightBtn();
		setTopTitleTV("测土");
		mapView = (MapView) findViewById(R.id.map);
		mapView.onCreate(savedInstanceState);// 此方法必须重写

		// 为0初次定位成功去接口请求数据标示，定位为循环定位，避免多次请求接口
		index = 0;
		init();

	}

	/**
	 * 初始化AMap对象
	 */
	private void init() {

		if (aMap == null) {
			aMap = mapView.getMap();
			aMap.clear();
			aMap.setOnMarkerClickListener(this);// 添加点击marker监听事件
			aMapLocManager = LocationManagerProxy.getInstance(this);

			aMap.setMyLocationEnabled(false);// 小蓝点的定位
			aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
			aMap.setLocationSource(this);// 设置定位监听

			/*
			 * mAMapLocManager.setGpsEnable(false);//
			 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
			 * API定位采用GPS和网络混合定位方式
			 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
			 */
			aMapLocManager.requestLocationData(
					LocationProviderProxy.AMapNetwork, 2000, 10, this);
			handler.postDelayed(this, 12000);// 设置超过12秒还没有定位到就停止定位

		}
	}

	private void loadData(final Double longitude, final Double latitude) {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					list = LandInfoParser.getList(longitude, latitude);

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {

							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void setView() {

		if (null != list && 0 < list.size()) {
			for (int i = 0; i < list.size(); i++) {
				String la = list.get(i).getLatitude();
				String lo = list.get(i).getLongitude();
				LatLng latLng = new LatLng(Double.parseDouble(la),
						Double.parseDouble(lo));

				MarkerOptions markerOptions = new MarkerOptions();
				markerOptions.position(latLng);
				markerOptions.title("" + list.get(i).getId());
				// markerOptions.snippet(""+i);

				markerOptions.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.marker));
				markerOptions.describeContents();
				aMap.addMarker(markerOptions);

			}
		} else {

			showShortToast("当前位置没有测土信息");
		}

	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		marker.getObject();

		Intent intent = new Intent(this, LandInfoDetailActivity.class);
		intent.putExtra(LandInfoDetailActivity.ID, marker.getTitle());
		startActivity(intent);
		return true;
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
		stopLocation();// 停止定位
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	/**
	 * 根据动画按钮状态，调用函数animateCamera或moveCamera来改变可视区域
	 */
	private void changeCamera(CameraUpdate update, CancelableCallback callback) {
		aMap.animateCamera(update, 1000, callback);
	}

	/**
	 * 销毁定位
	 */
	private void stopLocation() {
		if (aMapLocManager != null) {
			aMapLocManager.removeUpdates(this);
			aMapLocManager.destroy();
		}
		aMapLocManager = null;
	}

	/**
	 * 此方法已经废弃
	 */
	@Override
	public void onLocationChanged(Location location) {
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	/**
	 * 混合定位回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation location) {
		if (location != null) {
			// mListener.onLocationChanged(location);// 显示系统小蓝点
			this.aMapLocation = location;// 判断超时机制
			geoLat = location.getLatitude();
			geoLng = location.getLongitude();
			String cityCode = "";
			String desc = "";
			Bundle locBundle = location.getExtras();
			if (locBundle != null) {
				cityCode = locBundle.getString("citycode");
				desc = locBundle.getString("desc");
			}
			if (frist) {
				changeCamera(
						CameraUpdateFactory.newCameraPosition(new CameraPosition(
								new LatLng(geoLat, geoLng), 18, 0, 30)), null);
				frist = false;
			}
			String str = ("定位成功:(" + geoLng + "," + geoLat + ")"
					+ "\n精    度    :" + location.getAccuracy() + "米"
					+ "\n定位方式:" + location.getProvider() + "\n定位时间:"
					+ "\n城市编码:" + cityCode + "\n位置描述:" + desc + "\n省:"
					+ location.getProvince() + "\n市:" + location.getCity()
					+ "\n区(县):" + location.getDistrict() + "\n区域编码:" + location
					.getAdCode());
			// Log.v(TAG, "定位" + str);
			Log.i("gxx", "测土定位：" + str);
			// 初次定位成功去接口请求数据
			if (index == 0) {
				index++;
				loadData(geoLng, geoLat);
			}
		}
	}

	@Override
	public void run() {
		if (aMapLocation == null) {

			stopLocation();// 销毁掉定位12秒内还没有定位成功，停止定位
		}
	}

	public void setRightBtn() {
		rightBtn = (Button) findViewById(R.id.top_right_button);
		rightBtn.setVisibility(View.VISIBLE);
		;
		rightBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (PrefUtil.getBooleanPref(
						BasicMapActivity.this.getApplicationContext(),
						ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
					ZHAccount currentAccount = ZhAccountPrefUtil
							.getZHAccount(BasicMapActivity.this
									.getApplicationContext());
					// name = currentAccount.getUserName();

					Intent intent = new Intent(BasicMapActivity.this,
							Soil_Test.class);
					startActivity(intent);
				} else {
					Toast.makeText(BasicMapActivity.this, "请先登录！",
							Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(BasicMapActivity.this,
							MyDengLu.class);
					startActivity(intent);
				}

			}
		});
	}

	@Override
	public void activate(OnLocationChangedListener arg0) {
		// TODO Auto-generated method stub
		// mListener = listener;
		// if (mAMapLocationManager == null) {
		// mAMapLocationManager = LocationManagerProxy.getInstance(this);
		// //此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
		// //注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
		// //在定位结束后，在合适的生命周期调用destroy()方法
		// //其中如果间隔时间为-1，则定位只定一次
		// mAMapLocationManager.requestLocationData(
		// LocationProviderProxy.AMapNetwork, 60*1000, 10, this);
		// }
	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub

	}
}
