package com.sinonetwork.zhonghua;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;

public class DaJia extends Activity {

	private ImageView dajia_back;
	private WebView webView;
	private ImageView img_chaxun;
	private EditText edittext_dj;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_dajia);
		//默认情况下软件盘不自动弹出
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		dajia_back = (ImageView) findViewById(R.id.dajia_back);
		edittext_dj = (EditText) findViewById(R.id.edittext_dj);
		img_chaxun = (ImageView) findViewById(R.id.img_chaxun);
		webView = (WebView) findViewById(R.id.Toweb_dajia);
		webView.setVisibility(View.GONE);
		// webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setDefaultTextEncodingName("UTF-8");

		// Toast.makeText(DaJia.this, "进来了", Toast.LENGTH_SHORT).show();

		img_chaxun.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				webView.setVisibility(View.VISIBLE);
				webView.loadUrl("http://114.113.228.120/api/SearchAPI.aspx?txtCode=" + edittext_dj.getText().toString());
			}
		});
		WebSettings setting = webView.getSettings();
		// setting.setJavaScriptEnabled(true);
		// setting.setJavaScriptCanOpenWindowsAutomatically(true);
		// setting.setAllowFileAccess(true);// 设置允许访问文件数据
		// // setting.setSupportZoom(true);
		// setting.setBuiltInZoomControls(true);
		//
		// setting.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		// setting.setLoadWithOverviewMode(true);
		//
		// // setting.setJavaScriptCanOpenWindowsAutomatically(true);
		// setting.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		// setting.setDomStorageEnabled(true);
		// setting.setDatabaseEnabled(true);
		setting.setDefaultTextEncodingName("GBK");// 设置字符编码
		//
		//
		// webView.getSettings() .setUseWideViewPort(true);
		// webView.getSettings().setLoadWithOverviewMode(true);

		// 如果页面中链接，如果希望点击链接继续在当前browser中响应，
		// 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象
		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
		});
		dajia_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

}
