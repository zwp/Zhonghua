package com.sinonetwork.zhonghua;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EditUserPassword extends Activity {
	private ImageView back;
	private TextView title;
	private EditText  user_password1;
	private EditText  user_password2;
	private EditText  user_password0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edituser_password);
		initView();
	}
	private void initView() {
		// TODO Auto-generated method stub
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		user_password1=(EditText) findViewById(R.id.edituser_password_et1);
		user_password2=(EditText) findViewById(R.id.edituser_password_et2);
		user_password0=(EditText) findViewById(R.id.edituser_password_et0);
		
		title.setText("修改用户密码");
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditUserPassword.this.finish();
			}
		});
	}
	public  void name(View v){
		if (user_password0.getText().equals("")) {
			Toast.makeText(EditUserPassword.this, "旧密码不能为空！", 2000).show();
			return;
		}
		if (user_password1.getText().toString().equals("")||user_password2.getText().toString().equals("")) {
			Toast.makeText(EditUserPassword.this, "密码不能为空！", 2000).show();
			return;
		}
		if (!user_password1.getText().toString().equals(user_password2.getText().toString()) ) {
			Toast.makeText(EditUserPassword.this, "两次密码不一致!", 2000).show();
			return;
		}
		//获取当前登入用户的信息    
		ZHAccount currentAccount = ZhAccountPrefUtil.getZHAccount(EditUserPassword.this.getApplicationContext());
		//发送修改密码的url
		HttpHelp.getInstance().send(HttpMethod.GET, URLAddress.changeInfoName(currentAccount.getUserName(),user_password0.getText().toString(), user_password1.getText().toString(),""), new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				// TODO Auto-generated method stub
				Toast.makeText(EditUserPassword.this, "原密码错误！", Toast.LENGTH_LONG).show();
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				// TODO Auto-generated method stub
				JSONObject result = JSONObject.parseObject(arg0.result);
				Toast.makeText(EditUserPassword.this,
						result.getString("resultData"),
						Toast.LENGTH_LONG).show();
				EditUserPassword.this.finish();
			}

			
		});
		
	}
}
