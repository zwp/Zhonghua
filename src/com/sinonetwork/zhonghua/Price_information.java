package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.Price_InformationAdapter;
import com.sinonetwork.zhonghua.model.PriceHangqing.PriceHangqings;
import com.sinonetwork.zhonghua.model.ShengFenModel.ShengFenModels;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class Price_information extends LandBaseActivity  implements AMapLocationListener,
Runnable {
	private Spinner com_spinner1;
	private Spinner sp2;
	private AMapLocation aMapLocation;// 用于判断定位超时
	private int index;
	private double getLat;
	private double getLng;
	private LocationManagerProxy aMapLocManager = null;


	private ListView listView;
	private Price_InformationAdapter adapter;
	private ImageView back;

	// private String[] name = { "紫苏", "土鸡蛋", "猪五花肉", "紫苏", "土鸡蛋" };
	// private String[] jiage = { "56.76", "55", "67.65", "44", "22" };
	// private String[] shichang = { "老虎沟市场", "老虎沟市场", "老虎沟市场", "老虎沟市场", "老虎沟市场"
	// };
	// private String[] shijian = { "2015-07-02", "2015-07-02", "2015-07-02",
	// "2015-07-02", "2015-07-02" };
	private List<ShengFenModels> sdata;
	private List<String> list;
	private List<String> diquList;
	private String sheng, diqu;
	private List<PriceHangqings> dataList;
	private List<Map<String, Object>> data;
	private ArrayAdapter<String> adapters, adapters1;
	
	
	private double latitude=0.0;  
	private double longitude =0.0;  
	private String dqdiqu;
	private ProgressBar load_progressBar;
	
	private LocationManager locationManager;
//	protected ArrayList<Warning> list;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.price_information);
		com_spinner1 = (Spinner) findViewById(R.id.price_sp1);
		sp2 = (Spinner) findViewById(R.id.price_sp2);
		load_progressBar = (ProgressBar) findViewById(R.id.load_progressBar);

		listView = (ListView) findViewById(R.id.price_lv);
		back = (ImageView) findViewById(R.id.back);
		index = 0;
		init();
		


	
		list = new ArrayList<String>();
		adapters = new ArrayAdapter<String>(Price_information.this,
				R.layout.guojia_item, R.id.tvCateItem, list);
		com_spinner1.setAdapter(adapters);

		diquList = new ArrayList<String>();
		adapters1 = new ArrayAdapter<String>(Price_information.this,
				R.layout.guojia_item, R.id.tvCateItem, diquList);
		sp2.setAdapter(adapters1);

		data = new ArrayList<Map<String, Object>>();
		adapter = new Price_InformationAdapter(Price_information.this, data);
		listView.setAdapter(adapter);
		setMonitor();

	}

	private void init() {
		aMapLocManager = LocationManagerProxy.getInstance(this);
		/*
		 * mAMapLocManager.setGpsEnable(false);//
		 * 1.0.2版本新增方法，设置true表示混合定位中包含gps定位，false表示纯网络定位，默认是true Location
		 * API定位采用GPS和网络混合定位方式
		 * ，第一个参数是定位provider，第二个参数时间最短是2000毫秒，第三个参数距离间隔单位是米，第四个参数是定位监听者
		 */
		aMapLocManager.requestLocationData(LocationProviderProxy.AMapNetwork,
				2000, 10, this);
		handler.postDelayed(this, 12000);// 设置超过12秒还没有定位到就停止定位
	}

	public void setMonitor() {
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		com_spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (!list.equals("") && list != null && list.size() != 0) {
					sheng = list.get(position);
					dqdiqu = sheng;
//					Log.v("zhong","dqdiqu--"+dqdiqu);
					searchQuotationData();

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		sp2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (!diquList.equals("") && diquList != null
						&& diquList.size() != 0) {
					diqu = diquList.get(position);
					initData(diqu);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
	}
	
	
	


	
	@Override
	public void run() {
		if (aMapLocation == null) {

			stopLocation();// 销毁掉定位12秒内还没有定位成功，停止定位
		}
	}
	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		stopLocation();// 停止定位
	}

	/**
	 * 混合定位回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation location) {
		if (location != null) {
			// mListener.onLocationChanged(location);// 显示系统小蓝点
			this.aMapLocation = location;// 判断超时机制
			getLat = location.getLatitude();
			getLng = location.getLongitude();
			String cityCode = "";
			String desc = "";
			Bundle locBundle = location.getExtras();
			if (locBundle != null) {
				cityCode = locBundle.getString("citycode");
				desc = locBundle.getString("desc");
			}
			String str = ("定位成功:(" + getLat + "," + getLng + ")"
					+ "\n精    度    :" + location.getAccuracy() + "米"
					+ "\n定位方式:" + location.getProvider() + "\n定位时间:"
					+ "\n城市编码:" + cityCode + "\n位置描述:" + desc + "\n省:"
					+ location.getProvince() + "\n市:" + location.getCity()
					+ "\n区(县):" + location.getDistrict() + "\n区域编码:" + location
					.getAdCode());
//			 Log.v(TAG, "定位" + str);
			// 初次定位成功去接口请求数据
			if (index == 0) {
				index++;
				dqdiqu = location.getProvince();
//				Log.v("zhong","dqdiqu--"+dqdiqu);
				getProvinceData();
				
			}
		}
	}
	//拿到所有的省，默认显示根据定位省
	public void getProvinceData() {
		showLoadProgressBar();
			RequestParams params = new RequestParams();
			
			HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.SomeAreaURL,
					params, new RequestCallBack<String>() {

						@Override
						public void onFailure(HttpException arg0, String arg1) {
							hideLoadProgressBar();
							showShortToast("获取数据失败");
						}

						@Override
						public void onSuccess(ResponseInfo<String> resultInfo) {
							
							JSONObject json = JSONObject
									.parseObject(resultInfo.result);
//							Log.v("zhong","json--"+json);
							JSONArray sdata = json.getJSONArray("resultData");
								for (int i = 0; i < sdata.size(); i++) {
									JSONObject jObject = JSONObject
											.parseObject(sdata.get(i)
													.toString());
									String str= jObject.getString("AREANAME");
									list.add(str);
									if(list.get(i).contains(dqdiqu)){
										com_spinner1.setSelection(i);
									//com_spinner1.setSelection(18,true);
								}
								}

								adapters.notifyDataSetChanged();
									
								searchQuotationData();
							

						}
					});
		
	}
	//根据省获取区
	public void searchQuotationData() {
		
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "searchQuotationMarket");
		params.addBodyParameter("regionName", dqdiqu);
		
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
				params, new RequestCallBack<String>() {
			
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				hideLoadProgressBar();
				showShortToast("获取数据失败");
				
			}
			
			@Override
			public void onSuccess(ResponseInfo<String> resultInfo) {
				
				JSONObject json = JSONObject
						.parseObject(resultInfo.result);
//				Log.v("zhong","json--"+json);
				if(json.getString("resultcode").equals("error")){
					
				}else{
					diquList.clear();
					JSONArray a = json.getJSONArray("resultdata");
					String d = (String) a.get(0);
					for(int i=0;i<a.size();i++){
						diquList.add((String) a.get(i));
					}
					initData(diquList.get(0));
					adapters1.notifyDataSetChanged();
//					Log.v("zhong","json--"+d);
				}
				
				
			}
		});
		
	}
	//根据省和区拿到市场价格
	public void initData(String diqu){
		showLoadProgressBar();
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "searchQuotation");
		params.addBodyParameter("regionName", dqdiqu);
		params.addBodyParameter("marketName", diqu);
		
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
				params, new RequestCallBack<String>() {
			
			@Override
			public void onFailure(HttpException arg0, String arg1) {
				hideLoadProgressBar();
			}
			
			@Override
			public void onSuccess(ResponseInfo<String> resultInfo) {
				hideLoadProgressBar();
				JSONObject json = JSONObject
						.parseObject(resultInfo.result);
//				Log.v("zhong","initDatajson--"+json);
				if(json.getString("resultcode").equals("error")){
					
				}else{
					JSONArray dataList = json.getJSONArray("resultdata");
//					Log.v("zhong","a.size()--"+dataList.size());
					data.clear();
					for (int i = 0; i < dataList.size(); i++) {
						JSONObject jObject = JSONObject
								.parseObject(dataList.get(i)
										.toString());
						Map<String, Object> map = new HashMap<String, Object>();
						
						map.put("name", jObject.getString("d2"));
						map.put("jiage", jObject.getString("d3"));
						map.put("shichang", jObject.getString("d7"));
						map.put("shijian", jObject.getString("publishTime"));
						data.add(map);
					}
					adapter.notifyDataSetChanged();
				}
				
				
			}
		});
	}
	
	/**
	 * 销毁定位
	 */
	private void stopLocation() {
		if (aMapLocManager != null) {
			aMapLocManager.removeUpdates(this);
			aMapLocManager.destroy();
		}
		aMapLocManager = null;
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
}
