package com.sinonetwork.zhonghua;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

//import com.facebook.drawee.backends.pipeline.Fresco;

public class AppAplication extends Application {

	private static Context mContext;

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		//Fresco.initialize(mContext);
	}

	public static Context getContext() {
		return mContext;
	}
	//app不随系统字体的设置而改变，下周再改变
	@Override  
	public Resources getResources() {  
	    Resources res = super.getResources();    
	    Configuration config=new Configuration();    
	    config.setToDefaults();    
	    res.updateConfiguration(config,res.getDisplayMetrics() );  
	    return res;  
	}  
}