package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.searchExampleTypeAdapter;
import com.sinonetwork.zhonghua.model.searchExampleType;
import com.sinonetwork.zhonghua.model.searchExampleType.searchExampleTypes;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class searchExampleTypei extends Activity {

	private ListView searchExampleTypei;
	private searchExampleTypeAdapter adapter;
	private List<searchExampleTypes> data;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchexampletypei);
		searchExampleTypei = (ListView) findViewById(R.id.searchExamples_lv);
		
		
	}
	
	
	public void loadData(String method,String type) {
		Toast.makeText(searchExampleTypei.this, "来咯来咯", Toast.LENGTH_SHORT).show();
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("type", type);
		Logger.e("No1");
		data = new ArrayList<searchExampleTypes>();
		Logger.e("No1s");
		FastJsonRequest<searchExampleType> fastJson = new FastJsonRequest<searchExampleType>(
				Method.POST, URLAddress.searchExampleTypeURL, searchExampleType.class,
				null, map, new Response.Listener<searchExampleType >() {
					@Override
					public void onResponse(searchExampleType weather) {
						// 赋值
//						Logger.e("=====tiaoshu======"+weather.getResultdata().size());
						
						if(weather.getResultdata().size()==0||weather.getResultdata()==null){
							
							Toast.makeText(searchExampleTypei.this, "没有数据", Toast.LENGTH_SHORT).show();
						}else{
							data.addAll(weather.getResultdata());
							adapter.notifyDataSetChanged();
						}
						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(searchExampleTypei.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4"+fastJson);
		RequestManager.addRequest(fastJson, this);
	}
}
