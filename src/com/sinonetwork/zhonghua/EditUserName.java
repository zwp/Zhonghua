package com.sinonetwork.zhonghua;

import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EditUserName extends Activity {
	private ImageView back;
	private TextView title;
	private EditText  user_name;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edituser_name);
		initView();
	}
	private void initView() {
		// TODO Auto-generated method stub
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		user_name=(EditText) findViewById(R.id.edituser_name_et);
		
		title.setText("修改用户名称");
		back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				EditUserName.this.finish();
			}
		});
	}
	public  void name(View v){
		if (user_name.getText().toString().equals("")) {
			Toast.makeText(EditUserName.this, "新用户名不能为空", 2000).show();
			return;
		}
		HttpHelp.getInstance().send(HttpMethod.GET, URLAddress.changeInfo(user_name.getText().toString(), null, null,null), new RequestCallBack<String>() {

			@Override
			public void onFailure(HttpException arg0, String arg1) {
				// TODO Auto-generated method stub
				Toast.makeText(EditUserName.this, "修改失败！", Toast.LENGTH_LONG).show();
			}

			@Override
			public void onSuccess(ResponseInfo<String> arg0) {
				// TODO Auto-generated method stub
				Log.i("gxx", "修改名字："+arg0);
			}

			
		});
		
	}
}
