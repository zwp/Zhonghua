package com.sinonetwork.zhonghua;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * 关于农易宝
 * 
 * @author Enway
 * 
 */
public class AbortNongyibaoActivity extends LandBaseActivity {
	TextView abort_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_abort_update_layout);
		abort_text = (TextView) findViewById(R.id.reuse_textView1);
		abort_text.setVisibility(View.VISIBLE);
		abort_text.setText(getAbortText());
		setBackBtn();
		setTopTitleTV("关于农易宝");
	}

	private String getAbortText() {
		StringBuilder string = new StringBuilder();
		string.append("中化化肥农易宝，为广大农户提供一个从种到收的个人信息平台，为提高作物增产，科学使用化肥进行全程指导，利用互联网手段推动农村信息化建设，通过合理科学的施肥、选种使农户提前致富。");
		string.append("\n");
		string.append("\n");
		string.append("农易宝交流群：487955846");
		string.append("\n");
		string.append("业务合作：18911998667");
		return string.toString();

	}
}
