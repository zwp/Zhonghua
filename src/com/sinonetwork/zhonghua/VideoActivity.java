package com.sinonetwork.zhonghua;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.sinonetwork.zhonghua.model.News;
import com.sinonetwork.zhonghua.utils.URLAddress;
/**
 * 通过网址访问服务端的一些视频，现在此视频并不存在。
 * @author enway
 *
 */
public class VideoActivity extends LandBaseActivity {

	private String id;
	private WebView webView;
	protected News detail;
	private VideoView videoView;
	private Uri uri;
	private int mPositionWhenPaused;
	public static final String ID = "ID";
	public static final String NAME = "NAME";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);

		setBackBtn();
		setTopTitleTV("播放视频");
		String URL = getIntent().getStringExtra("URL");
		showLoadProgressBar();
		videoView = (VideoView) findViewById(R.id.video);
		Log.v("zhong","shi--"+videoView.isPlaying());

		uri = Uri.parse(URLAddress.URL+"/vod/"+URL+".mp4");
//		uri = Uri.parse(URLAddress.URL+"/vod/cetu_xiaomai.mp4");
		// uri = Uri.parse("android.resource://com.example.testwebview/" +
		// R.raw.v);

		videoView.setVideoURI(uri);
		 videoView.start();
		 
		/**
		 * w为其提供一个控制器，控制其暂停、播放……等功能
		 */
		 
		videoView.setMediaController(new MediaController(VideoActivity.this));

		videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer arg0) {
				// TODO Auto-generated method stub
				hideLoadProgressBar();
			}
		});
		/**
		 * 视频或者音频到结尾时触发的方法
		 */
		videoView
				.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer mp) {
					}
				});

		videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.i("通知", "播放中出现错误");
				return false;
			}
		});

	}

	// 开始
	public void onStart() {
		// Play Video
		// videoView.setVideoURI(uri);
		// videoView.start();

		super.onStart();
	}

	// 暂停

	public void onPause() {
		// Stop video when the activity is pause.
		mPositionWhenPaused = videoView.getCurrentPosition();
		videoView.stopPlayback();
		Log.d(TAG, "OnStop: mPositionWhenPaused = " + mPositionWhenPaused);
		Log.d(TAG, "OnStop: getDuration  = " + videoView.getDuration());

		super.onPause();
	}

	public void onResume() {
		// Resume video player
		if (mPositionWhenPaused >= 0) {
			videoView.seekTo(mPositionWhenPaused);
			mPositionWhenPaused = -1;
		}

		super.onResume();
	}
}