package com.sinonetwork.zhonghua;

import java.util.Timer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class EditUserPhone extends Activity {
	private ImageView back;
	private TextView title;
	private EditText user_phone;
	// private EditText edVerifyCode;
	// private Button btnVerifyCode;
//	private int randomCode;
//	private int seconds = 5;
//	private Timer timer;

	// private Handler handler = new Handler() {
	// public void handleMessage(android.os.Message msg) {
	// String content = (String) msg.obj;
	// btnVerifyCode.setText(content);
	// if (content.equals("获取验证码")) {
	// btnVerifyCode.setEnabled(true);
	// timer.cancel();
	// }
	// };
	// };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edituser_phone);
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		user_phone = (EditText) findViewById(R.id.edituser_password_et1);
		// edVerifyCode = (EditText) findViewById(R.id.ed_verifyCode);
		// btnVerifyCode = (Button) findViewById(R.id.btn_verify);
		title.setText("修改用户手机号");
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditUserPhone.this.finish();
			}
		});

		// btnVerifyCode.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Random random = new Random();
		// randomCode = random.nextInt(8999) + 1000;
		// String tel = ZhAccountPrefUtil.getZHAccount(EditUserPhone.this)
		// .getTelephone();
		// Toast.makeText(EditUserPhone.this, "验证码获取成功，没有收到60s后请重试", 2000)
		// .show();
		// btnVerifyCode.setEnabled(false);
		// timer = new Timer();
		// TimerTask task = new TimerTask() {
		//
		// @Override
		// public void run() {
		// Message msg = new Message();
		// if (seconds != 0) {
		// msg.obj = "请在" + seconds-- + "秒后重试";
		// } else {
		// seconds = 60;
		// msg.obj = "获取验证码";
		// }
		// handler.sendMessage(msg);
		// }
		// };
		// timer.schedule(task, 0, 1000);
		// RequestParams params = new RequestParams();
		// params.addBodyParameter("phone", tel);
		// params.addBodyParameter("message", randomCode + "");
		//
		// HttpHelp.getInstance().send(HttpMethod.POST,
		// URLAddress.SendMessage, params,
		// new RequestCallBack<String>() {
		//
		// @Override
		// public void onFailure(HttpException arg0,
		// String arg1) {
		//
		// }
		//
		// @Override
		// public void onSuccess(ResponseInfo<String> arg0) {
		//
		// }
		// });
		//
		// }
		// });
	}

	public void name(View v) {
		if (!user_phone
				.getText()
				.toString()
				.matches(
						"^((13[0-9])|(15[^4,\\D])|(18[0,0-9])|(14[0-9])|(17[0-9]))\\d{8}$")) {
			Toast.makeText(EditUserPhone.this, "请输入正规手机号！", 2000).show();
			return;
		}
		// if (!edVerifyCode.getText().toString().equals(randomCode + "")) {
		// Toast.makeText(EditUserPhone.this, "请输入正确地验证码！", 2000).show();
		// return;
		// }
		// 获取当前登入用户的信息
		ZHAccount currentAccount = ZhAccountPrefUtil
				.getZHAccount(EditUserPhone.this.getApplicationContext());
		Log.i("gxx", "获取到的登入用户的信息：" + currentAccount.getUserName());

		// 发送修改密码的url
		HttpHelp.getInstance().send(
				HttpMethod.GET,
				URLAddress.changeInfoPhone(currentAccount.getUserName(),
						user_phone.getText().toString(), ""),
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// TODO Auto-generated method stub
						Toast.makeText(EditUserPhone.this, "修改失败！",
								Toast.LENGTH_LONG).show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						// TODO Auto-generated method stub
						JSONObject result = JSONObject.parseObject(arg0.result);
						Toast.makeText(EditUserPhone.this,
								result.getString("resultData"),
								Toast.LENGTH_LONG).show();
						EditUserPhone.this.finish();
					}
				});

	}
}
