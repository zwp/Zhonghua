package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.ExparimentAdapter;
import com.sinonetwork.zhonghua.model.searchExampleType;
import com.sinonetwork.zhonghua.model.searchExampleType.searchExampleTypes;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class Expariment extends Activity {
	private RelativeLayout experiment_rl1, experiment_rl2, experiment_rl3;
	private ImageView experiment_img1, experiment_img2;
	private ListView experiment_list1, experiment_list2;
	private boolean ischoose = true;
	private ImageView back;
	private List<searchExampleTypes> data;
	private ExparimentAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.experiment);
		// 寻找控件
		initView();
		// 回退
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// 单品试验示范
		experiment_rl1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (ischoose) {
					experiment_img1.setImageResource(R.drawable.arrow_down);
					experiment_list1.setVisibility(View.VISIBLE);
					ischoose = false;
				} else {
					experiment_img1.setImageResource(R.drawable.arrow_right);
					experiment_list1.setVisibility(View.GONE);
					ischoose = true;
				}
				if (experiment_list1.getVisibility() == View.VISIBLE) {
					loadData("searchExampleType", "1");
					// 显示数据
					adapter = new ExparimentAdapter(Expariment.this, data);
					adapter.setData(data);
					experiment_list1.setAdapter(adapter);
				}
			}
		});
		// 套餐试验示范
		experiment_rl2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (ischoose) {
					experiment_img2.setImageResource(R.drawable.arrow_down);
					experiment_list2.setVisibility(View.VISIBLE);
					ischoose = false;
				} else {
					experiment_img2.setImageResource(R.drawable.arrow_right);
					experiment_list2.setVisibility(View.GONE);
					ischoose = true;
				}
				if (experiment_list2.getVisibility() == View.VISIBLE) {
					loadData("searchExampleType", "2");
					adapter = new ExparimentAdapter(Expariment.this, data);
					adapter.setData(data);
					experiment_list2.setAdapter(adapter);
				}
			}
		});
		// 优秀试验报告分享
		experiment_rl3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						DocActivity.class);
				startActivity(intent);
			}
		});
		//单品试验示范点击
		experiment_list1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(Expariment.this,
						searchExamples.class);
				Bundle bundle = new Bundle();
				// bundle.putString("exampleType", data.get(arg2).getType()
				// .toString());
				bundle.putString("exampleType", "1");
				bundle.putString("exampleTypeId", data.get(arg2).getId()
						.toString());
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		//套餐示范试验点击
		experiment_list2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(Expariment.this,
						searchExamples.class);
				Bundle bundle = new Bundle();
				// bundle.putString("exampleType", data.get(arg2).getType()
				// .toString());
				bundle.putString("exampleType", "2");
				bundle.putString("exampleTypeId", data.get(arg2).getId()
						.toString());
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});

	}

	// 初始化控件
	private void initView() {
		experiment_rl1 = (RelativeLayout) findViewById(R.id.experiment_rl1);
		experiment_rl2 = (RelativeLayout) findViewById(R.id.experiment_rl2);
		experiment_rl3 = (RelativeLayout) findViewById(R.id.experiment_rl3);
		experiment_img1 = (ImageView) findViewById(R.id.experiment_img1);
		experiment_img2 = (ImageView) findViewById(R.id.experiment_img2);
		experiment_list1 = (ListView) findViewById(R.id.experiment_list1);
		experiment_list2 = (ListView) findViewById(R.id.experiment_list2);
		back = (ImageView) findViewById(R.id.back);
	}

	// 加载数据
	public void loadData(String method, String type) {
		// Toast.makeText(Expariment.this, "来咯来咯", Toast.LENGTH_SHORT).show();
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("type", type);
		data = new ArrayList<searchExampleTypes>();
		// Volley框架
		FastJsonRequest<searchExampleType> fastJson = new FastJsonRequest<searchExampleType>(
				Method.POST, URLAddress.searchExampleTypeURL,
				searchExampleType.class, null, map,
				new Response.Listener<searchExampleType>() {
					@Override
					public void onResponse(searchExampleType weather) {
						data.addAll(weather.getResultdata());
						if (data.size() == 0 || data == null) {
							Toast.makeText(Expariment.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						}
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						Toast.makeText(Expariment.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}

}
