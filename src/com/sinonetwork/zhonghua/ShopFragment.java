package com.sinonetwork.zhonghua;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ShopFragment extends Fragment{
 
	private WebView vv;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_shop,
				container, false);
		vv = (WebView) v.findViewById(R.id.Toweb);
		vv.loadUrl("http://221.228.238.72:8888/fert_bbc/weixin/platform/index.htm");  
//        Toast.makeText(getActivity(), "来了", Toast.LENGTH_SHORT).show();
        //设置可自由缩放网页  
        vv.getSettings().setSupportZoom(true);  
        vv.getSettings().setBuiltInZoomControls(true);  
        
     // 如果页面中链接，如果希望点击链接继续在当前browser中响应，  
        // 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象  
        vv.setWebViewClient(new WebViewClient(){
        	public boolean shouldOverrideUrlLoading(WebView view, String url)  
            {   
                //  重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边  
                view.loadUrl(url);  
                        return true;  
            }
        });
     
		return v;
	}
}
