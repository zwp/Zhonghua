package com.sinonetwork.zhonghua;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.apache.http.Header;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.sinonetwork.zhonghua.adapter.QixiangQueryAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class QixiangQuery extends Activity {
	private ListView listView;
	private QixiangQueryAdapter adapter;
	private ImageView back;
	private JiWenModel entity;

	private int year;
	private int month;
	private int day;

	private String startDate = "";
	private String endDate = "";
	private String low = "5";
	private String type = "0";

	private TextView tv_start;
	private TextView tv_end;
	private TextView tv_low;
	private TextView tv_type;
	private Button bt_search;
	private ProgressBar pb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qixiang_query);

		listView = (ListView) findViewById(R.id.qixiang_quety_list);
		back = (ImageView) findViewById(R.id.back);
		tv_start = (TextView) findViewById(R.id.tv_start);
		tv_end = (TextView) findViewById(R.id.tv_end);
		tv_low = (TextView) findViewById(R.id.tv_low);
		tv_type = (TextView) findViewById(R.id.tv_type);
		bt_search = (Button) findViewById(R.id.bt_search);
		pb=(ProgressBar) findViewById(R.id.qixiang_quety_progressBar);
		initTime();

		setDefault();

		setListener();

		// initdatas();// 获取前一页面传过来的信息:城市编码 使用http中的get获取json数据 解析
	}

	private void setDefault() {
		tv_start.setText(startDate);
		tv_end.setText(endDate);
		tv_low.setText(low);
		tv_type.setText("活动积温");
	}

	private void setListener() {
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});

		tv_start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new DatePickerDialog(QixiangQuery.this, AlertDialog.THEME_HOLO_LIGHT, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						monthOfYear++;
						startDate = "" + year;
						if (monthOfYear < 10) {
							startDate += "0" + monthOfYear;
						} else {
							startDate += monthOfYear;
						}
						if (dayOfMonth < 10) {
							startDate += "0" + dayOfMonth;
						} else {
							startDate += dayOfMonth;
						}
						tv_start.setText(startDate);
					}
				}, year, month, day).show();
			}
		});

		tv_end.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				new DatePickerDialog(QixiangQuery.this, AlertDialog.THEME_HOLO_LIGHT, new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						monthOfYear++;
						endDate = "" + year;
						if (monthOfYear < 10) {
							endDate += "0" + monthOfYear;
						} else {
							endDate += monthOfYear;
						}
						if (dayOfMonth < 10) {
							endDate += "0" + dayOfMonth;
						} else {
							endDate += dayOfMonth;
						}
						tv_end.setText(endDate);
					}
				}, year, month, day).show();
			}
		});

		tv_low.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String[] array = new String[] { "0", "5", "10", "20" };

				new AlertDialog.Builder(QixiangQuery.this).setItems(array, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						low = array[which];
						tv_low.setText(array[which]);
					}
				}).show();
			}
		});

		tv_type.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String[] array = new String[] { "活动积温", "有效积温" };

				new AlertDialog.Builder(QixiangQuery.this).setItems(array, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						type = "" + which;
						tv_type.setText(array[which]);
					}
				}).show();
			}
		});

		bt_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listView.setVisibility(View.INVISIBLE);
				pb.setVisibility(View.VISIBLE);
				loadData();
			}
		});
	}

	private void initTime() {
		Calendar mycalendar = Calendar.getInstance(Locale.CHINA);
		year = mycalendar.get(Calendar.YEAR);
		month = mycalendar.get(Calendar.MONTH);
		day = mycalendar.get(Calendar.DAY_OF_MONTH);

		startDate = new SimpleDateFormat("yyyyMMdd").format(mycalendar.getTime());
		endDate = new SimpleDateFormat("yyyyMMdd").format(mycalendar.getTime());
	}

	private void loadData() {
		String bianma = getIntent().getStringExtra("data");
//		Log.i("lk", "传递过来的信息：" + bianma);
		// TODO
//		bianma = "101010100";
		String url = "http://211.94.93.238/zhnyxxgc/httpservice.action?method=searchAccumulatedTemperature&areaid="
				+ bianma + "&starttime=" + startDate + "&endtime=" + endDate + "&low=" + low + "&sort=" + type;
//		Log.i("gxx", "积温url：" + url);

		new AsyncHttpClient().get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				// 状态切换
				listView.setVisibility(View.VISIBLE);
				pb.setVisibility(View.INVISIBLE);
				// byte数据转换成String 数组
				String a = new String(response);
				// 使用自己定义的类 解析
				entity = JSON.parseObject(a, JiWenModel.class);
//				Log.i("gxx", "json解析的数据：" + entity);
//				Log.i("lk", "json解析的数据：" + entity);
				
				if (entity.getResultdata() != null) {
					adapter = new QixiangQueryAdapter(QixiangQuery.this, entity.getResultdata());
					
					listView.setAdapter(adapter);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
			}

			@Override
			public void onRetry(int retryNo) {
			}
		});

	}
}
