package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.MyReplyAdapter;
import com.sinonetwork.zhonghua.model.Reply;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;

public class MyReply extends LandBaseActivity {
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;
	private static final int NO_MORE_DARA = 12;
	MyReplyAdapter adapter;
	MyListView replyListView;
	List<Map<String, Object>> list;
	private List<Reply> replys = new ArrayList<Reply>();
	private int currentPage = 1;
	private int totalPage;
	private String userName= "";
	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case NO_MORE_DARA:
				replyListView.misHaveNewDatas = false;
				replyListView.onLoadMoreComplete();
				replyListView.setEndFootGone();
				break;
			case LOAD_DATA_FINISH:
				if (adapter != null) {
					adapter.setBigEventAdapterData((ArrayList<Reply>) msg.obj);
					adapter.notifyDataSetChanged();
				}
				replyListView.misHaveNewDatas = false;
				replyListView.onLoadMoreComplete();
				break;
			case REFRESH_DATA_FINISH:
				replyListView.setEndFootVisiable();
				if (adapter != null) {
					adapter = new MyReplyAdapter(MyReply.this, replys
							);
					adapter.setBigEventAdapterData(replys);
					replyListView.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}
				replyListView.onRefreshComplete();
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_reply);
		setBackBtn();
		setTopTitleTV("我的回复");
		if (PrefUtil.getBooleanPref(getApplicationContext(),
				ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			 userName = PrefUtil.getStringPref(getApplicationContext(), "zhonghua_userName");
			init();	
		}else {
			Toast.makeText(getApplicationContext(), "用户名为空，请先登录！", 2000).show();
		}
	}
	
	private void init(){
		Log.i("gxx", "进行到此处没问题");
		replyListView = (MyListView) findViewById(R.id.replyList);
		replyListView.setOnRefreshListener(new OnRefreshListener() {
			
			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				loadData(0);
			}
		});
		replyListView.setOnLoadListener(new OnLoadMoreListener() {
			
			@Override
			public void onLoadMore() {
				// TODO Auto-generated method stub
				loadData(1);
			}
		});
		//adapter = new MyReplyAdapter(getApplicationContext(), questions);
		replyListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MyReply.this,MyQuestionDetail.class);
				Bundle bundle = new Bundle();
				bundle.putBoolean("flag", true);
				bundle.putInt("QuestionId", replys.get(position-1).getQuestionId());
//				Log.i("gxx", "跳转时的item的id："+replys.get(position-1));
//				Log.i("gxx", "跳转时的问题的id："+replys.get(position-1).getQuestionId());
				intent.putExtras(bundle);
//				Log.i("gxx", "跳转时携带的信息："+bundle);
				startActivity(intent);
			}
		});
		loadData(0);//加载数据
		adapter = new MyReplyAdapter(getApplicationContext(), replys);
		replyListView.setAdapter(adapter);
	}
    
	private void loadData(final int type){
		new Thread() {
			public void run() {
				switch (type) {
				case 0:
					replys.clear();
					currentPage = 1;
					addContent(1, 0, userName);
					break;
				case 1:
					currentPage++;
					if (currentPage <= totalPage) {
						addContent(currentPage, 1, userName);
					} else {
						currentPage--;
						Message _Msg = mHandler.obtainMessage(NO_MORE_DARA);
						mHandler.sendMessage(_Msg);

					}
					break;
				}
			};
		}.start();
	}

    private void addContent(int currentPage, final int type, String userName){
    	RequestParams params = new RequestParams();
		params.addBodyParameter("method", "searchReply");
		params.addBodyParameter("userName", userName);
		params.addBodyParameter("curPage", currentPage + "");
		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLYI, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						// TODO Auto-generated method stub
						System.out.println("error");
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						// TODO Auto-generated method stub
						System.out.println("success");
						JSONObject jsonObject = JSONObject
								.parseObject(responseInfo.result);
						System.out.println(jsonObject.get("resultcode"));
						JSONObject json = JSONObject.parseObject(jsonObject
								.toString());
						totalPage = json.getIntValue("totalPages");
						JSONArray jsonArray = json.getJSONArray("resultdata");
						List<Reply> datas = JSONArray.parseArray(jsonArray.toJSONString(),
								Reply.class);
						System.out.println(datas.size()+"data");
						
						for (int i = 0; i < datas.size(); i++) {
							if (!datas.get(i).getQuestions().toString().equals("[null]")) {
								replys.add(datas.get(i));
							}
						}

//						replys.addAll(datas);
						if (type == 0) { // 下拉刷新
							Message _Msg = mHandler.obtainMessage(
									REFRESH_DATA_FINISH, replys);
							mHandler.sendMessage(_Msg);
						} else if (type == 1) {
							Message _Msg = mHandler.obtainMessage(
									LOAD_DATA_FINISH, replys);
							mHandler.sendMessage(_Msg);
						}
					}
				});
    }

   
}
