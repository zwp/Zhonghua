package com.sinonetwork.zhonghua.wxapi;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.sinonetwork.zhonghua.R;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXTextObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class MyWXApi {
	public static final String APP_ID = "wx3448ae4c520058e9";
	private static final int TIMELINE_SUPPORTED_VERSION = 0x21020001;
	// IWXAPI 是第三方app和微信通信的openapi接口
	private IWXAPI api;

	private Context context;

	public MyWXApi(Context context) {
		this.context = context;
		// 通过WXAPIFactory工厂，获取IWXAPI的实例
		api = WXAPIFactory.createWXAPI(context, APP_ID, false);
	}

	public void regToWx() {
		// 将该app注册到微信
		api.registerApp(APP_ID);
	}

	public boolean isWXInstalled() {
		return api.isWXAppInstalled();
	}

	/**
	 * 发送文本信息
	 * 
	 * @param text
	 * @param scene
	 *            1 表示分享到微信 2表示分享到微信朋友圈
	 */
	public void sendText(String text, int scene) {
		// 初始化一个WXTextObject对象
		WXTextObject textObj = new WXTextObject();
		textObj.text = text;

		// 用WXTextObject对象初始化一个WXMediaMessage对象
		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = textObj;
		// 发送文本类型的消息时，title字段不起作用
		// msg.title = "Will be ignored";
		msg.description = text;

		// 构造一个Req
		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("text"); // transaction字段用于唯一标识一个请求
		req.message = msg;

		if (scene == 1) {
			req.scene = SendMessageToWX.Req.WXSceneSession;
		} else if (scene == 2) {
			if (isTimeLineSupport()) {
				req.scene = SendMessageToWX.Req.WXSceneTimeline;
			} else {
				Toast.makeText(context, "您当前的微信版本不支持分享到朋友圈，请升级后再操作", Toast.LENGTH_SHORT).show();
				return;
			}
		}

		// 调用api接口发送数据到微信
		api.sendReq(req);
	}

	/**
	 * 发送图片
	 * 
	 * @param bitmap
	 * @param scene
	 *            1 表示分享到微信 2表示分享到微信朋友圈
	 */
	public void sendImg(Bitmap bitmap, int scene) {
		WXImageObject imgObj = new WXImageObject(bitmap);

		WXMediaMessage msg = new WXMediaMessage();
		msg.mediaObject = imgObj;
		// msg.title = title;

		// Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
		// bitmap.recycle();
		// msg.thumbData = Util.bmpToByteArray(thumbBmp, true);

		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("img");
		req.message = msg;

		if (scene == 1) {
			req.scene = SendMessageToWX.Req.WXSceneSession;
		} else if (scene == 2) {
			if (isTimeLineSupport()) {
				req.scene = SendMessageToWX.Req.WXSceneTimeline;
			} else {
				Toast.makeText(context, "您当前的微信版本不支持分享到朋友圈，请升级后再操作", Toast.LENGTH_SHORT).show();
				return;
			}
		}

		api.sendReq(req);
	}

	/**
	 * 发送网页
	 * 
	 * @param map
	 * @param scene
	 */
	public void sendWebPage(final Map<String, Object> map, final int scene) {
		if (map == null) {
			return;
		}

		map.put("scene", scene);
		final String imageUrl = (String) map.get("imageUrl");

		if (!TextUtils.isEmpty(imageUrl)) {
			if (imageUrl.startsWith("http://")) {
				// 网络图片
				new Thread() {
					public void run() {
						try {
							Bitmap bmp = BitmapFactory.decodeStream(new URL(imageUrl).openStream());
							Message msg = handler.obtainMessage(111);

							if (bmp != null) {
								map.put("bitmap", bmp);
							}

							msg.obj = map;
							handler.sendMessage(msg);
						} catch (MalformedURLException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					};
				}.start();
			} else {
				Message msg = handler.obtainMessage(111);
				// 本地图片
				Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
				if (bitmap != null) {
					map.put("bitmap", bitmap);
				}
				msg.obj = map;
				handler.sendMessage(msg);
			}
		} else {
			Message msg = handler.obtainMessage(111);
			Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
			if (bitmap != null) {
				map.put("bitmap", bitmap);
			}
			msg.obj = map;
			handler.sendMessage(msg);
		}
	}

	private void send(Map<String, Object> map) {
		if (map == null) {
			return;
		}

		WXWebpageObject webpage = new WXWebpageObject();
		webpage.webpageUrl = (String) map.get("url");
		final WXMediaMessage msg = new WXMediaMessage(webpage);
		msg.title = (String) map.get("title");
		msg.description = (String) map.get("abstract");
		Bitmap bitmap = (Bitmap) map.get("bitmap");

		if (bitmap != null) {
			Bitmap thumb = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
			bitmap.recycle();
			msg.thumbData = bmpToByteArray(thumb, true);
		}

		SendMessageToWX.Req req = new SendMessageToWX.Req();
		req.transaction = buildTransaction("webpage");
		req.message = msg;

		int scene = (Integer) map.get("scene");

		if (scene == 1) {
			req.scene = SendMessageToWX.Req.WXSceneSession;
		} else if (scene == 2) {
			req.scene = SendMessageToWX.Req.WXSceneTimeline;
		}

		api.sendReq(req);
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (msg.what == 111) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String, Object>) msg.obj;
				if (map != null) {
					send(map);
				}
			}
		};
	};

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
	}

	/**
	 * 是否支持发送到朋友圈， 4.2 以上支持发送到朋友圈
	 * 
	 * @return
	 */
	public boolean isTimeLineSupport() {
		int wxSdkVersion = api.getWXAppSupportAPI();

		if (wxSdkVersion >= TIMELINE_SUPPORTED_VERSION) {
			return true;
		}

		return false;
	}

	private byte[] bmpToByteArray(Bitmap bmp, boolean needRecycle) {
		java.io.ByteArrayOutputStream output = new java.io.ByteArrayOutputStream();
		bmp.compress(CompressFormat.PNG, 100, output);

		if (needRecycle) {
			bmp.recycle();
		}

		byte[] result = output.toByteArray();
		try {
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @param flag
	 *            1分享到微信 2分享到微信朋友圈
	 * @param shareContent
	 *            分享内容
	 */
	public void shareToWeiXin(int flag, Map<String, Object> shareContent) {
		if (shareContent == null) {
			return;
		}

		if (!isWXInstalled()) {
			Toast.makeText(context, "抱歉，您未安装微信客户端无法进行微信分享", 2000).show();
			return;
		}

		if (flag == 2 && !isTimeLineSupport()) {
			Toast.makeText(context, "您当前的微信版本不支持分享到朋友圈，请升级后再操作", 2000).show();
			return;
		}

		regToWx();

		if (flag == 1) {
			sendWebPage(shareContent, 1);
		} else if (flag == 2) {
			sendWebPage(shareContent, 2);
		}
	}
}
