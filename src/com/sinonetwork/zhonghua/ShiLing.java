package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.ShiLingAdapter;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList.farmingGuidInfoLists;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class ShiLing extends Activity {
	private ListView shiling_list;
	
	private List<farmingGuidInfoLists> data;
	private ShiLingAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shiling);
		shiling_list = (ListView)findViewById(R.id.shiling_list);
	}
	
	
	public void loadData(String method) {
//		Toast.makeText(ShiLing.this, "来咯来咯", Toast.LENGTH_SHORT).show();
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		data = new ArrayList<farmingGuidInfoLists>();
		FastJsonRequest<farmingGuidInfoList> fastJson = new FastJsonRequest<farmingGuidInfoList>(
				Method.POST, URLAddress.CropInfoListURL, farmingGuidInfoList.class,
				null, map, new Response.Listener<farmingGuidInfoList >() {
					@Override
					public void onResponse(farmingGuidInfoList weather) {
						// 赋值
						data.addAll(weather.getResultdata());
						if(data.size()==0||data==null){
							Toast.makeText(ShiLing.this, "没有数据", Toast.LENGTH_SHORT).show();
						}
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						Toast.makeText(ShiLing.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}
	
}
