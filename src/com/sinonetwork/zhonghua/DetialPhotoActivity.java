package com.sinonetwork.zhonghua;

import java.io.File;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;
import com.polites.android.GestureImageView;
import com.sinonetwork.zhonghua.utils.URLAddress;

public class DetialPhotoActivity extends Activity {
	private GestureImageView bigImage;

	private BitmapUtils bitmapUtils;
	private BitmapDisplayConfig bitmapDisplayConfig;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail_photo);
		bigImage = (GestureImageView) this.findViewById(R.id.bigImage);
		bitmapUtils = new BitmapUtils(this);
		bitmapDisplayConfig = new BitmapDisplayConfig();
		bitmapDisplayConfig.setLoadingDrawable(this.getResources().getDrawable(R.drawable.empty_photo));
		bitmapDisplayConfig.setLoadFailedDrawable(this.getResources().getDrawable(R.drawable.empty_photo));
		String imagePath = getIntent().getStringExtra("image");
		File file = null;
		try {
			file = bitmapUtils.getBitmapFileFromDiskCache(URLAddress.TPURL + imagePath);
		} catch (NullPointerException exception) {
			this.finish();

		}
		if (file != null && file.exists()) {

			bigImage.setImageBitmap(BitmapFactory.decodeFile(bitmapUtils.getBitmapFileFromDiskCache(URLAddress.TPURL + imagePath).getAbsolutePath()));
		} else {
			bigImage.setImageResource(R.drawable.empty_photo);
		}
		bigImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DetialPhotoActivity.this.finish();
			}
		});
	}
}
