package com.sinonetwork.zhonghua;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.CommentAdapter;
import com.sinonetwork.zhonghua.adapter.EventsImagesGridViewAdapter;
import com.sinonetwork.zhonghua.event.CommentDingEvent;
import com.sinonetwork.zhonghua.event.CommentDownEvent;
import com.sinonetwork.zhonghua.event.GetUserNameEvent;
import com.sinonetwork.zhonghua.model.Comment;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.utils.HttpHelp;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.TimeUtils;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;
import com.sinonetwork.zhonghua.view.NoScrollGridView;

import de.greenrobot.event.EventBus;

public class ReplyActivity extends Activity implements View.OnClickListener {
	private ImageView back;
	private TextView title;
	private MyListView mListView;

	private TextView questionTitle;
	private TextView content;
	private NoScrollGridView gv_images;
	private TextView time;
	private TextView userName;
	private Button answer;
	private LinearLayout write_box;
	private ImageView write_comments_calcel;
	private ImageView write_comments_ok;
	private EditText write_comments_edittext;

	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");

	private CommentAdapter adapter;
	private List<Comment> mDatas = new ArrayList<Comment>();
	private int pageNow = 1;
	private String questionId;
	private Question question;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_reply);
		EventBus.getDefault().register(this);
		//初始化控件
		initView();
		back.setOnClickListener(new View.OnClickListener() {//后退按钮

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		setQusetionData();//设置用户所提问题的数据展示
		setCommentData();//设置用户回复的数据展示
 

	}
	/*
//	 * 设置用户所提问题的数据展示
	 */
	private void setQusetionData() {
		questionId = this.getIntent().getStringExtra("questionId");
		question = (Question) this.getIntent().getSerializableExtra("question");
		View view = LayoutInflater.from(this).inflate(
				R.layout.activity_reply_header, null);//所提问题的布局
		mListView.addHeaderView(view);
		questionTitle = (TextView) findViewById(R.id.title);
		content = (TextView) this.findViewById(R.id.content);
		gv_images = (NoScrollGridView) this.findViewById(R.id.gv_images);
		time = (TextView) findViewById(R.id.time);
		userName = (TextView) findViewById(R.id.user_name);
		questionTitle.setText(question.getQuestionTitle());
		userName.setText(question.getUserName1());
		content.setText(question.getQuestionText(), BufferType.NORMAL);
		// 加载图片
				final List<String> images = getAllVisibleImages(question);
				if (!question.getPictures().equals("null")) {// 否则显示全部，最多5张
					gv_images.setVisibility(View.VISIBLE);
					EventsImagesGridViewAdapter mImagesAdapter = new EventsImagesGridViewAdapter(
							this, images);
					gv_images.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							Intent intent = new Intent(ReplyActivity.this,
									DetialPhotoActivity.class);
							intent.putExtra("image", images.get(position));
							startActivity(intent);
						}
					});
					gv_images.setAdapter(mImagesAdapter);//显示图片，问题的图片
				} else {
					 
				}
				time.setText(TimeUtils.getDateTime(question.getPublishTime()));
		
	}
	/**
	 * 设置用户回复数据的展示
	 */
   private void setCommentData() {
	   adapter = new CommentAdapter(mDatas, this);//用户评论的数据展示
		mListView.setAdapter(adapter);
		mListView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				pageNow = 1;
				adapter.notifyDataSetChanged();
				loadData(pageNow);
			}
		});
		mListView.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				pageNow++;
				loadData(pageNow);
			}
		});

		loadData(pageNow);
	}
	/*
 * 初始化布局中的控件
 */
	private void initView() {
		back = (ImageView) this.findViewById(R.id.back);
		title = (TextView) this.findViewById(R.id.top_title_textview);
		write_box = (LinearLayout) this.findViewById(R.id.write_box);
		write_comments_calcel = (ImageView) this
				.findViewById(R.id.write_comments_calcel);
		write_comments_ok = (ImageView) this
				.findViewById(R.id.write_comments_ok);
		write_comments_edittext = (EditText) this
				.findViewById(R.id.write_comments_edittext);
		write_comments_calcel.setOnClickListener(this);
		write_comments_ok.setOnClickListener(this);
		mListView = (MyListView) this.findViewById(R.id.lv);
		title.setText("用户回复");
		answer = (Button) this.findViewById(R.id.answer);
		answer.setOnClickListener(this);
	}

	/**
	 * 获取所有图片
	 * @param bigEvent
	 * @return
	 */
	private List<String> getAllVisibleImages(Question question) {
		List<String> list = new ArrayList<String>();
		String pictures = question.getPictures();
		if (pictures != null) {
			String images[] = pictures.split(",");
			for (int i = 0; i < images.length; i++) {
				if (!TextUtils.isEmpty(images[i])) {
					list.add(images[i]);
				}

			}
		}
		return list;
	}

	/**
	 * 读取数据
	 */
	private void loadData(int page) {
		//分页加载数据
		HttpHelp.getInstance().send(
				HttpMethod.GET,
				URLAddress.searchReplyByQuestionId("searchReplyByQuestionId",
						questionId, page + ""), new RequestCallBack<String>() {//查询回复的接口

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						JSONObject jsonObject = JSONObject
								.parseObject(responseInfo.result);
						JSONArray array = jsonObject.getJSONArray("resultdata");
						List<Comment> list = JSONArray.parseArray(
								array.toJSONString(), Comment.class);
						if (pageNow == 1)
							mDatas.clear();
						mDatas.addAll(list);
						setData();
					}
				});
	}

	private void setData() {
		try {
			Thread.sleep(1000);
			if (pageNow == 1) {
				mListView.onRefreshComplete();
				adapter.notifyDataSetChanged();
			} else {
				mListView.misHaveNewDatas = false;
				mListView.onLoadMoreComplete(); // 加载更多完成
				adapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//服务
	public void onEvent(CommentDingEvent event) {
		for (int i = 0; i < mDatas.size(); i++) {
			if (mDatas.get(i).getId().equals(event.replyId)) {
				if (mDatas.get(i).getReplyAgree() != null) {
					mDatas.get(i).setReplyAgree(
							Integer.parseInt(mDatas.get(i).getReplyAgree()) + 1
									+ "");
				} else {
					mDatas.get(i).setReplyAgree("1");

				}
			}
		}
		adapter.notifyDataSetChanged();
	}
//点击事件
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.answer:

			if (PrefUtil.getBooleanPref(this.getApplicationContext(),
					ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
				write_box.setVisibility(View.VISIBLE);
				write_comments_edittext.requestFocus();
			} else {
				Toast.makeText(this, "您尚未登录！", 2000).show();

			}

			break;
		case R.id.write_comments_calcel:
			write_box.setVisibility(View.GONE);

			break;
		case R.id.write_comments_ok:
			if (write_comments_edittext.getText().toString().equals("")) {
				Toast.makeText(this, "请输入评论内容", 2000).show();
				return;
			}
			addReply();//添加回复
			write_comments_edittext.setText("");
			write_box.setVisibility(View.GONE);

			break;
		default:
			break;
		}

	}
	/**
	 * 添加用户回复
	 */
	private void addReply() {
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", "addReply");
		params.addBodyParameter("replyText", write_comments_edittext
				.getText().toString());
		params.addBodyParameter("questionId", question.getId() + "");
		params.addBodyParameter("userName",
				PrefUtil.getStringPref(this, "zhonghua_userName"));

		HttpHelp.getInstance().send(HttpMethod.POST, URLAddress.URLER,
				params, new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {

					}

					@Override
					public void onSuccess(ResponseInfo<String> resultInfo) {
						JSONObject jsonObject = JSONObject
								.parseObject(resultInfo.result);
						if (jsonObject.getString("resultcode").equals("ok")) {
							Toast.makeText(ReplyActivity.this, "评论成功", 2000)
									.show();
							//向服务端发送数据
							HttpHelp.getInstance().send(
									HttpMethod.GET,
									URLAddress.getUserIntegral(//用户加，删智慧豆
											ZhAccountPrefUtil.getZHAccount(
													ReplyActivity.this)
													.getUserName(), "1",
											"add"),
									new RequestCallBack<String>() {

										@Override
										public void onFailure(
												HttpException arg0,
												String arg1) {

										}

										@Override
										public void onSuccess(
												ResponseInfo<String> arg0) {
											if (JSONObject
													.parseObject(
															arg0.result)
													.getString("resultCode")
													.equals("OK")) {
												Toast.makeText(
														ReplyActivity.this,
														"智慧豆 +1", 2000)
														.show();
												ZHAccount account = ZhAccountPrefUtil
														.getZHAccount(ReplyActivity.this);
												account.setUser_integ(account
														.getUser_integ() + 1);
												ZhAccountPrefUtil
														.saveZHAccount(
																ReplyActivity.this,
																account);
												EventBus.getDefault()
														.post(new GetUserNameEvent());
											}
										}
									});
							pageNow = 1;
							mDatas.clear();
							loadData(pageNow);
						}

					}
				});
		
	}
}
