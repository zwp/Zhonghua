package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.sinonetwork.zhonghua.adapter.QixiangWarmAdapter;
import com.sinonetwork.zhonghua.model.Warning;

public class QixiangWarm extends LandBaseActivity {
	private ListView listView;
	private QixiangWarmAdapter adapter;
	private ImageView back;
	private ArrayList<Warning> list;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qixiang_warm);
		listView = (ListView) findViewById(R.id.qixiang_warm_list);
		back = (ImageView) findViewById(R.id.back);
		Bundle bundleObject = getIntent().getExtras();
		list = (ArrayList<Warning>) bundleObject.getSerializable("data");
		if(list.size() == 0){
			showShortToast("当前地区没有灾害预警信息");
		}
		adapter = new QixiangWarmAdapter(QixiangWarm.this, list);
		
		listView.setAdapter(adapter);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(QixiangWarm.this, WarningDetailActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("data", list.get(arg2));
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
	}

}
