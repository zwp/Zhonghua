package com.sinonetwork.zhonghua;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ImageView;

public class MyShopFragment extends Activity {

	private ImageView shangcheng_back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 setContentView(R.layout.activity_shop);
//		 shangcheng_back = (ImageView) findViewById(R.id.shangcheng_back);
//	        
//	        //WebView
//	        WebView browser=(WebView)findViewById(R.id.Toweb);  
//	        browser.loadUrl("http://221.228.238.72:8888/fert_bbc/weixin/platform/index.htm");  
//	          
//	        //设置可自由缩放网页  
//	        browser.getSettings().setSupportZoom(true);  
//	        browser.getSettings().setBuiltInZoomControls(true);  
//	          
//	        // 如果页面中链接，如果希望点击链接继续在当前browser中响应，  
//	        // 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象  
//	        browser.setWebViewClient(new WebViewClient() {  
//	            public boolean shouldOverrideUrlLoading(WebView view, String url)  
//	            {   
//	                //  重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边  
//	                view.loadUrl(url);  
//	                        return true;  
//	            }         
//	             }); 
//	        shangcheng_back.setOnClickListener(new OnClickListener() {
//				
//				@Override
//				public void onClick(View arg0) {
//					// TODO Auto-generated method stub
//					finish();
//					Intent intent = new Intent(MyShopFragment.this,MainActivity.class);
//					startActivity(intent);
//				}
//			});
	}
	
	//go back
    @Override  
    public boolean onKeyDown(int keyCode, KeyEvent event) {  
        WebView browser=(WebView)findViewById(R.id.Toweb);  
        // Check if the key event was the Back button and if there's history  
        if ((keyCode == KeyEvent.KEYCODE_BACK) && browser.canGoBack()) {  
            browser.goBack();  
            return true;  
        }  
      //  return true;  
        // If it wasn't the Back key or there's no web page history, bubble up to the default  
        // system behavior (probably exit the activity)  
        return super.onKeyDown(keyCode, event);  
    } 
	
}
