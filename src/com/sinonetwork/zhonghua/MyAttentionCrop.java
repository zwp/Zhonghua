package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.adapter.CropSelectAdapter;
import com.sinonetwork.zhonghua.model.Categorys;
import com.sinonetwork.zhonghua.model.SubCategorys;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class MyAttentionCrop extends LandBaseActivity {

	protected ArrayList<Categorys> croplist;
	private CropSelectAdapter mAdapter;
	private ListView listView;
	private Button submitBtn;
	private TextView selectCropTxt;
	private String care = "";
    private ZHAccount account;
    public String ChangeUserInfoURL = URLAddress.SHOP_URL+"fert_bbc/changePlant.htm?";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plant);
		setBackBtn();
		setTopTitleTV("关注作物");
		account = ZhAccountPrefUtil
				.getZHAccount(getApplicationContext());
		if(account.getCare().length()>0){
			
			PrefUtil.savePref(MyAttentionCrop.this, "cropSelectPlant", account.getCare()+",");
		}else{
			
			PrefUtil.savePref(MyAttentionCrop.this, "cropSelectPlant", account.getCare());
		}
		listView = (ListView) findViewById(R.id.croplist);
		submitBtn = (Button) findViewById(R.id.submit);
		selectCropTxt = (TextView) findViewById(R.id.selected_crop);
		submitBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String str = PrefUtil.getStringPref(MyAttentionCrop.this, "cropSelectPlant");
				if(str.length()>0){
					care = str.replace(",", "");
					account.setCare(care);
					submit(account);
				}else{
					Toast.makeText(getApplicationContext(), "没有选择数据，请选择",
							Toast.LENGTH_SHORT).show();
				}

			}
		});
		loadData();
	}

	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					croplist = LandInfoParser.getCropDoctorList();

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void submit(ZHAccount account) {
		showLoadProgressBar();

		RequestParams params = new RequestParams();
		params.addBodyParameter("userName", account.getUserName());
		params.addBodyParameter("cul_area", account.getCul_area());
		params.addBodyParameter("care", account.getCare());
		params.addBodyParameter("plant", account.getPlant());
//		Log.v(TAG,"url--"+ChangeUserInfoURL+"userName="+account.getUserName()+"&cul_area="+account.getCul_area()+"&care="+account.getCare()+"&plant="+account.getPlant());
		HttpUtils httpUtils = new HttpUtils();
		httpUtils.send(HttpMethod.POST, ChangeUserInfoURL, params,
				new RequestCallBack<String>() {

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						hideLoadProgressBar();
						System.out.println("error");
						Toast.makeText(getApplicationContext(), "提交失败",
								Toast.LENGTH_LONG).show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						hideLoadProgressBar();
						JSONObject returnData = JSONObject
								.parseObject(responseInfo.result);
						
						System.out.println(returnData.toString());
						if ("OK".equals(returnData.getString("resultCode"))) {
							PrefUtil.savePref(MyAttentionCrop.this,
									"zhonghua_care", care);//本地保存作物关注id
							showSelectCrop(croplist, Integer.parseInt(care));
							Toast.makeText(getApplicationContext(), "提交成功",
									Toast.LENGTH_LONG).show();
						}else{
							Toast.makeText(getApplicationContext(), returnData.getString("resultData"),
									Toast.LENGTH_LONG).show();
						}
					}
				});
	}

	private void setView() {
		if(account.getCare().length()>0){
			
			showSelectCrop(croplist,Integer.parseInt(account.getCare()));
		}
		mAdapter = new CropSelectAdapter(this);
		mAdapter.setData(croplist);
		mAdapter.setCurrentPlant(1);// 0为种植作物，1为关注作物
		listView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}

	/**
	 * 获取用户当前关注的种类
	 */
	private void showSelectCrop(ArrayList<Categorys> croplist, int id) {
		for (int i = 0; i < croplist.size(); i++) {
			ArrayList<SubCategorys> subCategorys = croplist.get(i).getSubCategorysList();
			for (int j = 0; j <subCategorys.size(); j++) {
                    if (id == subCategorys.get(j).getId() ) {
                    	selectCropTxt.setText(subCategorys.get(j).getCropName());
                    	PrefUtil.savePref(MyAttentionCrop.this,
								"zhonghua_crop", subCategorys.get(j).getCropName());//保存作物名称
                    	break;
					}
			}
		}
	}
}
