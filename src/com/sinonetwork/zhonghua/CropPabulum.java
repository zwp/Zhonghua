package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.CropPabulumAdapter;
import com.sinonetwork.zhonghua.model.searchAllCrops;
import com.sinonetwork.zhonghua.model.searchAllCrops.searchAllCropss;
import com.sinonetwork.zhonghua.model.searchAllCropsi;
import com.sinonetwork.zhonghua.model.searchAllCropsi.searchAllCropssi;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class CropPabulum extends Activity {
	private ImageView crop_pabulum_back;
	private ListView crop_pabulum_list;
	private CropPabulumAdapter adapter;
	
	private List<searchAllCropss> datas;
	private List<searchAllCropssi> datai;
		
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crop_pabulum1);
		crop_pabulum_back = (ImageView) findViewById(R.id.crop_pabulum_back);
		crop_pabulum_list = (ListView) findViewById(R.id.crop_pabulum_list);
		
		crop_pabulum_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
//		adapter = new CropPabulumAdapter(CropPabulum.this, list);
		crop_pabulum_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
			}
		});
		
//		loadData("searchAllCrops");
//		adapter = new CropPabulumAdapter(CropPabulum.this, datas);
//		adapter.setDatas(datas);
//		crop_pabulum_list.setAdapter(adapter);
		
//		initChildViews();
	}
	
	
	
	public void loadData(String method) {
//		Toast.makeText(CropPabulum.this, "来咯来咯", Toast.LENGTH_SHORT).show();
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		datas = new ArrayList<searchAllCropss>();
		FastJsonRequest<searchAllCrops> fastJson = new FastJsonRequest<searchAllCrops>(
				Method.POST, URLAddress.searchAllCropssURL, searchAllCrops.class,
				null, map, new Response.Listener<searchAllCrops >() {
					@Override
					public void onResponse(searchAllCrops weather) {
						// 赋值
						datas.addAll(weather.getResultdata());
						if(datas.size()==0||datas==null){
							Toast.makeText(CropPabulum.this, "没有数据", Toast.LENGTH_SHORT).show();
						}
//						Toast.makeText(CropPabulum.this, "数据条数===="+datas.size(), Toast.LENGTH_SHORT).show();
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(CropPabulum.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4"+fastJson);
		RequestManager.addRequest(fastJson, this);
	}
	
	public void loadDatai(String method) {
//		Toast.makeText(CropPabulum.this, "来咯来咯", Toast.LENGTH_SHORT).show();
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		datai = new ArrayList<searchAllCropssi>();
		FastJsonRequest<searchAllCropsi> fastJson = new FastJsonRequest<searchAllCropsi>(
				Method.POST, URLAddress.searchAllCropssURL, searchAllCropsi.class,
				null, map, new Response.Listener<searchAllCropsi >() {
					@Override
					public void onResponse(searchAllCropsi weather) {
						// 赋值
						datai.addAll(weather.getResultdata());
						if(datai.size()==0||datai==null){
							Toast.makeText(CropPabulum.this, "没有数据----", Toast.LENGTH_SHORT).show();
						}
//						Toast.makeText(CropPabulum.this, "数据条数----"+datai.size(), Toast.LENGTH_SHORT).show();
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(CropPabulum.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4"+fastJson);
		RequestManager.addRequest(fastJson, this);
	}
	
}
