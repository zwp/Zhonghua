package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.MySolidAdapter;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.model.queryMyOrderTestSoil;
import com.sinonetwork.zhonghua.model.queryMyOrderTestSoil.queryMyOrderTestSoils;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class MySolid extends Activity {
	private ListView my_solid_list;
	private List<queryMyOrderTestSoils> data;
	private MySolidAdapter adapter;
	private String yyid; // 预约信息id
	private String yyorderTimeStr; // 时间
	private String yyregionId; // 地区id
	private String yyregionName; // 地区名
	private ImageView back;
	private String  name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_solid);
		//默认情况下软件盘不自动弹出
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		my_solid_list = (ListView) findViewById(R.id.my_solid_list);
		back = (ImageView) findViewById(R.id.back);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		ZHAccount currentAccount = ZhAccountPrefUtil
				.getZHAccount(this.getApplicationContext());
		
		name = currentAccount.getUserName();
		my_solid_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if("1".equals(data.get(position).getStatus())){
					Intent intent = new Intent(MySolid.this, CetuDetail.class);
					Bundle bundle = new Bundle();
					yyid = data.get(position).getId().toString();
//					Toast.makeText(MySolid.this, String.valueOf(data.get(position).getId().toString()), Toast.LENGTH_SHORT).show();
					bundle.putString("id", yyid);
					bundle.putString("yyorderTimeStr", data.get(position).getOrderTimeStr().toString());
					bundle.putString("yyregionId", data.get(position).getRegionId().toString());
					bundle.putString("yyregionName", data.get(position).getRegionName().toString());
					intent.putExtras(bundle);
					startActivity(intent);
				}

			}
		});

		loadData("queryMyOrderTestSoil", name);
		adapter = new MySolidAdapter(MySolid.this, data);
		adapter.setData(data);
		my_solid_list.setAdapter(adapter);
	}

	public void loadData(String method, String userName) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("userName", userName);
		Logger.e("No1");
		data = new ArrayList<queryMyOrderTestSoils>();
		Logger.e("No1s");
		FastJsonRequest<queryMyOrderTestSoil> fastJson = new FastJsonRequest<queryMyOrderTestSoil>(
				Method.POST, URLAddress.queryMyOrderTestSoilURL,
				queryMyOrderTestSoil.class, null, map,
				new Response.Listener<queryMyOrderTestSoil>() {
					@Override
					public void onResponse(queryMyOrderTestSoil weather) {
						// 赋值
						data.addAll(weather.getResultdata());
						if (data.size() == 0 || data == null) {
							Toast.makeText(MySolid.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						}
						adapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(MySolid.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}
}
