package com.sinonetwork.zhonghua.utils;

import android.util.Log;

public class URLAddress {
	public static final String URL = "http://211.94.93.238"; // 191
	public static final String URLYI = URL + "/zhnyxxgc/httpservice.action"; // 191
	public static final String URLFILE = URL + "/zhnyxxgc/attachment/"; // 191
	public static final String URLER = "http://211.94.93.238/zhnyxxgc/httpservice.action"; // 127外网
	public static final String TPURL = "http://211.94.93.238/zhnyxxgc/picture/"; // 191
	public static final String TPURLER = "http://211.94.93.238/zhnyxxgc/picture/"; // 128
	public static final String SHOP_URL = "http://www.n1b.cn/"; //商城模块

	/**
	 * 接口部分
	 */

	// 首页图片
	// public static final String
	// tupianURL="http://123.127.160.90:8080/zhnyxxgc/httpservice.action";
	// 作物列表查询接口
	public static final String CropInfoListURL = URLYI;
	// public static final String CropInfoListURL = URLYI;
	// 种子查询接口
	public static final String SeedInfoListURL = URLER;
	// 时令管理列表（我的问题）
	public static final String farmingGuidInfoListURL = URLER;
	// 时令管理二级信息详情菜单
	public static final String farmingGuidInfoByIdURL = URLER;
	// 时令管理二级信息详情菜单
	public static final String SeedInfoByIdURL = URLER;
	// 时令管理二级下级信息详情菜单
	public static final String farmingGuidInfoByIdBURL = URLER;
	// 时令管理列表（我的问题）
	public static final String queryMyOrderTestSoilURL = URLER;
	// 作物医生三级菜单
	public static final String serarchCropProtectionURL = URLER;
	// 农业新闻
	public static final String ssearchAgricultureNewsURL = URLYI;
	// 测土（解决方案里面的）
	public static final String orderTestSoilURL = URLER;
	// 作物医生列表
	public static final String searchAllCropssURL = URLER;
	// 试验示范（第一页）（第二页）
	public static final String searchExampleTypeURL = URLER;
	// 气象里面，土壤列表
	public static final String TAsmmURL = URLER;
	// 气象里面，实况数据查询
	public static final String TServiceURL = URLER;
	// 气象里面，日照时间查询
	public static final String TAwssURL = URLER;
	// 获取省
	public static final String SomeAreaURL = "http://www.n1b.cn/fert_bbc/getSomeArea.htm";
	// 获取省级下面的市 或者 获取市级下面的区县
	public static final String SomeAreasURL = "http://www.n1b.cn/fert_bbc/getSomeArea.htm?parentId=4521984";
	// 获取用户信息
	public static final String UserConfigByNameURL = "http://www.n1b.cn/fert_bbc/getUserConfigByName?username=lihao";
	// 获取地区
	// public static final String AreaUrl =
	// "http://192.168.3.128:8080/zhnyxxgc/httpservice.action";
	public static final String AreaUrl = URLER;

	// http://www.n1b.cn/fert_bbc/conn/changeInfo.htm?userName=lxq&password=123456&new_password=1234567&Telephone=13241074206
	// 修改用户信息
	public static final String changeInfo(String userName, String password,
			String new_password, String Telephone) {
		return SHOP_URL + "fert_bbc/conn/changeInfo.htm?userName=" + userName
				+ "&password=" + password + "&new_password=" + new_password
				+ "&Telephone=" + Telephone;
	}

	// 修改用户信息————修改密码
	// http://221.228.238.72:8888/fert_bbc/conn/changeInfo.htm?userName=CH2&password=&new_password=&telephone=
	public static final String changeInfoName(String userName, String password,
			String new_password, String Telephone) {
		Log.i("gxx", "修改信息的url：" + SHOP_URL
				+ "fert_bbc/conn/changeInfo.htm?userName=" + userName
				+ "&password=" + password + "&new_password=" + new_password
				+ "&Telephone=" + Telephone);
		return SHOP_URL + "fert_bbc/conn/changeInfo.htm?userName=" + userName
				+ "&password=" + password + "&new_password=" + new_password
				+ "&Telephone=" + Telephone;
	}

	// 修改用户手机号
	// http://221.228.238.72:8888/fert_bbc/changeTelAndArea.htm?userName=CH2&telephone=&area_id=
	public static final String changeInfoPhone(String userName,
			String Telephone, String area_id) {
		Log.i("gxx", "修改信息的url：" + SHOP_URL
				+ "fert_bbc/changeTelAndArea.htm?userName=" + userName
				+ "&telephone=" + Telephone + "&area_id=" + area_id);
		return SHOP_URL + "fert_bbc/changeTelAndArea.htm?userName=" + userName
				+ "&telephone=" + Telephone + "&area_id=" + area_id;
	}

	// 查询回复的接口
	public static final String searchReplyByQuestionId(String method,
			String id, String curPage) {
		return URLYI + "?method=" + method + "&id=" + id + "&curPage="
				+ curPage;
	}

	// http://211.94.93.238/zhnyxxgc/httpservice.action?method=addReplyClickNum&id=139&userName=lxq

	// 评论点赞的接口
	public static final String addReplyClickNum(String method, String id,
			String userName) {
		return URLYI + "?method=" + method + "&id=" + id + "&userName="
				+ userName;
	}

	// 评论踩的接口
	public static final String addReplyDisClickNum(String method, String id,
			String userName) {
		return URLYI + "?method=" + method + "&id=" + id + "&userName="
				+ userName;
	}

	/**
	 * 用户加减智慧豆
	 * 
	 * @param userName
	 *            用户名
	 * @param integral
	 *            智慧豆数量
	 * @param type
	 *            增加（add） 减少（min）
	 * @return
	 */
	public static final String getUserIntegral(String userName,
			String integral, String type) {
		return SHOP_URL + "fert_bbc/getUserIntegral.htm?userName=" + userName
				+ "&integral=" + integral + "&type=" + type;
	}

	// 发送短信验证码
	public static final String SendMessage = "http://oa.sinofert.com/C6/JHSoft.Web.SinofertMessage/SendMessage.aspx?";

	// 亿美短信sdk
	public static final String getMessageToOthers(String phone, String message) {
		return "http://sdk229ws.eucp.b2m.cn:8080/sdkproxy/sendtimesms.action?cdkey=9SDK-EMY-0229-JCXOK&password=9461198073F3F9537B44128B7B38382C&phone="
				+ phone + "&message=" + message + "&addserial=&sendtim=";
	}

}
