package com.sinonetwork.zhonghua.utils;

import java.util.HashMap;
import java.util.Map;

import android.support.v4.app.Fragment;

public class Commons {
	public static final String PREFS_NAME = "PreferencesFile";

	/**
	 * 相机 REQUEST_CODE
	 */
	public static final int REQUEST_CODE_CAMERA = 110;
	public static final int REQUEST_CODE_ALBUM = 111;
	public static final int REQUEST_CODE_CROP = 112;

	public static String phoneName;
	/**
	 * 缓存Fragment
	 */
	public static Map<String, Fragment> mCache = new HashMap<String, Fragment>();

	public static int DEVICEWIDTH;

	/**
	 * 选择专家
	 */
	public static final int CHOOSE_EXPERT = 113;
}
