package com.sinonetwork.zhonghua.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {

	// df2.format(df.parse(question.getPublishTime()))
	private static SimpleDateFormat df = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat df2 = new SimpleDateFormat("MM月dd日  HH:mm");

	public static String getDateTime(String date) {
		String str = "";
		try {
			Date date1 = df.parse(date);
			Date date2 = new Date();
			long time = date1.getTime();
			long currentTime = date2.getTime();
			long temp = currentTime - time;
			if (currentTime - time < 60 * 60 * 1000) {
				str = (currentTime - time) / (60 * 1000) + "分钟前";
			} else if (currentTime - time < 24 * 60 * 60 * 1000
					&& currentTime - time >= 60 * 60 * 1000) {
				str = (currentTime - time) / (60 * 60 * 1000) + "小时前";
			} else {
				str = df2.format(df.parse(date));
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return str;
	}

}
