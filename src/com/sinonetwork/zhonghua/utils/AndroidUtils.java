package com.sinonetwork.zhonghua.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;

public class AndroidUtils {

	private static String mSQLiteVersion = null;
	private static Boolean mSQLiteSupportsFTS3 = null;
	private static Boolean mSQLiteSupportsFTS4 = null;
	private static Boolean mSQLiteSupportsForeignKeys = null;
	private static Rect mStatusBarRect = new Rect();
	private static int[] mLocation = new int[2];

	/**
	 * Get free (unused) space on external storage.<br>
	 * <br>
	 * See {@link Environment#getExternalStorageDirectory()}.
	 * 
	 * @return free space in kilobytes
	 */
	public static int getFreeExteranlStorageSize() {
		StatFs stats = new StatFs(Environment.getExternalStorageDirectory()
				.getAbsolutePath());
		int availableBlocks = stats.getAvailableBlocks();
		int blockSizeInBytes = stats.getBlockSize();

		return availableBlocks * (blockSizeInBytes / 1024);
	}

	/**
	 * Is device connected to network (WiFi or mobile).<br>
	 * <br>
	 * <b>Hint</b>: A connection to WiFi does not guarantee Internet access.
	 * 
	 * @param context
	 * 
	 * @return {@code true} if device is connected to mobile network or WiFi
	 */
	public static boolean isConnected(Context context) {
		return isWiFiConnected(context) || isMobileNetworkConnected(context);
	}

	/**
	 * Is device connected to WiFi?<br>
	 * <br>
	 * <b>Hint</b>: A connection to WiFi does not guarantee Internet access.
	 * 
	 * @param context
	 * 
	 * @return {@code true} if device is connected to an access point
	 */
	public static boolean isWiFiConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
	}

	/**
	 * Is device connected to mobile network?
	 * 
	 * @param context
	 * 
	 * @return {@code true} if device is connected to mobile network
	 */
	public static boolean isMobileNetworkConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
	}

	/**
	 * Get installed SQLite version as string.
	 * 
	 * @return installed SQLite version as string (e.g. {@code "3.5.9"})
	 */
	public static String getSQLiteVersion() {
		if (mSQLiteVersion == null) {
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(":memory:",
					null);
			Cursor cursor = db.rawQuery(
					"select sqlite_version() AS sqlite_version", null);

			mSQLiteVersion = "";

			while (cursor.moveToNext()) {
				mSQLiteVersion += "." + cursor.getString(0);
			}

			if (mSQLiteVersion.startsWith(".")) {
				mSQLiteVersion = mSQLiteVersion.substring(1);
			}

			cursor.close();
			db.close();
		}

		return mSQLiteVersion;
	}

	/**
	 * Does SQLite support FTS3 (full text search)?
	 * 
	 * @return {@code true} if SQLite supports FTS3
	 */
	public static boolean doesSQLiteSupportFTS3() {
		if (mSQLiteSupportsFTS3 == null) {
			mSQLiteSupportsFTS3 = StringUtils.compareVersion(".", "3.4.0",
					getSQLiteVersion()) <= 0;
		}

		return mSQLiteSupportsFTS3;
	}

	/**
	 * Does SQLite support FTS4 (full text search)?
	 * 
	 * @return {@code true} if SQLite supports FTS4
	 */
	public static boolean doesSQLiteSupportFTS4() {
		if (mSQLiteSupportsFTS4 == null) {
			mSQLiteSupportsFTS4 = StringUtils.compareVersion(".", "3.7.4",
					getSQLiteVersion()) <= 0;
		}

		return mSQLiteSupportsFTS4;
	}

	/**
	 * Does SQLite support foreign keys?
	 * 
	 * @return {@code true} if SQLite supports foreign keys
	 */
	public static boolean doesSQLiteSupportsForeignKeys() {
		if (mSQLiteSupportsForeignKeys == null) {
			mSQLiteSupportsForeignKeys = StringUtils.compareVersion(".",
					"3.6.19", getSQLiteVersion()) <= 0;
		}

		return mSQLiteSupportsForeignKeys;
	}

	/**
	 * Get status bar height.<br>
	 * <br>
	 * <b>Note</b>: Call this when content is completely inflated. So
	 * {@code onCreate} of an activity won't work.
	 * 
	 * @param window
	 *            window of application
	 * 
	 * @return height of status bar in pixel
	 */
	public static int getStatusBarHeight(Window window) {
		window.getDecorView().getWindowVisibleDisplayFrame(mStatusBarRect);
		return mStatusBarRect.top;
	}

	/**
	 * Get title bar height.<br>
	 * <br>
	 * <b>Note</b>: Call this when content is completely inflated. So
	 * {@code onCreate} of an activity won't work.
	 * 
	 * @param window
	 *            window of application
	 * 
	 * @return height of status bar in pixel
	 */
	public static int getTitleBarHeight(View view) {
		view.getWindowVisibleDisplayFrame(mStatusBarRect);
		return mStatusBarRect.top;
	}

	/**
	 * Get offset of application content from top (so status bar + title bar).<br>
	 * <br>
	 * <b>Note</b>: Call this when content is completely inflated. So
	 * {@code onCreate} of an activity won't work.
	 * 
	 * @param view
	 *            any view which is attached to the content
	 * 
	 * @return offset to top in pixel
	 */
	public static int getContentOffsetFromTop(View view) {
		int offset = view.getRootView().findViewById(Window.ID_ANDROID_CONTENT)
				.getTop();

		if (offset == 0) {
			view.getWindowVisibleDisplayFrame(mStatusBarRect);
			offset = mStatusBarRect.top;
		}

		return offset;
	}

	/**
	 * Get location of view relative to screen.<br>
	 * <br>
	 * <b>Note</b>: Call this when content is completely inflated. So
	 * {@code onCreate} of an activity won't work.
	 * 
	 * @param view
	 *            any view which is attached to the content
	 * 
	 * @return location pixel
	 */
	public static Point getScreenLocation(View view) {
		view.getLocationOnScreen(mLocation);
		return new Point(mLocation[0], mLocation[1]);
	}

	/**
	 * Get location of view relative to application content (so no status bar
	 * and title bar).<br>
	 * <br>
	 * <b>Note</b>: Call this when content is completely inflated. So
	 * {@code onCreate} of an activity won't work.
	 * 
	 * @param view
	 *            any view which is attached to the content
	 * 
	 * @return location pixel
	 */
	public static Point getContentLocation(View view) {
		Point point = getScreenLocation(view);
		point.y -= getContentOffsetFromTop(view);
		return point;
	}

	public static void setPreferences(Context context, String key, String value) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String getPreferences(Context context, String key) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		return settings.getString(key, null);
	}

	public static void setPreferences(Context context, String key, int value) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static int getIntPreferences(Context context, String key) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		return settings.getInt(key, 0);
	}

	public static void clearPreferences(Context context) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.clear();
		editor.commit();
	}

	public static void clearPreferences(Context context, String key) {
		// set preference
		SharedPreferences settings = context.getSharedPreferences(
				Commons.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(key);
		editor.commit();
	}

	public static String getImei(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getDeviceId();

	}
	/**
	 * 返回屏幕宽高 int[宽，高]
	 * 
	 * @param activity
	 */
	public static int[] getScreenSize(Activity activity) {
		DisplayMetrics dm = new DisplayMetrics();
		// 获取屏幕信息
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenWidth = dm.widthPixels;
		int screenHeigh = dm.heightPixels;
		return new int[] { screenWidth, screenHeigh };
	}
}
