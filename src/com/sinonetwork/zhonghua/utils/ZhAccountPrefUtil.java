package com.sinonetwork.zhonghua.utils;

import android.content.Context;

import com.sinonetwork.zhonghua.model.ZHAccount;

public class ZhAccountPrefUtil {

	/** 账号信息保存 */
	public static final String IS_LOGINED_ACCOUNT = "is_logined_accout";
	public static final String CARE = "zhonghua_care";
	public static final String CUL_AREA = "zhonghua_cul_area";
	public static final String USERNAME = "zhonghua_userName";
	public static final String AREANAME = "zhonghua_areaName";
	public static final String AREAID = "zhonghua_areaId";
	public static final String PLANT = "zhonghua_plant";
	public static final String PASSWORD = "zhonghua_password";
	public static final String PARENT_AREANAME = "zhonghua_parent_areaName";
	public static final String PARENT_AREAID = "zhonghua_parent_areaId";
	public static final String PHONE = "zhonghua_phone";
	public static final String SEQUENCE = "zhonghua_sequence";
	public static final String INTEG = "zhonghua_integ";

	/**
	 * 获取账户信息
	 * 
	 * @param account
	 */
	public static void saveZHAccount(Context context, ZHAccount account) {
		PrefUtil.savePref(context, CARE, account.getCare());
		PrefUtil.savePref(context, CUL_AREA, account.getCul_area());
		PrefUtil.savePref(context, USERNAME, account.getUserName());
		PrefUtil.savePref(context, AREANAME, account.getAreaName());
		PrefUtil.savePref(context, AREAID, account.getAreaId() + "");
		PrefUtil.savePref(context, PLANT, account.getPlant());
		PrefUtil.savePref(context, PHONE, account.getTelephone());
		PrefUtil.savePref(context, PASSWORD, account.getPassword());
		PrefUtil.savePref(context, SEQUENCE, account.getSequence());
		PrefUtil.savePref(context, PARENT_AREANAME,
				account.getParent_areaName());
		PrefUtil.savePref(context, PARENT_AREAID, account.getParent_areaId()
				+ "");
		PrefUtil.savePref(context, INTEG, account.getUser_integ() + "");

	}

	/**
	 * 清除账号信息
	 * 
	 * @param account
	 */
	public static void cleanZHAccount(Context context) {
		PrefUtil.savePref(context, CARE, "");
		PrefUtil.savePref(context, CUL_AREA, "");
		PrefUtil.savePref(context, USERNAME, "");
		PrefUtil.savePref(context, AREANAME, "");
		PrefUtil.savePref(context, AREAID, "");
		PrefUtil.savePref(context, PLANT, "");
		PrefUtil.savePref(context, PASSWORD, "");
		PrefUtil.savePref(context, PARENT_AREANAME, "");
		PrefUtil.savePref(context, PARENT_AREAID, "");
		PrefUtil.savePref(context, PHONE, "");
		PrefUtil.savePref(context, SEQUENCE, "");
		PrefUtil.savePref(context, INTEG, "");

	}

	/**
	 * 保存账号信息
	 * 
	 * @param context
	 * @return
	 */
	public static ZHAccount getZHAccount(Context context) {
		ZHAccount account = new ZHAccount();
		account.setCare(PrefUtil.getStringPref(context, CARE));
		account.setCul_area(PrefUtil.getStringPref(context, CUL_AREA));
		account.setUserName(PrefUtil.getStringPref(context, USERNAME));
		account.setAreaName(PrefUtil.getStringPref(context, AREANAME));
		account.setAreaId(Integer.parseInt(PrefUtil.getStringPref(context,
				AREAID)));
		account.setPlant(PrefUtil.getStringPref(context, PLANT));
		account.setTelephone(PrefUtil.getStringPref(context, PHONE));
		account.setSequence(PrefUtil.getStringPref(context, SEQUENCE));
		account.setPassword(PrefUtil.getStringPref(context, PASSWORD));
		account.setParent_areaName(PrefUtil.getStringPref(context,
				PARENT_AREANAME));
		account.setParent_areaId(Integer.parseInt(PrefUtil.getStringPref(
				context, PARENT_AREAID)));

		if (PrefUtil.getStringPref(context, INTEG).equals("")
				|| PrefUtil.getStringPref(context, INTEG) == null) {
			account.setUser_integ(0);
		}else{
			
			account.setUser_integ(Integer.parseInt(PrefUtil.getStringPref(context,
					INTEG)));
		}

		return account;
	}
}
