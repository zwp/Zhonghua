package com.sinonetwork.zhonghua.utils;

import com.lidroid.xutils.HttpUtils;
/**
 * 网络请求帮助类，单例设计模式
 * @author enway
 *
 */
public class HttpHelp {
	private static HttpUtils httpUtils;
	private static HttpUtils httpUtilsWithCache;

	private HttpHelp() {

	}

	public static HttpUtils getInstance() {
		if (httpUtils == null) {
			httpUtils = new HttpUtils();
			httpUtils.configTimeout(60000);
			// 不设置缓存
			httpUtils.configDefaultHttpCacheExpiry(0);
		}

		return httpUtils;
	}

	public static HttpUtils getInstanceWithCache() {
		if (httpUtilsWithCache == null) {
			httpUtilsWithCache = new HttpUtils();
			httpUtilsWithCache.configTimeout(60000);
			httpUtilsWithCache.configDefaultHttpCacheExpiry(3 * 60 * 1000);
		}

		return httpUtilsWithCache;
	}
}
