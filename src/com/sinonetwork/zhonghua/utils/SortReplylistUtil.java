package com.sinonetwork.zhonghua.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.sinonetwork.zhonghua.model.Reply;

public class SortReplylistUtil implements Comparator<Reply>{
    
	private static SortReplylistUtil sDefault = null;
	
	public static SortReplylistUtil getInstance() {
		if (sDefault == null) {
			sDefault = new SortReplylistUtil();
		}
		return sDefault;
	}

	private SortReplylistUtil() {

	}
	//回复时间排序
	public List<Reply> sortByDefault(List<Reply> list) {
		List<Reply> sortResult = new ArrayList<Reply>();
		if (list != null) {
			int length = list.size();
			if (length > 1) {
				for (int i = 0; i < length; i++) {
					Reply item = list.get(i);
					if (i == 0) {
						sortResult.add(item);
					} else {
						sortResult.add(getIndexOf(sortResult, item), item);
					}
				}
			} else {
				sortResult = list;
			}
		}
		return sortResult;
	}
	private int getIndexOf(List<Reply> sortResult, Reply item) {
		for (int i = 0; i < sortResult.size(); i++) {
			if (compare(sortResult.get(i), item) < 0) {
				return i;
			}
		}
		return sortResult.size();
	}
	@Override
	public int compare(Reply reply,Reply reply2) {
		int flag = 0;
		try {
			SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
			Date lhsDate = mDateFormat.parse(reply.getReplyTime());
			Date rhsDate = mDateFormat.parse(reply2.getReplyTime());
			flag = lhsDate.compareTo(rhsDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

}
