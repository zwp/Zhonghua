package com.sinonetwork.zhonghua.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.sinonetwork.zhonghua.model.Question;

/**
 * List 排序工具类
 * 
 * @author lxq 2015年7月28日
 * 
 */
public class ListSortUtil implements Comparator<Question> {

	private static ListSortUtil sDefault = null;

	public static ListSortUtil getInstance() {
		if (sDefault == null) {
			sDefault = new ListSortUtil();
		}
		return sDefault;
	}

	private ListSortUtil() {

	}

	public List<Question> sortByDefault(List<Question> list) {
		List<Question> sortResult = new ArrayList<Question>();
		if (list != null) {
			int length = list.size();
			if (length > 1) {
				for (int i = 0; i < length; i++) {
					Question item = list.get(i);
					if (i == 0) {
						sortResult.add(item);
					} else {
						sortResult.add(getIndexOf(sortResult, item), item);
					}
				}
			} else {
				sortResult = list;
			}
		}
		return sortResult;
	}

	/**
	 * 获取item在sortResult中的排序索引
	 * 
	 * @param sortResult
	 * @param item
	 * @return 默认插入队尾
	 */
	private int getIndexOf(List<Question> sortResult, Question item) {
		for (int i = 0; i < sortResult.size(); i++) {
			if (compare(sortResult.get(i), item) < 0) {
				return i;
			}
		}
		return sortResult.size();
	}

	@Override 
	public int compare(Question lhs, Question rhs) {
		int flag = 0;
		try {
			SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
			Date lhsDate = mDateFormat.parse(lhs.getPublishTime());
			Date rhsDate = mDateFormat.parse(rhs.getPublishTime());
			flag = lhsDate.compareTo(rhsDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return flag;
	}

}
