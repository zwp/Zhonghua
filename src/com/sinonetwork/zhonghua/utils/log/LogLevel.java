package com.sinonetwork.zhonghua.utils.log;

/**
 * @author Orhan Obut
 */
public enum LogLevel {

	/**
	 * Prints all logs
	 */
	FULL,

	/**
	 * No log will be printed
	 */
	NONE
}
