package com.sinonetwork.zhonghua;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sinonetwork.zhonghua.model.YiShengXQ;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class PabulumDetail extends Activity {
	
	
	private ViewPager pabulum_detail_img;
	private int[] imageResId; // 图片ID
	private List<ImageView> imageViews; // 滑动的图片集合
	private ImageView back;
	
	private TextView disease_name_content;		//作物名称
	private TextView disease_chararter_content;		//作物特征
	private TextView tuijiannongyao;				//推荐农药
	private String cropId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pabulum_detail);
		pabulum_detail_img = (ViewPager) findViewById(R.id.pabulum_detail_img);
		back = (ImageView) findViewById(R.id.back);
		disease_name_content = (TextView) findViewById(R.id.disease_name_content);
		disease_chararter_content = (TextView) findViewById(R.id.disease_chararter_content);
		tuijiannongyao = (TextView) findViewById(R.id.tuijiannongyao);
		imageViews = new ArrayList<ImageView>();
		
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		cropId = bundle.getString("id");
		loadData("detailCropProtection",cropId);
	}
	
	
	public void loadData(String method, String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		Logger.e("method==="+method+"，id==="+id);
		FastJsonRequest<YiShengXQ> fastJson = new FastJsonRequest<YiShengXQ>(
				Method.POST, URLAddress.SeedInfoByIdURL, YiShengXQ.class,
				null, map, new Response.Listener<YiShengXQ>() {
					@Override
					public void onResponse(YiShengXQ weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(PabulumDetail.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							disease_name_content.setText(weather.getResultdata().getCpName().toString());		//作物名称
							try {
								disease_chararter_content.setText(URLDecoder.decode(weather.getResultdata().getTrait().toString(),"utf-8"));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}		//作物特征
							tuijiannongyao.setText(weather.getResultdata().getRecommendedPesticide().toString());				//推荐农药
							
							
							String tupian = weather.getResultdata()
									.getPictures().toString();
							Logger.e("图片++++++++++++++============"+tupian);
							if(tupian==null||"".equals(tupian)){
								Toast.makeText(PabulumDetail.this, "没有图片！",
										Toast.LENGTH_SHORT).show();
							}
							String[] seedPictures = null;
							// Log.i("----", tupian);
							if (!tupian.equals("") && tupian != null
									&& !tupian.equals("null")) {
								if (tupian.contains(",")) {
									seedPictures = tupian.split("\\,");
								} else {
									seedPictures = new String[1];
									seedPictures[0] = tupian;
								}
								initSeedPicture(seedPictures);
							}

							// Log.i("----", seedPictures[0]);

						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(PabulumDetail.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}
	
	public void initSeedPicture(String[] seedPictures) {
		// imageResId = new int[] { R.drawable.a, R.drawable.b };
		imageResId = new int[seedPictures.length];
		for (int i = 0; i < seedPictures.length; i++) {
			imageResId[i] = imageResId[i];
		}
		// 初始化图片资源
		for (int i = 0; i < seedPictures.length; i++) {
			ImageView imageView = new ImageView(PabulumDetail.this);
			ImageLoader.getInstance().displayImage(
					URLAddress.TPURLER + seedPictures[i], imageView);
			// imageView.setImageResource(imageResId[i]);
			imageView.setScaleType(ScaleType.FIT_XY);
			imageViews.add(imageView);
		}
		pabulum_detail_img.setAdapter(new SeedAdapter());// 设置填充ViewPager页面的适配器
	}
	
	
	
	/**
	 * 填充ViewPager页面的适配器
	 * 
	 * @author Administrator
	 * 
	 */
	private class SeedAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageResId.length;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imageViews.get(arg1));
			return imageViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}
	
}
