package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.sinonetwork.zhonghua.model.ShengFenModel;
import com.sinonetwork.zhonghua.model.ShengFenModel.ShengFenModels;
import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;
import com.sinonetwork.zhonghua.utils.log.Logger;
import com.sinonetwork.zhonghua.wheel.StrericWheelAdapter;
import com.sinonetwork.zhonghua.wheel.WheelView;

public class Soil_Test extends Activity {

	private ImageView back;
	private WheelView yearWheel, monthWheel, dayWheel;
	public static String[] yearContent = null;
	public static String[] monthContent = null;
	public static String[] dayContent = null;
	public static String[] hourContent = null;
	public static String[] minuteContent = null;
	public static String[] secondContent = null;
	private RelativeLayout relativeLayout;
	private TextView soil_shijian; // 时间
	private Spinner soil_diqu; // 地区

	private Button soil_bt1; // 预约按钮
	private EditText soil_lianxi; // 联系人
	private EditText soil_dianhua; // 联系电话
	private TextView yonghu_diqu;

	private List<ShengFenModels> sdata;
	private List<String> list;
	private String name;
	private String shengId;
	private String yhdq;

	private final int DATE_DIALOG = 1;

	private final int TIME_DIALOG = 2;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.soil_test);
		//默认情况下软件盘不自动弹出
		getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		//寻找控件
		soil_shijian = (TextView) findViewById(R.id.soil_shijian);
		relativeLayout = (RelativeLayout) findViewById(R.id.soil_rl3);
		soil_diqu = (Spinner) findViewById(R.id.soil_diqu);
		soil_bt1 = (Button) findViewById(R.id.soil_bt1);
		soil_lianxi = (EditText) findViewById(R.id.soil_lianxi);
		soil_dianhua = (EditText) findViewById(R.id.soil_dianhua);
		yonghu_diqu = (TextView) findViewById(R.id.yonghu_diqu);

		//保存账户信息
		ZHAccount currentAccount = ZhAccountPrefUtil
				.getZHAccount(Soil_Test.this.getApplicationContext());
		
		name = currentAccount.getUserName();
		shengId = String.valueOf(currentAccount.getParent_areaId());
		yhdq = String.valueOf(currentAccount.getParent_areaName());
		yonghu_diqu.setText(yhdq);
		
		Sehng(); // 省份
		initContent();
		back = (ImageView) findViewById(R.id.back);
		//向服务端提交数据
		soil_bt1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub //张三为临时测试app用户名
				if(isMobileNO(soil_dianhua.getText().toString())){
				register("orderTestSoil", name, soil_lianxi.getText()
						.toString(), soil_dianhua.getText().toString(),
						soil_shijian.getText().toString(),shengId,yhdq );}else{
							Toast.makeText(getApplicationContext(), "输入号码错误，请重新输入", 0).show();
							soil_dianhua.setText("");
						}
				//soil_diqu.getSelectedItem().toString()
				Logger.e("省ID========="+shengId);
			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		// ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
		// R.array.planets, android.R.layout.simple_spinner_item);

		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// soil_diqu.setAdapter(adapter);
		//时间选取模块
		relativeLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				View view = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE))
						.inflate(R.layout.time_picker, null);

				Calendar calendar = Calendar.getInstance();
				int curYear = calendar.get(Calendar.YEAR);
				int curMonth = calendar.get(Calendar.MONTH) + 1;
				int curDay = calendar.get(Calendar.DAY_OF_MONTH);
//				int curHour = calendar.get(Calendar.HOUR_OF_DAY);
//				int curMinute = calendar.get(Calendar.MINUTE);
//				int curSecond = calendar.get(Calendar.SECOND);

				yearWheel = (WheelView) view.findViewById(R.id.yearwheel);
				monthWheel = (WheelView) view.findViewById(R.id.monthwheel);
				dayWheel = (WheelView) view.findViewById(R.id.daywheel);
//				hourWheel = (WheelView) view.findViewById(R.id.hourwheel);
//				minuteWheel = (WheelView) view.findViewById(R.id.minutewheel);
//				secondWheel = (WheelView) view.findViewById(R.id.secondwheel);
				//选取时间
				AlertDialog.Builder builder = new AlertDialog.Builder(
						Soil_Test.this);
				builder.setView(view);

				yearWheel.setAdapter(new StrericWheelAdapter(yearContent));
				yearWheel.setCurrentItem(curYear - 2013);
				
				yearWheel.setCyclic(true);
				yearWheel
						.setInterpolator(new AnticipateOvershootInterpolator());

				monthWheel.setAdapter(new StrericWheelAdapter(monthContent));

				monthWheel.setCurrentItem(curMonth - 1);

				monthWheel.setCyclic(true);
				monthWheel
						.setInterpolator(new AnticipateOvershootInterpolator());

				dayWheel.setAdapter(new StrericWheelAdapter(dayContent));
				dayWheel.setCurrentItem(curDay - 1);
				dayWheel.setCyclic(true);
				dayWheel.setInterpolator(new AnticipateOvershootInterpolator());

//				hourWheel.setAdapter(new StrericWheelAdapter(hourContent));
//				hourWheel.setCurrentItem(curHour);
//				hourWheel.setCyclic(true);
//				hourWheel
//						.setInterpolator(new AnticipateOvershootInterpolator());
//
//				minuteWheel.setAdapter(new StrericWheelAdapter(minuteContent));
//				minuteWheel.setCurrentItem(curMinute);
//				minuteWheel.setCyclic(true);
//				minuteWheel
//						.setInterpolator(new AnticipateOvershootInterpolator());
//
//				secondWheel.setAdapter(new StrericWheelAdapter(secondContent));
//				secondWheel.setCurrentItem(curSecond);
//				secondWheel.setCyclic(true);
//				secondWheel
//						.setInterpolator(new AnticipateOvershootInterpolator());

				builder.setTitle("选取时间");
				builder.setPositiveButton("确  定",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								StringBuffer sb = new StringBuffer();
								int xyear = Integer.valueOf(yearWheel.getCurrentItemValue());//获取年份
							     int xmonth=Integer.valueOf(monthWheel.getCurrentItemValue());//获取月份 
							     int xday=Integer.valueOf(dayWheel.getCurrentItemValue());//获取日
								sb.append(yearWheel.getCurrentItemValue())
										.append("-")
										.append(monthWheel
												.getCurrentItemValue())
										.append("-")
										.append(dayWheel.getCurrentItemValue());
								
								Calendar ca = Calendar.getInstance();
								int year = ca.get(Calendar.YEAR);//获取年份
							     int month=ca.get(Calendar.MONTH);//获取月份 
							     int day=ca.get(Calendar.DATE);//获取日
//							     Toast.makeText(Soil_Test.this, "-----"+year+"-"+(month+1)+"-"+day, Toast.LENGTH_SHORT).show();
							     if((xyear>=year&&xmonth>(month+1))||(xyear>=year&&xmonth==(month+1)&&xday>=day)||xyear>year){
							    	 soil_shijian.setText(sb);
							     }else{
							    	 Toast.makeText(Soil_Test.this, "时间不能为以前！", Toast.LENGTH_SHORT).show();
							     }
//								sb.append(" ");
//								sb.append(hourWheel.getCurrentItemValue())
//										.append(":")
//										.append(minuteWheel
//												.getCurrentItemValue())
//										.append(":")
//										.append(secondWheel
//												.getCurrentItemValue());
								
								dialog.cancel();
							}
						});

				builder.show();
			}
		});
	}
	//判断一个号码是否为电话号码
	public  boolean isMobileNO(String mobiles){

		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");  

		Matcher m = p.matcher(mobiles);  

	    return m.matches();

	}

	// ------------------------------------------------------------------------------------------------------------------
	/**
	 * 测土（解决方案中的）
	 * 向服务器端提交数据
	 * @param method
	 * @param userName
	 * @param contactName
	 * @param phoneNumber
	 * @param orderTimeStr
	 * @param regionName
	 */
	private void register(String method, String userName, String contactName,
			String phoneNumber, String orderTime, String regionId,
			String regionName) {
		
		RequestParams params = new RequestParams();
		params.addBodyParameter("method", method);
		params.addBodyParameter("userName", userName);//App登录用户名
		params.addBodyParameter("contactName", contactName);//联系人
		params.addBodyParameter("phoneNumber", phoneNumber);//联系电话
		params.addBodyParameter("orderTime", orderTime);//预约时间
		params.addBodyParameter("regionId", regionId);
		params.addBodyParameter("regionName", regionName);//地区ƒ
		Logger.e("weather===" + method + "----" + userName + "-----"
				+ contactName + "------" + phoneNumber + "------" + orderTime
				+ "-----" + regionName);
		HttpUtils http = new HttpUtils();
		http.send(HttpMethod.POST, URLAddress.orderTestSoilURL, params,
				new RequestCallBack<String>() {
					@Override
					public void onLoading(long total, long current,
							boolean isUploading) {
						// testTextView.setText(current + "/" + total);
						// Toast.makeText(ItemActivity.this, "卸载",
						// Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						// Toast.makeText(ItemActivity.this, "进来",
						// Toast.LENGTH_SHORT).show();
						JSONObject resultcode = JSON
								.parseObject(responseInfo.result);
						String result = resultcode.getString("resultcode");
						// String msg = object.getString("message");
						// Toast.makeText(Soil_Test.this, result,
						// Toast.LENGTH_SHORT).show();
						if ("ok".equals(result)) {// 成功
						// Toast.makeText(Soil_Test.this, "提交成功",
						// Toast.LENGTH_SHORT).show();
							initToast();
							new Handler().postDelayed(new Runnable() {
								
								@Override
								public void run() {
									
									finish();
								}
							}, 2000);

						} else {
							Toast.makeText(Soil_Test.this, "提交失败",
									Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onStart() {
						// Toast.makeText(ItemActivity.this, "开始",
						// Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onFailure(HttpException error, String msg) {
						Toast.makeText(Soil_Test.this, "A服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
	}

	/**
	 * 自定义Tost
	 */
	private void initToast() {
		View toastRoot = getLayoutInflater().inflate(R.layout.custom_layout,
				null);
		TextView message = (TextView) toastRoot.findViewById(R.id.tvToast);
		message.setText("预约成功");

		Toast toastStart = new Toast(this);
		toastStart.setGravity(Gravity.CENTER, 0, 10);
		toastStart.setDuration(2000);
		toastStart.setView(toastRoot);
		toastStart.show();
	}

	/**
	 * 获取省
	 */
	public void Sehng() {
		sdata = new ArrayList<ShengFenModels>();
		Logger.e("No1s");
		FastJsonRequest<ShengFenModel> fastJson = new FastJsonRequest<ShengFenModel>(
				Method.POST, URLAddress.SomeAreaURL, ShengFenModel.class, null,
				null, new Response.Listener<ShengFenModel>() {
					@Override
					public void onResponse(ShengFenModel weather) {
						// 赋值

						sdata.addAll(weather.getResultdata());
						if (sdata.size() == 0 || sdata == null) {
							Toast.makeText(Soil_Test.this, "省份没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							// Toast.makeText(Comprehensive_list.this,
							// "省份数量"+String.valueOf(sdata.size())+"===="+sdata.get(1).getAREANAME().toString(),
							// Toast.LENGTH_SHORT).show();
							// adapter.notifyDataSetChanged();
							list = new ArrayList<String>();
							for (int i = 0; i < sdata.size(); i++) {
								list.add(sdata.get(i).getAREANAME().toString());
							}
							ArrayAdapter<String> adapters = new ArrayAdapter<String>(
									Soil_Test.this, R.layout.guojia_item,
									R.id.tvCateItem, list);

							soil_diqu.setAdapter(adapters);

						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(Soil_Test.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

	public void initContent() {
		yearContent = new String[10];
		for (int i = 0; i < 10; i++)
			yearContent[i] = String.valueOf(i + 2013);

		monthContent = new String[12];
		for (int i = 0; i < 12; i++) {
			monthContent[i] = String.valueOf(i + 1);
			if (monthContent[i].length() < 2) {
				monthContent[i] = "0" + monthContent[i];
			}
		}

		dayContent = new String[31];
		for (int i = 0; i < 31; i++) {
			dayContent[i] = String.valueOf(i + 1);
			if (dayContent[i].length() < 2) {
				dayContent[i] = "0" + dayContent[i];
			}
		}
		hourContent = new String[24];
		for (int i = 0; i < 24; i++) {
			hourContent[i] = String.valueOf(i);
			if (hourContent[i].length() < 2) {
				hourContent[i] = "0" + hourContent[i];
			}
		}

		minuteContent = new String[60];
		for (int i = 0; i < 60; i++) {
			minuteContent[i] = String.valueOf(i);
			if (minuteContent[i].length() < 2) {
				minuteContent[i] = "0" + minuteContent[i];
			}
		}
		secondContent = new String[60];
		for (int i = 0; i < 60; i++) {
			secondContent[i] = String.valueOf(i);
			if (secondContent[i].length() < 2) {
				secondContent[i] = "0" + secondContent[i];
			}
		}
	}

	public void buttonClick(View v) {
		int id = v.getId();
		if (id == R.id.soil_rl3) {

		}
	}
	 

}
