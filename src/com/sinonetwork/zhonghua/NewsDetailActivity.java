package com.sinonetwork.zhonghua;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.sinonetwork.zhonghua.model.News;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
/**
 * 新闻界面
 * @author enway
 *
 */
public class NewsDetailActivity extends LandBaseActivity {

	private String id;
	private WebView webView;
	protected News detail;
	public static final String ID = "ID";
	public static final String NAME = "NAME";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news_detail);
		setBackBtn();//返回上一张界面
		setTopTitleTV("新闻详情");//设置标题
		//从上一张界面中获取信息
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		id = bundle.getString(ID);
		//初始化控件
		webView = (WebView)findViewById(R.id.webView);
		loadData();
		
	}
	
	private void loadData() {
		showLoadProgressBar();//显示进度条
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					detail = LandInfoParser.getNewsDetail(id);//获取新闻详情信息

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {//程序运行在主线程

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	//加载网页的数据
	private void setView() {

		String html = "<html><body><div style='font-size: 18px;'>"+detail.getTitle()+"</div><div style='font-size: 16px;margin-top: 10px;'>"+detail.getDocContent()+"</div></body></html>";
		webView.loadDataWithBaseURL("",html, "text/html", "UTF-8",""); 
	}
}
