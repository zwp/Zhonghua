package com.sinonetwork.zhonghua;

import java.io.Serializable;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.sinonetwork.zhonghua.adapter.ChoiceLocationImagesAdapter;
import com.sinonetwork.zhonghua.model.ImageBucket;
import com.sinonetwork.zhonghua.utils.AlbumHelper;

/**
 * 
 * @author lxq
 */
public class ChoicePicActivity extends Activity {

	private GridView mGridView;
	private List<ImageBucket> dataList;
	private AlbumHelper helper;
	private ChoiceLocationImagesAdapter mAdapter;
	public static Bitmap bimap;
	public static final String EXTRA_IMAGE_LIST = "imagelist";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_picture_choice_piclist);
		helper = AlbumHelper.getHelper();
		helper.init(getApplicationContext());
		initData();
		initView();
	}

	public void initView() {
		((TextView) findViewById(R.id.top_title_textview)).setText("相册");
		((ImageView) findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ChoicePicActivity.this.finish();				
			}
		});
		mGridView = (GridView) findViewById(R.id.gv_pic);
		mAdapter = new ChoiceLocationImagesAdapter(ChoicePicActivity.this, dataList);
		mGridView.setAdapter(mAdapter);

		mGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				/**
				 * 通知适配器，绑定的数据发生了改变，应当刷新视图
				 */
				Intent intent = new Intent(ChoicePicActivity.this, ChoicePicGridActivity.class);
				intent.putExtra(ChoicePicActivity.EXTRA_IMAGE_LIST, (Serializable) dataList.get(position).imageList);
				intent.putExtra("groupId", getIntent().getStringExtra("groupId"));
				startActivity(intent);
				finish();
			}
		});
	}

	public void initData() {
		dataList = helper.getImagesBucketList(false);
	}

}
