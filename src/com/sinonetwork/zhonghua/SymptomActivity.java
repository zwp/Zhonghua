package com.sinonetwork.zhonghua;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sinonetwork.zhonghua.adapter.SymptomListAdapter;
import com.sinonetwork.zhonghua.model.Question;
import com.sinonetwork.zhonghua.model.Symptom;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;

public class SymptomActivity extends LandBaseActivity {

	private String id;
	private String name;
	public static final String ID = "ID";
	public static final String NAME = "NAME";
	protected ArrayList<Symptom> list;
	private SymptomListAdapter mAdapter;

	private ListView listView;
	private TextView cropName;
	private RelativeLayout diagnosis;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_symptom);
		setBackBtn();
		setTopTitleTV("作物医生");
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		id = bundle.getString(ID);
		name = bundle.getString(NAME);
		PrefUtil.savePref(SymptomActivity.this, "subSymptom", "");// 只在每次进入时清空本地字符串，从上个页面返回不请空
		PrefUtil.savePref(SymptomActivity.this, "symptomType", "");// 只在每次进入时清空本地字符串，从上个页面返回不请空

		listView = (ListView) findViewById(R.id.list);
		cropName = (TextView) findViewById(R.id.cropName);
		diagnosis = (RelativeLayout) findViewById(R.id.diagnosis);
		cropName.setText(name);
		diagnosis.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String str = PrefUtil.getStringPref(SymptomActivity.this,
						"subSymptom");
				String symptomName = "";
				if (str.length() > 0) {
					String idStr = str.substring(0, str.length() - 1);
					String[] strFormat = idStr.split(",");

					for (int i = 0; i < strFormat.length; i++) {
						String subId = strFormat[i];
						for (int j = 0; j < list.size(); j++) {
							Symptom symptom = list.get(j);

							for (int z = 0; z < symptom.getSubSymptomList()
									.size(); z++) {
								if (Integer.parseInt(subId) == symptom
										.getSubSymptomList().get(z).getId()) {
									String newStr = symptom
											.getSymptomsTypeName() + ",";
									if (symptomName.indexOf(newStr) == -1) {
										symptomName += symptom
												.getSymptomsTypeName() + ",";
									}
									continue;
								}
							}
						}
					}
					
					Intent intent = new Intent(SymptomActivity.this,
							Result.class);
					intent.putExtra("id", id);
					// Toast.makeText(SymptomActivity.this, String.valueOf(id),
					// Toast.LENGTH_SHORT).show();
					intent.putExtra("name", name);
					intent.putExtra("symptomsIds",
							str.substring(0, str.length() - 1));
					intent.putExtra("symptomsNames",
							symptomName.substring(0, symptomName.length() - 1)
									.replace(",", " "));
					startActivity(intent);
				} else {
					showShortToast("请选择至少一种症状");
				}
			}
		});
		loadData();

	}

	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					list = LandInfoParser.getSymptomList(id);

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}

	private void setView() {

		if (null != list) {
			if (list.size() > 0) {
				mAdapter = new SymptomListAdapter(this);
				mAdapter.setData(list);
				listView.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			} else {
				showShortToast("当前没有" + name + "的症状数据");
			}

		}

	}
}
