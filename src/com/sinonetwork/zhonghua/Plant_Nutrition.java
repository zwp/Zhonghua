package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;

public class Plant_Nutrition extends Activity {
	private ImageView back;
	private int[] resId = new int[] { R.drawable.zwyy_img01,
			R.drawable.zwyy_img02, R.drawable.zwyy_img03, R.drawable.zwyy_img04,
			R.drawable.zwyy_img05, R.drawable.zwyy_img06 };
	private String[] name = new String[] { "水稻缺氮症状", "花生缺钙症状", "白菜锌症状及防治", "番茄缺硼症状",
			"马铃薯缺素症状", "玉米雀林症状" };
	private GridView gridView;
  @Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.plant_nutrition);
    gridView = (GridView) findViewById(R.id.plant_gv);
    back = (ImageView) findViewById(R.id.back);

    
    ArrayList<HashMap<String, Object>> item = new ArrayList<HashMap<String, Object>>();
	for (int i = 0; i < resId.length; i++) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("itemImage", resId[i]);
		map.put("itemName", name[i]);
		item.add(map);
	}
	
	SimpleAdapter simpleAdapter = new SimpleAdapter(Plant_Nutrition.this, item,
			R.layout.plant_nutrition_item,
			new String[] { "itemImage", "itemName" }, new int[] {
					R.id.plant_item_img1, R.id.plant_item_txt1 });
	gridView.setAdapter(simpleAdapter);
	

	back.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
	});
}
}
