package com.sinonetwork.zhonghua;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

public class ShopActivity extends LandBaseActivity {
	private String homeUrl = URLAddress.SHOP_URL + "fert_bbc/weixin/platform/index.htm";
	private String loginUrl = URLAddress.SHOP_URL + "fert_bbc/weixin_login.htm?";
	private String currentUrl = "";
	private ZHAccount account;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop);
		PrefUtil.savePref(this, "fromShop", "shop");
		// WebView
		WebView browser = (WebView) findViewById(R.id.Toweb);
		showLoadProgressBar();
		if (PrefUtil.getBooleanPref(ShopActivity.this, ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, false)) {
			account = ZhAccountPrefUtil.getZHAccount(this);
			browser.loadUrl(loginUrl + "username=" + account.getUserName() + "&password=" + account.getPassword() + "&sequence=" + account.getSequence());


		} else {
			browser.loadUrl(homeUrl);
		}
		//设置字体大小固定，不随着系统设置而改变
		browser.getSettings().setDefaultFontSize(16);
		// 开启javascript设置
		browser.getSettings().setJavaScriptEnabled(true);
		// 把RIAExample的一个实例添加到js的全局对象window中
		// 这样就可以使用window.javatojs来调用它的方法
		browser.addJavascriptInterface(this, "webView");
		// 设置可自由缩放网页

		// 如果页面中链接，如果希望点击链接继续在当前browser中响应，
		// 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象
		browser.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				// Log.v("zhong", "weburl--" + url);
				currentUrl = url;
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				hideLoadProgressBar();
			}
		});
	}

	// go back
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		WebView browser = (WebView) findViewById(R.id.Toweb);
		// Check if the key event was the Back button and if there's history
		if ((keyCode == KeyEvent.KEYCODE_BACK) && browser.canGoBack()) {

			browser.goBack();

			return true;
		}
		// return true;
		// If it wasn't the Back key or there's no web page history, bubble up
		// to the default
		// system behavior (probably exit the activity)
		return super.onKeyDown(keyCode, event);
	}

	// 登录成功后js调用的方法如果target 大于等于API 17，则需要加上如下注解
	@JavascriptInterface
	public void GoHome() {

		// Log.v("zhong", "GoHome调用成功--");
		finish();

	}

	// 登录成功后js调用的方法如果target 大于等于API 17，则需要加上如下注解
	@JavascriptInterface
	public void Login(String data) throws Exception {

		// Log.v("zhong", "login调用成功--" + data);
		JSONObject obj = new JSONObject(data);

		// 保存用户信息
		ZHAccount account = LandInfoParser.SaveCurrentAccount(obj);
		ZhAccountPrefUtil.saveZHAccount(this, account);

		PrefUtil.savePref(this, ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, true);

	}

	// 登录失败后js调用的方法
	@JavascriptInterface
	public void LoginFailed(String data) throws JSONException {

		// Log.v("zhong", "login失败--" + data);
		Toast.makeText(ShopActivity.this, data, Toast.LENGTH_SHORT).show();

	}

	@Override
	protected void onDestroy() {

		super.onDestroy();

	}
}
