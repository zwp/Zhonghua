package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.Comprehensive_listAdapter;
import com.sinonetwork.zhonghua.model.ShengFenModel;
import com.sinonetwork.zhonghua.model.ShengFenModel.ShengFenModels;
import com.sinonetwork.zhonghua.model.TService;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList;
import com.sinonetwork.zhonghua.model.farmingGuidInfoList.farmingGuidInfoLists;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;
import com.sinonetwork.zhonghua.view.MyListView;
import com.sinonetwork.zhonghua.view.MyListView.OnLoadMoreListener;
import com.sinonetwork.zhonghua.view.MyListView.OnRefreshListener;

public class Comprehensive_list extends Activity {

	// private ListView listview;
	// private PullToRefreshExpandableListView listview;
	// private Comprehensive_listAdapter adapter;
	private Comprehensive_listAdapter adapter;
	private List<farmingGuidInfoLists> data = new ArrayList<farmingGuidInfoLists>();
	private List<ShengFenModels> sdata;
	private TextView textView3;

	private Spinner com_spinner1; // 地区
	private EditText com_edit_compre; // 名称
	private String page = "1"; // 页数
	private String pagerows = "10"; // 每页条数

	private String zuowu = "";
	private String diqu = "";
	private List<String> list;
	private ImageView back;

	private MyListView listview;
	private static final int LOAD_DATA_FINISH = 10;
	private static final int REFRESH_DATA_FINISH = 11;
	private static final int NO_MORE_DARA = 12;
	private int currentPage = 1;
	private int totalPage;
	private Bundle bundle;
	private ArrayAdapter<String> adapters;

	private LocationManager locationManager;
	private double latitude = 0.0;
	private double longitude = 0.0;
	private String dqdiqu;
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			double[] data = (double[]) msg.obj;
			// showJW.setText("经度："+data[0]+"\t纬度:"+data[1]);
			longitude = data[0];
			latitude = data[1];
			// Toast.makeText(Qixiang.this, "经度="+longitude+"，纬度="+latitude,
			// Toast.LENGTH_SHORT).show();
			String weidu = String.valueOf(longitude);
			String jingdu = String.valueOf(latitude);
			loadData("getTService", weidu, jingdu); // 获取实况气象数据
			// RiZhaoShiChang("getTAwss", weidu, jingdu); // 获取日照时长时间

			Logger.e("定位服务，坐标如下：----维度：" + weidu + "， ---- 经度：" + jingdu);
		}

	};

	// 刷新加载功能
	private Handler mHandler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case REFRESH_DATA_FINISH:
				if (adapter != null) {
					adapter = new Comprehensive_listAdapter(
							Comprehensive_list.this, data);
					adapter.setData(data);
					listview.setAdapter(adapter);
					adapter.notifyDataSetChanged();
				}
				listview.onRefreshComplete(); // 下拉刷新完成
				break;
			case LOAD_DATA_FINISH:
				if (adapter != null) {
					adapter.setData((ArrayList<farmingGuidInfoLists>) msg.obj);
					adapter.notifyDataSetChanged();
				}
				listview.misHaveNewDatas = false;
				listview.onLoadMoreComplete(); // 加载更多完成
				break;
			case NO_MORE_DARA:
				listview.misHaveNewDatas = false;
				listview.onLoadMoreComplete(); // 加载更多完成
				break;
			default:
				break;
			}
		};
	};

	// 刷新加载功能
	public void loadData(final int type) {
		new Thread() {
			@Override
			public void run() {
				switch (type) {
				case 0:
					data.clear();
					currentPage = 1;
					loadData1("getfarmingGuidInfoList", zuowu, diqu,
							String.valueOf(currentPage), pagerows, 0);
					break;

				case 1:
					currentPage++;
					if (currentPage <= totalPage) {
						loadData1("getfarmingGuidInfoList", zuowu, diqu,
								String.valueOf(currentPage), pagerows, 1);
					} else {
						currentPage--;
						Message _Msg = mHandler.obtainMessage(NO_MORE_DARA);
						mHandler.sendMessage(_Msg);

					}

					break;
				}

			}
		}.start();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comprehensive_lists);

		listview = (MyListView) findViewById(R.id.listview);
		com_spinner1 = (Spinner) findViewById(R.id.com_spinner1);
		com_edit_compre = (EditText) findViewById(R.id.com_edit_compre);
		textView3 = (TextView) findViewById(R.id.textView3);
		back = (ImageView) findViewById(R.id.back);

		// zuowu= com_edit_compre.getText().toString();

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Location location = locationManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (location != null) {
			latitude = location.getLatitude(); // 经度
			longitude = location.getLongitude(); // 纬度
			double[] data = { latitude, longitude };
			Message msg = handler.obtainMessage();
			msg.obj = data;
			handler.sendMessage(msg);
		}

		// // 添加省份
		 Sehng();

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Comprehensive_list.this,
						SeasonManager.class);
				bundle = new Bundle();
				bundle.putString("id", data.get(arg2 - 1).getId().toString());
				Log.e("ID:",data.get(arg2 - 1).getId().toString());
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		// 查找
		textView3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View position) {
				// TODO Auto-generated method stub
				data = new ArrayList<farmingGuidInfoList.farmingGuidInfoLists>();
				diqu = com_spinner1.getSelectedItem().toString();
				zuowu = com_edit_compre.getText().toString();
				data = new ArrayList<farmingGuidInfoList.farmingGuidInfoLists>();
				loadData1("getfarmingGuidInfoList", zuowu, diqu,
						String.valueOf(totalPage - 1), pagerows, 0);
				Logger.e("必传参数==getfarmingGuidInfoList" + "作物==" + zuowu
						+ "，地区==" + diqu + "，页数==" + currentPage + "，数量=="
						+ pagerows);
				adapter = new Comprehensive_listAdapter(
						Comprehensive_list.this, data);
				adapter.setData(data);
				listview.setAdapter(adapter);
				// loadData(0);
				// adapter.notifyDataSetChanged();
			}
		});

		// loadData("getfarmingGuidInfoList","","",page,pagerows);
		adapter = new Comprehensive_listAdapter(Comprehensive_list.this, data);
		adapter.setData(data);
		listview.setAdapter(adapter);

		// 刷新加载功能
		listview.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO 下拉刷新
				loadData(0);
			}
		});

		// 刷新加载功能
		listview.setOnLoadListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				// TODO 加载更多
				loadData(1);
			}
		});
		// 添加加载更多 footer view
		loadData(0);

	}

	/**
	 * 获取实时数据
	 * 
	 * @param method
	 *            方法名
	 * @param latitude
	 *            纬度
	 * @param longitude
	 *            //经度
	 */
	public void loadData(String method, String latitude, String longitude) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("latitude", latitude);
		map.put("longitude", longitude);
		// Toast.makeText(Price_information.this,
		// "纬度=aa="+latitude+"经度=aa="+longitude, Toast.LENGTH_SHORT).show();
		Logger.e("method======vvv======" + method + "latitude=====vvv====="
				+ latitude + "logitude=====vvv===" + longitude);
		FastJsonRequest<TService> fastJson = new FastJsonRequest<TService>(
				Method.POST, URLAddress.TServiceURL, TService.class, null, map,
				new Response.Listener<TService>() {
					@Override
					public void onResponse(TService weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(Comprehensive_list.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							dqdiqu = weather.getResultdata().getProvcn();
							// Toast.makeText(Comprehensive_list.this,
							// "当前地区=aa="+dqdiqu, Toast.LENGTH_SHORT).show();
							// 添加省份
							Sehng();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(Comprehensive_list.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

	// 获取数据
	public void loadData1(String method, String name, String area,
			String curPage, String pageRows, final int type) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("name", name);
		map.put("area", area);
		map.put("curPage", curPage);
		map.put("pageRows", pageRows);
		Logger.e("第几页====" + curPage + "，每页条数====" + pageRows);

		FastJsonRequest<farmingGuidInfoList> fastJson = new FastJsonRequest<farmingGuidInfoList>(
				Method.POST, URLAddress.farmingGuidInfoListURL,
				farmingGuidInfoList.class, null, map,
				new Response.Listener<farmingGuidInfoList>() {
					@Override
					public void onResponse(farmingGuidInfoList weather) {
						// 赋值
						totalPage = Integer.valueOf(weather.getTotalPages()); // 刷新加载功能(赋值页数)
						// currentPage = Integer.valueOf(weather.getCurPage());

						if (weather.getResultdata().size() == 0
								|| weather.getResultdata() == null) {
							Toast.makeText(Comprehensive_list.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							// 刷新加载功能
							if (type == 0) { // 下拉刷新
								// Collections.reverse(mList); //逆序
								data.addAll(weather.getResultdata());
								Message _Msg = mHandler.obtainMessage(
										REFRESH_DATA_FINISH);
								mHandler.sendMessage(_Msg);
							} else if (type == 1) {
								data.addAll(weather.getResultdata());
								Message _Msg = mHandler.obtainMessage(
										LOAD_DATA_FINISH, data);
								mHandler.sendMessage(_Msg);
							}

							// data.addAll(weather.getResultdata());
							adapter.notifyDataSetChanged();
						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(Comprehensive_list.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

	/**
	 * 获取省
	 */
	public void Sehng() {
		sdata = new ArrayList<ShengFenModels>();
		Logger.e("No1s");
		FastJsonRequest<ShengFenModel> fastJson = new FastJsonRequest<ShengFenModel>(
				Method.POST, URLAddress.SomeAreaURL, ShengFenModel.class, null,
				null, new Response.Listener<ShengFenModel>() {
					@Override
					public void onResponse(ShengFenModel weather) {
						// 赋值

						sdata.addAll(weather.getResultdata());
						if (sdata.size() == 0 || sdata == null) {
							Toast.makeText(Comprehensive_list.this, "省份没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							// Toast.makeText(Comprehensive_list.this,
							// "省份数量"+String.valueOf(sdata.size())+"===="+sdata.get(1).getAREANAME().toString(),
							// Toast.LENGTH_SHORT).show();

							list = new ArrayList<String>();
							for (int i = 0; i < sdata.size(); i++) {
								String heihei = sdata.get(i).getAREANAME()
										.toString();
								// list.add(sdata.get(i).getAREANAME().toString());
								System.out.println(heihei + "heihei");
								list.add(heihei);
								if (list.get(i).contains("北京")) {
									com_spinner1.setSelection(i);
								}

							}
							adapters = new ArrayAdapter<String>(
									Comprehensive_list.this,
									R.layout.guojia_item, R.id.tvCateItem, list);

							com_spinner1.setAdapter(adapters);
							adapter.notifyDataSetChanged();
						}

					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						Logger.e("No3");
						Logger.e("VolleyError===" + arg0.getMessage());
						Toast.makeText(Comprehensive_list.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		Logger.e("No4" + fastJson);
		RequestManager.addRequest(fastJson, this);
	}

}
