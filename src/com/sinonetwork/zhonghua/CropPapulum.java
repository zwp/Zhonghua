package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.sinonetwork.zhonghua.adapter.ExpandAdapter;

public class CropPapulum extends Activity {
	// private String[] title = { "粮食作物", "粮食作物", "粮食作物", "粮食作物", "粮食作物",
	// "粮食作物",
	// "粮食作物", "粮食作物", "粮食作物", "粮食作物" };
	// private String[] content = { "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱",
	// "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱",
	// "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱", "水稻，小麦，玉米，高粱" };
	// private List<Map<String, Object>> list;
	private ExpandableListView mListView = null;
	private ExpandAdapter mAdapter = null;
	private List<List<Item>> mData = new ArrayList<List<Item>>();

	private int[] mGroupArrays = { R.array.one, R.array.two, R.array.three,
			R.array.four };

	private int[][] mImageIds = new int[][] {
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1,
					R.drawable.check1, R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1,
					R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1 },
			{ R.drawable.check1, R.drawable.check1, R.drawable.check1 } };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crop_pabulum);
		mAdapter = new ExpandAdapter(this, mData);
		mListView = (ExpandableListView)findViewById(R.id.cropExpandlistview);
		mListView.setAdapter(mAdapter);
	}

	// @Override
	// public boolean onChildClick(ExpandableListView parent, View v,
	// int groupPosition, int childPosition, long id) {
	// // TODO Auto-generated method stub
	//
	//
	// return true;
	// }
	public boolean OnChildClick(ExpandableListView parent, View view,
			int groupPosition, int childPosition, long id) {
		// TODO Auto-generated method stub
		Item item = mAdapter.getChild(groupPosition, childPosition);
		// new AlertDialog.Builder(ts
		Intent intent = new Intent(this, Result.class);
		startActivity(intent);
		return true;
	}

	private void initData() {
		for (int i = 0; i < mGroupArrays.length; i++) {
			List<Item> list = new ArrayList<Item>();
			String[] childs = getStringArray(mGroupArrays[i]);
			// String[] details = getStringArray(mDetailIds[i]);
			for (int j = 0; j < childs.length; j++) {
				Item item = new Item(mImageIds[i][j], childs[j]);
				list.add(item);
			}
			mData.add(list);
		}
		// List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		// for (int i = 0; i < mGroupArrays.length; i++) {
		// Map<String, Object> map = new HashMap<String, Object>();
		// map.put("mGroupArrays", mGroupArrays[i]);
		// map.put("mImageIds", mImageIds[i]);
		// list.add(map);
		// }
	}

	private String[] getStringArray(int mGroupArrays2) {
		return getResources().getStringArray(mGroupArrays2);
	}
}
