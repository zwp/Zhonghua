package com.sinonetwork.zhonghua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.adapter.searchExamplesAdapter;
import com.sinonetwork.zhonghua.model.searchExamplei;
import com.sinonetwork.zhonghua.model.searchExamplei.searchExampleis;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class searchExamples extends Activity {

	private ListView searchExamples_lv;
	private searchExamplesAdapter adapter;
	private List<searchExampleis> data;
	private ImageView searchexamplety_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchexampletypei);
		// 查找控件
		searchExamples_lv = (ListView) findViewById(R.id.searchExamples_lv);
		searchexamplety_back = (ImageView) findViewById(R.id.searchexamplety_back);
		// 返回
		searchexamplety_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		// 从前一个界面获取数据
		Bundle bundle = getIntent().getExtras();
		String exampleType = bundle.getString("exampleType");
		String exampleTypeId = bundle.getString("exampleTypeId");
		// 加载数据
		loadData("searchExamples", exampleType, exampleTypeId);
		adapter = new searchExamplesAdapter(searchExamples.this, data);
		adapter.setData(data);
		//显示数据
		searchExamples_lv.setAdapter(adapter);

		searchExamples_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(searchExamples.this,
						ShiYanShiFan.class);
				Bundle bundle = new Bundle();
				bundle.putString("id", data.get(arg2).getId().toString());
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});

	}

	// 加载数据
	public void loadData(String method, String exampleType, String exampleTypeId) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("exampleType", exampleType);
		map.put("exampleTypeId", exampleTypeId);
		data = new ArrayList<searchExampleis>();
		FastJsonRequest<searchExamplei> fastJson = new FastJsonRequest<searchExamplei>(
				Method.POST, URLAddress.searchExampleTypeURL,
				searchExamplei.class, null, map,
				new Response.Listener<searchExamplei>() {
					@Override
					public void onResponse(searchExamplei weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(searchExamples.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							data.addAll(weather.getResultdata());
							adapter.notifyDataSetChanged();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						 
						Toast.makeText(searchExamples.this, "服务器数据异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}
}
