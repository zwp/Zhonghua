package com.sinonetwork.zhonghua.model;

import java.util.List;

public class PriceHangqing extends BaseModel {
	private String curPage;
	private String totalPages;
	private List<PriceHangqings> resultdata;

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public List<PriceHangqings> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<PriceHangqings> resultdata) {
		this.resultdata = resultdata;
	}

	public class PriceHangqings {
		private String id;
		private String d1;
		private String d2;
		private String d3;
		private String d4;
		private String d5;
		private String d6;
		private String d7;
		private String publishTime;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getD1() {
			return d1;
		}

		public void setD1(String d1) {
			this.d1 = d1;
		}

		public String getD2() {
			return d2;
		}

		public void setD2(String d2) {
			this.d2 = d2;
		}

		public String getD3() {
			return d3;
		}

		public void setD3(String d3) {
			this.d3 = d3;
		}

		public String getD4() {
			return d4;
		}

		public void setD4(String d4) {
			this.d4 = d4;
		}

		public String getD5() {
			return d5;
		}

		public void setD5(String d5) {
			this.d5 = d5;
		}

		public String getD6() {
			return d6;
		}

		public void setD6(String d6) {
			this.d6 = d6;
		}

		public String getD7() {
			return d7;
		}

		public void setD7(String d7) {
			this.d7 = d7;
		}

		public String getPublishTime() {
			return publishTime;
		}

		public void setPublishTime(String publishTime) {
			this.publishTime = publishTime;
		}

	}

}
