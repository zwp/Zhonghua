package com.sinonetwork.zhonghua.model;

public class TAwss extends BaseModel {

	private String curPage;
	private String totalPages;
	private TAwsss resultdata;
	
	
	
	public String getCurPage() {
		return curPage;
	}



	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}



	public String getTotalPages() {
		return totalPages;
	}



	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}



	public TAwsss getResultdata() {
		return resultdata;
	}



	public void setResultdata(TAwsss resultdata) {
		this.resultdata = resultdata;
	}



	public class TAwsss{
		private String id;
		private String areaid;
		private String nameen;
		private String namecn;
		private String districten;
		private String districtcn;
		private String proven;
		private String provcn;
		private String nationen;
		private String nationcn;
		private String longitude;
		private String latitude;
		private String sunShine;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getAreaid() {
			return areaid;
		}
		public void setAreaid(String areaid) {
			this.areaid = areaid;
		}
		public String getNameen() {
			return nameen;
		}
		public void setNameen(String nameen) {
			this.nameen = nameen;
		}
		public String getNamecn() {
			return namecn;
		}
		public void setNamecn(String namecn) {
			this.namecn = namecn;
		}
		public String getDistricten() {
			return districten;
		}
		public void setDistricten(String districten) {
			this.districten = districten;
		}
		public String getDistrictcn() {
			return districtcn;
		}
		public void setDistrictcn(String districtcn) {
			this.districtcn = districtcn;
		}
		public String getProven() {
			return proven;
		}
		public void setProven(String proven) {
			this.proven = proven;
		}
		public String getProvcn() {
			return provcn;
		}
		public void setProvcn(String provcn) {
			this.provcn = provcn;
		}
		public String getNationen() {
			return nationen;
		}
		public void setNationen(String nationen) {
			this.nationen = nationen;
		}
		public String getNationcn() {
			return nationcn;
		}
		public void setNationcn(String nationcn) {
			this.nationcn = nationcn;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getSunShine() {
			return sunShine;
		}
		public void setSunShine(String sunShine) {
			this.sunShine = sunShine;
		}
		
		
	}
	
}
