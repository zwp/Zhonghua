package com.sinonetwork.zhonghua.model;

public class SubSymptom {

	private int id;
	private String symptomsName;
	private String remark;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSymptomsName() {
		return symptomsName;
	}

	public void setSymptomsName(String symptomsName) {
		this.symptomsName = symptomsName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
