package com.sinonetwork.zhonghua.model;

import java.util.ArrayList;

public class Categorys{

	private int id;
	private String cropName;
	private ArrayList<SubCategorys> subCategorysList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCropName() {
		return cropName;
	}
	public void setCropName(String cropName) {
		this.cropName = cropName;
	}
	public ArrayList<SubCategorys> getSubCategorysList() {
		return subCategorysList;
	}
	public void setSubCategorysList(ArrayList<SubCategorys> subCategorysList) {
		this.subCategorysList = subCategorysList;
	}

}
