package com.sinonetwork.zhonghua.model;

public class YiShengXQ extends BaseModel {

	private String curPage;
	private String totalPages;
	private YiShengXQs resultdata;
	
	
	
	public String getCurPage() {
		return curPage;
	}



	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}



	public String getTotalPages() {
		return totalPages;
	}



	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}



	public YiShengXQs getResultdata() {
		return resultdata;
	}



	public void setResultdata(YiShengXQs resultdata) {
		this.resultdata = resultdata;
	}



	public class YiShengXQs{
		private String id;
		private String cpName;
		private String pictures;
		private String trait;
		private String recommendedPesticide;
		private String bhoryy;
		private String cropNames;
		private String diseaseNames;
		private String diseaseControl;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCpName() {
			return cpName;
		}
		public void setCpName(String cpName) {
			this.cpName = cpName;
		}
		public String getPictures() {
			return pictures;
		}
		public void setPictures(String pictures) {
			this.pictures = pictures;
		}
		public String getTrait() {
			return trait;
		}
		public void setTrait(String trait) {
			this.trait = trait;
		}
		public String getRecommendedPesticide() {
			return recommendedPesticide;
		}
		public void setRecommendedPesticide(String recommendedPesticide) {
			this.recommendedPesticide = recommendedPesticide;
		}
		public String getBhoryy() {
			return bhoryy;
		}
		public void setBhoryy(String bhoryy) {
			this.bhoryy = bhoryy;
		}
		public String getCropNames() {
			return cropNames;
		}
		public void setCropNames(String cropNames) {
			this.cropNames = cropNames;
		}
		public String getDiseaseNames() {
			return diseaseNames;
		}
		public void setDiseaseNames(String diseaseNames) {
			this.diseaseNames = diseaseNames;
		}
		public String getDiseaseControl() {
			return diseaseControl;
		}
		public void setDiseaseControl(String diseaseControl) {
			this.diseaseControl = diseaseControl;
		}
		
		
	}
	
}
