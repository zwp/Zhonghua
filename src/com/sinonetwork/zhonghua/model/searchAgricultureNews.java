package com.sinonetwork.zhonghua.model;

import java.util.List;

public class searchAgricultureNews extends BaseModel {

	private String curPage;
	private String totalPages;
	private List<searchAgricultureNewss> resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}


	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}


	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	

	public List<searchAgricultureNewss> getResultdata() {
		return resultdata;
	}


	public void setResultdata(List<searchAgricultureNewss> resultdata) {
		this.resultdata = resultdata;
	}



	public class searchAgricultureNewss{
		private String id;
		private String title;
		private String docContent;
		private String time;
		private String infoTypeId;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getTime() {
			return time;
		}
		public void setTime(String time) {
			this.time = time;
		}
		public String getDocContent() {
			return docContent;
		}
		public void setDocContent(String docContent) {
			this.docContent = docContent;
		}
		public String getInfoTypeId() {
			return infoTypeId;
		}
		public void setInfoTypeId(String infoTypeId) {
			this.infoTypeId = infoTypeId;
		}
		
		
	}
	
}
