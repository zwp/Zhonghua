package com.sinonetwork.zhonghua.model;

public class ZHAccount {
	// {"care":"","cul_area":"","userName":"testBuyer1","areaName":"北京市","areaId":4521985,"plant":"","password":"4297f44b13955235245b2497399d7a93","parent_areaName":"北京市","parent_areaId":4521984}

	private String care;//关注的作物的Id号
	private String cul_area;//种植的面积
	private String userName;//用户名
	private String areaName;//城市名称，二级城市
	private int areaId;//城市ID
	private String plant;//种植的作物
	private String password;//密码
	private String parent_areaName;//城市名称，一级城市
	private String telephone;//手机号
	private String sequence; // 序列号
	private int parent_areaId;//一级城市Id号
	private int user_integ;//智慧豆

	public String getCare() {
		return care;
	}

	public void setCare(String care) {
		this.care = care;
	}

	public String getCul_area() {
		return cul_area;
	}

	public void setCul_area(String cul_area) {
		this.cul_area = cul_area;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getParent_areaName() {
		return parent_areaName;
	}

	public void setParent_areaName(String parent_areaName) {
		this.parent_areaName = parent_areaName;
	}

	public int getParent_areaId() {
		return parent_areaId;
	}

	public void setParent_areaId(int parent_areaId) {
		this.parent_areaId = parent_areaId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public int getUser_integ() {
		return user_integ;
	}

	public void setUser_integ(int user_integ) {
		this.user_integ = user_integ;
	}

}
