package com.sinonetwork.zhonghua.model;

import java.util.List;

public class DiQuModel extends BaseModel {
	private String curPage;
	private String totalPages;
	private List<String> resultdata;

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public List<String> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<String> resultdata) {
		this.resultdata = resultdata;
	}

	

}
