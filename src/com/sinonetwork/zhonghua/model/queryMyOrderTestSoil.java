package com.sinonetwork.zhonghua.model;

import java.io.Serializable;
import java.util.List;

public class queryMyOrderTestSoil extends BaseModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<queryMyOrderTestSoils> resultdata;
	
	
	
	public List<queryMyOrderTestSoils> getResultdata() {
		return resultdata;
	}



	public void setResultdata(List<queryMyOrderTestSoils> resultdata) {
		this.resultdata = resultdata;
	}


	//内部类
	public class queryMyOrderTestSoils{
		private String id;//预约信息id
		private String userName;//登录用户名称
		private String contactName;//联系人
		private String phoneNumber;//联系电话
		private String regionId;//地区id
		private String orderTimeStr;//预约时间
		private String regionName;//地区名
		private String uniqueId;//关联测土配肥
		private String status;//状态：0 处理中，1已完成，2已关闭
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getContactName() {
			return contactName;
		}
		public void setContactName(String contactName) {
			this.contactName = contactName;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getRegionId() {
			return regionId;
		}
		public void setRegionId(String regionId) {
			this.regionId = regionId;
		}
		public String getRegionName() {
			return regionName;
		}
		public void setRegionName(String regionName) {
			this.regionName = regionName;
		}
		public String getUniqueId() {
			return uniqueId;
		}
		public void setUniqueId(String uniqueId) {
			this.uniqueId = uniqueId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getOrderTimeStr() {
			return orderTimeStr;
		}
		public void setOrderTimeStr(String orderTimeStr) {
			this.orderTimeStr = orderTimeStr;
		}
		
		
	}
	
}
