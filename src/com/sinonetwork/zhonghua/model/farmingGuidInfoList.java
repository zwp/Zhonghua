package com.sinonetwork.zhonghua.model;

import java.util.List;

public class farmingGuidInfoList extends BaseModel {

	private String pageRows;
	private String curPage;
	private String totalPages;
	private String totalRows;
	private List<farmingGuidInfoLists> resultdata;
	
	
	
	public String getPageRows() {
		return pageRows;
	}



	public void setPageRows(String pageRows) {
		this.pageRows = pageRows;
	}



	public String getCurPage() {
		return curPage;
	}



	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}



	public String getTotalPages() {
		return totalPages;
	}



	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}



	public String getTotalRows() {
		return totalRows;
	}



	public void setTotalRows(String totalRows) {
		this.totalRows = totalRows;
	}





	public List<farmingGuidInfoLists> getResultdata() {
		return resultdata;
	}



	public void setResultdata(List<farmingGuidInfoLists> resultdata) {
		this.resultdata = resultdata;
	}





	public class farmingGuidInfoLists{
		private String id;
		private String cropId;
		private String breed;
		private String area;
		private String targetYield;
		private String yieldComponent;
		private String seedTypes;
		private String name;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getCropId() {
			return cropId;
		}
		public void setCropId(String cropId) {
			this.cropId = cropId;
		}
		public String getBreed() {
			return breed;
		}
		public void setBreed(String breed) {
			this.breed = breed;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getTargetYield() {
			return targetYield;
		}
		public void setTargetYield(String targetYield) {
			this.targetYield = targetYield;
		}
		public String getYieldComponent() {
			return yieldComponent;
		}
		public void setYieldComponent(String yieldComponent) {
			this.yieldComponent = yieldComponent;
		}
		public String getSeedTypes() {
			return seedTypes;
		}
		public void setSeedTypes(String seedTypes) {
			this.seedTypes = seedTypes;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		
	}
	
}
