package com.sinonetwork.zhonghua.model;

import java.util.List;

public class searchExampleType extends BaseModel {

	private String curPage;//当前页
	private String totalPages;//总页数
	private List<searchExampleTypes> resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}



	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}



	public String getTotalPages() {
		return totalPages;
	}



	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}



	public List<searchExampleTypes> getResultdata() {
		return resultdata;
	}



	public void setResultdata(List<searchExampleTypes> resultdata) {
		this.resultdata = resultdata;
	}



	public class searchExampleTypes{
		private String id;
		private String type;
		private String name;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		
	}
	
}
