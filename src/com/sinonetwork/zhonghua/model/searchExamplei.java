package com.sinonetwork.zhonghua.model;

import java.util.List;
//示范列表
public class searchExamplei extends BaseModel {

	private String curPage;
	private String totalPages;
	private List<searchExampleis> resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}


	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}


	public String getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}


	public List<searchExampleis> getResultdata() {
		return resultdata;
	}


	public void setResultdata(List<searchExampleis> resultdata) {
		this.resultdata = resultdata;
	}


	public class searchExampleis{
		private String id;
		private String exampleType;//示范类型
		private String principal;//负责人
		private String principalContact;//负责人联系方式
		private String exampleFamily;//示范户
		private String exampleFamilyContact;//示范户联系手机
		private String seedIds;//作物id
		private String productComboInfo;//产品及套餐信息
		private String exampleScheme;//示范方案
		private String processTracking;//过程跟踪
		private String testAssess;//测产评估
		private String summaryAnalyze;//总结分析
		private String keyWord;//化肥关键字
		private String exampleTypeId;//
		private String pictures;//
		private String cropName;//作物名称
		private String exampleTypeName;//
		
		public String getPictures() {
			return pictures;
		}
		public void setPictures(String pictures) {
			this.pictures = pictures;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getExampleType() {
			return exampleType;
		}
		public void setExampleType(String exampleType) {
			this.exampleType = exampleType;
		}
		public String getPrincipal() {
			return principal;
		}
		public void setPrincipal(String principal) {
			this.principal = principal;
		}
		public String getPrincipalContact() {
			return principalContact;
		}
		public void setPrincipalContact(String principalContact) {
			this.principalContact = principalContact;
		}
		public String getExampleFamily() {
			return exampleFamily;
		}
		public void setExampleFamily(String exampleFamily) {
			this.exampleFamily = exampleFamily;
		}
		public String getExampleFamilyContact() {
			return exampleFamilyContact;
		}
		public void setExampleFamilyContact(String exampleFamilyContact) {
			this.exampleFamilyContact = exampleFamilyContact;
		}
		public String getSeedIds() {
			return seedIds;
		}
		public void setSeedIds(String seedIds) {
			this.seedIds = seedIds;
		}
		public String getProductComboInfo() {
			return productComboInfo;
		}
		public void setProductComboInfo(String productComboInfo) {
			this.productComboInfo = productComboInfo;
		}
		public String getExampleScheme() {
			return exampleScheme;
		}
		public void setExampleScheme(String exampleScheme) {
			this.exampleScheme = exampleScheme;
		}
		public String getProcessTracking() {
			return processTracking;
		}
		public void setProcessTracking(String processTracking) {
			this.processTracking = processTracking;
		}
		public String getTestAssess() {
			return testAssess;
		}
		public void setTestAssess(String testAssess) {
			this.testAssess = testAssess;
		}
		public String getSummaryAnalyze() {
			return summaryAnalyze;
		}
		public void setSummaryAnalyze(String summaryAnalyze) {
			this.summaryAnalyze = summaryAnalyze;
		}
		public String getKeyWord() {
			return keyWord;
		}
		public void setKeyWord(String keyWord) {
			this.keyWord = keyWord;
		}
		public String getExampleTypeId() {
			return exampleTypeId;
		}
		public void setExampleTypeId(String exampleTypeId) {
			this.exampleTypeId = exampleTypeId;
		}
		public String getCropName() {
			return cropName;
		}
		public void setCropName(String cropName) {
			this.cropName = cropName;
		}
		public String getExampleTypeName() {
			return exampleTypeName;
		}
		public void setExampleTypeName(String exampleTypeName) {
			this.exampleTypeName = exampleTypeName;
		}
	
	}

}
