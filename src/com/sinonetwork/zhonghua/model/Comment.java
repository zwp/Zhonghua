package com.sinonetwork.zhonghua.model;

import java.io.Serializable;

public class Comment implements Serializable {

	private static final long serialVersionUID = 6784862976839725919L;

	private String id;
	private String replyText;
	private String replyTime;
	private String userName;
	private String keyWord;
	private String questionId;
	private String status;
	private String replyAgree;
	private String replyDisAgree;

	public String getId() {
		return id;
	}

	public String getReplyText() {
		return replyText;
	}

	public String getReplyTime() {
		return replyTime;
	}

	public String getUserName() {
		return userName;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public String getQuestionId() {
		return questionId;
	}

	public String getStatus() {
		return status;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}

	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReplyAgree() {
		return replyAgree;
	}

	public String getReplyDisAgree() {
		return replyDisAgree;
	}

	public void setReplyAgree(String replyAgree) {
		this.replyAgree = replyAgree;
	}

	public void setReplyDisAgree(String replyDisAgree) {
		this.replyDisAgree = replyDisAgree;
	}

}
