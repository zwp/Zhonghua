package com.sinonetwork.zhonghua.model;

import java.util.List;

public class searchAllCropsi extends BaseModel {

	private String curPage;
	private String totalPages;
	private List<searchAllCropssi> resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}


	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}


	public String getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}


	public List<searchAllCropssi> getResultdata() {
		return resultdata;
	}


	public void setResultdata(List<searchAllCropssi> resultdata) {
		this.resultdata = resultdata;
	}




	public class searchAllCropssi{
		private String id;
		private String cropName;
		private String cropPicture;
		private String cropDescription;
		private String cropType;
		private String fatherId;
//		private List<searchAllCropssi> childern;
		private String childern;
		
		
		
		public String getId() {
			return id;
		}



		public void setId(String id) {
			this.id = id;
		}



		public String getCropName() {
			return cropName;
		}



		public void setCropName(String cropName) {
			this.cropName = cropName;
		}



		public String getCropPicture() {
			return cropPicture;
		}



		public void setCropPicture(String cropPicture) {
			this.cropPicture = cropPicture;
		}



		public String getCropDescription() {
			return cropDescription;
		}



		public void setCropDescription(String cropDescription) {
			this.cropDescription = cropDescription;
		}



		public String getCropType() {
			return cropType;
		}



		public void setCropType(String cropType) {
			this.cropType = cropType;
		}



		public String getFatherId() {
			return fatherId;
		}



		public void setFatherId(String fatherId) {
			this.fatherId = fatherId;
		}



		public String getChildern() {
			return childern;
		}



		public void setChildern(String childern) {
			this.childern = childern;
		}






		
	}
	
}
