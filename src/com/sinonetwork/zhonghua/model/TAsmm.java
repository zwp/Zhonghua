package com.sinonetwork.zhonghua.model;

import java.util.List;

public class TAsmm extends BaseModel {

	private String curPage;
	private String totalPages;
	private List<TAsmms> resultdata;

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public List<TAsmms> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<TAsmms> resultdata) {
		this.resultdata = resultdata;
	}

	public class TAsmms {
		private String id;
		private String areaid;
		private String nameen;
		private String namecn;
		private String districten;
		private String districtcn;
		private String proven;
		private String provcn;
		private String nationen;
		private String nationcn;
		private String longitude;
		private String latitude;
		private String elevation;
		private String seeYear;
		private String seeMonth;
		private String seeDay;
		private String seeHour;
		private String seeMinute;
		private String seeSecond;
		private String HS12;
		private String HS45;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getAreaid() {
			return areaid;
		}

		public void setAreaid(String areaid) {
			this.areaid = areaid;
		}

		public String getNameen() {
			return nameen;
		}

		public void setNameen(String nameen) {
			this.nameen = nameen;
		}

		public String getNamecn() {
			return namecn;
		}

		public void setNamecn(String namecn) {
			this.namecn = namecn;
		}

		public String getDistricten() {
			return districten;
		}

		public void setDistricten(String districten) {
			this.districten = districten;
		}

		public String getDistrictcn() {
			return districtcn;
		}

		public void setDistrictcn(String districtcn) {
			this.districtcn = districtcn;
		}

		public String getProven() {
			return proven;
		}

		public void setProven(String proven) {
			this.proven = proven;
		}

		public String getProvcn() {
			return provcn;
		}

		public void setProvcn(String provcn) {
			this.provcn = provcn;
		}

		public String getNationen() {
			return nationen;
		}

		public void setNationen(String nationen) {
			this.nationen = nationen;
		}

		public String getNationcn() {
			return nationcn;
		}

		public void setNationcn(String nationcn) {
			this.nationcn = nationcn;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getElevation() {
			return elevation;
		}

		public void setElevation(String elevation) {
			this.elevation = elevation;
		}

		public String getSeeYear() {
			return seeYear;
		}

		public void setSeeYear(String seeYear) {
			this.seeYear = seeYear;
		}

		public String getSeeMonth() {
			return seeMonth;
		}

		public void setSeeMonth(String seeMonth) {
			this.seeMonth = seeMonth;
		}

		public String getSeeDay() {
			return seeDay;
		}

		public void setSeeDay(String seeDay) {
			this.seeDay = seeDay;
		}

		public String getSeeHour() {
			return seeHour;
		}

		public void setSeeHour(String seeHour) {
			this.seeHour = seeHour;
		}

		public String getSeeMinute() {
			return seeMinute;
		}

		public void setSeeMinute(String seeMinute) {
			this.seeMinute = seeMinute;
		}

		public String getSeeSecond() {
			return seeSecond;
		}

		public void setSeeSecond(String seeSecond) {
			this.seeSecond = seeSecond;
		}

		public String getHS12() {
			return HS12;
		}

		public void setHS12(String hS12) {
			HS12 = hS12;
		}

		public String getHS45() {
			return HS45;
		}

		public void setHS45(String hS45) {
			HS45 = hS45;
		}

	}

}
