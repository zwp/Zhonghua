package com.sinonetwork.zhonghua.model;

import java.io.Serializable;
/**
 * 知道界面的条目
 * @author enway
 *
 */
public class Question implements Serializable {

	private static final long serialVersionUID = 7542932736419777577L;

	private int clickNum;//点击次数
	private String clickNumUsers;
	private int id;
	private String pictures;//逗号隔开的图片名,图片路径:根目录/ picture/图片名，/ picture/是写死的
	private String publishTime;//发布时间
	private String questionText;//问题内容
	private String questionTitle;//问题标题
	private String region;
	private String replies;
	private String status;//状态0-	未审核 2-已通过
	private String userName1;//发布人
	private String userName2;//被咨询专家
	private String totalReply;

	public int getClickNum() {
		return clickNum;
	}

	public int getId() {
		return id;
	}

	public String getPictures() {
		return pictures;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public String getQuestionText() {
		return questionText;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public String getRegion() {
		return region;
	}

	public String getReplies() {
		return replies;
	}

	public String getStatus() {
		return status;
	}

	public String getUserName1() {
		return userName1;
	}

	public String getUserName2() {
		return userName2;
	}

	public void setClickNum(int clickNum) {
		this.clickNum = clickNum;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setReplies(String replies) {
		this.replies = replies;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUserName1(String userName1) {
		this.userName1 = userName1;
	}

	public void setUserName2(String userName2) {
		this.userName2 = userName2;
	}

	public String getClickNumUsers() {
		return clickNumUsers;
	}

	public void setClickNumUsers(String clickNumUsers) {
		this.clickNumUsers = clickNumUsers;
	}

	public String getTotalReply() {
		return totalReply;
	}

	public void setTotalReply(String totalReply) {
		this.totalReply = totalReply;
	}
	
	
	

}
