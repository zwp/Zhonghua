package com.sinonetwork.zhonghua.model;

import java.util.List;

public class ShengFenModel extends BaseModel {

	private String timestamp;
	private List<ShengFenModels> resultdata;
	
	
	
	public String getTimestamp() {
		return timestamp;
	}



	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}





	public List<ShengFenModels> getResultdata() {
		return resultdata;
	}



	public void setResultdata(List<ShengFenModels> resultdata) {
		this.resultdata = resultdata;
	}





	public class ShengFenModels{
		private String PARENT_ID;
		private String SEQUENCE;
		private String LEVEL;
		private String AREANAME;
		private String COMMON;
		private String ID;
		private String DELETESTATUS;
		public String getPARENT_ID() {
			return PARENT_ID;
		}
		public void setPARENT_ID(String pARENT_ID) {
			PARENT_ID = pARENT_ID;
		}
		public String getSEQUENCE() {
			return SEQUENCE;
		}
		public void setSEQUENCE(String sEQUENCE) {
			SEQUENCE = sEQUENCE;
		}
		public String getLEVEL() {
			return LEVEL;
		}
		public void setLEVEL(String lEVEL) {
			LEVEL = lEVEL;
		}
		public String getAREANAME() {
			return AREANAME;
		}
		public void setAREANAME(String aREANAME) {
			AREANAME = aREANAME;
		}
		public String getCOMMON() {
			return COMMON;
		}
		public void setCOMMON(String cOMMON) {
			COMMON = cOMMON;
		}
		public String getID() {
			return ID;
		}
		public void setID(String iD) {
			ID = iD;
		}
		public String getDELETESTATUS() {
			return DELETESTATUS;
		}
		public void setDELETESTATUS(String dELETESTATUS) {
			DELETESTATUS = dELETESTATUS;
		}
		
		
	}
	
}
