package com.sinonetwork.zhonghua.model;


public class farmingGuidInfoById extends BaseModel {

	private String curPage;
	private String totalPages;
	
	private farmingGuidInfoByIds resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}


	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}


	public String getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}


	public farmingGuidInfoByIds getResultdata() {
		return resultdata;
	}


	public void setResultdata(farmingGuidInfoByIds resultdata) {
		this.resultdata = resultdata;
	}


	public class farmingGuidInfoByIds{
		private String id;
		private String area;
		private String cropId;
		private String breed;
		private String targetYield;
		private String yieldComponent;
		private String seedTypes;
		private String explain;
		private String name;
		private String cropName;
		
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getArea() {
			return area;
		}
		public void setArea(String area) {
			this.area = area;
		}
		public String getCropId() {
			return cropId;
		}
		public void setCropId(String cropId) {
			this.cropId = cropId;
		}
		public String getBreed() {
			return breed;
		}
		public void setBreed(String breed) {
			this.breed = breed;
		}
		public String getTargetYield() {
			return targetYield;
		}
		public void setTargetYield(String targetYield) {
			this.targetYield = targetYield;
		}
		public String getYieldComponent() {
			return yieldComponent;
		}
		public void setYieldComponent(String yieldComponent) {
			this.yieldComponent = yieldComponent;
		}
		public String getSeedTypes() {
			return seedTypes;
		}
		public void setSeedTypes(String seedTypes) {
			this.seedTypes = seedTypes;
		}
		public String getExplain() {
			return explain;
		}
		public void setExplain(String explain) {
			this.explain = explain;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		public String getCropName() {
			return cropName;
		}
		public void setCropName(String cropName) {
			this.cropName = cropName;
		}
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "Product [id=" + id + ", area=" + area
					+ ", cropId=" + cropId + ", breed=" + breed
					+ ", targetYield=" + targetYield + ", yieldComponent=" + yieldComponent
					+ ", seedTypes=" + seedTypes + ", explain=" + explain+"name="+name + "]";
		}
	}
	
}
