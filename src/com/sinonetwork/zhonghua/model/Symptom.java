package com.sinonetwork.zhonghua.model;

import java.util.ArrayList;

public class Symptom {

	private int id;
	private String symptomsTypeName;
	private ArrayList<SubSymptom> subSymptomList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSymptomsTypeName() {
		return symptomsTypeName;
	}

	public void setSymptomsTypeName(String symptomsTypeName) {
		this.symptomsTypeName = symptomsTypeName;
	}

	public ArrayList<SubSymptom> getSubSymptomList() {
		return subSymptomList;
	}

	public void setSubSymptomList(ArrayList<SubSymptom> subSymptomList) {
		this.subSymptomList = subSymptomList;
	}
}
