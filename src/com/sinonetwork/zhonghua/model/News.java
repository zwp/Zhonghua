package com.sinonetwork.zhonghua.model;

import java.io.Serializable;

public class News implements Serializable{
	private String id;
	private String title;//标题
	private String docContent;//详情
	private String time;//时间
	private String infoTypeId;//信息的类型
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDocContent() {
		return docContent;
	}
	public void setDocContent(String docContent) {
		this.docContent = docContent;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getInfoTypeId() {
		return infoTypeId;
	}
	public void setInfoTypeId(String infoTypeId) {
		this.infoTypeId = infoTypeId;
	}
}
