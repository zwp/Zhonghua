package com.sinonetwork.zhonghua.model;

public class TService extends BaseModel {

	private String curPage;
	private String totalPages;
	private TServices resultdata;
	
	
	public String getCurPage() {
		return curPage;
	}


	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}


	public String getTotalPages() {
		return totalPages;
	}


	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}


	public TServices getResultdata() {
		return resultdata;
	}


	public void setResultdata(TServices resultdata) {
		this.resultdata = resultdata;
	}


	public class TServices{
		private String id;
		private String areaid;
		private String nameen;
		private String namecn;
		private String districten;
		private String districtcn;
		private String proven;
		private String provcn;
		private String nationen;
		private String nationcn;
		private String longitude;
		private String latitude;
		private String seeTime;
		private String temperature;
		private String windDirection;
		private String windPower;
		private String humidity;
		private String rainFall;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getAreaid() {
			return areaid;
		}
		public void setAreaid(String areaid) {
			this.areaid = areaid;
		}
		public String getNameen() {
			return nameen;
		}
		public void setNameen(String nameen) {
			this.nameen = nameen;
		}
		public String getNamecn() {
			return namecn;
		}
		public void setNamecn(String namecn) {
			this.namecn = namecn;
		}
		public String getDistricten() {
			return districten;
		}
		public void setDistricten(String districten) {
			this.districten = districten;
		}
		public String getDistrictcn() {
			return districtcn;
		}
		public void setDistrictcn(String districtcn) {
			this.districtcn = districtcn;
		}
		public String getProven() {
			return proven;
		}
		public void setProven(String proven) {
			this.proven = proven;
		}
		public String getProvcn() {
			return provcn;
		}
		public void setProvcn(String provcn) {
			this.provcn = provcn;
		}
		public String getNationen() {
			return nationen;
		}
		public void setNationen(String nationen) {
			this.nationen = nationen;
		}
		public String getNationcn() {
			return nationcn;
		}
		public void setNationcn(String nationcn) {
			this.nationcn = nationcn;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getSeeTime() {
			return seeTime;
		}
		public void setSeeTime(String seeTime) {
			this.seeTime = seeTime;
		}
		public String getTemperature() {
			return temperature;
		}
		public void setTemperature(String temperature) {
			this.temperature = temperature;
		}
		public String getWindDirection() {
			return windDirection;
		}
		public void setWindDirection(String windDirection) {
			this.windDirection = windDirection;
		}
		public String getWindPower() {
			return windPower;
		}
		public void setWindPower(String windPower) {
			this.windPower = windPower;
		}
		public String getHumidity() {
			return humidity;
		}
		public void setHumidity(String humidity) {
			this.humidity = humidity;
		}
		public String getRainFall() {
			return rainFall;
		}
		public void setRainFall(String rainFall) {
			this.rainFall = rainFall;
		}
		
		
	}
	
}
