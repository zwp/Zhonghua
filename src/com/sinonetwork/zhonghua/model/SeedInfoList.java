package com.sinonetwork.zhonghua.model;

import java.util.List;

public class SeedInfoList extends BaseModel {

	private String curPage;
	private String totalPages;
	private List<SeedInfoLists> resultdata;

	public List<SeedInfoLists> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<SeedInfoLists> resultdata) {
		this.resultdata = resultdata;
	}

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public static class SeedInfoLists {
		private String id;
		private String seedName;
		private String seedType;
		private String seedPicture;
		private String seedTrait;

		private String yieldShow;
		private String techPoint;
		private String regionId;
		private String cropId;
		private String judgeCode;
		private String breedUnit;
		private String seedSource;
		private String judgeIdea;
		private String cropName;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSeedName() {
			return seedName;
		}

		public void setSeedName(String seedName) {
			this.seedName = seedName;
		}

		public String getSeedType() {
			return seedType;
		}

		public void setSeedType(String seedType) {
			this.seedType = seedType;
		}

		public String getSeedPicture() {
			return seedPicture;
		}

		public void setSeedPicture(String seedPicture) {
			this.seedPicture = seedPicture;
		}

		public String getSeedTrait() {
			return seedTrait;
		}

		public void setSeedTrait(String seedTrait) {
			this.seedTrait = seedTrait;
		}

		public String getYieldShow() {
			return yieldShow;
		}

		public void setYieldShow(String yieldShow) {
			this.yieldShow = yieldShow;
		}

		public String getTechPoint() {
			return techPoint;
		}

		public void setTechPoint(String techPoint) {
			this.techPoint = techPoint;
		}

		public String getRegionId() {
			return regionId;
		}

		public void setRegionId(String regionId) {
			this.regionId = regionId;
		}

		public String getCropId() {
			return cropId;
		}

		public void setCropId(String cropId) {
			this.cropId = cropId;
		}

		public String getJudgeCode() {
			return judgeCode;
		}

		public void setJudgeCode(String judgeCode) {
			this.judgeCode = judgeCode;
		}

		public String getBreedUnit() {
			return breedUnit;
		}

		public void setBreedUnit(String breedUnit) {
			this.breedUnit = breedUnit;
		}

		public String getSeedSource() {
			return seedSource;
		}

		public void setSeedSource(String seedSource) {
			this.seedSource = seedSource;
		}

		public String getJudgeIdea() {
			return judgeIdea;
		}

		public void setJudgeIdea(String judgeIdea) {
			this.judgeIdea = judgeIdea;
		}

		public String getCropName() {
			return cropName;
		}

		public void setCropName(String cropName) {
			this.cropName = cropName;
		}

	}

}
