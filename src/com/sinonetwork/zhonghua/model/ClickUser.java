package com.sinonetwork.zhonghua.model;

import java.io.Serializable;

public class ClickUser implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String questionId;
	private String userName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
