package com.sinonetwork.zhonghua.model;

import java.util.List;

public class farmingGuidInfoByIdB extends BaseModel {

	
	private List<farmingGuidInfoByIdBs> resultdata;
	
	


	public List<farmingGuidInfoByIdBs> getResultdata() {
		return resultdata;
	}




	public void setResultdata(List<farmingGuidInfoByIdBs> resultdata) {
		this.resultdata = resultdata;
	}




	public class farmingGuidInfoByIdBs{
		private String id;
		private String startMonth;
		private String endMonth;
		private String context;
		private String relevancyId;
		private String keyWord;
		private String type;
		private String startMonthName;
		private String endMonthName;
		private String typeName;
		private String cuoshiContent;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getStartMonth() {
			return startMonth;
		}
		public void setStartMonth(String startMonth) {
			this.startMonth = startMonth;
		}
		public String getEndMonth() {
			return endMonth;
		}
		public void setEndMonth(String endMonth) {
			this.endMonth = endMonth;
		}
		public String getContext() {
			return context;
		}
		public void setContext(String context) {
			this.context = context;
		}
		public String getRelevancyId() {
			return relevancyId;
		}
		public void setRelevancyId(String relevancyId) {
			this.relevancyId = relevancyId;
		}
		public String getKeyWord() {
			return keyWord;
		}
		public void setKeyWord(String keyWord) {
			this.keyWord = keyWord;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getStartMonthName() {
			return startMonthName;
		}
		public void setStartMonthName(String startMonthName) {
			this.startMonthName = startMonthName;
		}
		public String getEndMonthName() {
			return endMonthName;
		}
		public void setEndMonthName(String endMonthName) {
			this.endMonthName = endMonthName;
		}
		public String getTypeName() {
			return typeName;
		}
		public void setTypeName(String typeName) {
			this.typeName = typeName;
		}
		public String getCuoshiContent() {
			return cuoshiContent;
		}
		public void setCuoshiContent(String cuoshiContent) {
			this.cuoshiContent = cuoshiContent;
		}
		
	}
	
}
