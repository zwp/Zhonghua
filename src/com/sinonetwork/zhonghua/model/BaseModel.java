package com.sinonetwork.zhonghua.model;

import java.io.Serializable;

public class BaseModel implements Serializable {
	private String resultcode;

	private String resultdesc;

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getResultdesc() {
		return resultdesc;
	}

	public void setResultdesc(String resultdesc) {
		this.resultdesc = resultdesc;
	}

}
