package com.sinonetwork.zhonghua.model;

public class TestSoilInfoListByCoordinates extends BaseModel {
	private TestSoilInfoListByCoordinates resultdata;

	public TestSoilInfoListByCoordinates getResultdata() {
		return resultdata;
	}

	public void setResultdata(TestSoilInfoListByCoordinates resultdata) {
		this.resultdata = resultdata;
	}

	public class TestSoilInfoListByCoordinatess {
		private String id;
		private String orderCode;
		private String company;
		private String sendPerson;
		private String labNo;
		private String samplesNo;
		private String sampleDepth;
		private String regionId;
		private String area;
		private String longitude;
		private String latitude;
		private String agrotype;
		private String character;
		private String cropId1;
		private String targetYield;
		private String irrCondition;
		private String cropId2;
		private String yieldLev;
		private String keyWord;
		private String ph;
		private String aa;
		private String om;
		private String nh4n;
		private String no3n;
		private String phosp;
		private String kalium;
		private String calcium;
		private String magnesium;
		private String sulfur;
		private String ferrum;
		private String copper;
		private String manganese;
		private String zinc;
		private String boron;
		private String uniqueId;
		private String province;
		private String city;
		private String village;
		private String burg;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getOrderCode() {
			return orderCode;
		}

		public void setOrderCode(String orderCode) {
			this.orderCode = orderCode;
		}

		public String getCompany() {
			return company;
		}

		public void setCompany(String company) {
			this.company = company;
		}

		public String getSendPerson() {
			return sendPerson;
		}

		public void setSendPerson(String sendPerson) {
			this.sendPerson = sendPerson;
		}

		public String getLabNo() {
			return labNo;
		}

		public void setLabNo(String labNo) {
			this.labNo = labNo;
		}

		public String getSamplesNo() {
			return samplesNo;
		}

		public void setSamplesNo(String samplesNo) {
			this.samplesNo = samplesNo;
		}

		public String getSampleDepth() {
			return sampleDepth;
		}

		public void setSampleDepth(String sampleDepth) {
			this.sampleDepth = sampleDepth;
		}

		public String getRegionId() {
			return regionId;
		}

		public void setRegionId(String regionId) {
			this.regionId = regionId;
		}

		public String getArea() {
			return area;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getLongitude() {
			return longitude;
		}

		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}

		public String getLatitude() {
			return latitude;
		}

		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}

		public String getAgrotype() {
			return agrotype;
		}

		public void setAgrotype(String agrotype) {
			this.agrotype = agrotype;
		}

		public String getCharacter() {
			return character;
		}

		public void setCharacter(String character) {
			this.character = character;
		}

		public String getCropId1() {
			return cropId1;
		}

		public void setCropId1(String cropId1) {
			this.cropId1 = cropId1;
		}

		public String getTargetYield() {
			return targetYield;
		}

		public void setTargetYield(String targetYield) {
			this.targetYield = targetYield;
		}

		public String getIrrCondition() {
			return irrCondition;
		}

		public void setIrrCondition(String irrCondition) {
			this.irrCondition = irrCondition;
		}

		public String getCropId2() {
			return cropId2;
		}

		public void setCropId2(String cropId2) {
			this.cropId2 = cropId2;
		}

		public String getYieldLev() {
			return yieldLev;
		}

		public void setYieldLev(String yieldLev) {
			this.yieldLev = yieldLev;
		}

		public String getKeyWord() {
			return keyWord;
		}

		public void setKeyWord(String keyWord) {
			this.keyWord = keyWord;
		}

		public String getPh() {
			return ph;
		}

		public void setPh(String ph) {
			this.ph = ph;
		}

		public String getAa() {
			return aa;
		}

		public void setAa(String aa) {
			this.aa = aa;
		}

		public String getOm() {
			return om;
		}

		public void setOm(String om) {
			this.om = om;
		}

		public String getNh4n() {
			return nh4n;
		}

		public void setNh4n(String nh4n) {
			this.nh4n = nh4n;
		}

		public String getNo3n() {
			return no3n;
		}

		public void setNo3n(String no3n) {
			this.no3n = no3n;
		}

		public String getPhosp() {
			return phosp;
		}

		public void setPhosp(String phosp) {
			this.phosp = phosp;
		}

		public String getKalium() {
			return kalium;
		}

		public void setKalium(String kalium) {
			this.kalium = kalium;
		}

		public String getCalcium() {
			return calcium;
		}

		public void setCalcium(String calcium) {
			this.calcium = calcium;
		}

		public String getMagnesium() {
			return magnesium;
		}

		public void setMagnesium(String magnesium) {
			this.magnesium = magnesium;
		}

		public String getSulfur() {
			return sulfur;
		}

		public void setSulfur(String sulfur) {
			this.sulfur = sulfur;
		}

		public String getFerrum() {
			return ferrum;
		}

		public void setFerrum(String ferrum) {
			this.ferrum = ferrum;
		}

		public String getCopper() {
			return copper;
		}

		public void setCopper(String copper) {
			this.copper = copper;
		}

		public String getManganese() {
			return manganese;
		}

		public void setManganese(String manganese) {
			this.manganese = manganese;
		}

		public String getZinc() {
			return zinc;
		}

		public void setZinc(String zinc) {
			this.zinc = zinc;
		}

		public String getBoron() {
			return boron;
		}

		public void setBoron(String boron) {
			this.boron = boron;
		}

		public String getUniqueId() {
			return uniqueId;
		}

		public void setUniqueId(String uniqueId) {
			this.uniqueId = uniqueId;
		}

		public String getProvince() {
			return province;
		}

		public void setProvince(String province) {
			this.province = province;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getVillage() {
			return village;
		}

		public void setVillage(String village) {
			this.village = village;
		}

		public String getBurg() {
			return burg;
		}

		public void setBurg(String burg) {
			this.burg = burg;
		}

	}
}
