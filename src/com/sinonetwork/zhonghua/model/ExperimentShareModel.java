package com.sinonetwork.zhonghua.model;

import java.util.List;

public class ExperimentShareModel extends BaseModel {

	private String curPage;
	private String totalPages;

	private List<ExperimentShareResultdata> resultdata;

	public String getCurPage() {
		return curPage;
	}

	public void setCurPage(String curPage) {
		this.curPage = curPage;
	}

	public String getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(String totalPages) {
		this.totalPages = totalPages;
	}

	public List<ExperimentShareResultdata> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<ExperimentShareResultdata> resultdata) {
		this.resultdata = resultdata;
	}

	public class ExperimentShareResultdata {
		private int id;
		private int exampleType;
		private String principal;
		private String principalContact;
		private String exampleFamily;
		private String exampleFamilyContact;
		private String seedIds;
		private String productComboInfo;
		private String exampleScheme;
		private String processTracking;
		private String testAssess;
		private String summaryAnalyze;
		private String keyWord;
		private String exampleTypeId;
		private String pictures;
		private String cropName;
		private String exampleTypeName;
		private List<ExperimentShareFiles> files;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getExampleType() {
			return exampleType;
		}

		public void setExampleType(int exampleType) {
			this.exampleType = exampleType;
		}

		public String getPrincipal() {
			return principal;
		}

		public void setPrincipal(String principal) {
			this.principal = principal;
		}

		public String getPrincipalContact() {
			return principalContact;
		}

		public void setPrincipalContact(String principalContact) {
			this.principalContact = principalContact;
		}

		public String getExampleFamily() {
			return exampleFamily;
		}

		public void setExampleFamily(String exampleFamily) {
			this.exampleFamily = exampleFamily;
		}

		public String getExampleFamilyContact() {
			return exampleFamilyContact;
		}

		public void setExampleFamilyContact(String exampleFamilyContact) {
			this.exampleFamilyContact = exampleFamilyContact;
		}

		public String getSeedIds() {
			return seedIds;
		}

		public void setSeedIds(String seedIds) {
			this.seedIds = seedIds;
		}

		public String getProductComboInfo() {
			return productComboInfo;
		}

		public void setProductComboInfo(String productComboInfo) {
			this.productComboInfo = productComboInfo;
		}

		public String getExampleScheme() {
			return exampleScheme;
		}

		public void setExampleScheme(String exampleScheme) {
			this.exampleScheme = exampleScheme;
		}

		public String getProcessTracking() {
			return processTracking;
		}

		public void setProcessTracking(String processTracking) {
			this.processTracking = processTracking;
		}

		public String getTestAssess() {
			return testAssess;
		}

		public void setTestAssess(String testAssess) {
			this.testAssess = testAssess;
		}

		public String getSummaryAnalyze() {
			return summaryAnalyze;
		}

		public void setSummaryAnalyze(String summaryAnalyze) {
			this.summaryAnalyze = summaryAnalyze;
		}

		public String getKeyWord() {
			return keyWord;
		}

		public void setKeyWord(String keyWord) {
			this.keyWord = keyWord;
		}

		public String getExampleTypeId() {
			return exampleTypeId;
		}

		public void setExampleTypeId(String exampleTypeId) {
			this.exampleTypeId = exampleTypeId;
		}

		public String getPictures() {
			return pictures;
		}

		public void setPictures(String pictures) {
			this.pictures = pictures;
		}

		public String getCropName() {
			return cropName;
		}

		public void setCropName(String cropName) {
			this.cropName = cropName;
		}

		public String getExampleTypeName() {
			return exampleTypeName;
		}

		public void setExampleTypeName(String exampleTypeName) {
			this.exampleTypeName = exampleTypeName;
		}

		public List<ExperimentShareFiles> getFiles() {
			return files;
		}

		public void setFiles(List<ExperimentShareFiles> files) {
			this.files = files;
		}

		public class ExperimentShareFiles {
			private int id;
			private int exampleId;
			private String filepath;
			private String rname;
			private String contextType;
			private String uptime;

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public int getExampleId() {
				return exampleId;
			}

			public void setExampleId(int exampleId) {
				this.exampleId = exampleId;
			}

			public String getFilepath() {
				return filepath;
			}

			public void setFilepath(String filepath) {
				this.filepath = filepath;
			}

			public String getRname() {
				return rname;
			}

			public void setRname(String rname) {
				this.rname = rname;
			}

			public String getContextType() {
				return contextType;
			}

			public void setContextType(String contextType) {
				this.contextType = contextType;
			}

			public String getUptime() {
				return uptime;
			}

			public void setUptime(String uptime) {
				this.uptime = uptime;
			}

		}

	}
}
