package com.sinonetwork.zhonghua;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.sinonetwork.zhonghua.model.Warning;

public class WarningDetailActivity extends LandBaseActivity {

	private WebView webView;
	protected Warning detail;
	private TextView title;
	private TextView description;
	public static final String NAME = "NAME";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_warning_detail);
		setBackBtn();
		setTopTitleTV("灾害预警详情");
		Intent intent = this.getIntent();
		detail = (Warning) intent.getSerializableExtra("data");
		
		title = (TextView) findViewById(R.id.title);
		description = (TextView) findViewById(R.id.description);
		title.setText(detail.getTitle());
		String str = detail.getDescription();
		str = str.replace("&nbsp;", "");
		description.setText(str);

	}
}
