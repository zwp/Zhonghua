package com.sinonetwork.zhonghua;

import java.util.List;

public class JiWenModel {
	private String resultcode;
	private String resultdesc;
	private Integer curPage;
	private Integer totalPages;
	private List<JiWenDataModel> resultdata;

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getResultdesc() {
		return resultdesc;
	}

	public void setResultdesc(String resultdesc) {
		this.resultdesc = resultdesc;
	}

	public Integer getCurPage() {
		return curPage;
	}

	public void setCurPage(Integer curPage) {
		this.curPage = curPage;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public List<JiWenDataModel> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<JiWenDataModel> resultdata) {
		this.resultdata = resultdata;
	}

	@Override
	public String toString() {
		return "JiWenModel [resultcode=" + resultcode + ", resultdesc=" + resultdesc + ", curPage=" + curPage
				+ ", totalPages=" + totalPages + ", resultdata=" + resultdata + "]";
	}

}
