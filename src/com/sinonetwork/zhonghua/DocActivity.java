package com.sinonetwork.zhonghua;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.sinonetwork.zhonghua.adapter.DocListAdapter;
import com.sinonetwork.zhonghua.model.Doc;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.URLAddress;
//优秀试验报告
public class DocActivity extends LandBaseActivity {
	private ListView listView;
	protected ArrayList<Doc> list;
	private DocListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doc);
		setBackBtn();
		setTopTitleTV("优秀实验报告分享");
		listView = (ListView) findViewById(R.id.list);
		loadData();
	}
	//加载数据
	private void loadData() {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					list = LandInfoParser.getDocList();

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

							setView();
						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}
	//显示数据
	private void setView() {

		mAdapter = new DocListAdapter(this);
		mAdapter.setData(list);
		listView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String url = URLAddress.URLFILE + list.get(arg2).getUptime()
						+ "/" + list.get(arg2).getFilepath();

				openData(url,list.get(arg2).getContextType());
			}
		});
	}

	private void openData(final String urlString, final String fileType) {
		showLoadProgressBar();
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

					open(urlString,fileType);

					success = true;
				} catch (Exception e) {
					success = false;
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {
						if (success) {

						} else {
							showShortToast("获取数据失败");
						}
						hideLoadProgressBar();

					}

				});
			}
		}).start();
	}
	
	public void open(final String urlString, final String fileType) {
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("GET");
			// 实现连接
			connection.connect();

			System.out.println("connection.getResponseCode()="
					+ connection.getResponseCode());
			if (connection.getResponseCode() == 200) {
				InputStream is = connection.getInputStream();
				// 以下为下载操作
				byte[] arr = new byte[1];
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				BufferedOutputStream bos = new BufferedOutputStream(baos);
				int n = is.read(arr);
				while (n > 0) {
					bos.write(arr);
					n = is.read(arr);
				}
				bos.close();
				String path = Environment.getExternalStorageDirectory()
						+ "/download/";
				String[] name = urlString.split("/");
				path = path + name[name.length - 1];
				System.out.println("name=" + name);
				System.out.println("path=" + path);
				File file = new File(path);
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(baos.toByteArray());
				fos.close();
				// 关闭网络连接
				connection.disconnect();
				System.out.println("下载完成");
				if (file.exists()) {
					System.out.println("打开");
					Uri path1 = Uri.fromFile(file);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					if (fileType.equals("image")) {
						intent.setDataAndType(path1, "image/*");
					} else {

						intent.setDataAndType(path1, "application/" + getMIMEType(fileType));
					}
					// intent.setDataAndType(path1, "application/vnd.ms-excel");
					// intent.setDataAndType(path1, "application/pdf");
					// intent.setDataAndType(path1, "application/msword");
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					try {
						startActivity(intent);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(DocActivity.this, "文件类型异常，打开失败。", 2000)
								.show();
						System.out.println("打开失败");
					}
				}
			} else {
				Toast.makeText(DocActivity.this, "文件地址异常，打开失败。", 2000).show();
			}
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

	}

	/**
	 * 根据文件后缀名获得对应的MIME类型。
	 * 
	 * @param file
	 */
	private String getMIMEType(String file) {

		String type = "."+file;
//		String fName = file.getName();
//		// 获取后缀名前的分隔符"."在fName中的位置。
//		int dotIndex = fName.lastIndexOf(".");
//		if (dotIndex < 0) {
//			return type;
//		}
//		/* 获取文件的后缀名 */
//		String end = fName.substring(dotIndex, fName.length()).toLowerCase();
//		if (end == "")
//			return type;
		// 在MIME和文件类型的匹配表中找到对应的MIME类型。
		for (int i = 0; i < MIME_MapTable.length; i++) { //
			if (type.equals(MIME_MapTable[i][0]))
				type = MIME_MapTable[i][1];
		}
		return type;
	}

	private final String[][] MIME_MapTable = {
			// {后缀名，MIME类型}
			{ ".3gp", "video/3gpp" },
			{ ".apk", "application/vnd.android.package-archive" },
			{ ".asf", "video/x-ms-asf" },
			{ ".avi", "video/x-msvideo" },
			{ ".bin", "application/octet-stream" },
			{ ".bmp", "image/bmp" },
			{ ".c", "text/plain" },
			{ ".class", "application/octet-stream" },
			{ ".conf", "text/plain" },
			{ ".cpp", "text/plain" },
			{ ".doc", "application/msword" },
			{ ".docx",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
			{ ".xls", "application/vnd.ms-excel" },
			{ ".xlsx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
			{ ".exe", "application/octet-stream" },
			{ ".gif", "image/gif" },
			{ ".gtar", "application/x-gtar" },
			{ ".gz", "application/x-gzip" },
			{ ".h", "text/plain" },
			{ ".htm", "text/html" },
			{ ".html", "text/html" },
			{ ".jar", "application/java-archive" },
			{ ".java", "text/plain" },
			{ ".jpeg", "image/jpeg" },
			{ ".jpg", "image/jpeg" },
			{ ".js", "application/x-javascript" },
			{ ".log", "text/plain" },
			{ ".m3u", "audio/x-mpegurl" },
			{ ".m4a", "audio/mp4a-latm" },
			{ ".m4b", "audio/mp4a-latm" },
			{ ".m4p", "audio/mp4a-latm" },
			{ ".m4u", "video/vnd.mpegurl" },
			{ ".m4v", "video/x-m4v" },
			{ ".mov", "video/quicktime" },
			{ ".mp2", "audio/x-mpeg" },
			{ ".mp3", "audio/x-mpeg" },
			{ ".mp4", "video/mp4" },
			{ ".mpc", "application/vnd.mpohun.certificate" },
			{ ".mpe", "video/mpeg" },
			{ ".mpeg", "video/mpeg" },
			{ ".mpg", "video/mpeg" },
			{ ".mpg4", "video/mp4" },
			{ ".mpga", "audio/mpeg" },
			{ ".msg", "application/vnd.ms-outlook" },
			{ ".ogg", "audio/ogg" },
			{ ".pdf", "application/pdf" },
			{ ".png", "image/png" },
			{ ".pps", "application/vnd.ms-powerpoint" },
			{ ".ppt", "application/vnd.ms-powerpoint" },
			{ ".pptx",
					"application/vnd.openxmlformats-officedocument.presentationml.presentation" },
			{ ".prop", "text/plain" }, { ".rc", "text/plain" },
			{ ".rmvb", "audio/x-pn-realaudio" }, { ".rtf", "application/rtf" },
			{ ".sh", "text/plain" }, { ".tar", "application/x-tar" },
			{ ".tgz", "application/x-compressed" }, { ".txt", "text/plain" },
			{ ".wav", "audio/x-wav" }, { ".wma", "audio/x-ms-wma" },
			{ ".wmv", "audio/x-ms-wmv" },
			{ ".wps", "application/vnd.ms-works" }, { ".xml", "text/plain" },
			{ ".z", "application/x-compress" },
			{ ".zip", "application/x-zip-compressed" }, { "", "*/*" } };
}
