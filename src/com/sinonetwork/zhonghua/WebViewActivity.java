package com.sinonetwork.zhonghua;

import org.json.JSONException;
import org.json.JSONObject;

import com.sinonetwork.zhonghua.model.ZHAccount;
import com.sinonetwork.zhonghua.parser.LandInfoParser;
import com.sinonetwork.zhonghua.utils.PrefUtil;
import com.sinonetwork.zhonghua.utils.ZhAccountPrefUtil;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

public class WebViewActivity extends Activity {

	private WebView webView;
	private ImageView back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);

		webView = (WebView) findViewById(R.id.webview);
		back = (ImageView) findViewById(R.id.back);
		
		webView.getSettings().setDefaultTextEncodingName("UTF-8");
		webView.getSettings().setJavaScriptEnabled(true);
		// 这样就可以使用window.javatojs来调用它的方法
		webView.addJavascriptInterface(this, "webView");
		// 如果页面中链接，如果希望点击链接继续在当前browser中响应，
		// 而不是新开Android的系统browser中响应该链接，必须覆盖webview的WebViewClient对象
		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 重写此方法表明点击网页里面的链接还是在当前的webview里跳转，不跳到浏览器那边
				view.loadUrl(url);
				return true;
			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		String url = getIntent().getStringExtra("url");
		webView.loadUrl(url);
	}

	// 登录成功后js调用的方法如果target 大于等于API 17，则需要加上如下注解
		@JavascriptInterface
		public void GoHome() {

//			Log.v("zhong", "GoHome调用成功--");
			finish();

		}

		// 登录成功后js调用的方法如果target 大于等于API 17，则需要加上如下注解
		@JavascriptInterface
		public void Login(String data) throws Exception {

//			Log.v("zhong", "login调用成功--" + data);
			JSONObject obj = new JSONObject(data);

			// 保存用户信息
			ZHAccount account = LandInfoParser.SaveCurrentAccount(obj);
			ZhAccountPrefUtil.saveZHAccount(this, account);

			PrefUtil.savePref(this, ZhAccountPrefUtil.IS_LOGINED_ACCOUNT, true);

		}

		// 登录失败后js调用的方法
		@JavascriptInterface
		public void LoginFailed(String data) throws JSONException {

//			Log.v("zhong", "login失败--" + data);
			Toast.makeText(WebViewActivity.this, data, Toast.LENGTH_SHORT).show();

		}
}
