package com.sinonetwork.zhonghua;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import Decoder.BASE64Decoder;
import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.FastJsonRequest;
import com.sinonetwork.zhonghua.model.ShiYanShiLiModel;
import com.sinonetwork.zhonghua.net.RequestManager;
import com.sinonetwork.zhonghua.utils.BitmapHelp;
import com.sinonetwork.zhonghua.utils.URLAddress;
import com.sinonetwork.zhonghua.utils.log.Logger;

public class ShiYanShiFan extends Activity {

	private ViewPager seed_viewpager_s; // android-support-v4中的滑动组件
	private List<ImageView> imageViews; // 滑动的图片集合

	private int[] imageResId; // 图片ID
	// private List<View> dots; // 图片标题正文的那些点
	private TextView tv_title;
	private int currentItem = 0; // 当前图片的索引号
	private ScheduledExecutorService scheduledExecutorService;
	// 数据信息
	private TextView seed_code_content_s; // 审定编号
	private TextView seed_name_content_s; // 品种名称
	private TextView seed_unit_content_s; // 选育单位
	private TextView seed_orign_content_s; // 品种来源

	private TextView charact_content1;
	private TextView output_content2;
	private TextView skill_content3;
	private TextView skill_content4;
	private TextView skill_content5;
	private TextView skill_content6;
	private ImageView detail_back_s;

	private String tupian;

	private String id;

	// private Handler handler = new Handler() {
	// public void handleMessage(android.os.Message msg) {
	// viewPager.setCurrentItem(currentItem);// 切换当前显示的图片
	// };
	// };
	private String[] seedPictures;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shiyanshifan);
		initView();
		imageViews = new ArrayList<ImageView>();
		// imagesID = new int[] { R.drawable.a, R.drawable.b, R.drawable.c,
		// R.drawable.d, R.drawable.e };
		// for (int i = 0; i < imageViews.size(); i++) {
		// ImageView imageView = new ImageView(this);
		// imageView.setImageResource(imagesID[i]);
		// imageView.setScaleType(ScaleType.CENTER_CROP);
		// imageViews.add(imageView);
		// }
		//返回键
		detail_back_s.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		// 设置一个监听器，当ViewPager中的页面改变时调用
		seed_viewpager_s.setOnPageChangeListener(new MyPageChangeListener());
		//获取id号
		Bundle bundle = getIntent().getExtras();
		id = bundle.getString("id");
		loadData("detailExample", id);
	}
	//初始化控件
	private void initView() {
		seed_viewpager_s = (ViewPager) findViewById(R.id.seed_viewpager_s);
		detail_back_s = (ImageView) findViewById(R.id.detail_back_s);

		seed_code_content_s = (TextView) findViewById(R.id.seed_code_content_s); // 审定编号
		seed_name_content_s = (TextView) findViewById(R.id.seed_name_content_s); // 品种名称
		seed_unit_content_s = (TextView) findViewById(R.id.seed_unit_content_s); // 选育单位
		seed_orign_content_s = (TextView) findViewById(R.id.seed_orign_content_s); // 品种来源

		charact_content1 = (TextView) findViewById(R.id.charact_content1);
		output_content2 = (TextView) findViewById(R.id.output_content2);
		skill_content3 = (TextView) findViewById(R.id.skill_content3);
		skill_content4 = (TextView) findViewById(R.id.skill_content4);
		skill_content5 = (TextView) findViewById(R.id.skill_content5);
		skill_content6 = (TextView) findViewById(R.id.skill_content6);
		detail_back_s = (ImageView) findViewById(R.id.detail_back_s);
		seed_viewpager_s = (ViewPager) findViewById(R.id.seed_viewpager_s);
		
	}

	// 加载数据
	public void loadData(String method, String id) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("method", method);
		map.put("id", id);
		// volley框架访问并解析和显示数据
		FastJsonRequest<ShiYanShiLiModel> fastJson = new FastJsonRequest<ShiYanShiLiModel>(
				Method.POST, URLAddress.SeedInfoByIdURL,
				ShiYanShiLiModel.class, null, map,
				new Response.Listener<ShiYanShiLiModel>() {
					@Override
					public void onResponse(ShiYanShiLiModel weather) {
						if (weather.getResultdata() == null) {
							Toast.makeText(ShiYanShiFan.this, "没有数据",
									Toast.LENGTH_SHORT).show();
						} else {
							seed_code_content_s.setText(weather.getResultdata()
									.getPrincipal().toString());// 负责人
							seed_name_content_s.setText(weather.getResultdata()
									.getPrincipalContact().toString());// 负责人联系方式
							seed_unit_content_s.setText(weather.getResultdata()
									.getExampleFamily().toString());// 示范户
							seed_orign_content_s.setText(weather
									.getResultdata().getExampleFamilyContact()
									.toString());// 示范户联系方式
							charact_content1.setText(weather.getResultdata()
									.getProductComboInfo().toString());// 产品及套餐信息
							try {
								output_content2.setText(URLDecoder.decode(
										weather.getResultdata()
												.getExampleScheme().toString(),
										"utf-8"));// 使用utf-8解析示范方案
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}
							skill_content3.setText(weather.getResultdata()
									.getProcessTracking().toString());// 过程追踪
							skill_content4.setText(weather.getResultdata()
									.getTestAssess().toString());// 测产评估
							skill_content5.setText(weather.getResultdata()
									.getSummaryAnalyze().toString());// 总结分析
							skill_content6.setText(weather.getResultdata()
									.getCropName().toString());// 作物名称
							// detail_back_s.setText(weather.getResultdata().get().toString());
							// 获取图片
							String tupian = weather.getResultdata()
									.getPictures().toString();
							String[] seedPictures = null;

							if (!tupian.equals("") && tupian != null
									&& !tupian.equals("null")) {
								if (tupian.contains(",")) {
									seedPictures = tupian.split("\\,");
								} else {
									// 集合中只有一张图片的情况
									seedPictures = new String[1];
									seedPictures[0] = tupian;
								}
								initSeedPicture(seedPictures);
							}

						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {						 
						Toast.makeText(ShiYanShiFan.this, "服务器异常！",
								Toast.LENGTH_SHORT).show();
					}
				});
		RequestManager.addRequest(fastJson, this);
	}

	public void initSeedPicture(String[] seedPictures) {
		imageResId = new int[seedPictures.length];
		for (int i = 0; i < seedPictures.length; i++) {
			imageResId[i] = imageResId[i];
		}
		// 初始化图片资源
		for (int i = 0; i < seedPictures.length; i++) {
			ImageView imageView = new ImageView(ShiYanShiFan.this);
			// ImageLoader.getInstance().displayImage(
			// URLAddress.TPURLER + seedPictures[i], imageView);
			BitmapHelp.getBitmapUtils(this).display(imageView,
					URLAddress.TPURLER + seedPictures[i]);
			// imageView.setImageResource(imageResId[i]);
			imageView.setScaleType(ScaleType.FIT_XY);
			imageViews.add(imageView);
		}
		seed_viewpager_s.setAdapter(new SeedAdapter());// 设置填充ViewPager页面的适配器
	}

	@Override
	public void onStart() {
		scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		// 当Activity显示出来后，每两秒钟切换一次图片显示
		// scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2,
		// TimeUnit.SECONDS);
		scheduledExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 2,
				TimeUnit.SECONDS);
		super.onStart();
	}

	@Override
	public void onStop() {
		// 当Activity不可见的时候停止切换
		scheduledExecutorService.shutdown();
		super.onStop();
	}

	private class ScrollTask implements Runnable {

		public void run() {
			synchronized (seed_viewpager_s) {
				System.out.println("currentItem: " + currentItem);
				currentItem = (currentItem + 1) % imageViews.size();
				// handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
			}
		}

	}

	private class MyPageChangeListener implements OnPageChangeListener {
		private int oldPosition = 0;

		/**
		 * This method will be invoked when a new page becomes selected.
		 * position: Position index of the new selected page.
		 */
		public void onPageSelected(int position) {
			currentItem = position;
			// tv_title.setText(titles[position]);
			// dots.get(oldPosition).setBackgroundResource(R.drawable.dot_normal);
			// dots.get(position).setBackgroundResource(R.drawable.dot_focused);
			oldPosition = position;
		}

		public void onPageScrollStateChanged(int arg0) {

		}

		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}
	}

	/**
	 * 填充ViewPager页面的适配器
	 * 
	 * @author Administrator
	 * 
	 */
	private class SeedAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imageResId.length;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imageViews.get(arg1));
			return imageViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}
}
